//
//  UIButton.m
//  BusinessTabble
//
//  Created by Haichen Song on 23/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "UIButton+Custom.h"

@implementation UIButton (Custom)

- (void) drawRectForUnderline:(CGRect)rect {
    [super drawRect:rect];
    CGRect textRect = self.titleLabel.frame;
    
    // need to put the line at top of descenders (negative value)
    CGFloat descender = self.titleLabel.font.descender;
    
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    
    // set to same colour as text
    CGContextSetStrokeColorWithColor(contextRef, self.titleLabel.textColor.CGColor);
    
    CGContextMoveToPoint(contextRef, textRect.origin.x, textRect.origin.y + textRect.size.height + descender);
    
    CGContextAddLineToPoint(contextRef, textRect.origin.x + textRect.size.width, textRect.origin.y + textRect.size.height + descender);
    
    CGContextClosePath(contextRef);
    
    CGContextDrawPath(contextRef, kCGPathStroke);
}

@end
