//
//  UIButton.h
//  BusinessTabble
//
//  Created by Haichen Song on 23/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Custom)
- (void) drawRectForUnderline:(CGRect)rect;
@end
