//
//  UIButton+Image.h
//  BusinessTabble
//

//

#import <UIKit/UIKit.h>

@interface UIButton_Image : UIButton
- (void)centerImageAndTitle:(float)space;
- (void)centerImageAndTitle;
@end
