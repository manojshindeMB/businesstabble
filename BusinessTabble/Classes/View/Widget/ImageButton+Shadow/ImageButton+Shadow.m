//
//  ImageButton+Shadow.m
//  BusinessTabble
//
//  Created by Haichen Song on 20/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "ImageButton+Shadow.h"

@implementation ImageButton_Shadow


- (id)initWithFrame:(CGRect)frame
{
    if((self = [super initWithFrame:frame])){
        [self setupView];
    }
    
    return self;
}

- (void)awakeFromNib {
    [self setupView];
}

# pragma mark - main

- (void)setupView
{
//    self.layer.borderWidth = 1.0;
//    self.layer.borderColor = [UIColor colorWithRed:167.0/255.0 green:140.0/255.0 blue:98.0/255.0 alpha:0.25].CGColor;
//    self.layer.shadowColor = [UIColor blackColor].CGColor;
//    self.layer.shadowRadius = 0;
    
    CALayer *upperBorder = [CALayer layer];
    upperBorder.backgroundColor = [UIColor colorWithRed:167.0/255.0 green:140.0/255.0 blue:98.0/255.0 alpha:0.25].CGColor;
    upperBorder.frame = CGRectMake(CGRectGetWidth(self.frame) - 1, 0, 5.0f, self.frame.size.height);
    [self setBackgroundColor:[UIColor grayColor]];
    [self.layer addSublayer:upperBorder];
    
    //[self clearHighlightView];
    
//    CAGradientLayer *gradient = [CAGradientLayer layer];
//    gradient.frame = self.layer.bounds;
//    gradient.cornerRadius = 0;
//    gradient.colors = [NSArray arrayWithObjects:
//                       (id)[UIColor clearColor].CGColor,
//                       (id)[UIColor clearColor].CGColor,
//                       (id)[UIColor clearColor].CGColor,
//                       (id)[UIColor clearColor].CGColor,
//                       nil];
//    float height = gradient.frame.size.height;
//    gradient.locations = [NSArray arrayWithObjects:
//                          [NSNumber numberWithFloat:0.0f],
//                          [NSNumber numberWithFloat:0.0f],
//                          [NSNumber numberWithFloat:0.0f],
//                          [NSNumber numberWithFloat:0.0f],
//                          nil];
////    [button.layer insertSublayer:gradient atIndex:0];
////    [button.layer insertSublayer:gradient2 atIndex:1];
//    [self.layer insertSublayer:gradient atIndex:3];

}
- (void)highlightView
{
    self.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    self.layer.shadowOpacity = 0.25;
}

- (void)clearHighlightView {
    self.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    self.layer.shadowOpacity = 0.5;
}

- (void)setHighlighted:(BOOL)highlighted
{
    if (highlighted) {
        [self highlightView];
    } else {
        [self clearHighlightView];
    }
    [super setHighlighted:highlighted];
}


@end
