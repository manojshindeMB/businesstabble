//
//  TextFieldWithBound.m
//  Sticker
//
//  Created by AnYong on 1/7/15.
//  Copyright (c) 2015 AppDay Limited. All rights reserved.
//

#import "TextFieldWithBound.h"

@implementation TextFieldWithBound

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        // Initialization code
//        self.layer.borderWidth = 1.f;
//        self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    }
    
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (CGRect)textRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 10, 0);
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 10, 0);
}

@end
