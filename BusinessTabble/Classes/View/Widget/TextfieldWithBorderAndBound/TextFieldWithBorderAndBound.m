//
//  TextFieldWithBorderAndBound.m
//  Sticker
//
//  Created by Rixian on 1/7/15.
//  Copyright (c) 2015 AppDay Limited. All rights reserved.
//

#import "TextFieldWithBorderAndBound.h"

@implementation TextFieldWithBorderAndBound

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    [self.layer setBorderWidth:0.f];

    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        // Initialization code
        self.layer.borderWidth = 0.f;
        self.layer.borderColor = [[UIColor grayColor] CGColor];
    }
    return self;
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 10, 0);
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return CGRectInset(bounds, 10, 0);
}

@end
