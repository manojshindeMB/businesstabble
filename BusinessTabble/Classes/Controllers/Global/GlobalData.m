//
//  GlobalData.m
//  Sticker
//
//  Created by Rixian Bai on 7/26/12.
//


#import "GlobalData.h"
#import "Global.h"
#import "IQKeyboardManager.h"

#import <mach/mach.h>
#import <mach/mach_host.h>
#import "UIImageView+WebCache.h"
#import "CRToast.h"
#import "ChatViewController.h"
#import "HomeViewController.h"
#import "LoginNavigationController.h"
GlobalData *_globalData = nil;

@implementation GlobalData


@synthesize sms_verification_sent_count = _sms_verification_sent_count;
@synthesize contactaccess_permission_gained = _contactaccess_permission_gained;
@synthesize pushnotification_permission_gained = _pushnotification_permission_gained;
@synthesize previousTabIndex = _previousTabIndex;
@synthesize isBack = _isBack;
@synthesize mPopViewController;
@synthesize mArrMyBubbleList;
@synthesize mDictBubbleRecentPost;
@synthesize isChatLoading;
@synthesize isMessageLoading;
@synthesize isBubbleLoading;
@synthesize mUnreadPushNotifications;
@synthesize currentCity, currentLatitude, currentLongitude,currentState, currentCountry;
@synthesize mUserInfo;
@synthesize isMainFirstLoading;
// ===============================================================
// Basic & Initializing Methods
// ===============================================================

+(id) sharedData
{
	@synchronized(self)
    {
        if (_globalData == nil)
        {
            _globalData = [[self alloc] init]; // assignment not done here
        }		
	}
	return _globalData;
}
+(id) allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if (_globalData == nil)
        {
			_globalData = [super allocWithZone:zone];
			
			return _globalData;  
        }
    }
	
    return nil; //on subsequent allocation attempts return nil	
}

-(id) init
{
	if ((self = [super init])) {
		// @todo
	}
	
	return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}


- (void)loadInitData
{
    [self loadConfig];
}

-(void) loadConfig
{
//    NSUserDefaults *config = [NSUserDefaults standardUserDefaults];
    
    [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha:0.8f]];
    [SVProgressHUD setOffsetFromCenter:UIOffsetZero];
    [SVProgressHUD setOffsetFromCenter:UIOffsetMake(0, 0)];
    [SVProgressHUD setBackgroundColor:[UIColor darkGrayColor]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    currentCity = @""; currentState = @"";
    currentLatitude = 0; currentLongitude = 0;
    mUserInfo = [[JUserInfo alloc]init];
    mArrMyBubbleList = [[NSMutableArray alloc]init];
    mDictBubbleRecentPost = [[NSMutableDictionary alloc]init];
    mUnreadPushNotifications = 0;
    isMainFirstLoading = YES;
}

#pragma mark - Property Getter and Setter


- (NSNumber *)contactaccess_permission_gained
{
    return _contactaccess_permission_gained;
}



// ===============================================================
// NSUserDefault Related Methods
// ===============================================================
#pragma mark - Config Manager
-(BOOL) readBoolEntry:(NSUserDefaults *)config key:(NSString *) key defaults:(BOOL)defaults
{
    if (key == nil)
        return defaults;
    
    NSString *str = [config objectForKey:key];
    
    if (str == nil) {
        return defaults;
    } else {
        return str.boolValue;
    }
    
    return defaults;
}

-(float) readFloatEntry:(NSUserDefaults *)config key:(NSString *) key defaults:(float)defaults
{
    if (key == nil)
        return defaults;
    
    NSString *str = [config objectForKey:key];
    
    if (str == nil) {
        return defaults;
    } else {
        return str.floatValue;
    }
    
    return defaults;
}

- (int) readIntEntry:(NSUserDefaults *)config key:(NSString *) key defaults:(int)defaults
{
    if (key == nil)
        return defaults;
    
    NSString *str = [config objectForKey:key];
    
    if (str == nil) {
        return defaults;
    } else {
        return str.intValue;
    }
    
    return defaults;
}

- (double) readDoubleEntry:(NSUserDefaults *)config key:(NSString *) key defaults:(double)defaults
{
    if (key == nil)
        return defaults;
    
    NSString *str = [config objectForKey:key];
    
    if (str == nil) {
        return defaults;
    } else {
        return str.doubleValue;
    }
    
    return defaults;
}

- (NSNumber *)readNumberEntry:(NSUserDefaults *)config key:(NSString *)key defaults:(NSNumber *)defaults
{
    if (key == nil)
        return defaults;
    
    NSNumber *number = [config objectForKey:key];
    
    if (number == nil) {
        return defaults;
    } else {
        return number;
    }
    
    return defaults;
}

-(NSString *) readEntry:(NSUserDefaults *)config key:(NSString *) key defaults:(NSString *)defaults
{
    if (key == nil)
        return defaults;
    
    NSString *str = [config objectForKey:key];
    
    if (str == nil) {
        return defaults;
    } else {
        return str;
    }
    
    return defaults;
}

- (NSDictionary *)readDictionaryEntry:(NSUserDefaults *)config key:(NSString *) key defaults:(NSDictionary *)defaults
{
    if (key == nil)
        return defaults;
    
    NSDictionary *dic = [NSDictionary dictionaryWithDictionary:[config objectForKey:key]];
    
    if (dic == nil) {
        return defaults;
    } else {
        return dic;
    }
    
    return defaults;
}

// ===============================================================
// NSString Related Methods
// ===============================================================

// Check Emptiness of String
- (BOOL)isNull:(NSString *)strVal
{
    if (strVal == nil || [@"<null>" isEqualToString:strVal] || [strVal isKindOfClass:[NSNull class]])
        return YES;
    else
        return NO;
}

// Check Emptiness of String
- (BOOL)isEmpty:(NSString *)strVal
{
    if (strVal == nil || [@"" isEqualToString:strVal] || [@"<null>" isEqualToString:strVal] || [strVal isKindOfClass:[NSNull class]])
        return YES;
    else
        return NO;
}
// Get Actual Data from Dictionary from WebService
- (NSString *) getExistingString:(NSString *)string
{
    if (string == nil || [string isKindOfClass:[NSNull class]] || [@"" isEqualToString:string] || [@"<null>" isEqualToString:string]) {
        return @"";
    } else {
        return [NSString stringWithFormat:@"%@", string];
    }
}

- (NSString *)getImageURLFromAWSURL:(NSString *)image_url
{
    NSString *strImageURL = image_url;
    NSRange endRange = [strImageURL rangeOfString:@"&Signature"];
    strImageURL = [strImageURL substringToIndex:endRange.location];
    
    return strImageURL;
}

-(BOOL) validateEmailWithString:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (NSString *)convertEmojiStringToUnicode:(NSString *)strEmo
{
    NSData *data = [strEmo dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *valueUnicode = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return valueUnicode;
}
- (NSString *)convertUnicodeToEmojiString:(NSString *)strUnicode
{
    NSData *data = [strUnicode dataUsingEncoding:NSUTF8StringEncoding];
    NSString *valueEmoj = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
    return valueEmoj;
}
// ===============================================================
// NSDate Related Methods
// ===============================================================

- (NSString *)stringFromDate:(NSDate *)date DateFormat:(NSString *)strDateFormat
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    if ([self isEmpty:strDateFormat])
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    else
        [dateFormatter setDateFormat:strDateFormat];

//        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    return [dateFormatter stringFromDate:date];
}

- (NSString *)stringFromDateInUTC:(NSDate *)utcDate DateFormat:(NSString *)strDateFormat
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    
    if ([self isEmpty:strDateFormat])
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    else
        [formatter setDateFormat:strDateFormat];
    
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    return [formatter stringFromDate:utcDate];
}

- (NSDate *)dateFromString:(NSString *)strDate DateFormat:(NSString *)strDateFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if ([self isEmpty:strDateFormat])
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    else
        [dateFormatter setDateFormat:strDateFormat];

    return [dateFormatter dateFromString:strDate];
}
- (NSString *)stringFromLocaleDate:(NSDate *)date DateFormat:(NSString *)strDateFormat
{
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    if ([self isEmpty:strDateFormat])
    {
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
        [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
        //        [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
        //        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"Europe/Stockholm"]];
        [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    }
    else
        [dateFormatter setDateFormat:strDateFormat];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    //        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    return [dateFormatter stringFromDate:date];
    
}
- (NSDate *)dateFromUTCString:(NSString *)strUTCDate DateFormat:(NSString *)strDateFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if ([self isEmpty:strDateFormat])
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    else
        [dateFormatter setDateFormat:strDateFormat];
    
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    return [dateFormatter dateFromString:strUTCDate];
}

//- (NSDate *)localDateFromUTCString:(NSString *)strUTCDate DateFormat:(NSString *)strDateFormat
//{
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    if ([self isEmpty:strDateFormat])
//        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
//    else
//        [dateFormatter setDateFormat:strDateFormat];
//    
//    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
//    NSDate *utcDate = [dateFormatter dateFromString:strUTCDate];
//    
//    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
//    NSString *strDate = [dateFormatter stringFromDate:utcDate];
//    
//    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
//    
//    return [dateFormatter dateFromString:strDate];
//}

//- (NSString *)getCurrentDateTime:(NSString *)strDateFormat;
//{    
//    NSString        *datetimeString;
//    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
//    
//    if ([self isEmpty:strDateFormat])
//        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
//    else
//        [formatter setDateFormat:strDateFormat];
//
//    datetimeString = [formatter stringFromDate:[NSDate date]];
//    
//    return datetimeString;
//}

- (NSString *)getCurrentDateTimeInServerTimestampFormat
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSDate *dateMinusFiveMinutes = [[NSDate date] dateByAddingTimeInterval:(-1) * 60 * 2];
    
    return [formatter stringFromDate:dateMinusFiveMinutes];
}

- (NSString *)getCurrentDateTimeInUTCWithFormat:(NSString *)strFormat
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    
    if ([self isEmpty:strFormat])
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    else
        [formatter setDateFormat:strFormat];
    
    [formatter setDateFormat:strFormat];
    
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    return [formatter stringFromDate:[NSDate date]];
}

- (NSDate *)currentDateWithoutMilliseconds
{
    return [self dateFromString:[self stringFromDate:[NSDate date] DateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"] DateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
}

- (NSString *)dateTimeStringFromDate:(NSDate *)date
{
    NSTimeInterval interval = [date timeIntervalSinceNow];
    double elapse = 0 - (double)interval;
    //        NSLog(@"Date: %@", date);
    if (elapse < 2 * 60) {
        return @"Just now";
    } else if (elapse < 45 * 60) {
        int minute = round(elapse / 60);
        return [NSString stringWithFormat:@"%d minutes ago", minute];
    } else if (elapse < 1.5 * 60 * 60) {
        return @"An hour ago";
    } else if (elapse < 23.5 * 60 * 60) {
        int hour = round(elapse / 60 / 60);
        return [NSString stringWithFormat:@"%d hours ago", hour];
    } else if (elapse < 48 * 60 * 60) {
        return @"Yesterday";
    } else if (elapse < 8 * 24 * 60 * 60) {
        int day = floor(elapse / 24 / 60 / 60);
        return [NSString stringWithFormat:@"%d days ago", day];
    } else {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"MMM dd, yyyy";
        return [formatter stringFromDate:date];
    }
}
- (NSString *) changeDateTimeFormat: (NSString *) strInputDateTime
                       inputformat : (NSString *) inputDateFormat
                      outputformat : (NSString *) outputDateFormat
{
    //************************* How to use e.g.***************************************
    //    NSDateFormatter *inputdateFormat = [[NSDateFormatter alloc] init];
    //    [inputdateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //
    //    NSDateFormatter *outputdateFormat = [[NSDateFormatter alloc] init];
    //    [outputdateFormat setDateFormat:@"EEEE, d MMM"];
    //
    //    NSDate *startDate = [inputdateFormat dateFromString: pShout.startdatetime];
    //    //        NSDate *endDate = [inputdateFormat dateFromString: pShout.enddatetime];
    //
    //    lblEventDate.text = [NSString stringWithFormat:@"%@", [outputdateFormat stringFromDate: startDate]];
    
    if([strInputDateTime isEqualToString: @""])
        return @"";
    
    NSDateFormatter *inputdateFormat = [[NSDateFormatter alloc] init];
    [inputdateFormat setDateFormat: inputDateFormat];
    
    NSDateFormatter *outputdateFormat = [[NSDateFormatter alloc] init];
    [outputdateFormat setDateFormat: outputDateFormat];
    
    NSDate *date = [inputdateFormat dateFromString: strInputDateTime];
    
    return [outputdateFormat stringFromDate: date];
}


//calculate time difference, result is in minutes
- (CGFloat)calculateTimeDifferenceWithTimeFormat:(NSString *)timeFormat startedDateTime:(NSString *)startedTime completedDateTime:(NSString *)completedTime
{
    NSDateFormatter* dateFormatter_server = [[NSDateFormatter alloc] init];
    NSDateFormatter* dateFormatter_local = [[NSDateFormatter alloc] init];
    [dateFormatter_local setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter_server setTimeZone:[NSTimeZone timeZoneWithName:@"US/Mountain"]];
    [dateFormatter_local setDateFormat:timeFormat];
    [dateFormatter_server setDateFormat:timeFormat];
    
    NSDate* firstDate = [dateFormatter_server dateFromString: startedTime];
    NSDate* secondDate = [dateFormatter_local dateFromString: completedTime];
    
    return [secondDate timeIntervalSinceDate:firstDate];
}

//calculate time difference, result is in minutes
- (CGFloat)calculateTimeDifferenceWithStartedDateTime:(NSDate *)startedTime completedDateTime:(NSDate *)completedTime
{
    return [completedTime timeIntervalSinceDate:startedTime];
}

- (NSString *)getTimeStringFromSeconds:(CGFloat)seconds
{
    NSInteger _seconds = (int)seconds % 60;
    NSString *sec = [NSString stringWithFormat:@"%02d", (int)_seconds];// [NSString stringWithFormat: _seconds<10 ? @"0%d" : @"%d", (int)_seconds];
    NSInteger _minutes = ((int)seconds / 60) % 60;
    NSString *min = [NSString stringWithFormat:@"%02d", (int)_minutes];//[NSString stringWithFormat: _minutes<10 ? @"0%d" : @"%d", (int)_minutes];
    NSInteger _hours = (int)seconds / 3600;
    NSString *hrs = [NSString stringWithFormat:@"%02d", (int)_hours];//[NSString stringWithFormat: _hours<10 ? @"0%d" : @"%d", (int)_hours];
    NSString *strTotalTime = [NSString stringWithFormat:@"%@:%@:%@", hrs, min, sec];
    
    return strTotalTime;
}
- (NSDate *)getDateFromDateComponent: (NSInteger)year month:(NSInteger)month day:(NSInteger)day
{
    NSDateComponents *components = [[NSDateComponents alloc] init];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    components.year = year;
    components.day = day;
    components.month = month;
    NSDate* resultDate = [calendar dateFromComponents:components];
    return resultDate;
}
- (NSString *)getDateStringFromTimeStamp:(NSInteger)timestamp
{
    NSString *result = @"";
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    result = [self stringFromDate:date DateFormat:@"MM-dd-yyyy"];
    
//    [self stringFromDate:[NSDate date] DateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"] DateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"]
    return result;
}
- (NSString *)getTimeStringFromTimeStamp:(NSInteger)timestamp
{
    NSString *result = @"";
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    result = [self stringFromDate:date DateFormat:@"HH:mm:ss"];
    return result;
}

#pragma mark - Timestamp
- (NSString *)currentTimestamp
{
    return [NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]];
}
- (NSInteger)getDateTimestampFromDate :(NSDate *)date
{
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"yyyy-MM-dd"] ;
    NSTimeInterval interval  = [date timeIntervalSince1970] ;
    return interval;
}
- (NSDate *)getAddDatedFromDate:(NSDate *)date dayNum:(NSInteger )dayNum
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = dayNum;
    NSDate *newDate = [calendar dateByAddingComponents:components toDate:date options:0];
    return newDate;
}
- (NSDate *)getAddMinuteFromDate:(NSDate *)date minNum:(NSInteger )minNum
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.minute = minNum;
    NSDate *newDate = [calendar dateByAddingComponents:components toDate:date options:0];
    return newDate;
}
- (NSDate *)getAddYearFromDate:(NSDate *)date yearNum:(NSInteger )yearNum
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.year = yearNum;
    NSDate *newDate = [calendar dateByAddingComponents:components toDate:date options:0];
    return newDate;
}
// ===============================================================
// File Related Methods
// ===============================================================

#pragma mark - Application's Documents directory
// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSURL *)applicationTempDirectory
{
    return [NSURL fileURLWithPath:NSTemporaryDirectory() isDirectory:YES];
}

- (NSURL *)applicationPictureDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSPicturesDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSURL *)fileLocalURLForFileName:(NSString *)strFileName
{
    NSURL *path = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[self getExistingString:strFileName]];
    return path;
}

#pragma mark - Read/Write Data From/To file

-(BOOL)writeDictionaryData:(NSDictionary *)dict toFileName:(NSString *)fileName
{
   // Build the path to the data file
    NSURL *path = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[self getExistingString:fileName]];
    
    BOOL successfulWrite = [NSKeyedArchiver archiveRootObject:dict toFile:path.path];
    
    if(successfulWrite){
        NSLog(@"Writing dictionary to file success.");
        [self addSkipBackupAttributeToItemAtURL:path];
        return YES;
    } else {
        NSLog(@"Writing dictionary to file failed.");
        return NO;
    }
}

- (NSDictionary *)readDictionaryDataFromFile:(NSString *)fileName
{
    // Build the path to the data file
    NSURL *path = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[self getExistingString:fileName]];
    NSDictionary *dictFromFile = [NSKeyedUnarchiver unarchiveObjectWithFile:path.path];
    
    if (dictFromFile){
        NSLog(@"Reading dictionary success.");
    } else {
        NSLog(@"Reading dictionary failed.");
    }
    
    return dictFromFile;
}

-(BOOL)writeArrayData:(NSArray *)array toFileName:(NSString *)fileName
{
    // Build the path to the data file
    NSURL *path = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[self getExistingString:fileName]];
    
//    NSMutableArray *newArray = [NSMutableArray arrayWithArray:[self readArrayDataFromFile:fileName]];
//    if (newArray == nil)
//        newArray = [NSMutableArray array];
//    
//    [newArray addObjectsFromArray:array];
    
    BOOL successfulWrite = [NSKeyedArchiver archiveRootObject:array toFile:path.path];
    
    if(successfulWrite){
        NSLog(@"Writing Array to file success. : %@", fileName);
        [self addSkipBackupAttributeToItemAtURL:path];
        return YES;
    } else {
        NSLog(@"Writing Array to file failed. : %@", fileName);
        return NO;
    }
}

- (NSArray *)readArrayDataFromFile:(NSString *)fileName
{
    // Build the path to the data file
    NSURL *path = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[self getExistingString:fileName]];
    NSArray *arrayFromFile = (NSArray *)[NSKeyedUnarchiver unarchiveObjectWithFile:path.path];
    
    if (arrayFromFile){
        NSLog(@"Reading Array success. : %@", fileName);
    } else {
        NSLog(@"Reading Array failure. : %@", fileName);
    }
    
    return arrayFromFile;
}

-(NSString *)writeData:(NSData *)data toFileName:(NSString *)fileName
{
    // Build the path to the data file
    NSURL *path = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[self getExistingString:fileName]];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: path.path]) {
        [self DeleteFile:fileName];
    }
    
    BOOL successfulWrite = [data writeToURL:path atomically:YES];
    
    if(successfulWrite){
        [self addSkipBackupAttributeToItemAtURL:path];
        NSLog(@"Writing Data to file success. : %@", fileName);
        return fileName;
    } else {
        NSLog(@"Writing Data to file failed. : %@", fileName);
        return @"";
    }
}

-(NSData *)readDatafromFile:(NSString *)fileName
{
    // Build the path to the data file
    NSURL *path = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[self getExistingString:fileName]];
    NSData *imgData = [NSData dataWithContentsOfURL:path];
    
    if (imgData){
        NSLog(@"Reading binary data success. : %@", fileName);
    } else {
       NSLog(@"Reading binary data failure. : %@", fileName);
    }
    
    return imgData;
}

-(NSData *)readDatafromFilePath:(NSString *)filePath
{
    // Build the path to the data file
    NSURL *path = [NSURL fileURLWithPath:filePath];
    NSData *imgData = [NSData dataWithContentsOfURL:path];
    
    if (imgData){
        NSLog(@"Reading binary data success. : %@", filePath);
    } else {
        NSLog(@"Reading binary data failure. : %@", filePath);
    }
    
    return imgData;
}

-(NSString *)writeData:(NSData *)data toFilePath:(NSString *)filePath
{
    // Build the path to the data file
    [self DeleteFileForFilePath:filePath];
    BOOL successfulWrite = [data writeToFile:filePath atomically:YES];
    
    if (successfulWrite){
        NSLog(@"Writing Data to file success. : %@", filePath);
        [self addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:filePath]];
        return filePath;
    } else {
        NSLog(@"Writing Data to file failed. : %@", filePath);
        return @"";
    }
}

- (BOOL)DeleteFile:(NSString *)strFileNameToDelete
{
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    // Build the path to the data file
    NSURL *path = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[self getExistingString:strFileNameToDelete]];
    
    if ([filemgr fileExistsAtPath: path.path]) {
        NSError *error;
        return [filemgr removeItemAtPath:path.path error:&error];
    }
    
    return NO;
}

- (BOOL)DeleteFileForFilePath:(NSString *)strFilePathToDelete
{
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    // Build the path to the data file
    NSURL *path = [NSURL URLWithString:strFilePathToDelete];
//    NSURL *path = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:[NSString stringWithFormat:@"%@", strFileNameToDelete]];
    
    if ([filemgr fileExistsAtPath: path.path]) {
        NSError *error;
        return [filemgr removeItemAtPath:path.path error:&error];
    }
    
    return NO;
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}


// ===============================================================
// Other Methods
// ===============================================================

// pseudo function
- (NSString *)stringFromColor:(UIColor *)color
{
    const size_t totalComponents = CGColorGetNumberOfComponents(color.CGColor);
    const CGFloat * components = CGColorGetComponents(color.CGColor);
    return [NSString stringWithFormat:@"#%02X%02X%02X",
            (int)(255 * components[MIN(0,totalComponents-2)]),
            (int)(255 * components[MIN(1,totalComponents-2)]),
            (int)(255 * components[MIN(2,totalComponents-2)])];
}


- (void)showAlertWithTitle:(NSString *)title
                AndMessage:(NSString *)message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil,nil];
    // Show ;
    [ alertView show ] ;
}


#pragma mark - UUID Generator

- (NSString *)UUID
{
    CFUUIDRef uuidRef = CFUUIDCreate(nil);
    NSString *strUUID = CFBridgingRelease(CFUUIDCreateString(nil, uuidRef));
    
    return strUUID;
}

#pragma mark - Memory Test Proc
+ (natural_t) test_memory
{
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t page_size;
    
    host_port = mach_host_self();
    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(host_port, &page_size);
    vm_statistics_data_t vm_stat;
    
    if ( host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS )
    {
        NSLog(@"chae error");
        return 0;
    }
    
    natural_t mem_free = (natural_t)(vm_stat.free_count * page_size);
    
    NSLog(@"memory : %d", (int)mem_free);
    return mem_free;
}

#pragma mark - Notification Formatter
- (NSDictionary *)dictForToastWithMessage:(NSString *)strMessage
                          BackgroundColor:(UIColor *)backgroundColor
{
    NSDictionary *options = @{
                              kCRToastTextKey : strMessage,
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastBackgroundColorKey : backgroundColor,
                              kCRToastAnimationInTypeKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                              kCRToastNotificationPresentationTypeKey : @(CRToastPresentationTypeCover),
                              kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar)
                              

                              };
    return options;
}

- (CGSize)messageSize:(NSString*)message font:(UIFont*)font  width:(NSInteger)width
{
    CGSize sz=[message boundingRectWithSize:CGSizeMake(width, 100000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font}  context:nil].size;
    return sz;
}

#pragma mark Slide Transition
- (void) slideLayerInDirection:(NSString *)direction andFrom:(UIViewController*)srcVC andPush:(UIViewController *)dstVC{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = direction;
    [srcVC.navigationController.view.layer addAnimation:transition forKey:kCATransition];
    [srcVC.navigationController pushViewController:dstVC animated:NO];
}
#pragma mark Bubble Data
+ (void)setBadgeOfNewMsg
{
  //Needs to be implemented soon  [[AppDelegate SharedDelegate].g_rootTabView.m_imgNewMsg setHidden:NO];
}

+ (void)setBadgeOfNewNotif
{
}

+ (void)clearTmpDirectory
{
    NSArray* tmpDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:NSTemporaryDirectory() error:NULL];
    for (NSString *file in tmpDirectory) {
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), file] error:NULL];
    }
}
+ (void)saveFailedLog:(NSString*)strMsg
{
    
}

#pragma mark Image 
+ (UIImage *)scaleAndRotateImage:(UIImage *)image
{
    
    int kMaxResolution = 640; // Or whatever
    CGImageRef imgRef = image.CGImage;
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}
#pragma mark Bubble Methods
- (BOOL) isSessionExpired
{
    if(!mUserInfo || !mUserInfo.mId)
        return YES;
    return NO;
}
- (void)uploadPhotoWithName:(NSString *)name oldname:(NSString *)oldname image:(UIImage *)image type:(NSString *)type
                 completion:(void (^)())successHandler FailureHandler:(void (^)())failureHandler
{
    if([self isEmpty:oldname])
        oldname = @"";
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"Bearer #_token_goes_here#" forHTTPHeaderField:@"Authorization"];
    NSDictionary *parameters = @{@"name" : @"image1",
                                 @"newname" : name,
                                 @"oldname" : oldname,
                                 @"type" : type,
                                 @"imagecount" : @1};
    NSData *imageData = UIImageJPEGRepresentation(image, 1.f);
    NSString*urlString=[NSString stringWithFormat:@"%@%@",WEB_SITE_BASE_URL, @"api/message/douploadnew"];
    [manager POST:urlString parameters:parameters  constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imageData name:@"image1" fileName:name mimeType:@"image/jpeg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        successHandler();
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failureHandler();
    }];
}
- (NSString *)urlencode :(NSString *) string{
    NSString *newString =[string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return newString;
}
- (void)showImageWithImage:(UIImageView *)imageView name:(NSString *)name placeholder:(NSString *)placeholder;
{
    UIImage *placeHolder;
    if([self isEmpty:placeholder])
        placeHolder = [UIImage imageNamed:@"placeholder.png"];
    else
        placeHolder = [UIImage imageNamed:placeholder];
    NSString *fullPath = [NSString stringWithFormat:@"%@%@",g_storagePrefix,name];

//    SDImageCache *cache = [SDImageCache sharedImageCache];
//    [cache removeImageForKey:fullPath];
    
    [imageView setImageWithURL:[NSURL URLWithString:fullPath] placeholderImage:placeHolder options:SDWebImageRefreshCached progress:^(NSInteger receivedSize, NSInteger expectedSize) {
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    } usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    

}
- (NYAlertViewController *)createNewAlertViewWithTitle:(NSString *)title view:(UIView *)view
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    
    alertViewController.title = NSLocalizedString(@"", nil);
    alertViewController.message = NSLocalizedString(title, nil);
    
    alertViewController.buttonCornerRadius = 20.0f;
    alertViewController.view.tintColor = [UIColor lightGrayColor];
    
    alertViewController.titleFont = [UIFont fontWithName:@"Avenir-Medium" size:18.0f];
    alertViewController.messageFont = [UIFont fontWithName:@"Avenir-Book" size:16.0f];
    alertViewController.buttonTitleFont = [UIFont fontWithName:@"Avenir-Medium" size:14];
    alertViewController.cancelButtonTitleFont = [UIFont fontWithName:@"Avenir-Medium" size:14];
    
    
    alertViewController.titleColor = MAIN_COLOR;
    alertViewController.messageColor = [UIColor colorWithWhite:0.92f alpha:1.0f];
    
    alertViewController.buttonColor = MAIN_COLOR;
    alertViewController.buttonTitleColor = [UIColor colorWithWhite:0.92f alpha:1.0f];
    
    alertViewController.cancelButtonColor = MAIN_COLOR;
    alertViewController.cancelButtonTitleColor = [UIColor colorWithWhite:0.92f alpha:1.0f];
    
    
    alertViewController.alertViewBackgroundColor = [UIColor colorWithWhite:0.38f alpha:1.0f];
    alertViewController.alertViewCornerRadius = 10.0f;
    
    return alertViewController;
    
}
- (CustomIOSAlertView *)createBusinessCreationNoticeViewWithTitle:(NSString *)title
{
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 100)];
    [demoView.layer setCornerRadius:7.f];
    UILabel *lblTitle = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 250, 100)];
    lblTitle.center = demoView.center;
    [lblTitle setFont:[UIFont boldSystemFontOfSize:16.0f]];
    lblTitle.numberOfLines = 0;
    [lblTitle setTextAlignment:NSTextAlignmentCenter];
    [lblTitle setTextColor:[UIColor darkGrayColor]];
    [demoView setBackgroundColor: [UIColor colorWithRed:242/255.f green:242/255.f blue:242/255.f alpha:1.f]];
    [lblTitle setText:title];
    [demoView addSubview:lblTitle];
    // Add some custom content to the alert view
    [alertView setContainerView:demoView];
    
    // Modify the parameters
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Okay", nil]];
    
    
    // You may use a Block, rather than a delegate.
    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    
    return alertView;
}

- (void)onOpenChatViewWhenPushReceived:(UIViewController *)controller notification:(NSNotification *)notification;
{
//    if([[self topViewController] isMemberOfClass:[ChatViewController class]])
//        return;
    UIViewController *topViewController = [self topViewController];
    
    NSDictionary *dict = notification.object;
    if(dict && [dict objectForKey:@"type"] && [dict objectForKey:@"extra"])
    {
        NSString *type = [dict objectForKey:@"type"];
        NSString *extra = [dict objectForKey:@"extra"];
        if([type isEqualToString:@"chat"])
        {
            if([controller isKindOfClass:[HomeViewController class]])
                [controller.navigationController popToRootViewControllerAnimated:NO];
            if([extra integerValue])
            {
                Bubble *curBubble = [Bubble getBubbleById:[extra integerValue]];
                if(!curBubble) return;
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Detail" bundle:nil];
                ChatViewController *chatVC = [storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
                chatVC.mCurrentBubble = curBubble;
                if([topViewController isKindOfClass:[LoginNavigationController class]])
                    [controller.navigationController pushViewController:chatVC animated:YES];
                else
                    [topViewController.navigationController pushViewController:chatVC animated:YES];
            }
        }
    }
}

- (UIViewController *)topViewController{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isMemberOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}
@end
