//
//  GlobalData.h
//  Sticker
//
//  Created by Rixian Bai on 7/26/12.
//

#import <Foundation/Foundation.h>
#import "JUserInfo.h"
#import "NYAlertViewController.h"
#import "CustomIOSAlertView.h"
@interface GlobalData : NSObject

typedef enum {
    SocialMediaFacebook = 2001,
    SocialMediaQQ
} SocialMediaType;

// Variables
typedef enum {
    CustomTabbarButtonIndex1 = 1,
    CustomTabbarButtonIndex2,
   
} CustomTabbarButtonIndex;

typedef enum{
    Male = 0,
    Female = 1
} Gender;

typedef enum
{
    ACCOUNT_TYPE_SIGNUP = 0,
    ACCOUNT_TYPE_EDIT_USER_INFO = 1
} AccountEditType;
typedef enum
{
    TERMS_TYPE_FROM_LOGIN = 0,
    TERMS_TYPE_FROM_HOME = 1
} TermsViewAccessType;

typedef enum
{
    POST_TYPE_WRITE_PHOTO = 0,
    POST_TYPE_COUPON = 1,
    POST_TYPE_SELL_ITEM = 2
} PostType;

@property (nonatomic, strong) UINavigationController *topNavigationController;
@property (nonatomic, readwrite) CustomTabbarButtonIndex previousTabIndex;
@property (nonatomic, strong) NSNumber *sms_verification_sent_count;
@property (nonatomic, strong) NSNumber *contactaccess_permission_gained;
@property (nonatomic, strong) NSNumber *pushnotification_permission_gained;
@property (nonatomic) BOOL isBack;
@property (nonatomic, retain) NSString *mPopViewController;
@property (nonatomic) NSInteger mUnreadPushNotifications;

@property (nonatomic, strong) NSMutableArray *mArrMyBubbleList;
@property (nonatomic, strong) NSMutableDictionary *mDictBubbleRecentPost;

@property (nonatomic, strong) NSString *currentCity;
@property (nonatomic, strong) NSString *currentCountry;
@property (nonatomic, strong) NSString *currentState;
@property (nonatomic ) CGFloat currentLatitude;
@property (nonatomic) CGFloat currentLongitude;
@property (nonatomic, strong) JUserInfo *mUserInfo;

@property (nonatomic) BOOL isChatLoading;
@property (nonatomic) BOOL isBubbleLoading;
@property (nonatomic) BOOL isMessageLoading;
@property (nonatomic) BOOL isChatViewOpened;
@property (nonatomic) BOOL isMessageViewOpened;
@property (nonatomic) BOOL isMainFirstLoading;
//Bubble Methods
- (BOOL) isSessionExpired;


- (void)uploadPhotoWithName:(NSString *)name oldname:(NSString *)oldname image:(UIImage *)image type:(NSString *)type
                 completion:(void (^)())successHandler FailureHandler:(void (^)())failureHandler;


- (NSString *)urlencode :(NSString *) string;
- (void)showImageWithImage:(UIImageView *)image name:(NSString *)name placeholder:(NSString *)placeholder;

// ===============================================================
// Basic & Initializing Methods
// ===============================================================
+ (id)sharedData;
- (void)loadInitData;

// Alert
- (NYAlertViewController *)createNewAlertViewWithTitle:(NSString *)title view:(UIView *)view;

// ===============================================================
// NSUserDefault Related Methods
// ===============================================================

- (BOOL)readBoolEntry:(NSUserDefaults *)config key:(NSString *) key defaults:(BOOL)defaults;
- (float)readFloatEntry:(NSUserDefaults *)config key:(NSString *) key defaults:(float)defaults;
- (int)readIntEntry:(NSUserDefaults *)config key:(NSString *) key defaults:(int)defaults;
- (double)readDoubleEntry:(NSUserDefaults *)config key:(NSString *) key defaults:(double)defaults;
- (NSNumber *)readNumberEntry:(NSUserDefaults *)config key:(NSString *) key defaults:(NSNumber *)defaults;
- (NSString *)readEntry:(NSUserDefaults *)config key:(NSString *) key defaults:(NSString *)defaults;
- (NSDictionary *)readDictionaryEntry:(NSUserDefaults *)config key:(NSString *) key defaults:(NSDictionary *)defaults;

// ===============================================================
// NSString Related Methods
// ===============================================================
// Check Emptiness of String
- (BOOL)isNull:(NSString *)strVal;
// Check Emptiness of String
- (BOOL)isEmpty:(NSString *)strVal;
// Get Actual Data from Dictionary from WebService
- (NSString *) getExistingString:(NSString *)string;
// Get Working URL of image from AWS URL
- (NSString *)getImageURLFromAWSURL:(NSString *)image_url;
// check that an email address is valid.
- (BOOL)validateEmailWithString:(NSString *)checkString;
- (NSString *)convertEmojiStringToUnicode:(NSString *)strEmo;
- (NSString *)convertUnicodeToEmojiString:(NSString *)strUnicode;

- (NSString *)stringFromDate:(NSDate *)date DateFormat:(NSString *)strDateFormat;
- (NSString *)stringFromLocaleDate:(NSDate *)date DateFormat:(NSString *)strDateFormat;

// ===============================================================
// NSDate Related Methods
// ===============================================================
- (NSDate *)getDateFromDateComponent: (NSInteger)year month:(NSInteger)month day:(NSInteger)day;
- (NSDate *)dateFromString:(NSString *)strDate DateFormat:(NSString *)strDateFormat;
- (NSDate *)dateFromUTCString:(NSString *)strUTCDate DateFormat:(NSString *)strDateFormat;
//- (NSDate *)localDateFromUTCString:(NSString *)strUTCDate DateFormat:(NSString *)strDateFormat;

- (NSString *)stringFromDateInUTC:(NSDate *)utcDate DateFormat:(NSString *)strDateFormat;
//- (NSString *)getCurrentDateTime:(NSString *)strDateFormat;
- (NSString *)getCurrentDateTimeInServerTimestampFormat;
- (NSInteger)getDateTimestampFromDate :(NSDate *)date;
- (NSString *)getDateStringFromTimeStamp:(NSInteger)timestamp;
- (NSString *)getTimeStringFromTimeStamp:(NSInteger)timestamp;
- (NSString *)getCurrentDateTimeInUTCWithFormat:(NSString *)strFormat;
- (NSDate *)currentDateWithoutMilliseconds;
- (NSString *) changeDateTimeFormat: (NSString *) strInputDateTime
                       inputformat : (NSString *) inputDateFormat
                      outputformat : (NSString *) outputDateFormat;
- (NSString *)dateTimeStringFromDate:(NSDate *)date;
//calculate time difference, result is in minutes
- (CGFloat)calculateTimeDifferenceWithTimeFormat:(NSString *)timeFormat startedDateTime:(NSString *)startedTime completedDateTime:(NSString *)completedTime;
//calculate time difference, result is in minutes
- (CGFloat)calculateTimeDifferenceWithStartedDateTime:(NSDate *)startedTime completedDateTime:(NSDate *)completedTime;
- (NSString *)getTimeStringFromSeconds:(CGFloat)seconds;
- (NSString *)currentTimestamp;
- (NSDate *)getAddDatedFromDate:(NSDate *)date dayNum:(NSInteger )dayNum;
- (NSDate *)getAddMinuteFromDate:(NSDate *)date minNum:(NSInteger )minNum;
- (NSDate *)getAddYearFromDate:(NSDate *)date yearNum:(NSInteger )yearNum;
// ===============================================================
// File Related Methods
// ===============================================================
- (NSURL *)applicationDocumentsDirectory;
- (NSURL *)applicationTempDirectory;
- (NSURL *)applicationPictureDirectory;

// Read/Write From/To JSON file
- (NSURL *)fileLocalURLForFileName:(NSString *)strFileName;
//-(BOOL)writeDictionaryData:(NSDictionary *)dict toFileName:(NSString *)fileName;
//- (NSDictionary *)readDictionaryDataFromFile:(NSString *)fileName;
-(BOOL)writeArrayData:(NSArray *)array toFileName:(NSString *)fileName;
- (NSArray *)readArrayDataFromFile:(NSString *)fileName;
-(NSString *)writeData:(NSData *)data toFileName:(NSString *)fileName;
-(NSData *)readDatafromFile:(NSString *)fileName;
//-(NSString *)writeData:(NSData *)data toFilePath:(NSString *)filePath;
//-(NSData *)readDatafromFilePath:(NSString *)filePath;
- (BOOL)DeleteFile:(NSString *)strFileNameToDelete;
//- (BOOL)DeleteFileForFilePath:(NSString *)strFilePathToDelete;

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;

// ===============================================================
// Other Methods
// ===============================================================
- (NSString *)UUID;
- (NSString *)stringFromColor:(UIColor *)color;
- (void)showAlertWithTitle:(NSString *)title
                AndMessage:(NSString *)message;
+ (natural_t) test_memory;

- (NSDictionary *)dictForToastWithMessage:(NSString *)strMessage
                          BackgroundColor:(UIColor *)backgroundColor;
- (CGSize)messageSize:(NSString*)message font:(UIFont*)font  width:(NSInteger)width;

- (void) slideLayerInDirection:(NSString *)direction andFrom:(UIViewController*)srcVC andPush:(UIViewController *)dstVC;
- (CustomIOSAlertView *)createBusinessCreationNoticeViewWithTitle:(NSString *)title;
#pragma mark Bubble
+ (void)setBadgeOfNewMsg;
+ (void)setBadgeOfNewNotif;
+ (void)clearTmpDirectory;
+ (void)saveFailedLog:(NSString*)strMsg;
+ (UIImage *)scaleAndRotateImage:(UIImage *)image;

- (void)onOpenChatViewWhenPushReceived:(UIViewController *)controller notification:(NSNotification *)notification;

@end
