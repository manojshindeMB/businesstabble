//
//  Global.h
//  Sticker
//
//  Created by Liumin on 7/26/12.
//

#pragma once

#import "GlobalData.h"
#import "SVProgressHUD.h"

#pragma mark - Define Protocol -
@protocol NetworkConnectProtocol <NSObject>
-(void) callbackDisconnect;
@end

// enums that will be used as tags


#pragma mark - Define Variable

extern GlobalData *_globalData;

#pragma mark - Define MACRO -

#define localRegistry [NSUserDefaults standardUserDefaults]
#define TimeStamp [NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]]

//#define g_storagePrefix @"http://38.100.118.21/fballr/uploads/"




#define FBALLR_WEB_SERVICES_URL              @"http://38.100.118.21/fballr/"  //webservice.php



//#define g_storagePrefix             @"http://192.168.1.103:8080/businesstabble/api/upload/upload/"
//#define WEB_SERVICES_URL            @"http://192.168.1.103:8080/businesstabble/webservice/webservice.php"
//#define WEB_SITE_BASE_URL           @"http://192.168.1.103:8080/businesstabble/"

//#define g_storagePrefix             @"http://zold.goartapp.com/api/upload/upload/"
//#define WEB_SERVICES_URL            @"http://zold.goartapp.com/webservice/webservice.php"
//#define WEB_SITE_BASE_URL           @"http://zold.goartapp.com/"

#define g_storagePrefix             @"http://54.152.21.104/businesstabble/api/upload/upload/"
#define WEB_SERVICES_URL            @"http://54.152.21.104/businesstabble/webservice/webservice.php"
#define WEB_SITE_BASE_URL           @"http://54.152.21.104/businesstabble/"

#define WEB_SERVICE_RELATIVE_URL    @"webservice/webservice.php"
#define COUPON_CREATOR_URL          @"https://itunes.apple.com/us/app/coupon-creator-by-tabble/id999488290?mt=8"
#define kStrSupportEmail            @"support@tabble.com"
#define kStrManagerName             @"Chris Register"


#define kGoogleAPIKey @"AIzaSyC_dEOJ3DWJ3kl_xlFKuCsW7X_uuSThC5c"
#define kOneSignalKey @"5a3750c8-de80-4794-867a-0b3f186647dc"
#define kOneSignalAPIKey @"YmRmMzc5NzktMTc0Yi00OWQ3LTlkZDEtODk3ZDAxZTQ0Y2Y0"
// screen
#define IS_WIDESCREEN   (fabs((double)[[UIScreen mainScreen] bounds].size.height - (double)568) < DBL_EPSILON)
#define IS_IPAD         (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE       (([[[UIDevice currentDevice] model] isEqualToString:@"iPhone"]) || ([[[UIDevice currentDevice] model] isEqualToString:@"iPhone Simulator"]))
#define IS_IPOD         ([[[UIDevice currentDevice] model] isEqualToString:@"iPod touch"])
#define IS_IPHONE_5     (IS_IPHONE && IS_WIDESCREEN)
#define IS_IPHONE6 ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 667)
#define IS_IPHONE6PLUS ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 736)
#define IS_IPHONEX ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 812)

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define IPHONE6_SCREENWIDTH 375  //667
#define IPHONE6PLUS_SCREENWIDTH 736


#define IOS_VERSION     [[[UIDevice currentDevice] systemVersion] floatValue]

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define SAVE_IMAGE_PATH [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]
#define TEMP_IMAGE_PATH [NSTemporaryDirectory() stringByAppendingString:@"/image.png"]

// define meters per mile-------
#define METERS_PER_MILE 1609.344

//#define CHAT_ESTABLISHMENT_RADIUS_IN_METER 1500.f
#define CHAT_ESTABLISHMENT_RADIUS_IN_METER 5000000000.f
#define LOCATION_UPDATE_TIME_INTERVAL 10.f
#define MAX_BUBBLE_DISTANCE 35
//#define MAX_BUBBLE_DISTANCE 2500000

#define CHAT_POST_MAX_LENGTH 65
// iPhone Keyboard Height
#define IPHONE_PORTRAIT_KEYBOARD_HEIGHT 216.f


// Notifications
#define NOTIFICATION_SHOW_SIGNUP_VIEW @"NOTIFICATION_SHOW_SIGNUP_VIEW"
#define NOTIFICATION_TAB_BAR_SHOW @"NOTIFICATION_TAB_BAR_SHOW"
#define NOTIFICATION_TAB_BAR_HIDE @"NOTIFICATION_TAB_BAR_HIDE"
#define NOTIFICATION_COUNTRYCODESELECTED @"CountryCodeSelected"
#define NOTIFICATION_SHOW_STATUS_BAR @"SHOW_STATUS_BAR"
#define NOTIFICATION_HIDE_STATUS_BAR @"HIDE_STATUS_BAR"
#define NOTIFICATION_LOGOUT @"NOTIFICATION_LOGOUT"
#define NOTIFICATION_SHOW_HOME @"NOTIFICATION_SHOW_HOME"
#define NOTIFICATION_PUSH_NOTIFICATION_RECEIVED @"NOTIFICATION_PUSH_NOTIFICATION_RECEIVED"

#define NOTIFICATION_RELOAD_BUBBLE_DATA @"NOTIFICATION_RELOAD_BUBBLE_DATA"
#define NOTIFICATION_RELOAD_BUBBLE_MEMBER_LIST @"NOTIFICATION_RELOAD_BUBBLE_MEMBER_LIST"
#define NOTIFICATION_RELOAD_BUBBLE_MENU_LIST @"NOTIFICATION_RELOAD_BUBBLE_MENU_LIST"
#define NOTIFICATION_RELOAD_BUBBLE_MESSAGE_LIST @"NOTIFICATION_RELOAD_BUBBLE_MESSAGE_LIST"
#define NOTIFICATION_RELOAD_BUBBLE_CHAT_LIST @"NOTIFICATION_RELOAD_BUBBLE_CHAT_LIST"
#define NOTIFICATION_RELOAD_BUBBLE_CHAT_HIDE_POSTER_LIST @"NOTIFICATION_RELOAD_BUBBLE_CHAT_HIDE_POSTER_LIST"

#define NOTIFICATION_RELOAD_COUPON_USE_LIST @"NOTIFICATION_RELOAD_COUPON_USE_LIST"
#define NOTIFICATION_RELOAD_ACTIVITY_LIKE_LIST @"NOTIFICATION_RELOAD_ACTIVITY_LIKE_LIST"

#define NOTIFICATION_RELOAD_BUBBLE_NAME_IMAGE @"NOTIFICATION_RELOAD_BUBBLE_NAME_IMAGE"
#define NOTIFICATION_CHAT_LIST_RECEIVED @"NOTIFICATION_CHAT_LIST_RECEIVED"
// Global Values
#define BUBBLE_CATEGORY_BAR @"Bars/Restaurants"
#define BUBBLE_CATEGORY_EVENTS @"Events"
#define BUBBLE_CATEGORY_RESTAURANTS @"Restaurants"
#define BUBBLE_CATEGORY_RETAIL @"Retail"
#define BUBBLE_DETAIL_PAGE_POST_VIEW @"post"
#define BUBBLE_DETAIL_PAGE_WHOSHERE @"whoshere"
#define BUBBLE_DETAIL_PAGE_BACK_WHOSHERE @"whoshere_back"
#define BUBBLE_DETAIL_PAGE_BUBBLEMENU @"menu"
#define BUBBLE_DETAIL_PAGE_BUBBLESETTING @"setting"
#define BUBBLE_DETAIL_PAGE_CHAT @"chat"

#define kBgQueue	dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define kMainQueue	dispatch_get_main_queue()

#define BUBBLE_DETAIL_PAGE_BUBBLESETTING_UPDATE_BACK @"setting_update_back"
#define BUBBLE_DETAIL_PAGE_BUBBLESETTING_MEMBER_BACK @"setting_member_back"

#define BUBBLE_COMPANY_COLORS @[[UIColor colorWithRed:250/255.0 green:168/255.0 blue:11/255.0 alpha:1.0], [UIColor colorWithRed:19/255.0 green:22/255.0 blue:22/255.0 alpha:1.0], [UIColor colorWithRed:71/255.0 green:105/255.0 blue:201/255.0 alpha:1.0], [UIColor colorWithRed:126/255.0 green:71/255.0 blue:201/255.0 alpha:1.0],[UIColor colorWithRed:211/255.0 green:56/255.0 blue:49/255.0 alpha:1.0],[UIColor colorWithRed:74/255.0 green:201/255.0 blue:71/255.0 alpha:1.0]]


// Toast Colors
#define MAIN_COLOR [UIColor colorWithRed:74/255.0 green:201/255.0 blue:71/255.0 alpha:1.0]
#define PLACE_COLOR [UIColor colorWithRed:38/255.0 green:62/255.0 blue:185/255.0 alpha:1.0]
#define ACTIVITY_COLOR [UIColor colorWithRed:38/255.0 green:159/255.0 blue:185/255.0 alpha:1.0]
#define EVENT_COLOR [UIColor colorWithRed:13/255.0 green:24/255.0 blue:82/255.0 alpha:1.0]

#define MAIN_TEXT_COLOR [UIColor colorWithRed:132/255.0 green:132/255.0 blue:132/255.0 alpha:1.0]
#define TEXT_UNDECLARED_COLOR [UIColor colorWithRed:222/255.0 green:222/255.0 blue:222/255.0 alpha:1.0]

//#define TOAST_ERROR_COLOR [UIColor redColor]
#define TOAST_ERROR_COLOR [UIColor colorWithRed:68/255.0 green:68/255.0 blue:68/255.0 alpha:1.0]
#define TOAST_SUCCESS_COLOR [UIColor blueColor]
#define COLOR_WHITE_KEYBOARD_TOOLBAR_BACKGROUND [UIColor colorWithRed:199/255.f green:203/255.f blue:210/255.f alpha:1]



#define kAPP_TITLE @"Placez"

// Local Registry
#define kMyUserName @"MyUserName"
#define kMyUserDict @"MyUserDict"
#define kMyUserPW @"MyUserPassword"
#define kMyUserRole @"Role"
#define kMyEmail @"MyEmail"
#define kCurLoginType @"MyLoginType"


#define kLastFetchTimeStampForBusinessBubbleList @"BusinessBubbleListLastTimeStamp"
#define kLastFetchTimeStampForBubbleMemberList @"BubbleMemberListLastTimeStamp"
#define kLastFetchTimeStampForBubbleMenuList @"BubbleMenuListLastTimeStamp"
#define kLastFetchTimeStampForBubbleMessageList @"BubbleMessageListLastTimeStamp"
#define BubbleHidePosterListLastTimeStamp @"BubbleHidePosterListLastTimeStamp"
#define kLastFetchTimeStampForBubbleChatList @"kLastFetchTimeStampForBubbleChatList"
#define kLastFetchTimeStampForCouponList @"BubbleCouponListLastTimeStamp"
#define kLastFetchTimeStampForActivityLikeList @"kLastFetchTimeStampForActivityLikeList"

//

#define kStrStatusInARelationship @"In a Relationship"
#define kUserDefaultsCurPhoto               @"TEMP_PHOTO"


#define kStrFBProfileLinkHeader @"http://graph.facebook.com/id/"
#define kFBImgLink(id) [NSString stringWithFormat:@"%@%@",kStrFBProfileLinkHeader, id]
typedef void (^CompletionHandler)(BOOL success, NSString *error);

// Other
#define g_compressRatio 1.0
#define kMaxVideoLenth 20.f
#define kStrMessage_InternetConnect_Photo @"Uploading Photo to server is failed. Internet connection is blocked during transaction.  Please try to upload later."

/// Bubble Table
#define kPF_TblBubbles @"Bubble"



//Bubble Notification Constants
#define kStrBadge @"badgeString"
#define kNotifType_BubbleRefresh @"RefreshBubble"
#define kNotifType_PrivatChatRefresh @"RefreshChatPriv"
#define kNotifType_Block @"Blocked From Bubble"
#define kNotifType_UnBlock @"Unblocked From Bubble"
#define kPF_FldNotifComment @"comment"
#define kfn_alertBubblekey(bubbleName) [NSString stringWithFormat:@"alert_bubble_%@",bubbleName]
#define kfn_alertBubblekey(bubbleName) [NSString stringWithFormat:@"alert_bubble_%@",bubbleName]
#define kStrNotifMsgWantToBeFriend @"Wants to be your friend"
#define kStrNotifMsgWantJoin @"Wants to join your bubble \"%@\""
#define kStrNotifMsgAccepted @"Accepted Your Friend Request"
#define kStrNotifMsgBlocked @"You have been discharged from a bubble by its creator."
#define kStrNotifMsgUnBlocked @"You have been reinstated for a bubble by its creator."
#define kStrNotifMsgCommented @"Commented on your message"
#define kNotifType_FriendRequestAccepted @"FriendRequestAccepted"
#define kNotifType_FriendRequestDeclined @"FriendRequestDeclined"
#define kPF_TblNotifHash @"NotifHash"
#define kStrActiveBubbleName @"StrActiveBubbleName"
#define kNotifType_loc_FriendRequestAccepted @"loc_FriendRequestAccepted"
#define kPF_TblNotif @"Notifications"
#define kPF_FldNotifReceiver @"receiver"
#define kPF_FldNotifDisplayed @"displayed"
#define kStrNotif_PrivateMsg @"PrivateMessage"
#define kStrNotif_Commented @"Commented on your post"
#define kStrPrivMsgSender @"PrivMsgSender"
#define kStrNotif_UserLocationReceived @"USER_LOCATION_RECEIVED"

#define MSG_NETWORK_CONNECTION_FAILED @"Network connection failed."
#define MSG_INVALID_USERNAME_PASSWORD @"Please provide valid username and password"
