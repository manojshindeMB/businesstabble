//
//  MainViewController.h
//  BusinessTabble
//
//  Created by Tian Ming on 09/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    NSArray *menuItems;
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitleMenu;
@property (weak, nonatomic) IBOutlet UITableView *tblMenu;

@end
