//
//  MainViewController.m
//  BusinessTabble
//
//  Created by Tian Ming on 09/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "MainViewController.h"
#import "MainMenuTableViewCell.h"
#import "HomeViewController.h"
#import "HomeTabBarController.h"
#import "BubbleFormViewController.h"
#import "SignupViewController.h"
#import "TermsAndPolicyViewController.h"
#import "FeedbackViewController.h"
#import "ReportViewController.h"
#import "AboutTabbleViewController.h"
#import "ReviewBubbleViewController.h"
#import "ActivityFormViewController.h"
#import "EventFormViewController.h"
#import "BubbleFormTypeViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController
@synthesize lblTitleMenu;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:lblTitleMenu.text];
    
    float spacing = 3.0f;
    [attributedString addAttribute:NSKernAttributeName
                             value:@(spacing)
                             range:NSMakeRange(0, [lblTitleMenu.text length])];
    
    lblTitleMenu.attributedText = attributedString;
    if([[localRegistry objectForKey:kMyUserRole] isEqualToString:@"1"])
    {
        menuItems = @[@"Home", @"Create a Place",   @"My Account", @"Report Problems", @"Terms of Service", @"Log Out", @"Review Business Bubbles",  @"Signed in:"];
        //@"Create an Activity", @"Create an Event",
//        @"Coupon Creator",
    }
    else
        menuItems = @[@"Home", @"Add Your Business", @"My Account", @"Report Problems", @"Terms of Service", @"Log Out",  @"Signed in:"]; //@"Create an Activity", @"Create an Event",
    // @"Coupon Creator",
    
    [_tblMenu reloadData];
    [self.view setBackgroundColor:MAIN_COLOR];

//    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentSandbox : @"AfxA-7FOAz-MPUN98jJ0Ov6U-JLf957mU7BKsld8Xr8mVZrqQfFl_I82upkOfcVdmm3TCDwlgqOlS3PY"}];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return menuItems.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if([tableView isEqual:_tblMenu])
    {
        NSString *CellIdentifier = @"MainMenuTableViewCell";
        MainMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        [cell.lblMenuTitle setText:[menuItems objectAtIndex:indexPath.row]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell.imgHead setHidden:YES];
        if([[menuItems objectAtIndex:indexPath.row] isEqualToString:@"Signed in:"])
        {
            [cell.lblTransparentBackground setHidden:YES];
            [cell.lblMenuTitle setText:[NSString stringWithFormat:@"%@ %@",[menuItems objectAtIndex:indexPath.row],[[GlobalData sharedData]mUserInfo].mUserName]];
        }
        else if([[menuItems objectAtIndex:indexPath.row] isEqualToString:@"Add Your Business"])
        {

//            [cell.lblTransparentBackground setAlpha:1.f];
//            [cell.lblTransparentBackground setBackgroundColor:[UIColor colorWithCGColor:PLACE_COLOR.CGColor]];
            cell.layout_constraint_title_left_padding.constant = 50;
            [cell.imgHead setHidden:NO];
            [cell.imgHead setImage:[UIImage imageNamed:@"icon_menu_placez"]];
        }
        else if([[menuItems objectAtIndex:indexPath.row] isEqualToString:@"Create an Activity"])
        {
//            [cell.lblTransparentBackground setHidden:YES];
            [cell.lblTransparentBackground setAlpha:1.f];
            [cell.lblTransparentBackground setBackgroundColor:[UIColor colorWithCGColor:ACTIVITY_COLOR.CGColor]];
            cell.layout_constraint_title_left_padding.constant = 50;
            [cell.imgHead setHidden:NO];
            [cell.imgHead setImage:[UIImage imageNamed:@"icon_menu_activity"]];
        }
        else if([[menuItems objectAtIndex:indexPath.row] isEqualToString:@"Create an Event"])
        {
//            [cell.lblTransparentBackground setHidden:YES];
            [cell.lblTransparentBackground setAlpha:1.f];
            [cell.lblTransparentBackground setBackgroundColor:[UIColor colorWithCGColor:EVENT_COLOR.CGColor]];
            cell.layout_constraint_title_left_padding.constant = 50;
            [cell.imgHead setHidden:NO];
            [cell.imgHead setImage:[UIImage imageNamed:@"icon_menu_event"]];
        }
        return cell;
    }
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SWRevealViewController *revealViewController = [self revealViewController];
    if([[menuItems objectAtIndex:indexPath.row] isEqualToString:@"Log Out"])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
    }
    else if([[menuItems objectAtIndex:indexPath.row] isEqualToString:@"Home"])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_DATA object:nil];
        HomeTabBarController *pVCHome = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeTabBarController"];
        [revealViewController pushFrontViewController:pVCHome animated:YES];
    }
    else if([[menuItems objectAtIndex:indexPath.row] isEqualToString:@"Create an Activity"])
    {
        UIStoryboard *storyboard = [ UIStoryboard storyboardWithName:@"Activity" bundle:nil];
        ActivityFormViewController *pVCHome = [storyboard instantiateViewControllerWithIdentifier:@"ActivityFormViewController"];
        pVCHome.mCurrentPageType = 1;
        [revealViewController pushFrontViewController:pVCHome animated:YES];
    }
    else if([[menuItems objectAtIndex:indexPath.row] isEqualToString:@"Create an Event"])
    {
        UIStoryboard *storyboard = [ UIStoryboard storyboardWithName:@"Event" bundle:nil];
        EventFormViewController *pVCHome = [storyboard instantiateViewControllerWithIdentifier:@"EventFormViewController"];
        pVCHome.mCurrentPageType = 1;
        [revealViewController pushFrontViewController:pVCHome animated:YES];
    }

    else if([[menuItems objectAtIndex:indexPath.row] isEqualToString:@"Add Your Business"])
    {
//        BubbleFormViewController *pVcBubbleForm = [self.storyboard instantiateViewControllerWithIdentifier:@"BubbleFormViewController"];
//        [revealViewController pushFrontViewController:pVcBubbleForm animated:YES];
        BubbleFormTypeViewController *pVcBubbleForm = [self.storyboard instantiateViewControllerWithIdentifier:@"BubbleFormTypeViewController"];
        [revealViewController pushFrontViewController:pVcBubbleForm animated:YES];

    }
    else if([[menuItems objectAtIndex:indexPath.row] isEqualToString:@"Coupon Creator"])
    {
        NSString *strCouponUrl = COUPON_CREATOR_URL;
        if ([[UIApplication sharedApplication]
             canOpenURL:[NSURL URLWithString:strCouponUrl]])
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strCouponUrl]];
        }
        else
        {
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Currently there is an issue. Please try later." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
            }];
        }
    }
    else if([[menuItems objectAtIndex:indexPath.row] isEqualToString:@"My Account"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        SignupViewController *signupVC = [storyboard instantiateViewControllerWithIdentifier:@"SignupViewController"];
        signupVC.mAccountAccessType = ACCOUNT_TYPE_EDIT_USER_INFO;
        UINavigationController *naviController = [[UINavigationController alloc]initWithRootViewController: signupVC];
        [naviController setNavigationBarHidden:YES];
        [self presentViewController:naviController animated:YES completion:^{
        }];
    }
    else if([[menuItems objectAtIndex:indexPath.row] isEqualToString:@"Make Tabble Business Better"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        FeedbackViewController *feedbackVC = [storyboard instantiateViewControllerWithIdentifier:@"FeedbackViewController"];
        [self presentViewController:feedbackVC animated:YES completion:^{
            
        }];
    }
    else if([[menuItems objectAtIndex:indexPath.row] isEqualToString:@"Report Problems"])
    {
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        ReportViewController *feedbackVC = [storyboard instantiateViewControllerWithIdentifier:@"ReportViewController"];
        UINavigationController *naviController = [[UINavigationController alloc]initWithRootViewController: feedbackVC];
        [naviController setNavigationBarHidden:YES];
        [self presentViewController:naviController animated:YES completion:^{
            
        }];
    }
    else if([[menuItems objectAtIndex:indexPath.row] isEqualToString:@"Terms of Service"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        TermsAndPolicyViewController *termsVC = [storyboard instantiateViewControllerWithIdentifier:@"TermsAndPolicyViewController"];
        termsVC.mTermsViewFromType = TERMS_TYPE_FROM_HOME;
        [self presentViewController:termsVC animated:YES completion:^{
            
        }];
    }
    else if([[menuItems objectAtIndex:indexPath.row] isEqualToString:@"About Tabble"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
        AboutTabbleViewController *pAboutVC = [storyboard instantiateViewControllerWithIdentifier:@"AboutTabbleViewController"];
        [self presentViewController:pAboutVC animated:YES completion:nil];
    }
    else if([[menuItems objectAtIndex:indexPath.row] isEqualToString:@"Review Business Bubbles"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Admin" bundle:nil];
        ReviewBubbleViewController *pReviewVC = [storyboard instantiateViewControllerWithIdentifier:@"ReviewBubbleViewController"];
        [revealViewController pushFrontViewController:pReviewVC animated:YES];
    }
    

}

@end
