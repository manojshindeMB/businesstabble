//
//  MainMenuTableViewCell.h
//  BusinessTabble
//
//  Created by Tian Ming on 09/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblMenuTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTransparentBackground;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_title_left_padding;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_head_image_left_padding;
@property (weak, nonatomic) IBOutlet UIImageView *imgHead;

@end
