//
//  FeedbackViewController.h
//  BusinessTabble
//
//  Created by Haichen Song on 25/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *txtContent;

@end
