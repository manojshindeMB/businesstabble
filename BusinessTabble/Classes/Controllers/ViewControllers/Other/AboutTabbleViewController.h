//
//  TermsAndPolicyViewController.h
//  BusinessTabble
//
//  Created by Tian Ming on 09/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutTabbleViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webViewContent;
@property (nonatomic) NSInteger mTermsViewFromType;
+(AboutTabbleViewController*)sharedController;
@end
