//
//  TermsAndPolicyViewController.m
//  BusinessTabble
//
//  Created by Tian Ming on 09/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "AboutTabbleViewController.h"

@interface AboutTabbleViewController ()

@end

@implementation AboutTabbleViewController
@synthesize webViewContent;
@synthesize mTermsViewFromType;
+(AboutTabbleViewController*)sharedController
{
    __strong static AboutTabbleViewController* sharedController = nil ;
    static dispatch_once_t onceToken ;
    
    dispatch_once( &onceToken, ^{
        sharedController = [ [ AboutTabbleViewController alloc ] initWithNibName : @"AboutTabbleViewController" bundle : nil ] ;
    } ) ;
    return sharedController ;
    
 
}


- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *ppt = [[NSBundle mainBundle] pathForResource:@"about" ofType:@"doc"];
    if(!ppt)
        return;
    NSURL *pptURL = [NSURL fileURLWithPath:ppt];
    NSURLRequest *request = [NSURLRequest requestWithURL:pptURL];
    [webViewContent loadRequest:request];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (IBAction)onTouchBtnBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{

    }];
}

@end
