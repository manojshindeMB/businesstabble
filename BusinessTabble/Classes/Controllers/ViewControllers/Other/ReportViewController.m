//
//  FeedbackViewController.m
//  BusinessTabble
//
//  Created by Haichen Song on 25/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "ReportViewController.h"
#import "OtherService.h"

@interface ReportViewController ()

@end

@implementation ReportViewController
@synthesize txtContent;

- (void)viewDidLoad {
    [super viewDidLoad];
    [_btnSend.layer setCornerRadius:17.f];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Actions
- (IBAction)onTouchBtnBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)onTouchBtnSubmit:(id)sender {
    if([[GlobalData sharedData]isSessionExpired])
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
    if([[GlobalData sharedData]isEmpty:txtContent.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide content." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:^{
        }];
        return;
    }
    NSMutableDictionary * params = [NSMutableDictionary new];
    params[@"toEmail"] = kStrSupportEmail;
    params[@"toName"] = kStrManagerName;
    params[@"fromUserId"] = [[GlobalData sharedData]mUserInfo].mId;
    params[@"fromEmail"] = [[GlobalData sharedData]mUserInfo].mEmail;
    params[@"fromName"] =  [NSString stringWithFormat:@"%@ %@",[[GlobalData sharedData]mUserInfo].mFirstName, [[GlobalData sharedData]mUserInfo].mLastName];
    params[@"text"] =  txtContent.text;
    params[@"subject"] = @"Report";
    [SVProgressHUD show];
    [[OtherService sharedData]sendReportWithDictionary:params success:^(id _responseObject) {
        [SVProgressHUD dismiss];
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Your feedback has been successfully sent." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
    } failure:^(NSInteger _errorCode) {
        [SVProgressHUD dismiss];
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:^{
        }];
    }];
    


}

@end
