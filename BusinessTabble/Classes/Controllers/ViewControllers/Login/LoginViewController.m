//
//  ViewController.m
//  BusinessTabble
//
//  Created by LiuMin on 06/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "LoginViewController.h"

#import "SignupViewController.h"
#import "TermsAndPolicyViewController.h"
#import "SWRevealViewController.h"
#import "HomeViewController.h"
#import "MainNavigationController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import "UserService.h"
#import <MagicalRecord/MagicalRecord.h>
#import "Bubble+CoreDataProperties.h"
#import "BubbleMember+CoreDataProperties.h"
#import <Google/SignIn.h>
#import <OneSignal/OneSignal.h>
#import "LoginNavigationController.h"
#import "MainLoadingViewController.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define FORGOT_PASSWORD_ALERTVIEW_TAG 33
@interface LoginViewController ()

@end

@implementation LoginViewController
@synthesize txtUserName;
@synthesize txtPassword;
@synthesize btnFacebook,btnForgotPassword,btnGooglePlus,btnLogin,btnTermsOfUse;

- (void)viewDidLoad {
    [super viewDidLoad];
    [txtUserName.layer setCornerRadius:15.f];
    [txtPassword.layer setCornerRadius:15.f];
    [txtUserName.layer setBorderWidth:1.f];
    [txtPassword.layer setBorderWidth:1.f];
    [txtUserName.layer setBorderColor:[UIColor darkGrayColor].CGColor];
    [txtPassword.layer setBorderColor:[UIColor darkGrayColor].CGColor];
    [btnLogin.layer setCornerRadius:15.f];
    
    txtUserName.clearButtonMode = UITextFieldViewModeWhileEditing;
    txtPassword.clearButtonMode = UITextFieldViewModeWhileEditing;

    
    btnForgotPassword.titleLabel.layer.shadowColor = [[UIColor whiteColor] CGColor];
//    btnForgotPassword.titleLabel.layer.shadowRadius = 2.0f;
//    btnForgotPassword.titleLabel.layer.shadowOpacity = 1.f;
    btnForgotPassword.titleLabel.layer.masksToBounds = YES;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onLogout:) name:NOTIFICATION_LOGOUT object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showMainView:) name:NOTIFICATION_SHOW_HOME object:nil];
    
    GIDSignIn*signIn=[GIDSignIn sharedInstance];
    signIn.uiDelegate = self;
    signIn.delegate = self;
    signIn.shouldFetchBasicProfile = YES;
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.view endEditing:YES];
    [btnTermsOfUse setSelected:YES];
    if(([localRegistry objectForKey:kMyUserName]) && [localRegistry objectForKey:kMyUserDict])
    {
        if([[localRegistry objectForKey:kCurLoginType] isEqualToString:@"email"])
        {
//            txtUserName.text = [localRegistry objectForKey:kMyUserName];
//            txtPassword.text = [localRegistry objectForKey:kMyUserPW];
            [btnTermsOfUse setSelected:YES];
        }
        JUserInfo *myInfo = [NSKeyedUnarchiver unarchiveObjectWithData:[localRegistry objectForKey:kMyUserDict]];
        [MagicalRecord setupCoreDataStackWithStoreNamed:[NSString stringWithFormat:@"Tabble_%@",myInfo.mId]];
        [[GlobalData sharedData] setMUserInfo:myInfo];
        [self showMainView:nil];
        return;
    }
    if([localRegistry objectForKey:kMyUserName])
    {
//        txtUserName.text = [localRegistry objectForKey:kMyUserName];
//        txtPassword.text = [localRegistry objectForKey:kMyUserPW];
        [btnTermsOfUse setSelected:YES];
    }
    
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)showMainView:(id)sender
{
    [self hideProgress];
    [OneSignal sendTag:@"user_id" value:[[GlobalData sharedData]mUserInfo].mId];

    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MainLoadingViewController *viewController = [mainstoryboard instantiateViewControllerWithIdentifier:@"MainLoadingViewController"];
    [self.navigationController pushViewController:viewController animated:NO];
//    [self hideProgressView];
}
#pragma mark Actions
- (IBAction)onTouchBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)onTouchBtnSignup:(id)sender {
    [self.view endEditing:YES];
    SignupViewController *signupView = [self.storyboard instantiateViewControllerWithIdentifier:@"SignupViewController"];
    signupView.mAccountAccessType = ACCOUNT_TYPE_SIGNUP;
    [self.navigationController pushViewController:signupView animated:YES];
}

- (IBAction)onTouchBtnTermsOfUse:(id)sender {
    UIButton *button = sender;
    button.selected = !button.selected;
}
- (IBAction)onTouchBtnForgotPassword:(id)sender {
    if (!btnTermsOfUse.selected) {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Must accept our Terms and Conditions to continue." BackgroundColor:TOAST_ERROR_COLOR]
                                    completionBlock:^{
                                    }];
        return;
    }
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:kAPP_TITLE message:@"Please input your username" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Go", nil];
    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertView textFieldAtIndex:0].keyboardType = UIKeyboardTypeEmailAddress;
    alertView.tag = FORGOT_PASSWORD_ALERTVIEW_TAG;
    [alertView show];

}
- (IBAction)onTouchBtnShowTermsOfUse:(id)sender {
    TermsAndPolicyViewController *pTermsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsAndPolicyViewController"];

    [self presentViewController:pTermsVC animated:YES completion:nil];
}


- (IBAction)onTouchBtnLogin:(id)sender {

    NSString *username = [txtUserName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *password = txtPassword.text;
    if([[GlobalData sharedData]isEmpty:username])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide username" BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        return;
    }
    if([[GlobalData sharedData]isEmpty:password])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Password should be at least 6 l§ength long." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        return;
    }
    if ([localRegistry objectForKey:kMyUserName] == nil) {
        //Need to refresh bubbles (set value)
    }
    if (txtUserName.text.length && [txtUserName.text isEqualToString:[localRegistry objectForKey:kMyUserName]]) {
        //No Need to refresh bubbles (set value)
    }
    [self.view endEditing:YES];
    [localRegistry setObject:@"email" forKey:kCurLoginType];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:username forKey:@"username"];
    [params setObject:password forKey:@"password"];
    [params setObject:[NSString stringWithFormat:@"%f", [[GlobalData sharedData]currentLatitude]] forKey:@"latitude"];
    [params setObject:[NSString stringWithFormat:@"%f", [[GlobalData sharedData]currentLongitude]] forKey:@"longitude"];
    [self showProgress:@"Please wait..."];
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[UserService sharedData]loginRequestByUserNameWithParams:params success:^(id _responseObject) {
        
        [MagicalRecord setupCoreDataStackWithStoreNamed:[NSString stringWithFormat:@"Tabble_%@", [_responseObject objectForKey:@"id"]]];
        
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [[[GlobalData sharedData]mUserInfo]setDataWithDictionary:_responseObject];
//        [localRegistry setObject:[[GlobalData sharedData]mUserInfo] forKey:kMyUserDict];
        
         NSData *personEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:[[GlobalData sharedData]mUserInfo]];
        [localRegistry setObject:personEncodedObject forKey:kMyUserDict];
        
        [localRegistry setObject:username forKey:kMyUserName]; //[username lowercaseString]
        //[localRegistry setObject:[[GlobalData sharedData]mUserInfo].mEmail forKey:kMyEmail];
        
        [localRegistry setObject:@"email" forKey:kCurLoginType];
        [localRegistry setObject:txtPassword.text forKey:kMyUserPW];
        [localRegistry setObject:[_responseObject objectForKey:@"role"] forKey:kMyUserRole];
        
        
        [self showMainView:nil];
        
    } failure:^(NSInteger _errorCode) {
        [self hideProgress];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        NSString *errorStr = MSG_NETWORK_CONNECTION_FAILED;
        if(_errorCode == 400)
            errorStr = MSG_INVALID_USERNAME_PASSWORD;
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:errorStr BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        return;
        
    }];
}
#pragma mark Facebook
- (IBAction)onTouchBtnFacebook:(id)sender {
    if (!btnTermsOfUse.selected) {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Must accept our Terms and Conditions to continue." BackgroundColor:TOAST_ERROR_COLOR]
                                    completionBlock:^{
                                    }];
        return;
    }
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"email",@"public_profile", @"user_birthday"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"result %@", result);
             if ([FBSDKAccessToken currentAccessToken])
             {
                 NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
                 
                 [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name,gender, last_name, picture.type(large), email, birthday, location ,hometown "}]
                  startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                      if (!error)
                      {                          
                          [self loginWithFacebook:result];
                      }
                      else
                      {
                          NSLog(@"Error %@",error);
                      }
                  }];
                 
             }
         }
     }];
}
- (void)loginWithFacebook:(id) result

{
    if(!result) return;
    NSString *email = @"";
    NSString *first_name = @"";
    NSString *last_name = @"";
    NSString *facebook_id = @"";
    NSString *link = @"";
    NSString *photo_url = @"";
    NSString *gender = @"1"; //1:male, 2:female
    if([result objectForKey:@"email"] && ![[GlobalData sharedData]isEmpty:[result objectForKey:@"email"]])
        email = [result objectForKey:@"email"];
    if([result objectForKey:@"first_name"] && ![[GlobalData sharedData]isEmpty:[result objectForKey:@"first_name"]])
        first_name = [result objectForKey:@"first_name"];
    if([result objectForKey:@"last_name"] && ![[GlobalData sharedData]isEmpty:[result objectForKey:@"last_name"]])
        last_name = [result objectForKey:@"last_name"];
    if([result objectForKey:@"link"] && ![[GlobalData sharedData]isEmpty:[result objectForKey:@"link"]])
        link = [result objectForKey:@"link"];
    if([result objectForKey:@"id"] && ![[GlobalData sharedData]isEmpty:[result objectForKey:@"id"]])
        facebook_id = [result objectForKey:@"id"];
    if([result objectForKey:@"gender"] && ![[GlobalData sharedData]isEmpty:[result objectForKey:@"gender"]])
        gender = [[result objectForKey:@"gender"] isEqualToString:@"female"]?@"2":@"1";
    
    if([result objectForKey:@"picture"])
    {
        if([[result objectForKey:@"picture"] objectForKey:@"data"] && [[[result objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"])
            photo_url = [[[result objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"];
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:facebook_id forKey:@"login_id"];
    [params setObject:email forKey:@"email"];
    [params setObject:@"facebook" forKey:@"logintype"];
    [params setObject:@"" forKey:@"birthday"];
    [params setObject:first_name forKey:@"firstname"];
    [params setObject:last_name forKey:@"lastname"];
    [params setObject:gender forKey:@"gender"];
    [params setObject:kStrStatusInARelationship forKey:@"relationship"];
    if([[GlobalData sharedData]currentCity])
        [params setObject:[[GlobalData sharedData]currentCity] forKey:@"city"];
    else
        [params setObject:@"" forKey:@"city"];
    
    if([[GlobalData sharedData]currentState])
        [params setObject:[[GlobalData sharedData]currentState] forKey:@"state"];
    else
        [params setObject:@"" forKey:@"state"];
    if([[GlobalData sharedData]currentCountry])
        [params setObject:[[GlobalData sharedData]currentCountry] forKey:@"country"];
    else
        [params setObject:@"" forKey:@"country"];
    CGFloat latitude =  [[GlobalData sharedData]currentLatitude];
    CGFloat longitude = [[GlobalData sharedData]currentLongitude];
    [params setObject:[NSString stringWithFormat:@"%f", latitude] forKey:@"latitude"];
    [params setObject:[NSString stringWithFormat:@"%f", longitude] forKey:@"longitude"];
    
    if(photo_url && [photo_url length] > 0)
        [params setObject:photo_url forKey:@"photourl"];
    else
        [params setObject:@"" forKey:@"photourl"];
    
    [self showProgress:@"Please wait..."];
    [self submitUserLoginWithSocialMedia:params SuccessHandler:^{
        [self showMainView:nil];
    } FailureHandler:^{
        [self hideProgress];
    }];
}
#pragma mark Google Plus API
- (IBAction)onTouchBtnGooglePlus:(id)sender {
    if (!btnTermsOfUse.selected) {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Must accept our Terms and Conditions to continue." BackgroundColor:TOAST_ERROR_COLOR]
                                    completionBlock:^{
                                    }];
        return;
    }
    [[GIDSignIn sharedInstance] signIn];
    
}
- (void)toggleAuthUI {
    [self hideProgress];
    if ([GIDSignIn sharedInstance].currentUser.authentication == nil) {
        // Not signed in
        NSLog(@"NOT SIGNED IN");
    } else {
        NSLog(@"SIGNED IN");
    }
}
// [END toggle_auth]
- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController
{
    [self presentViewController:viewController animated:YES completion:nil];
}
- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    [self hideProgress];
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *name = user.profile.name;
    NSString *email = user.profile.email;
    NSString *photoUrl = @"";
    if (user.profile.hasImage)
    {
        
        NSUInteger dimension = round(100 * [[UIScreen mainScreen] scale]);
        NSURL *imageURL = [user.profile imageURLWithDimension:dimension];
        photoUrl = imageURL.absoluteString;
    }
    
    NSLog(@"Customer details: %@ %@ %@ %@", userId, idToken, name, email);
    // ...
    NSDictionary* parameters=[[NSDictionary alloc] initWithObjectsAndKeys:
                                                                        @"login_signup",@"type",
                                                                        @"login",@"cmd",
                                                                        @"google",@"usertype",
                                                                        userId,@"google_id",
                                                                        photoUrl,@"photourl",
                                                                        user.profile.givenName,@"firstname",
                                                                        user.profile.familyName,@"lastname",
                                                                        @"",@"locationid",
                                                                        email , @"email",
                                                                        @"", @"birthday",
                                                                        nil];
                                              [self fetchedGooglePlusData:parameters];

}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:@"ToggleAuthUINotification"
     object:nil];
    
}

- (void) receiveToggleAuthUINotification:(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"ToggleAuthUINotification"]) {
        [self toggleAuthUI];
//        self.statusText.text = [notification userInfo][@"statusText"];
    }
}


-(void)fetchedGooglePlusData:(NSDictionary *)json{
    
    NSString *strGender = [[json objectForKey:@"gender"] isEqualToString:@"male"]?@"1":@"0";
    NSString *email = @"";
    NSString *birthday = @"";
    if([json objectForKey:@"email"] && ![[json objectForKey:@"email"] isKindOfClass:[NSNull class]])
        email = [json objectForKey:@"email"];
    else
        return;
    if([json objectForKey:@"birthday"] && ![[json objectForKey:@"birthday"] isKindOfClass:[NSNull class]])
        birthday = [json objectForKey:@"birthday"];
    
    NSMutableDictionary *dictLogin = [[NSMutableDictionary alloc]init];
    [dictLogin setObject:[json objectForKey: @"google_id"] forKey:@"login_id"];
    [dictLogin setObject:[json objectForKey: @"photourl"] forKey:@"photourl"];
    [dictLogin setObject:email forKey:@"email"];
    [dictLogin setObject:birthday forKey:@"birthday"];
    [dictLogin setObject:[json objectForKey: @"firstname"] forKey:@"firstname"];
    [dictLogin setObject:[json objectForKey: @"lastname"] forKey:@"lastname"];
    [dictLogin setObject:strGender forKey:@"gender"];
    [dictLogin setObject:kStrStatusInARelationship forKey:@"relationship"];
    [dictLogin setObject:@"" forKey:@"city"];
    [dictLogin setObject:@"" forKey:@"country"];
    [dictLogin setObject:@"google" forKey:@"logintype"];
    [self showProgress:@"Please wait..."];
    [self submitUserLoginWithSocialMedia:dictLogin SuccessHandler:^{
        [self showMainView:nil];
    } FailureHandler:^{
        [self hideProgress];
    }];
    
    
}
//


#pragma mark login/register with Social Media
- (void)submitUserLoginWithSocialMedia:(NSMutableDictionary *)dictUserInformation
           SuccessHandler:(void (^)())successHandler
           FailureHandler:(void (^)())failureHandler
{
    NSString *email = [dictUserInformation objectForKey:@"email"];
    NSString *username = @"";
    if(email && email.length > 0 && [[GlobalData sharedData]validateEmailWithString:email])
    {
        NSArray *pArrTemp = [email componentsSeparatedByString:@"@"];
        username = [pArrTemp objectAtIndex:0];
    }
    else
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Your email address can't be retrieved." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    [dictUserInformation setObject:username forKey:@"username"];
    NSDate *date = [NSDate date];
    NSString *strfilename = [NSString stringWithFormat:@"%@%ld.jpg", [dictUserInformation objectForKey:@"firstname"] , (long)[date timeIntervalSince1970]];
    
    if([[GlobalData sharedData]currentCity])
        [dictUserInformation setObject:[[GlobalData sharedData]currentCity] forKey:@"city"];
    else
        [dictUserInformation setObject:@"" forKey:@"city"];    
    
    if([[GlobalData sharedData]currentState])
        [dictUserInformation setObject:[[GlobalData sharedData]currentState] forKey:@"state"];
    else
        [dictUserInformation setObject:@"" forKey:@"state"];
    
    [dictUserInformation setObject:strfilename forKey:@"photo_filename"];
    [dictUserInformation setObject:username forKey:@"username"];
    CGFloat latitude =  [[GlobalData sharedData]currentLatitude];
    CGFloat longitude = [[GlobalData sharedData]currentLongitude];
    [dictUserInformation setObject:[NSString stringWithFormat:@"%f", latitude] forKey:@"latitude"];
    [dictUserInformation setObject:[NSString stringWithFormat:@"%f", longitude] forKey:@"longitude"];
    [[UserService sharedData]loginRequestBySocialMediaWithParams:dictUserInformation success:^(id _responseObject, NSInteger result_code) {
        
        [MagicalRecord setupCoreDataStackWithStoreNamed:[NSString stringWithFormat:@"Tabble_%@", [_responseObject objectForKey:@"id"]]];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [localRegistry setObject:username forKey:kMyUserName]; //[username lowercaseString]
        [localRegistry setObject:email forKey:kMyEmail];
        [localRegistry setObject:[dictUserInformation objectForKey:@"logintype"] forKey:kCurLoginType];
        [localRegistry setObject:[dictUserInformation objectForKey:@"login_id"] forKey:kMyUserPW];
        [localRegistry setObject:[_responseObject objectForKey:@"role"] forKey:kMyUserRole];
        [[[GlobalData sharedData]mUserInfo]setDataWithDictionary:_responseObject];
        NSData *personEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:[[GlobalData sharedData]mUserInfo]];
        [localRegistry setObject:personEncodedObject forKey:kMyUserDict];
        
        if(result_code == 2)//signup
        {
            if([[dictUserInformation objectForKey:@"photourl"] length] > 0)
            {
                [self uploadProfilePhotoWithUrl:[dictUserInformation objectForKey:@"photourl"] name:strfilename];
            }
        }
        successHandler();
        return ;
    } failure:^(NSInteger _errorCode) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }];
}
- (void)uploadProfilePhotoWithUrl :(NSString *)url name:(NSString *)name
{
    NSString *strfilename = name;
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
    UIImage *image = [UIImage imageWithData:imageData];
    NSData *imgData = UIImageJPEGRepresentation(image, g_compressRatio) ;
    [[GlobalData sharedData]uploadPhotoWithName:strfilename oldname:@"" image:image type:@"" completion:^{
        [localRegistry removeObjectForKey:kUserDefaultsCurPhoto];
    } FailureHandler:^{
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:kStrMessage_InternetConnect_Photo BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
    }];
}
#pragma mark Notifications
-(void)onLogout:(id)sender
{
    //First need to remove remaining object from Global Data
//    [localRegistry removeObjectForKey:kMyEmail];
//    [localRegistry removeObjectForKey:kMyUserName];
//    [localRegistry removeObjectForKey:kMyUserPW];
 
    [localRegistry removeObjectForKey:kCurLoginType];
    [localRegistry removeObjectForKey:kLastFetchTimeStampForBusinessBubbleList];
    [localRegistry removeObjectForKey:kLastFetchTimeStampForBubbleMemberList];
    [localRegistry removeObjectForKey:kLastFetchTimeStampForBubbleMenuList];
    [localRegistry removeObjectForKey:kLastFetchTimeStampForBubbleMessageList];
    [localRegistry removeObjectForKey:kLastFetchTimeStampForCouponList];
    [localRegistry removeObjectForKey:kMyUserDict];
    [localRegistry removeObjectForKey:kLastFetchTimeStampForActivityLikeList];
    [localRegistry removeObjectForKey:kLastFetchTimeStampForBubbleChatList];
    [localRegistry removeObjectForKey:BubbleHidePosterListLastTimeStamp];
    
//    [localRegistry removeObjectForKey:kLastFetchTimeStampForMyBubbleList];
    [MagicalRecord cleanUp];
    [self.navigationController popToRootViewControllerAnimated:YES];
    [[GlobalData sharedData]setIsMainFirstLoading:YES];
}


#pragma mark AlertView deletegates
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == FORGOT_PASSWORD_ALERTVIEW_TAG && buttonIndex == 1)
    {
        UITextField *txtView = [alertView textFieldAtIndex:0];
        if(!txtView)
            return;
        if([[GlobalData sharedData]isEmpty:txtView.text])
        {
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide your username" BackgroundColor:TOAST_ERROR_COLOR]
                                        completionBlock:^{
                                        }];
            return;
        }
        [self sendRequestForPasswordRecovery:txtView.text];
    }
}
- (void)sendRequestForPasswordRecovery:(NSString *)username
{
    username = [username stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:username forKey:@"username"];
    [[UserService sharedData]passwordRecoveryWithParams:params success:^(id _responseObject) {
        
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Email has been sent. Please check your inbox and follow the instruction to reset password." BackgroundColor:TOAST_SUCCESS_COLOR]
                                    completionBlock:^{
                                    }];
        return;
    } failure:^(NSInteger _errorCode) {
        if(_errorCode == 404)
        {
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide valid username" BackgroundColor:TOAST_ERROR_COLOR]
                                        completionBlock:^{
                                        }];
            return;
        }
        else
        {
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR]
                                        completionBlock:^{
                                        }];
            return;
            
        }
    }];
}


@end
