//
//  LoadingViewController.h
//  BusinessTabble
//
//  Created by Tian Ming on 20/01/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingViewController : UIViewController
{
    BOOL bShowAnimation;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgFront;
@property (weak, nonatomic) IBOutlet UIImageView *imgBack;
@property (weak, nonatomic) IBOutlet UIButton *btnUser;
@property (weak, nonatomic) IBOutlet UIButton *btnNew;

@property (nonatomic) NSInteger mCurrentBackgroundImageNumber;
@end
