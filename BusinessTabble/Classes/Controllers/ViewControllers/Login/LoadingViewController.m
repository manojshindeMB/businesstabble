//
//  LoadingViewController.m
//  BusinessTabble
//
//  Created by LiuMin on 20/01/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import "LoadingViewController.h"
#import "LoginViewController.h"
#import "SignupViewController.h"
#import "IntroductionViewController.h"
@interface LoadingViewController ()

@end

@implementation LoadingViewController
@synthesize mCurrentBackgroundImageNumber = _mCurrentBackgroundImageNumber;
- (void)viewDidLoad {
    [super viewDidLoad];
    [_btnNew.layer setCornerRadius:5.f];
    [_btnUser.layer setCornerRadius:5.f];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showSignupView:) name:NOTIFICATION_SHOW_SIGNUP_VIEW object:nil];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
  
      if ([localRegistry objectForKey:kMyUserName] && [localRegistry objectForKey:kMyUserDict]) {
          [self showLoginPage];
          return;
      }
    self.mCurrentBackgroundImageNumber = 0;
    bShowAnimation = YES;
    [_imgFront setAlpha:0.4f];
    [_imgBack setAlpha:0.f];
    [UIView animateWithDuration:0.1 animations:^{
        [_imgFront setAlpha:1.f];
    } completion:^(BOOL finished) {
        [_imgBack setAlpha:1.f];
//        [self animateBackgroundView];
                    [_imgFront setImage:[UIImage imageNamed:@"pure_background"]];
    }];

    
}
- (void)viewWillDisappear:(BOOL)animated
{
    bShowAnimation = NO;
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)animateBackgroundView
{
    if(bShowAnimation == NO)
        return;
    [UIView animateWithDuration:1.5 delay:2.5 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.mCurrentBackgroundImageNumber = (self.mCurrentBackgroundImageNumber + 1) % 5;
        if(_imgFront.alpha == 1)
        {
            [_imgFront setAlpha:0];
            [_imgBack setAlpha:1];
            [_imgBack setImage:[UIImage imageNamed:[NSString stringWithFormat:@"loadingimage%ld",(long)self.mCurrentBackgroundImageNumber]]];

        }
        else
        {
            [_imgFront setAlpha:1];
            [_imgBack setAlpha:0];
            [_imgFront setImage:[UIImage imageNamed:[NSString stringWithFormat:@"loadingimage%ld",(long)self.mCurrentBackgroundImageNumber]]];
            
        }
    } completion:^(BOOL finished){
        if(bShowAnimation)
            [self animateBackgroundView];
    }];
}
-(void)setMCurrentBackgroundImageNumber:(NSInteger)mCurrentBackgroundImageNumber
{
    if(mCurrentBackgroundImageNumber == 0)
        mCurrentBackgroundImageNumber = 1;
    _mCurrentBackgroundImageNumber = mCurrentBackgroundImageNumber;
//
}
- (IBAction)onTouchBtnLogin:(id)sender {
    [self showLoginPage];
}
- (IBAction)onTouchBtnNewUser:(id)sender {
    IntroductionViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"IntroductionViewController"];
    [self presentViewController:loginVC animated:YES completion:nil];
    
//    SignupViewController *signupVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SignupViewController"];
//    [self presentViewController:signupVC animated:YES completion:^{
//
//    }];
    
    
//    [UIView animateWithDuration:0.7 animations:^{
//        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
//        [self presentViewController:signupVC animated:YES completion:^{
//            
//        }];
//    } completion:^(BOOL finished) {
//       
//    }];
}
- (void)showSignupView:(id)sender
{
    SignupViewController *signupVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SignupViewController"];
    [self presentViewController:signupVC animated:YES completion:^{
        
    }];
}
- (void)showLoginPage
{
    
    LoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:loginVC animated:NO];

}
@end
