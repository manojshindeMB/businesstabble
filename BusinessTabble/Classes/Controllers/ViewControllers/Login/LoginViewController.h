//
//  ViewController.h
//  BusinessTabble
//
//  Created by Tian Ming on 06/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <GoogleOpenSource/GoogleOpenSource.h>
#import <Google/SignIn.h>

@interface LoginViewController : UIViewController<UIGestureRecognizerDelegate, UIAlertViewDelegate, GIDSignInUIDelegate, GIDSignInDelegate>

@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtUserName;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnTermsOfUse;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnGooglePlus;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnTerms;





@end

