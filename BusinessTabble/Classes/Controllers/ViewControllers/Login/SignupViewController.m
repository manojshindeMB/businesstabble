//
//  SignupViewController.m
//  BusinessTabble
//
//  Created by Tian Ming on 08/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "SignupViewController.h"
#import "RSKImageCropViewController.h"
#import "UserService.h"
#import <MagicalRecord/MagicalRecord.h>
#import <SDWebImage/UIButton+WebCache.h>
#import "LoginViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "TermsAndPolicyViewController.h"
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
#define ACCEPTABLE_CHARACTERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_"

@interface SignupViewController ()

@end

@implementation SignupViewController
@synthesize photoActionSheet;
@synthesize mUserNameValid = _mUserNameValid;
@synthesize mImgUserPhoto = _mImgUserPhoto;
@synthesize mAccountAccessType;

@synthesize userPhotoChanged;
- (void)viewDidLoad {
    [super viewDidLoad];
    _txtPassword.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtEmail.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtCity.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtUserName.clearButtonMode = UITextFieldViewModeWhileEditing;
    float cornerRadius = 15.f;
    
    [_txtUserName.layer setCornerRadius:cornerRadius];
    [_txtEmail.layer setCornerRadius:cornerRadius];
    [_txtCity.layer setCornerRadius:cornerRadius];
    [_txtPassword.layer setCornerRadius:cornerRadius];
    
    photoActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel"  destructiveButtonTitle:nil otherButtonTitles:@"From Camera",@"From Photo Library", nil];
    [_btnCreate setEnabled:YES];
    [_txtUserName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_btnCreate.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_btnCreate.layer setBorderWidth:2.f];
    [self.view layoutIfNeeded];
    [_btnLogo.layer setBorderColor:MAIN_COLOR.CGColor];
    [_btnLogo.layer setBorderWidth:2.f];
    [_btnLogo.layer setCornerRadius:_btnLogo.frame.size.width / 2];
    [_btnLogo.layer setMasksToBounds:YES];
    _btnLogo.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _btnLogo.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onUserLocationReceived:) name:kStrNotif_UserLocationReceived object:nil];
    if(mAccountAccessType == ACCOUNT_TYPE_EDIT_USER_INFO)
    {
        [self setDefaultInformation];
        [_lblTitle setText:@"Edit Account"];
        [_vwSeparator setHidden:YES];
        [_vwSocialButtonContainer setHidden:YES];
        _layout_coinstraint_social_buttons_height.constant = 0;
        [_vwSignUp setHidden:YES];
    }
    
    GIDSignIn*signIn=[GIDSignIn sharedInstance];
    signIn.uiDelegate = self;
    signIn.delegate = self;
    signIn.shouldFetchBasicProfile = YES;
    userPhotoChanged = NO;
    self.mUserNameValid = YES;
}
- (void)setDefaultInformation
{
    JUserInfo *currentUser = [[GlobalData sharedData]mUserInfo];
    _txtEmail.text = currentUser.mEmail;
    _txtUserName.text = currentUser.mUserName;
    _txtPassword.text = [localRegistry objectForKey:kMyUserPW];
    if([[GlobalData sharedData]isEmpty:currentUser.mCity])
        _txtCity.text = [NSString stringWithFormat:@"%@,%@",[[GlobalData sharedData]currentCity], [[GlobalData sharedData]currentState]];
    else
        _txtCity.text = [NSString stringWithFormat:@"%@",currentUser.mCity];

    [_btnCreate setTitle:@"Update" forState:UIControlStateNormal];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",g_storagePrefix,currentUser.mPhotoFileName]];
    [_btnLogo sd_setImageWithURL:url forState:UIControlStateNormal placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.mImgUserPhoto = image;
    }];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self onUserLocationReceived:nil];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Actions
- (IBAction)onTouchBtnBack:(id)sender {
    if(mAccountAccessType == ACCOUNT_TYPE_EDIT_USER_INFO)
        [self dismissViewControllerAnimated:YES completion:nil];
    else
        [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)onTouchBtnShowTerms:(id)sender {
    TermsAndPolicyViewController *pTermsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsAndPolicyViewController"];
    
    [self presentViewController:pTermsVC animated:YES completion:nil];
}

-(void)onUserLocationReceived:(id)sender
{
    if(![[GlobalData sharedData]isEmpty:[[GlobalData sharedData]currentCity]])
        _txtCity.text = [NSString stringWithFormat:@"%@,%@",[[GlobalData sharedData]currentCity], [[GlobalData sharedData]currentState]];
}
- (IBAction)onTouchBtnUploadPhoto:(id)sender {
    [photoActionSheet showInView:[self.view window]];
}
- (IBAction)onTouchBtnCreate:(id)sender {
    [self checkFormValidation:^{
        NSDate *date = [NSDate date];
        NSString *strfilename = [[NSString stringWithFormat:@"%@%ld.jpg", @"user_" , (long)[date timeIntervalSince1970]]  stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString *strOldFileName = @"";
        NSString *userId = @"";
        if(mAccountAccessType == ACCOUNT_TYPE_EDIT_USER_INFO)
        {
            strOldFileName = [[GlobalData sharedData]mUserInfo].mPhotoFileName;
            userId = [[GlobalData sharedData]mUserInfo].mId;
        }
        if(userPhotoChanged)
            strfilename = [[GlobalData sharedData]urlencode:strfilename];
        else
            strfilename = strOldFileName;
        
        NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
        [params setObject:[_txtUserName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  forKey:@"username"];
        [params setObject:_txtPassword.text forKey:@"password"];
        [params setObject:_txtEmail.text forKey:@"email"];
        [params setObject:userId forKey:@"user_id"];
        [params setObject:@"" forKey:@"firstname"];
        [params setObject:@"" forKey:@"lastname"];
        [params setObject:_txtCity.text forKey:@"city"];
        [params setObject:@"" forKey:@"state"];
        [params setObject:[NSString stringWithFormat:@"%f", [[GlobalData sharedData]currentLatitude]] forKey:@"latitude"];
        [params setObject:[NSString stringWithFormat:@"%f", [[GlobalData sharedData]currentLongitude]] forKey:@"longitude"];
        [params setObject:strfilename forKey:@"photo_filename"];
        [self showProgress:@"Please wait..."];
        [[UserService sharedData]signUpRequestByUserNameWithParams:params type:mAccountAccessType success:^(id _responseObject) {
            [self hideProgress];
            if(userPhotoChanged)
                [self uploadProfilePhotoWithName:strfilename];

            [[[GlobalData sharedData]mUserInfo]setDataWithDictionary:_responseObject];
            [MagicalRecord setupCoreDataStackWithStoreNamed:[NSString stringWithFormat:@"Tabble_%@", [_responseObject objectForKey:@"id"]]];
            [localRegistry setObject:_txtUserName.text forKey:kMyUserName];
            [localRegistry setObject:_txtEmail.text forKey:kMyEmail];
            [localRegistry setObject:_txtPassword.text forKey:kMyUserPW];// ?? pw might be risk
            
            NSData *personEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:[[GlobalData sharedData]mUserInfo]];
            [localRegistry setObject:personEncodedObject forKey:kMyUserDict];
            
            if(mAccountAccessType == ACCOUNT_TYPE_SIGNUP)
            {
                [localRegistry setObject:@"email" forKey:kCurLoginType];
                [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Your account has been successfully created." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
                    [self dismissViewControllerAnimated:YES completion:^{

                    }];

                }];
            }
            else
            {
                
                [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Your account has been successfully updated." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
//                    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
                    [self dismissViewControllerAnimated:YES completion:nil];
                }];
            
            }           
            
        } failure:^(NSInteger _errorCode) {
            [self hideProgress];
            if(_errorCode == 400)
            {
                [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"The username already exists." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
                return;
            }
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        }];
    } FailureHandler:^{
        
    }];
}

#pragma mark  actionsheet didDismissWithButtonIndex
- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 2 )
        return;
    if ( actionSheet == photoActionSheet )
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.editing = YES;
        picker.videoMaximumDuration = kMaxVideoLenth;
        {
            if (buttonIndex == 0) {
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
            } else if (buttonIndex == 1) {
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            }
        }
        picker.mediaTypes = [NSArray arrayWithObjects: (NSString*)kUTTypeImage, nil];//[NSArray arrayWithObjects:(NSString*)kUTTypeMovie, (NSString*)kUTTypeImage, nil];
        [self presentViewController:picker animated:TRUE completion:nil ];
    }
}
#pragma mark didFinishPickingMediaWithInfo
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *imgtmp = [info objectForKey:UIImagePickerControllerOriginalImage];
    if(!imgtmp || imgtmp == nil)
    {
         [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"You can only use photo." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        return;
    }
    imgtmp = [GlobalData scaleAndRotateImage:imgtmp];
    [self dismissViewControllerAnimated:YES completion:^{
        PECropViewController *controller = [[PECropViewController alloc] init];
        controller.delegate = self;
        controller.image = imgtmp;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            controller.modalPresentationStyle = UIModalPresentationFormSheet;
        }
        [controller setKeepingCropAspectRatio:NO];
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        }
        [self presentViewController:navigationController animated:YES completion:NULL];

    }];
}
#pragma mark Cropper Delegate
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    self.mImgUserPhoto = croppedImage;
    userPhotoChanged = YES;
}
- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark Property Getter & Setter
-(void)setMImgUserPhoto:(UIImage *)mImgUserPhoto
{
    _mImgUserPhoto = mImgUserPhoto;
    [_btnLogo setImage:mImgUserPhoto forState:UIControlStateNormal];
    [self.view layoutIfNeeded];
    [_btnLogo.layer setCornerRadius:_btnLogo.frame.size.width / 2];
    [_btnLogo.layer setMasksToBounds:YES];
    [_btnLogo.layer setBorderWidth:2.f];
    [_btnLogo.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_btnLogo setTitle:@"" forState:UIControlStateNormal];
}
- (void)setMUserNameValid:(BOOL)mUserNameValid
{
    _mUserNameValid = mUserNameValid;
    if(mUserNameValid)
    {
        [_btnCreate setEnabled:YES];
        [_lblUsernameTaken setHidden:YES];
//        [_imgUserNameTaken setHidden:NO];
//        [_imgUserNameTaken setImage:[UIImage imageNamed:@"ok_taken.png"]];
    }
    else
    {
        [_btnCreate setEnabled:NO];
        [_lblUsernameTaken setHidden:NO];
//        [_imgUserNameTaken setHidden:NO];
//        [_imgUserNameTaken setImage:[UIImage imageNamed:@"err_taken.png"]];
    }
}
#pragma mark TextField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    if (textField == _txtUserName) {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    return YES;
}
-(void)textFieldDidChange :(UITextField *)textField{
    
  
    
}
#pragma mark UITextField Delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([textField isEqual:_txtCity])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SearchAddressViewController *searchVC = [storyboard instantiateViewControllerWithIdentifier:@"SearchAddressViewController"];
        searchVC.delegate = self;
        [self presentViewController:searchVC animated:YES completion:nil];
        return NO;
    }
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField reason:(UITextFieldDidEndEditingReason)reason
{
    if (textField == _txtUserName) {
        NSString *strName = [_txtUserName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (strName && strName.length) {
            if(mAccountAccessType == ACCOUNT_TYPE_SIGNUP)
            {
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                [params setObject:strName forKey:@"username"];
                [[UserService sharedData] checkUserNameAvailabilityWithParams:params success:^(id _responseObject) {
                    self.mUserNameValid = YES;
                } failure:^(NSInteger _errorCode) {
                    self.mUserNameValid = NO;
                }];
            }
            else
            {
                NSString *username = [[[GlobalData sharedData]mUserInfo].mUserName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                if(![[strName lowercaseString] isEqualToString:[username lowercaseString]])
                {
                    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                    [params setObject:strName forKey:@"username"];
                    [[UserService sharedData] checkUserNameAvailabilityWithParams:params success:^(id _responseObject) {
                        self.mUserNameValid = YES;
                    } failure:^(NSInteger _errorCode) {
                        self.mUserNameValid = NO;
                    }];
                }
            }
        } else {
            
        }
        
    }
}
#pragma mark other methods
- (void)checkFormValidation:(void (^)())successHandler FailureHandler:(void (^)())failureHandler
{
    if(!self.mUserNameValid)
    {
        failureHandler();
        return;
    }
    if([[GlobalData sharedData]isEmpty:_txtUserName.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide username" BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        
    }
    if([[GlobalData sharedData]isEmpty:_txtPassword.text] || _txtPassword.text.length < 6)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Password should be at least 6 length long." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if([[GlobalData sharedData]isEmpty:_txtEmail.text] || ![[GlobalData sharedData]validateEmailWithString:_txtEmail.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide valid email address." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if([[GlobalData sharedData]isEmpty:_txtCity.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide location." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if(!self.mImgUserPhoto)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide photo." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if(_txtUserName.text.length < 4)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Username should be at least 4 length long." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    successHandler();
}


#pragma mark Google Plus API
- (IBAction)onTouchBtnGooglePlus:(id)sender {
    [self showProgress:@"Please wait..."];
    [[GIDSignIn sharedInstance] signIn];
    
}

- (void)toggleAuthUI {
    [self hideProgress];
    if ([GIDSignIn sharedInstance].currentUser.authentication == nil) {
        NSLog(@"NOT SIGNED IN");
    } else {
        NSLog(@"SIGNED IN");
    }
}
// [END toggle_auth]
- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController
{
    [self presentViewController:viewController animated:YES completion:nil];
}
- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    [self hideProgress];
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *name = user.profile.name;
    NSString *email = user.profile.email;
    NSString *photoUrl = @"";
    if (user.profile.hasImage)
    {
        
        NSUInteger dimension = round(100 * [[UIScreen mainScreen] scale]);
        NSURL *imageURL = [user.profile imageURLWithDimension:dimension];
        photoUrl = imageURL.absoluteString;
    }
    
    NSLog(@"Customer details: %@ %@ %@ %@", userId, idToken, name, email);
    // ...
    NSDictionary* parameters=[[NSDictionary alloc] initWithObjectsAndKeys:
                              @"login_signup",@"type",
                              @"login",@"cmd",
                              @"google",@"usertype",
                              userId,@"google_id",
                              photoUrl,@"photourl",
                              user.profile.givenName,@"firstname",
                              user.profile.familyName,@"lastname",
                              @"",@"locationid",
                              email , @"email",
                              @"", @"birthday",
                              nil];
    [self fetchedGooglePlusData:parameters];
    
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]
     removeObserver:self
     name:@"ToggleAuthUINotification"
     object:nil];
    
}

- (void) receiveToggleAuthUINotification:(NSNotification *) notification {
    if ([[notification name] isEqualToString:@"ToggleAuthUINotification"]) {
        [self toggleAuthUI];
        //        self.statusText.text = [notification userInfo][@"statusText"];
    }
}


-(void)fetchedGooglePlusData:(NSDictionary *)json{
    NSString *strGender = [[json objectForKey:@"gender"] isEqualToString:@"male"]?@"1":@"0";
    NSString *email = @"";
    NSString *birthday = @"";
    if(![[json objectForKey:@"email"] isKindOfClass:[NSNull class]])
        email = [json objectForKey:@"email"];
    if([json objectForKey:@"birthday"] && ![[json objectForKey:@"birthday"] isKindOfClass:[NSNull class]])
        birthday = [json objectForKey:@"birthday"];
    
    NSMutableDictionary *dictLogin = [[NSMutableDictionary alloc]init];
    [dictLogin setObject:[json objectForKey: @"google_id"] forKey:@"login_id"];
    [dictLogin setObject:[json objectForKey: @"photourl"] forKey:@"photourl"];
    [dictLogin setObject:email forKey:@"email"];
    [dictLogin setObject:birthday forKey:@"birthday"];
    [dictLogin setObject:[json objectForKey: @"firstname"] forKey:@"firstname"];
    [dictLogin setObject:[json objectForKey: @"lastname"] forKey:@"lastname"];
    [dictLogin setObject:strGender forKey:@"gender"];
    [dictLogin setObject:kStrStatusInARelationship forKey:@"relationship"];
    [dictLogin setObject:@"" forKey:@"city"];
    [dictLogin setObject:@"" forKey:@"country"];
    [dictLogin setObject:@"google" forKey:@"logintype"];
    
    [self submitUserLoginWithSocialMedia:dictLogin SuccessHandler:^{

    } FailureHandler:^{
        
    }];
}
#pragma mark Facebook

- (IBAction)onTouchBtnFacebook:(id)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"email",@"public_profile", @"user_birthday"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"result %@", result);
             if ([FBSDKAccessToken currentAccessToken])
             {
                 NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
                 
                 [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name,gender, last_name, picture.type(large), email, birthday, location ,hometown "}]
                  startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                      if (!error)
                      {
                          
                          [self loginWithFacebook:result];
                      }
                      else
                      {
                          NSLog(@"Error %@",error);
                      }
                  }];
                 
             }
         }
     }];
}
- (void)loginWithFacebook:(id) result

{
    if(!result) return;
    NSString *email = @"";
    NSString *first_name = @"";
    NSString *last_name = @"";
    NSString *facebook_id = @"";
    NSString *link = @"";
    NSString *photo_url = @"";
    NSString *gender = @"1"; //1:male, 2:female
    if([result objectForKey:@"email"] && ![[GlobalData sharedData]isEmpty:[result objectForKey:@"email"]])
        email = [result objectForKey:@"email"];
    if([result objectForKey:@"first_name"] && ![[GlobalData sharedData]isEmpty:[result objectForKey:@"first_name"]])
        first_name = [result objectForKey:@"first_name"];
    if([result objectForKey:@"last_name"] && ![[GlobalData sharedData]isEmpty:[result objectForKey:@"last_name"]])
        last_name = [result objectForKey:@"last_name"];
    if([result objectForKey:@"link"] && ![[GlobalData sharedData]isEmpty:[result objectForKey:@"link"]])
        link = [result objectForKey:@"link"];
    if([result objectForKey:@"id"] && ![[GlobalData sharedData]isEmpty:[result objectForKey:@"id"]])
        facebook_id = [result objectForKey:@"id"];
    if([result objectForKey:@"gender"] && ![[GlobalData sharedData]isEmpty:[result objectForKey:@"gender"]])
        gender = [[result objectForKey:@"gender"] isEqualToString:@"female"]?@"2":@"1";
    
    if([result objectForKey:@"picture"])
    {
        if([[result objectForKey:@"picture"] objectForKey:@"data"] && [[[result objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"])
            photo_url = [[[result objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"];
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:facebook_id forKey:@"login_id"];
    [params setObject:email forKey:@"email"];
    [params setObject:@"facebook" forKey:@"logintype"];
    [params setObject:@"" forKey:@"birthday"];
    [params setObject:first_name forKey:@"firstname"];
    [params setObject:last_name forKey:@"lastname"];
    [params setObject:gender forKey:@"gender"];
    [params setObject:kStrStatusInARelationship forKey:@"relationship"];
    if([[GlobalData sharedData]currentCity])
        [params setObject:[[GlobalData sharedData]currentCity] forKey:@"city"];
    else
        [params setObject:@"" forKey:@"city"];
    
    if([[GlobalData sharedData]currentState])
        [params setObject:[[GlobalData sharedData]currentState] forKey:@"state"];
    else
        [params setObject:@"" forKey:@"state"];
    if([[GlobalData sharedData]currentCountry])
        [params setObject:[[GlobalData sharedData]currentCountry] forKey:@"country"];
    else
        [params setObject:@"" forKey:@"country"];
    CGFloat latitude =  [[GlobalData sharedData]currentLatitude];
    CGFloat longitude = [[GlobalData sharedData]currentLongitude];
    [params setObject:[NSString stringWithFormat:@"%f", latitude] forKey:@"latitude"];
    [params setObject:[NSString stringWithFormat:@"%f", longitude] forKey:@"longitude"];
    
    if(photo_url && [photo_url length] > 0)
        [params setObject:photo_url forKey:@"photourl"];
    else
        [params setObject:@"" forKey:@"photourl"];
    
    [self showProgress:@"Please wait..."];
    [self submitUserLoginWithSocialMedia:params SuccessHandler:^{
        
    } FailureHandler:^{
        [self hideProgress];
    }];
}




#pragma mark login/register with Social Media
- (void)submitUserLoginWithSocialMedia:(NSMutableDictionary *)dictUserInformation
                        SuccessHandler:(void (^)())successHandler
                        FailureHandler:(void (^)())failureHandler
{
    NSString *email = [dictUserInformation objectForKey:@"email"];
    NSString *username = @"";
    if(email && email.length > 0 && [[GlobalData sharedData]validateEmailWithString:email])
    {
        NSArray *pArrTemp = [email componentsSeparatedByString:@"@"];
        username = [pArrTemp objectAtIndex:0];
    }
    else
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Your email address can't be retrieved." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    [dictUserInformation setObject:username forKey:@"username"];
    NSDate *date = [NSDate date];
    NSString *strfilename = [NSString stringWithFormat:@"%@%ld.jpg", [dictUserInformation objectForKey:@"firstname"] , (long)[date timeIntervalSince1970]];
    [dictUserInformation setObject:[[GlobalData sharedData]currentCity] forKey:@"city"];
    [dictUserInformation setObject:[[GlobalData sharedData]currentState] forKey:@"state"];
    [dictUserInformation setObject:strfilename forKey:@"photo_filename"];
    [dictUserInformation setObject:username forKey:@"username"];
    [dictUserInformation setObject:[NSString stringWithFormat:@"%f", [[GlobalData sharedData]currentLatitude]] forKey:@"latitude"];
    [dictUserInformation setObject:[NSString stringWithFormat:@"%f", [[GlobalData sharedData]currentLongitude]] forKey:@"longitude"];
    [[UserService sharedData]loginRequestBySocialMediaWithParams:dictUserInformation success:^(id _responseObject, NSInteger result_code) {
        
        [MagicalRecord setupCoreDataStackWithStoreNamed:[NSString stringWithFormat:@"Tabble_%@", [_responseObject objectForKey:@"id"]]];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [localRegistry setObject:username forKey:kMyUserName]; //[username lowercaseString]
        [localRegistry setObject:email forKey:kMyEmail];
        [localRegistry setObject:[dictUserInformation objectForKey:@"logintype"] forKey:kCurLoginType];
        [localRegistry setObject:[dictUserInformation objectForKey:@"login_id"] forKey:kMyUserPW];
        [localRegistry setObject:[_responseObject objectForKey:@"role"] forKey:kMyUserRole];
        //        [[GlobalData sharedData]setMUserInfo:_responseObject];
        [[[GlobalData sharedData]mUserInfo]setDataWithDictionary:_responseObject];
        NSData *personEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:[[GlobalData sharedData]mUserInfo]];
        [localRegistry setObject:personEncodedObject forKey:kMyUserDict];
        
        if(result_code == 2)//signup
        {
            if([[dictUserInformation objectForKey:@"photourl"] length] > 0)
            {
                [self uploadProfilePhotoWithUrl:[dictUserInformation objectForKey:@"photourl"] name:strfilename oldname:@""];
            }
        }
        [self dismissViewControllerAnimated:YES completion:nil];
        successHandler();
        return ;
    } failure:^(NSInteger _errorCode) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }];
}
- (void)uploadProfilePhotoWithUrl :(NSString *)url name:(NSString *)name oldname:(NSString *)oldname
{
    NSString *strfilename = name;
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
    UIImage *image = [UIImage imageWithData:imageData];
  
    [[GlobalData sharedData] uploadPhotoWithName:name oldname:oldname image:image type:@"" completion:nil FailureHandler:nil];
    
}
- (void)uploadProfilePhotoWithName:(NSString *)name
{
    NSString *strfilename = name;
    NSString *oldfilename = @"";
    if(mAccountAccessType == ACCOUNT_TYPE_EDIT_USER_INFO)
    {
        oldfilename = [[GlobalData sharedData]mUserInfo].mPhotoFileName;
    }
    [[GlobalData sharedData] uploadPhotoWithName:strfilename oldname:oldfilename image:self.mImgUserPhoto type:@"" completion:^{
        [localRegistry removeObjectForKey:kUserDefaultsCurPhoto];
    } FailureHandler:^{
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:kStrMessage_InternetConnect_Photo BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
    }];
    
}
#pragma mark Address Select Delegate
- (void)selectAddressViewController:(SearchAddressViewController *)viewController didSelectedLocation:(NSString *)address coordinate:(CLLocationCoordinate2D)coordinate
{
//    locationCoordinate = coordinate;
    [_txtCity setText:address];
}
@end
