//
//  TermsAndPolicyViewController.h
//  BusinessTabble
//
//  Created by Tian Ming on 09/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsAndPolicyViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webViewContent;
@property (nonatomic) NSInteger mTermsViewFromType;
+(TermsAndPolicyViewController*)sharedController;
@end
