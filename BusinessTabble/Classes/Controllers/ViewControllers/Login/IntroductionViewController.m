//
//  IntroductionViewController.m
//  Tabble Business
//
//  Created by macbook on 14/11/2017.
//  Copyright © 2017 Liu. All rights reserved.
//

#import "IntroductionViewController.h"
#import "SignupViewController.h"

@implementation IntroductionViewController
@synthesize currentPageNum = _currentPageNum;
- (void)viewDidLoad {
    [super viewDidLoad];
    [_btnRegister.layer setCornerRadius:7.f];
    _pageControl.pageIndicatorTintColor = [UIColor grayColor];
    _pageControl.currentPageIndicatorTintColor = MAIN_COLOR;
    self.currentPageNum = 0;
    float cornerRadius = 10;
    _vwContent1Header.layer.cornerRadius = cornerRadius;
    _vwContent2Header.layer.cornerRadius = cornerRadius;
    _vwContent3Header.layer.cornerRadius = cornerRadius;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onTouchBtnRegister:(id)sender {
   
    [self dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_SHOW_SIGNUP_VIEW object:nil];
    }];
}
- (IBAction)onTouchBtnSkip:(id)sender {
    [self onTouchBtnRegister:sender];
}
- (IBAction)onTouchBtnBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.frame.size.width;
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.currentPageNum = page;
}
#pragma mark Property Getter & Setter
- (void)setCurrentPageNum:(NSInteger)currentPageNum
{
    _currentPageNum = currentPageNum;
    _pageControl.currentPage = currentPageNum;
}
@end
