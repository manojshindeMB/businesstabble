//
//  IntroductionViewController.h
//  Tabble Business
//
//  Created by macbook on 14/11/2017.
//  Copyright © 2017 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroductionViewController : UIViewController <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (nonatomic) NSInteger currentPageNum;
@property (weak, nonatomic) IBOutlet UIView *vwContent1Header;
@property (weak, nonatomic) IBOutlet UIView *vwContent2Header;
@property (weak, nonatomic) IBOutlet UIView *vwContent3Header;

@end
