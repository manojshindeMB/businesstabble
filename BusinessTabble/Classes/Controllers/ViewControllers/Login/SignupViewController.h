//
//  SignupViewController.h
//  BusinessTabble
//
//  Created by Tian Ming on 08/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PECropViewController.h"

#import <GoogleOpenSource/GoogleOpenSource.h>
#import "SearchAddressViewController.h"

@interface SignupViewController : UIViewController<UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,PECropViewControllerDelegate, UITextFieldDelegate, GIDSignInUIDelegate, GIDSignInDelegate, SelectAddressViewControllerDelegate>

@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtUserName;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtPassword;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtEmail;

@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtCity;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *vwSignUp;


@property (weak, nonatomic) IBOutlet UIButton *btnLogo;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserNameTaken;
@property (weak, nonatomic) IBOutlet UILabel *lblUsernameTaken;
@property (weak, nonatomic) IBOutlet UIButton *btnCreate;
@property (weak, nonatomic) IBOutlet UIView *vwSeparator;
@property (weak, nonatomic) IBOutlet UIView *vwSocialButtonContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_coinstraint_social_buttons_height;

@property (strong, nonatomic) UIActionSheet * photoActionSheet;
@property (strong, nonatomic) UIImage *mImgUserPhoto;
@property (nonatomic) BOOL mUserNameValid;
@property (nonatomic) NSInteger mAccountAccessType;
@property (nonatomic) BOOL userPhotoChanged;

@end
