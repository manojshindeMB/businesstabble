//
//  PreLoadingViewController.h
//  BusinessTabble
//
//  Created by Tian Ming on 20/01/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ADTransitionController.h"
@interface PreLoadingViewController : UIViewController
{
    ADTransitionOrientation _orientation;
}
@end
