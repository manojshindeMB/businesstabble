//
//  PreLoadingViewController.m
//  BusinessTabble
//
//  Created by LiuMin on 20/01/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import "PreLoadingViewController.h"
#import "LoadingViewController.h"

@interface PreLoadingViewController ()

@end

@implementation PreLoadingViewController

- (void)viewDidLoad {
    [super viewDidLoad];


   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    LoadingViewController *loadingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoadingViewController"];
    [UIView  beginAnimations: @"Showinfo"context: nil];
    [UIView setAnimationCurve: UIViewAnimationCurveEaseOut];
    [UIView setAnimationDuration:0.0];
    [self.navigationController pushViewController: loadingVC animated:NO];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlUp forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
    
//    ADTransition * animation = [[ADFadeTransition alloc] initWithDuration:0.5];
//    [self _pushViewControllerWithTransition:animation];

    
//    CATransition *transition = [CATransition animation];
//    transition.duration = 6.5f;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionFade;
//    [self.navigationController.view.layer addAnimation:transition forKey:nil];
//    [self.navigationController pushViewController:loadingVC animated:NO];
//    
//    [UIView animateWithDuration:0.7 animations:^{
//        //[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
//        //[Engine setIsBackAction:FALSE];
//        
//       
//    } completion:^(BOOL finished) {
//        [self.navigationController.view.layer removeAnimationForKey:@"start"];
//    }];
}
- (void)_pushViewControllerWithTransition:(ADTransition *)transition {
   LoadingViewController *loadingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LoadingViewController"];
    //    [self.navigationController pushViewController:loadingVC animated:YES];
    [self.transitionController pushViewController:loadingVC withTransition:transition];
}

@end
