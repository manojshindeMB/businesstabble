//
//  TermsAndPolicyViewController.m
//  BusinessTabble
//
//  Created by Tian Ming on 09/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "TermsAndPolicyViewController.h"

@interface TermsAndPolicyViewController ()

@end

@implementation TermsAndPolicyViewController
@synthesize webViewContent;
@synthesize mTermsViewFromType;
+(TermsAndPolicyViewController*)sharedController
{
    __strong static TermsAndPolicyViewController* sharedController = nil ;
    static dispatch_once_t onceToken ;
    
    dispatch_once( &onceToken, ^{
        sharedController = [ [ TermsAndPolicyViewController alloc ] initWithNibName : @"TermsAndPolicyViewController" bundle : nil ] ;
    } ) ;
    return sharedController ;
    
 
}


- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *ppt = [[NSBundle mainBundle] pathForResource:@"terms.doc" ofType:nil];
    NSURL *pptURL = [NSURL fileURLWithPath:ppt];
    NSURLRequest *request = [NSURLRequest requestWithURL:pptURL];
    [webViewContent loadRequest:request];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (IBAction)onTouchBtnBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{

    }];
}

@end
