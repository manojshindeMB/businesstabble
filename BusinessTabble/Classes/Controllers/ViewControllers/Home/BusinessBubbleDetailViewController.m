//
//  BusinessBubbleDetailViewController.m
//  BusinessTabble
//
//  Created by Haichen Song on 17/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "BusinessBubbleDetailViewController.h"
#import <MagicalRecord/MagicalRecord.h>

@interface BusinessBubbleDetailViewController ()

@end

@implementation BusinessBubbleDetailViewController
@synthesize mCurrentBubble;
@synthesize lblBubbleDescription,lblBubbleLocation,lblBubbleName,imgBubble,layout_constraint_lbldescription_height;
- (void)viewDidLoad {
    [super viewDidLoad];
    if(![[GlobalData sharedData]isEmpty:mCurrentBubble.content])
    {
        [lblBubbleDescription setText:mCurrentBubble.content];
    }
    NSString *photo_filename = [[GlobalData sharedData]urlencode:mCurrentBubble.photo_filename];
    [[GlobalData sharedData]showImageWithImage:imgBubble name:photo_filename placeholder:nil];
    [self.view layoutIfNeeded];
    [imgBubble.layer setCornerRadius:imgBubble.frame.size.width / 2];
    [imgBubble.layer setMasksToBounds:YES];
    [imgBubble.layer setBorderWidth:1.f];
    [imgBubble.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [imgBubble setContentMode:UIViewContentModeScaleAspectFill];
    [imgBubble.layer setMasksToBounds:YES];
    lblBubbleName.text = mCurrentBubble.bubblename;
    lblBubbleLocation.text = mCurrentBubble.location;
}
- (void)viewDidLayoutSubviews
{
    if(![[GlobalData sharedData]isEmpty:mCurrentBubble.content])
    {
        CGSize cz = [[GlobalData sharedData]messageSize:lblBubbleDescription.text font:lblBubbleDescription.font width:lblBubbleDescription.frame.size.width];
        layout_constraint_lbldescription_height.constant = cz.height;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Actions
- (IBAction)onTouchBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onTouchBtnJoinBubble:(id)sender {
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:mCurrentBubble.bubble_id forKey:@"bubble_id"];
    [self showProgress:@"Please wait..."];
    [[BubbleService sharedData]joinBubbleWithDictionary:params success:^(id _responseObject) {
        [self hideProgress];
        mCurrentBubble.joined = [NSNumber numberWithInt:1];
        mCurrentBubble.membercount = [NSNumber numberWithInteger:[mCurrentBubble.membercount integerValue] + 1];
        [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreAndWait];
        
        [localRegistry removeObjectForKey:kLastFetchTimeStampForBubbleMemberList];
        [localRegistry removeObjectForKey:kLastFetchTimeStampForBubbleMenuList];
        [localRegistry removeObjectForKey:kLastFetchTimeStampForBubbleMessageList];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MEMBER_LIST object:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MENU_LIST object:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MESSAGE_LIST object:nil];
        
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Successfully joined." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
       
        
//        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
//            [localRegistry setObject:TimeStamp forKey:kLastFetchTimeStampForBusinessBubbleList];
//            [Bubble MR_s];
//        }];
//        mCurrentBubble.joined = @"1";
        
    } failure:^(NSInteger _errorCode) {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:^{
        }];
        [self hideProgress];
    }];

}


@end
