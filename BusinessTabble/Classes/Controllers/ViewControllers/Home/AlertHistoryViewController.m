//
//  AlertHistoryViewController.m
//  BusinessTabble
//
//  Created by macbook on 03/06/2017.
//  Copyright © 2017 Liu. All rights reserved.
//

#import "AlertHistoryViewController.h"

@interface AlertHistoryViewController ()

@end

@implementation AlertHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapTableViewGesture:)];
    tapGesture.delegate = self;
    [self.tableView addGestureRecognizer:tapGesture];
    [self.tableView setUserInteractionEnabled:YES];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self reloadDataWithCategoryName:@"" SearchString:@""];
    [[GlobalData sharedData]setMUnreadPushNotifications:0];
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_TAB_BAR_HIDE object:nil];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_TAB_BAR_SHOW object:nil];
    [super viewWillDisappear:animated];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onTouchBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onTouchBtnClearHistory:(id)sender {
    NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
    [Alert MR_deleteAllMatchingPredicate:nil inContext:context];
    
    
}

- (void)reloadDataWithCategoryName:(NSString *)strCategoryName
                      SearchString:(NSString *)strSearchString
{
    NSPredicate *predicate = nil;
//    predicate = [NSPredicate predicateWithFormat:@"user_id=%@ AND status=1", [[GlobalData sharedData]mUserInfo].mId];
    
    NSFetchRequest *fetchRequest = [Alert MR_requestAllSortedBy:@"arrived" ascending:NO withPredicate:predicate];
    [fetchRequest setFetchLimit:10];
    NSFetchedResultsController *theFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[NSManagedObjectContext MR_defaultContext] sectionNameKeyPath:nil cacheName:nil];
    self.fetchedResultsController = theFetchedResultsController;
}
- (void)onTapTableViewGesture:(UIGestureRecognizer *)gestureRecognizer
{
    [self.view endEditing:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger totalNum = [[self.fetchedResultsController fetchedObjects] count];
    if(totalNum > 25)
        return 25;
    return totalNum;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"WhosHereTableViewCell";
    WhosHereTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell = [self configureBasicCell:cell indexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (WhosHereTableViewCell *)configureBasicCell:(WhosHereTableViewCell *) cell indexPath:(NSIndexPath *)indexPath
{
    Alert *currentAlert = self.fetchedResultsController.fetchedObjects[indexPath.row];
    cell.lblMemberName.text = currentAlert.message;
    NSDate *approveFrom = [NSDate dateWithTimeIntervalSince1970:[currentAlert.arrived doubleValue]];
    [cell.lblTime setText:[NSString stringWithFormat:@"%@", [[GlobalData sharedData]dateTimeStringFromDate:approveFrom]]];
    
    if(currentAlert.message_id && ![[GlobalData sharedData]isEmpty:currentAlert.arrived])
    {
        Bubble *bubble = [Bubble getBubbleById:[currentAlert.message_id integerValue]];
          NSString *photo_filename = [[GlobalData sharedData]urlencode:bubble.photo_filename];
        [[GlobalData sharedData]showImageWithImage:cell.imgMember name:photo_filename placeholder:nil];
        [cell.imgMember.layer setBorderColor:[UIColor colorWithWhite:0.1 alpha:0.3].CGColor];
        [cell.imgMember.layer setBorderWidth:1.f];

    }
    
    // Right Utility Buttons
    cell.delegate = self;
//    cell.rightUtilityButtons = [self rightButtons:currentAlert];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{
    
}
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    return YES;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{

}
- (NSArray *)rightButtons :(Alert *)currentAlert
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    NSString *string = @"Hide User";
    string = @"Delete";
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [string length])];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0] range:NSMakeRange(0, [string length])];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [string length])];
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor darkGrayColor] attributedTitle:attributedString];
    return rightUtilityButtons;
}
@end
