//
//  HomeViewController.h
//  BusinessTabble
//
//  Created by Tian Ming on 09/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGLDropDownItem.h"
#import "IGLDropDownMenu.h"
#import "CoreDataViewController.h"
#import "SWTableViewCell.h"
#import "MyBubbleNewTableViewCell.h"
@interface HomeViewController : CoreDataViewController<SWTableViewCellDelegate, SWRevealViewControllerDelegate, MyBubbleDelegate>
{
    SWRevealViewController *revealViewController;
}
@property (nonatomic) BOOL sidebarMenuOpen;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITableView *tvMyBubbleList;
@property (strong, nonatomic) NSMutableArray *mArrBubbleList;
@property (weak, nonatomic) IBOutlet UIView *vwEmptyMask;

@property (weak, nonatomic) IBOutlet UILabel *lblEmptyTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblEmptyDescription;
@property (weak, nonatomic) IBOutlet UIImageView *imgAlertIcon;
@property (nonatomic) BOOL hasUnreadPushNotifications;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end
