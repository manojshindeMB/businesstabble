//
//  HomeViewController.m
//  BusinessTabble
//
//  Created by Tian Ming on 09/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "HomeViewController.h"
#import "MyBubbleTableViewCell.h"
#import <MagicalRecord/MagicalRecord.h>
#import "PostViewController.h"
#import "MenuDetailViewController.h"
#import "MyBubbleNewTableViewCell.h"
#import "HomeTabBarController.h"
#import "AlertHistoryViewController.h"
#import "LoginNavigationController.h"
#import "BubbleMenuFullViewController.h"
#import "BubbleMapViewController.h"
#import "InventoryViewController.h"
#import "PhotoFullViewController.h"
@interface HomeViewController ()

@end

@implementation HomeViewController
@synthesize tvMyBubbleList;
@synthesize mArrBubbleList;
@synthesize sidebarMenuOpen;
@synthesize hasUnreadPushNotifications = _hasUnreadPushNotifications;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:_lblTitle.text];
    float spacing = 1.0f;

    [attributedString addAttribute:NSKernAttributeName
                             value:@(spacing)
                             range:NSMakeRange(0, [_lblTitle.text length])];
    _lblTitle.attributedText = attributedString;
    revealViewController = [self revealViewController];
    [revealViewController panGestureRecognizer];
    [revealViewController tapGestureRecognizer];
    mArrBubbleList = [[NSMutableArray alloc]init];
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
   
    
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5.f];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:paragraphStyle forKey:NSParagraphStyleAttributeName];
    _lblEmptyDescription.attributedText = [[NSMutableAttributedString alloc] initWithString:_lblEmptyDescription.text attributes:attrsDictionary];
    
    paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5.f];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    attrsDictionary = [NSDictionary dictionaryWithObject:paragraphStyle forKey:NSParagraphStyleAttributeName];
    _lblEmptyTitle.attributedText = [[NSMutableAttributedString alloc] initWithString:_lblEmptyTitle.text attributes:attrsDictionary];

    [_vwEmptyMask setHidden:NO];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 160.0f;
    
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkUnreadPushNotification:) name:UIApplicationWillEnterForegroundNotification object:nil];
    self.revealViewController.delegate = self;
    [self reloadDataWithCategoryName:@"" SearchString:@""];
    
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.tableView;
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(handleTableViewRefresh:) forControlEvents:UIControlEventValueChanged];
    tableViewController.refreshControl = self.refreshControl;
    
}
- (void)handleTableViewRefresh:(id)sender
{
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_DATA object:nil];
    [[MainService sharedData] getBussinessBubbleListWithSuccessHandler:^{
        [self.refreshControl endRefreshing];
    } FailureHandler:^{
        [self.refreshControl endRefreshing];
    }];
 
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView layoutIfNeeded];
    [self.tableView reloadData];
    [self checkUnreadPushNotification:nil];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}
- (void)checkUnreadPushNotification:(id)sender
{
    if([[GlobalData sharedData]mUnreadPushNotifications] > 0)
        self.hasUnreadPushNotifications = YES;
    else
        self.hasUnreadPushNotifications = NO;
}
- (void)viewWillDisappear:(BOOL)animated
{
    
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (void)reloadDataWithCategoryName:(NSString *)strCategoryName
                      SearchString:(NSString *)strSearchString
{
    if(![localRegistry objectForKey:kMyUserDict])
        return;
    NSDate *oneWeekAgo = [[GlobalData sharedData]getAddDatedFromDate:[NSDate date] dayNum:- 70];
    NSString *strOneWeekAgo = [[GlobalData sharedData]stringFromLocaleDate:oneWeekAgo DateFormat:@""];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"reviewstatus = 1 AND field_deleted == 0 AND joined = 1 AND (NOT(category CONTAINS %@)) AND (NOT(category CONTAINS %@)) AND (updatedTime > %@)", @"Activity", @"Event", oneWeekAgo];
    self.fetchedResultsController = [Bubble MR_fetchAllSortedBy:@"lastmsgtime" ascending:NO withPredicate:predicate groupBy:nil delegate:self];
    [self refreshScreen];
}
- (void)refreshScreen
{
    if([[self.fetchedResultsController fetchedObjects] count] > 0)
        [_vwEmptyMask setHidden:YES];
    else
        [_vwEmptyMask setHidden:NO];
}
- (float)getDistanceFromPlace :(Bubble *)currentBubble
{
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:[currentBubble.latitude floatValue] longitude:[currentBubble.longitude floatValue]];
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:[[GlobalData sharedData]currentLatitude] longitude:[[GlobalData sharedData]currentLongitude]];
    CLLocationDistance distance = [locA distanceFromLocation:locB];
    return distance;
}
#pragma mark Actions
- (IBAction)onTouchBtnLeftMenu:(id)sender {
    [revealViewController revealToggle:sender];
//
}
- (IBAction)onTouchBtnAlert:(id)sender {
    
    AlertHistoryViewController *alertHistory = [self.storyboard instantiateViewControllerWithIdentifier:@"AlertHistoryViewController"];
    [self.navigationController pushViewController:alertHistory animated:YES];
}
#pragma mark - Table view data source
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"MyBubbleNewTableViewCell";
    MyBubbleNewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    Bubble *currentBubble = self.fetchedResultsController.fetchedObjects[indexPath.row];
    cell.mCurrentBubble = currentBubble;
    [cell.lblBubbleName setText:currentBubble.bubblename];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell.lblLastPost setText:@""];
    [cell.lblLastPost setText:currentBubble.text];
    [cell.lblPostDate setHidden:YES];
    [cell.imgLastMessagePhoto setHidden:YES];
    if(indexPath.row == 2)
    {
//        NSLog(@"bubble at 2 %@", currentBubble);
    }
    if(currentBubble.lastmsg_id && [currentBubble.lastmsg_id integerValue] > 0)
    {
        [cell.lblPostDate setHidden:NO];
        if([currentBubble.lastmsgtime integerValue] > 0)
        {
            NSDate *lastMsgDate = [NSDate dateWithTimeIntervalSince1970:[currentBubble.lastmsgtime doubleValue]];
            [cell.lblPostDate setText:[NSString stringWithFormat:@"%@", [[GlobalData sharedData]dateTimeStringFromDate:lastMsgDate]]];
        }
        else
            [cell.lblPostDate setText:@""];
    }
    
    
    NSString *photo_filename = [[GlobalData sharedData]urlencode:currentBubble.photo_filename];
    [[GlobalData sharedData]showImageWithImage:cell.imgBubble name:photo_filename placeholder:nil];
    cell.layout_constraint_last_msg_img_bottom_space.constant = 0;
    
    [cell.imgBuySellItem setHidden:YES];
    [cell.lblSellItemPrice setHidden:YES];
    [cell.lblSellItemDeliverPrice setHidden:YES];
    if(![[GlobalData sharedData]isEmpty: currentBubble.last_message_photo_filename])
    {
        [cell.imgLastMessagePhoto setHidden:NO];
        NSString *photo_filename = [[GlobalData sharedData]urlencode:currentBubble.last_message_photo_filename ];
        [[GlobalData sharedData]showImageWithImage:cell.imgLastMessagePhoto name:photo_filename placeholder:nil];
        [cell.imgLastMessagePhoto.layer setCornerRadius:20.f];
        [cell.imgLastMessagePhoto.layer setMasksToBounds:YES];
        cell.layout_constraint_attachphoto_height.constant = 200.f;
        Message *msg = [Message getMessageByMessageId:[currentBubble.lastmsg_id integerValue]];
        if(msg)
        {
            if([msg.coupon integerValue] == 2) //means sell item
            {
                [cell.imgBuySellItem setHidden:NO];
                [cell.lblSellItemPrice setHidden:NO];
                [cell.lblSellItemDeliverPrice setHidden:NO];
                cell.layout_constraint_last_msg_img_bottom_space.constant = 15;
                cell.lblSellItemPrice.text = [NSString stringWithFormat:@"$%@", msg.price];
                cell.lblSellItemDeliverPrice.text = @"FREE Shipping";
                if([msg.shipping_price floatValue] > 0)
                    cell.lblSellItemDeliverPrice.text = [NSString stringWithFormat:@"Shipping Price : $%@", msg.shipping_price];
            }
        }
    }
    else
        cell.layout_constraint_attachphoto_height.constant = 0;
    
//    [cell.vwBubbleBanner setBackgroundColor:[UIColor colorWithRed:[currentBubble.color_red floatValue] green:[currentBubble.color_green floatValue] blue:[currentBubble.color_blue floatValue] alpha:1]];
//    [cell.vwBubbleBanner setBackgroundColor:MAIN_COLOR];
    cell.rightUtilityButtons = [self rightButtons];
    cell.delegate = self;
    cell.myBubbleDelegate = self;
    
    
    if([currentBubble.category isEqualToString:@"Retail"])
    {
        [cell.btnInfoPhotoOrChat setTitle:@"Photos" forState:UIControlStateNormal];
        [cell.btnInfoPhotoOrChat setImage:[UIImage imageNamed:@"btn_info_photos"] forState:UIControlStateNormal];
        [cell.btnInfoPhotoOrChat setImageEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [cell.btnInfoPhotoOrChat setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
        
        [cell.btnInfoMenu setTitle:@"Inventory" forState:UIControlStateNormal];
        [cell.btnInfoMenu setImage:[UIImage imageNamed:@"btn_info_inventory"] forState:UIControlStateNormal];
        [cell.btnInfoMenu setImageEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        [cell.btnInfoMenu setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
        [cell.btnInfoMenu setHidden:YES];
    }
    else{
        [cell.btnInfoPhotoOrChat setTitle:@"Chat" forState:UIControlStateNormal];
        [cell.btnInfoPhotoOrChat setImageEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];
        [cell.btnInfoPhotoOrChat setTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
        
        Chat *lastChat = [Chat getLastChatByBubbleId:[currentBubble.bubble_id integerValue]];
        [cell.btnInfoPhotoOrChat setImage:[UIImage imageNamed:@"btn_info_chat"] forState:UIControlStateNormal]; //btn_info_chat //btn_chat_yellow
        if(lastChat)
        {
            if( ([[NSDate date] timeIntervalSince1970] - [lastChat.createdAt integerValue])  <= 30)
            {
                NSLog(@"time diff %f", ([[NSDate date] timeIntervalSince1970] - [lastChat.createdAt integerValue]) );
//                [cell.btnInfoPhotoOrChat setImage:[UIImage imageNamed:@"btn_chat_yellow"] forState:UIControlStateNormal];
                
                
                UIImage *imgOne = [UIImage imageNamed:@"btn_chat_yellow"];
                UIImage *imgTwo = [UIImage imageNamed:@"btn_info_chat"];
                NSArray *imagesArray = @[imgOne, imgTwo];
                
                [cell.btnInfoPhotoOrChat.imageView setAnimationImages:imagesArray];

                cell.btnInfoPhotoOrChat.imageView.animationDuration = 1.0f;
                [cell.btnInfoPhotoOrChat.imageView startAnimating];
//                [cell.btnInfoPhotoOrChat.imageView stopAnimating];
                [cell.btnInfoPhotoOrChat.imageView.layer removeAllAnimations];
                
                [UIView transitionWithView:cell.imageView
                                  duration:1.0f
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:^{[cell.btnInfoPhotoOrChat.imageView setImage:[UIImage imageNamed:@"btn_chat_yellow"]];}
                                completion:NULL];
            }
        }
        
        
        [cell.btnInfoMenu setTitle:@"Menu" forState:UIControlStateNormal];
        [cell.btnInfoMenu setImage:[UIImage imageNamed:@"btn_info_menu"] forState:UIControlStateNormal];
        [cell.btnInfoMenu setImageEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 0)];
        [cell.btnInfoMenu setTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
    }
    return cell;
}
//
//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    MyBubbleNewTableViewCell *curCell = (MyBubbleNewTableViewCell *) cell;
//    Bubble *currentBubble = self.fetchedResultsController.fetchedObjects[indexPath.row];
//    if([currentBubble.category isEqualToString:@"Retail"])
//        return;
//    Chat *lastChat = [Chat getLastChatByBubbleId:[currentBubble.bubble_id integerValue]];
//    [curCell.btnInfoPhotoOrChat setImage:[UIImage imageNamed:@"btn_info_chat"] forState:UIControlStateNormal]; //btn_info_chat //btn_chat_yellow
//
//    if(lastChat)
//    {
//        if( ([[NSDate date] timeIntervalSince1970] - [lastChat.createdAt integerValue])  <= 3000)
//        {
//            NSLog(@"time diff %f", ([[NSDate date] timeIntervalSince1970] - [lastChat.createdAt integerValue]) );
//            [curCell.btnInfoPhotoOrChat setImage:[UIImage imageNamed:@"btn_chat_yellow"] forState:UIControlStateNormal];
//
//
//            UIImage *imgOne = [UIImage imageNamed:@"btn_chat_yellow"];
//            UIImage *imgTwo = [UIImage imageNamed:@"btn_info_chat"];
//            NSArray *imagesArray = @[imgOne, imgTwo];
//
//            [curCell.btnInfoPhotoOrChat.imageView setAnimationImages:imagesArray];
//
//            curCell.btnInfoPhotoOrChat.imageView.animationDuration = 1.0f;
//            [curCell.btnInfoPhotoOrChat.imageView startAnimating];
//            //                [cell.btnInfoPhotoOrChat.imageView stopAnimating];
//            [curCell.btnInfoPhotoOrChat.imageView.layer removeAllAnimations];
//        }
//    }
//
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Detail" bundle:nil];
    Bubble *currentBubble = self.fetchedResultsController.fetchedObjects[indexPath.row];
    MenuDetailViewController *postVC = [storyboard instantiateViewControllerWithIdentifier:@"MenuDetailViewController"];
    postVC.mCurrentBubble = currentBubble;    
    [self.navigationController pushViewController:postVC animated:YES];
}
- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f] icon:[UIImage imageNamed:@"icon_disconnect_text"]];
//    [rightUtilityButtons sw_addUtilityButtonWithColor:
//     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
//                                                title:@"Disconnect"];
    return rightUtilityButtons;
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    MyBubbleTableViewCell *currentCell = (MyBubbleTableViewCell *)cell;
    Bubble *currentBubble = currentCell.mCurrentBubble;
    [cell hideUtilityButtonsAnimated:YES];
    if([currentBubble.creator_id isEqualToNumber:[NSNumber numberWithInteger:[[[GlobalData sharedData]mUserInfo].mId integerValue]]])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:[NSString stringWithFormat:@"This bubble was created by you. \n You can't delete the bubble this way."] BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        return;
    }
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]init];
    [activityIndicator setCenter:currentCell.vwCellContent.center];
    [currentCell.vwCellContent addSubview:activityIndicator];
    [activityIndicator startAnimating];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:currentBubble.bubble_id forKey:@"bubble_id"];
    [[BubbleService sharedData]unJoinBubbleWithDictionary:params success:^(id _responseObject) {
      
        currentBubble.joined = [NSNumber numberWithInt:0];
        currentBubble.membercount = [NSNumber numberWithInteger:[currentBubble.membercount integerValue] - 1];

        [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
//            [self reloadDataWithCategoryName:@"" SearchString:@""];
//             [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_DATA object:nil];
             [self refreshScreen];
        }];
//        [localRegistry removeObjectForKey:kLastFetchTimeStampForBubbleMemberList];
//        [localRegistry removeObjectForKey:kLastFetchTimeStampForBubbleMenuList];
//        [localRegistry removeObjectForKey:kLastFetchTimeStampForBubbleMessageList];
//
//
//        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MEMBER_LIST object:nil];
//        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MENU_LIST object:nil];
//        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MESSAGE_LIST object:nil];
        
        
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Successfully deleted." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
            [activityIndicator stopAnimating];
            [activityIndicator removeFromSuperview];
        }];
        
    } failure:^(NSInteger _errorCode) {
        [activityIndicator stopAnimating];
        [activityIndicator removeFromSuperview];
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:^{
        }];
    }];

}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{
    
}
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    return YES;
}
#pragma mark Push Notification handler

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionRight) {
        self.view.userInteractionEnabled = NO;
        sidebarMenuOpen = NO;
    } else {
        self.view.userInteractionEnabled = YES;
        sidebarMenuOpen = YES;
    }
}

- (void)revealController:(SWRevealViewController *)revealController didMoveToPosition:(FrontViewPosition)position
{
    if(position == FrontViewPositionRight) {
        //self.view.userInteractionEnabled = YES;
        sidebarMenuOpen = NO;
    } else {
        //self.view.userInteractionEnabled = NO;
        sidebarMenuOpen = YES;
    }
}

- (void)setHasUnreadPushNotifications:(BOOL)hasUnreadPushNotifications
{
    _hasUnreadPushNotifications = hasUnreadPushNotifications;
    if(hasUnreadPushNotifications)
        [_imgAlertIcon setImage:[UIImage imageNamed:@"alert_icon_unread"]];
    else
        [_imgAlertIcon setImage:[UIImage imageNamed:@"alert_icon"]];
}
#pragma mark MyBubble Delegate
- (void)onTapBubbleMap:(Bubble *)currentBubble
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Detail" bundle:nil];
    BubbleMapViewController *mapVC = [storyboard instantiateViewControllerWithIdentifier:@"BubbleMapViewController"];
    mapVC.mCurrentBubble = currentBubble;
    [self presentViewController:mapVC animated:YES completion:nil];
}
- (void)onTapBubbleCall:(Bubble *)currentBubble
{
    NSString *phoneNumber = currentBubble.contactnumber;
    if([[GlobalData sharedData]isEmpty:currentBubble.contactnumber])
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"The phone number is not set yet." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneNumber]]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneNumber]]];
    }
    else {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"This Device Doesn't Support Call Functionality" BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
    }
}
- (void)onTapBubblePhotosOrChat:(Bubble *)currentBubble
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Detail" bundle:nil];
    if([currentBubble.category isEqualToString:@"Retail"]){
        BubbleMenuFullViewController *fullMenuVC = [storyboard instantiateViewControllerWithIdentifier:@"BubbleMenuFullViewController"];
        fullMenuVC.mCurrentBubble = currentBubble;
        [self presentViewController:fullMenuVC animated:YES completion:nil];
    }
    else{
        ChatViewController *chatVC = [storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
        chatVC.mCurrentBubble = currentBubble;
        [self.navigationController pushViewController:chatVC animated:YES];
    }
}
- (void)onTapBubbleMenuOrInventory:(Bubble *)currentBubble
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Detail" bundle:nil];
    if([currentBubble.category isEqualToString:@"Retail"]){
        InventoryViewController *inventoryVC = [storyboard instantiateViewControllerWithIdentifier:@"InventoryViewController"];
        inventoryVC.currentBubble = currentBubble;
        [self.navigationController pushViewController:inventoryVC animated:YES];
    }
    else
    {
        BubbleMenuFullViewController *fullMenuVC = [storyboard instantiateViewControllerWithIdentifier:@"BubbleMenuFullViewController"];
        fullMenuVC.mCurrentBubble = currentBubble;
        [self presentViewController:fullMenuVC animated:YES completion:nil];
    }
}
- (void)onTapPhoto:(UIImage *)image
{
    if(!image)
        return;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Detail" bundle:nil];
    PhotoFullViewController *fullMenuVC = [storyboard instantiateViewControllerWithIdentifier:@"PhotoFullViewController"];
    fullMenuVC.mImage = image;
    [self presentViewController:fullMenuVC animated:YES completion:nil];
}
@end
