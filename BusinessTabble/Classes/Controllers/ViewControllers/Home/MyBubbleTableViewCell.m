//
//  MyBubbleTableViewCell.m
//  BusinessTabble
//
//  Created by Tian Ming on 10/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "MyBubbleTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation MyBubbleTableViewCell
@synthesize imgBubble,lblBubbleName,lblLastPost,lblPostDate;
@synthesize layout_constraint_image_top;
@synthesize imgMessageIcon;
@synthesize layout_constraint_message_width;
@synthesize layout_constraint_message_left;


- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _vwBubbleImageShadow.bounds;
    gradient.colors = @[(id)[[UIColor whiteColor] CGColor],
                        (id)[[UIColor lightGrayColor] CGColor]];
    [self layoutIfNeeded];
    [_vwBubbleImageShadow.layer setCornerRadius:_vwBubbleImageShadow.frame.size.width / 2];
    [_vwBubbleImageShadow.layer setMasksToBounds:YES];
    [_vwBubbleImageShadow.layer insertSublayer:gradient atIndex:0];
    
    [self layoutIfNeeded];
    [imgBubble.layer setCornerRadius:imgBubble.frame.size.width / 2];
    [imgBubble.layer setMasksToBounds:YES];
    [imgBubble.layer setBorderWidth:1.0f];
    [imgBubble.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [imgBubble.layer setShadowColor:[UIColor whiteColor].CGColor];
    [imgBubble.layer setShadowOpacity:1.f];
    [imgBubble.layer setShadowRadius:1.0];
    [imgBubble.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    imgBubble.clipsToBounds = YES;
    [imgBubble setContentMode:UIViewContentModeScaleToFill];
    
    // Configure the view for the selected state
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:lblLastPost.text];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:2.f];
    [style setAlignment:NSTextAlignmentCenter];
    [attributedString addAttribute:NSParagraphStyleAttributeName
                             value:style
                             range:NSMakeRange(0, lblLastPost.text.length)];
    lblLastPost.attributedText = attributedString;
    
 
    
    
//    NSInteger lineCount = 0;
//
//    CGSize labelSize = (CGSize){lblBubbleName.frame.size.width, MAXFLOAT};
//    CGRect requiredSize = [lblBubbleName.text boundingRectWithSize:labelSize  options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: lblBubbleName.font} context:nil];
//    float charSize = lroundf(lblBubbleName.font.leading);
//    charSize = 16; //needs to be changed later
//    long rHeight = lroundf(requiredSize.size.height);
//    if(charSize != 0)
//    {
//        lineCount = rHeight/charSize;
//        if(lineCount < 1)
//            layout_constraint_image_top.constant = 10;
//    }
//    if(SCREEN_WIDTH <= 320)
//    {
//        layout_constraint_message_width.constant = 200;
//        layout_constraint_message_left.constant = 30;
//    }
//    else
//        layout_constraint_message_left.constant = 15;
    
//
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

     [super setSelected:selected animated:animated];

    //[lblBubbleName sizeToFit];
}

@end
