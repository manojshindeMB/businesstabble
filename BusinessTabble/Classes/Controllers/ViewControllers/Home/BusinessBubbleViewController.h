//
//  BusinessBubbleViewController.h
//  BusinessTabble
//
//  Created by Liumin on 15/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataViewController.h"

@interface BusinessBubbleViewController : CoreDataViewController<UISearchBarDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet UIButton_Image *btnBubbleTypeAll;
@property (weak, nonatomic) IBOutlet UIButton_Image *btnBubbleTypeBars;
@property (weak, nonatomic) IBOutlet UIButton_Image *btnBubbleTypeEvents;
@property (weak, nonatomic) IBOutlet UIButton_Image *btnBubbleTypeRestaurants;
@property (weak, nonatomic) IBOutlet UIButton_Image *btnBubbleTypeRetail;
@property (weak, nonatomic) IBOutlet UIButton_Image *btnShare;

@property (weak, nonatomic) IBOutlet UIView *vwDescriptionContainer;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIView *vwEmpty;

@property (nonatomic, strong) UIRefreshControl *refreshControl;
//property
@property (strong, nonatomic) NSString * mBubbleCategory;
@property (strong, nonatomic) NSMutableArray *mArrBubbleList;
@property (nonatomic, strong) NSMutableArray *arrCollapseStatus;
@end
