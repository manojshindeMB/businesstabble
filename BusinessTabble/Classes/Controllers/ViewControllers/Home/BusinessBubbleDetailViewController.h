//
//  BusinessBubbleDetailViewController.h
//  BusinessTabble
//
//  Created by Haichen Song on 17/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Bubble+CoreDataProperties.h"
@interface BusinessBubbleDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_lbldescription_height;
@property (weak, nonatomic) IBOutlet UILabel *lblBubbleName;
@property (weak, nonatomic) IBOutlet UILabel *lblBubbleLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblBubbleDescription;

@property (weak, nonatomic) IBOutlet UIImageView *imgBubble;
@property (nonatomic, strong) Bubble *mCurrentBubble;
@end
