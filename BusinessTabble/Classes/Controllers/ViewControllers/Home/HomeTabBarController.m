//
//  TabBarController.m
//  Sticker
//
//  Created by Rixian on 04/08/15.
//  Copyright (c) 2015 AppDay Limited. All rights reserved.
//

#import "HomeTabBarController.h"
#import "ChatViewController.h"
@interface HomeTabBarController ()

@end

@implementation HomeTabBarController
// Controls
@synthesize customTabBar;

// Variables
@synthesize activeButtonIndex = _activeButtonIndex;

- (void)viewDidLoad
{
    [super viewDidLoad];
    customTabBar = (YACustomTabbar *)[[[NSBundle mainBundle] loadNibNamed:@"YACustomTabbar" owner:nil options:nil] objectAtIndex:0];
//    [UITabBar appearance] setShadowImage:<#(UIImage * _Nullable)#>
    [self.tabBar setClipsToBounds:YES];
    

    [self.tabBar addSubview:customTabBar];
    [customTabBar setFrame:CGRectMake(0, 0, self.view.frame.size.width, customTabBar.frame.size.height)];
//    NSLog(@"tabbar height %f", customTabBar.frame.size.height) ;
    
    [customTabBar setDelegate:self];
    self.activeButtonIndex = CustomTabbarButtonIndex1;
    [self.navigationController.navigationBar setHidden:YES];
    self.navigationController.navigationItem.hidesBackButton = YES;
    
    [self.tabBar setBackgroundColor:[UIColor whiteColor]];
    self.tabBar.tintColor = [UIColor whiteColor];
    self.tabBar.barTintColor = [UIColor whiteColor];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onHideTabbar) name:NOTIFICATION_TAB_BAR_HIDE object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onShowTabbar) name:NOTIFICATION_TAB_BAR_SHOW object:nil];
}
- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   // [self.navigationController.navigationBar setHidden:YES];
}
- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}
- (void) viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Property Getter & Setter
- (CustomTabbarButtonIndex)activeButtonIndex
{
    return _activeButtonIndex;
}

- (void)setActiveButtonIndex:(CustomTabbarButtonIndex)activeButtonIndex
{
    _globalData.previousTabIndex = _activeButtonIndex;
    _activeButtonIndex = activeButtonIndex;
    [customTabBar setActiveButtonIndex:_activeButtonIndex];
    [self setSelectedIndex:((NSUInteger)activeButtonIndex - 1)];

}
#pragma mark - TabBar Delegate
- (void)DidClickTabbarButtonWithIndex:(CustomTabbarButtonIndex)buttonIndex
{
    [self setActiveButtonIndex:buttonIndex];
}
- (void)onHideTabbar
{
    [self.tabBar setHidden:YES];
//    [self.customTabBar setHidden:YES];
//    [self.customTabBar setAlpha:0.f];
//    [self.tabBar setAlpha:0.f];
}
- (void)onShowTabbar
{
    [self.tabBar setHidden:NO];
//    [self.customTabBar setHidden:NO];
//    [self.customTabBar setAlpha:1.f];
//    [self.tabBar setAlpha:1.f];
}


@end
