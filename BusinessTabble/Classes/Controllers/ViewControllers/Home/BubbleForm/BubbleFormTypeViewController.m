//
//  BubbleFormTypeViewController.m
//  Tabble Business
//
//  Created by Dong Jin Jo on 02/07/2018.
//  Copyright © 2018 Liu. All rights reserved.
//

#import "BubbleFormTypeViewController.h"
#import "BubbleFormViewController.h"
#import "HomeTabBarController.h"
@interface BubbleFormTypeViewController ()

@end

@implementation BubbleFormTypeViewController
@synthesize mBubbleCategory = _mBubbleCategory;
@synthesize btnUploadLogo;
@synthesize mImgBubble = _mImgBubble;

- (void)viewDidLoad {
    [super viewDidLoad];
    revealViewController = [self revealViewController];
    [revealViewController panGestureRecognizer];
    [revealViewController tapGestureRecognizer];
    [_lblCreatorName setText:[NSString stringWithFormat:@"Creator: %@",[[GlobalData sharedData]mUserInfo].mUserName]];
    [_btnBars centerImageAndTitle:5.f];
    [_btnRetail centerImageAndTitle:5.f];
    
    [_vwCKRetailContainer.layer  setBorderWidth:1.f];
    [_vwCKBarContainer.layer  setBorderWidth:1.f];
    [_vwCKBarContainer.layer setBorderColor:[UIColor blackColor].CGColor];
    [_vwCKBarContainer.layer setBorderColor:[UIColor blackColor].CGColor];
    [_vwCKRetailContainer.layer setCornerRadius:_vwCKRetailContainer.frame.size.width / 2];
    [_vwCKBarContainer.layer setCornerRadius:_vwCKRetailContainer.frame.size.width / 2];
    
    
    [_btnCkBars.layer setCornerRadius:_btnCkBars.frame.size.width / 2];
    [_btnCkBars.layer setMasksToBounds:YES];
    
    [_btnCkRetail.layer setCornerRadius:_btnCkRetail.frame.size.width / 2];
    [_btnCkRetail.layer setMasksToBounds:YES];
    self.mBubbleCategory = BUBBLE_CATEGORY_BAR;
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = MAIN_COLOR.CGColor;
    border.frame = CGRectMake(0, _txtBubbleName.frame.size.height - borderWidth, _txtBubbleName.frame.size.width, _txtBubbleName.frame.size.height);
    border.borderWidth = borderWidth;
    [_txtBubbleName.layer addSublayer:border];
    _txtBubbleName.layer.masksToBounds = YES;
    
    photoActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel"  destructiveButtonTitle:nil otherButtonTitles:@"From Camera",@"From Photo Library", nil];
    btnUploadLogo.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    btnUploadLogo.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.view layoutIfNeeded];
    [btnUploadLogo.layer setCornerRadius:btnUploadLogo.frame.size.width / 2];
    [btnUploadLogo.layer setMasksToBounds:YES];
    [btnUploadLogo.layer setBorderWidth:2.f];
    [btnUploadLogo.layer setBorderColor:MAIN_COLOR.CGColor];
    [_btnBack setHidden:YES];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([[GlobalData sharedData]isBack])
    {
        [[GlobalData sharedData]setIsBack:NO];
        HomeTabBarController *pVCHome = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeTabBarController"];
        [revealViewController revealToggle:nil];
        [revealViewController pushFrontViewController:pVCHome animated:YES];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Actions
- (IBAction)onTouchBtnMenu:(id)sender {
    [revealViewController revealToggle:sender];
}

- (IBAction)onTouchBtnCategory:(id)sender {
    UIButton *btn = (UIButton *)sender;
    if([btn isEqual:_btnCkBars] || [btn isEqual:_btnBars])
        self.mBubbleCategory = BUBBLE_CATEGORY_BAR;
    else if([btn isEqual:_btnCkRetail] || [btn isEqual:_btnRetail])
        self.mBubbleCategory = BUBBLE_CATEGORY_RETAIL;
    
    
}
- (IBAction)onTouchBtnUploadImage:(id)sender {
    [photoActionSheet showInView:[self.view window]];
}
- (IBAction)onTouchBtnBack:(id)sender {
    if(_svMain.contentOffset.x == 0)
        return;
    [_svMain setContentOffset:CGPointMake(_svMain.contentOffset.x - self.view.frame.size.width, 0) ];
}
- (IBAction)onTouchBtnNext:(id)sender {
  
    if((_svMain.contentOffset.x == self.view.frame.size.width || _svMain.contentOffset.x == self.view.frame.size.width * 2)
       && [[GlobalData sharedData] isEmpty:[_txtBubbleName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide the name of business." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        [_txtBubbleName becomeFirstResponder];
        [_svMain setContentOffset:CGPointMake(self.view.frame.size.width , 0) ];
        return;
    }
    if(_svMain.contentOffset.x == self.view.frame.size.width * 2 && self.mImgBubble == nil)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide your company logo." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
    }
    if(_svMain.contentOffset.x == self.view.frame.size.width * 2)
    {
        BubbleFormViewController *bubbleFormVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BubbleFormViewController"];
        bubbleFormVC.mImgBubble = self.mImgBubble;
        bubbleFormVC.mBubbleName = [_txtBubbleName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        bubbleFormVC.mBubbleCategory = self.mBubbleCategory;
        [self.navigationController pushViewController:bubbleFormVC animated:YES];
        return;
    }
    [_svMain setContentOffset:CGPointMake(_svMain.contentOffset.x + self.view.frame.size.width, 0) ];
}
#pragma mark TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:_txtBubbleName])
    {
        [textField endEditing:YES];
        return true;
    }
    return true;
}
#pragma mark ScrollView Delegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(![scrollView isEqual:_svMain])
        return;
    [_btnBack setHidden:NO];
    [_btnNext setHidden:NO];
    if(scrollView.contentOffset.x == 0)
        [_btnBack setHidden:YES];
    
    
}
#pragma mark  actionsheet didDismissWithButtonIndex
- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ( buttonIndex == 2 )
        return;
    if ( actionSheet == photoActionSheet )
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.editing = YES;
        picker.videoMaximumDuration = kMaxVideoLenth;
        {
            if (buttonIndex == 0) {
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
            } else if (buttonIndex == 1) {
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            }
        }
        picker.mediaTypes = [NSArray arrayWithObjects: (NSString*)kUTTypeImage, nil];//[NSArray arrayWithObjects:(NSString*)kUTTypeMovie, (NSString*)kUTTypeImage, nil];
        [self presentViewController:picker animated:TRUE completion:nil ];
    }
}
#pragma mark didFinishPickingMediaWithInfo
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *imgtmp = [info objectForKey:UIImagePickerControllerOriginalImage];
    if(!imgtmp || imgtmp == nil)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"You can only use photo." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        return;
    }
    imgtmp = [GlobalData scaleAndRotateImage:imgtmp];
    [self dismissViewControllerAnimated:YES completion:^{
        
        PECropViewController *controller = [[PECropViewController alloc] init];
        controller.delegate = self;
        controller.image = imgtmp;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            controller.modalPresentationStyle = UIModalPresentationFormSheet;
        }
        [controller setKeepingCropAspectRatio:NO];
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        }
        [self presentViewController:navigationController animated:YES completion:NULL];
    }];
    
    
}
#pragma mark Cropper Delegate
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    self.mImgBubble = croppedImage;
}
- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark Property Getter & Setter
- (void)setMBubbleCategory:(NSString *)mBubbleCategory
{
    _mBubbleCategory = mBubbleCategory;
    //    [btnBubbleCategory setTitle:mBubbleCategory forState:UIControlStateNormal];
    [_btnCkBars setBackgroundColor:[UIColor whiteColor]];
    _btnCkBars.selected = NO;
    [_btnCkRetail setBackgroundColor:[UIColor whiteColor]];
    _btnCkRetail.selected = NO;
  
    
    if([mBubbleCategory isEqualToString:BUBBLE_CATEGORY_BAR])
    {
        [_btnCkBars setBackgroundColor:[UIColor blackColor]];
        _btnCkBars.selected = YES;
    }
  
    else if([mBubbleCategory isEqualToString:BUBBLE_CATEGORY_RETAIL])
    {
        [_btnCkRetail setBackgroundColor:[UIColor blackColor]];
        _btnCkRetail.selected = YES;
    }
    
    
}
- (void)setMImgBubble:(UIImage *)mImgBubble
{
    _mImgBubble = mImgBubble;
    if(mImgBubble != nil)
    {
        [btnUploadLogo setBackgroundImage:mImgBubble forState:UIControlStateNormal];
        [btnUploadLogo setTitle:@"" forState:UIControlStateNormal];
    }
}

@end
