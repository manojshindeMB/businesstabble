//
//  BubbleFormViewController.m
//  BusinessTabble
//
//  Created by Tian Ming on 14/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "BubbleFormViewController.h"
#import "NIDropDown.h"
#import "HomeViewController.h"
#import "HomeTabBarController.h"
#import "SearchAddressViewController.h"

@interface BubbleFormViewController ()

@end

@implementation BubbleFormViewController
@synthesize lblCreatorName, btnUploadLogo,btnSubmit,lblVerifyDescription,svMain,btnBubbleCategory,dropDownCategory, dropDownColor, btnCompanyColor,locationCoordinate;
@synthesize mImgBubble = _mImgBubble;
@synthesize mBubbleCategory = _mBubbleCategory;
@synthesize mComanyColor = _mComanyColor;
@synthesize mCurBubbleCompanyColorIndex = _mCurBubbleCompanyColorIndex;

/*
 mCurBubbleCompanyColorIndex:
 0 = [UIColor colorWithRed:250/255.0 green:168/255.0 blue:11/255.0 alpha:1.0]
 1 = [UIColor colorWithRed:19/255.0 green:22/255.0 blue:22/255.0 alpha:1.0]
 2 = [UIColor colorWithRed:71/255.0 green:105/255.0 blue:201/255.0 alpha:1.0]
 3 = [UIColor colorWithRed:126/255.0 green:71/255.0 blue:201/255.0 alpha:1.0]
 4 = [UIColor colorWithRed:211/255.0 green:56/255.0 blue:49/255.0 alpha:1.0]
 5 = [UIColor colorWithRed:74/255.0 green:201/255.0 blue:71/255.0 alpha:1.0]
 */

- (void)viewDidLoad {
    [super viewDidLoad];
    revealViewController = [self revealViewController];
    [revealViewController panGestureRecognizer];
    [revealViewController tapGestureRecognizer];

    btnUploadLogo.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    btnUploadLogo.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.view layoutIfNeeded];
    [btnUploadLogo.layer setCornerRadius:btnUploadLogo.frame.size.width / 2];
    [btnUploadLogo.layer setMasksToBounds:YES];
    [btnUploadLogo.layer setBorderWidth:2.f];
    [btnUploadLogo.layer setBorderColor:MAIN_COLOR.CGColor];

    lblVerifyDescription.font = [UIFont italicSystemFontOfSize:12.0f];
    [btnSubmit.layer setBorderWidth:2.f];
    [btnSubmit.layer setBorderColor:[UIColor colorWithRed:67/255.0 green:173/255.0 blue:50/255.0 alpha:1.0].CGColor];
    [btnSubmit.layer setMasksToBounds:YES];
    
    [btnBubbleCategory.layer setBorderWidth:2.f];
    [btnBubbleCategory.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [btnCompanyColor.layer setBorderWidth:2.f];
    [btnCompanyColor.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    photoActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel"  destructiveButtonTitle:nil otherButtonTitles:@"From Camera",@"From Photo Library", nil];
    
    [_btnCategoryBars.layer  setBorderWidth:3.f];
    [_btnCategoryBars.layer setCornerRadius:_btnCategoryBars.frame.size.width / 2];
    [_btnCategoryBars.layer setMasksToBounds:YES];
    [_btnCategoryBars.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [_btnCategoryRestaurants.layer  setBorderWidth:3.f];
    [_btnCategoryRestaurants.layer setCornerRadius:_btnCategoryRestaurants.frame.size.width / 2];
    [_btnCategoryRestaurants.layer setMasksToBounds:YES];
    [_btnCategoryRestaurants.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [_btnCategoryRetail.layer  setBorderWidth:3.f];
    [_btnCategoryRetail.layer setCornerRadius:_btnCategoryBars.frame.size.width / 2];
    [_btnCategoryRetail.layer setMasksToBounds:YES];
    [_btnCategoryRetail.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    
    float cornerRadius = 15.f;
    
    [_txtBubbleName.layer setCornerRadius:cornerRadius];
    [_txtWebsite.layer setCornerRadius:cornerRadius];
    [_txtLocation.layer setCornerRadius:cornerRadius];
    [_txtDescription.layer setCornerRadius:cornerRadius];
    [_txtOwner.layer setCornerRadius:cornerRadius];
    [_txtContactNumber.layer setCornerRadius:cornerRadius];
    [_txtPaypal.layer setCornerRadius:cornerRadius];
    
    [self setDefaultValues];
    //
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Friendly Notice" message:@"You must own a Bar, Restaurant, Retail Store, or Specialty Product Business to be approved on Placez" delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
//    [alertView showWithAnimation:YES];
    [alertView show];
    
    
    if(self.mImgBubble)
        self.mImgBubble = self.mImgBubble;
    if(![self.mBubbleCategory isEqual:nil])
        self.mBubbleCategory = self.mBubbleCategory;
    if(self.mBubbleName)
        _txtBubbleName.text = self.mBubbleName;
}
- (void)setDefaultValues
{
//    if(![[GlobalData sharedData]isEmpty:[[GlobalData sharedData]currentCity]])
//    {
//        locationCoordinate = CLLocationCoordinate2DMake([[GlobalData sharedData]currentLatitude], [[GlobalData sharedData]currentLongitude]);
//        [_txtLocation setText:[NSString stringWithFormat:@"%@, %@, %@",[[GlobalData sharedData]currentCity], [[GlobalData sharedData]currentState], [[GlobalData sharedData]currentCountry]]];
//    }
    self.mComanyColor = BUBBLE_COMPANY_COLORS[5];
    self.mCurBubbleCompanyColorIndex = 5;
//    self.mBubbleCategory = BUBBLE_CATEGORY_BAR;
    [lblCreatorName setText:[NSString stringWithFormat:@"Creator: %@",[[GlobalData sharedData]mUserInfo].mUserName]];
//

}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Actions
- (IBAction)onTouchBtnMenu:(id)sender {
//    [revealViewController revealToggle:sender];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onTouchBtnUploadImage:(id)sender {
    [photoActionSheet showInView:[self.view window]];
}
- (IBAction)onTouchBtnCategorySelected:(id)sender {
    if(dropDownColor)
    {
        [dropDownColor hideDropDown:btnCompanyColor];
        dropDownColor = nil;
    }
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:BUBBLE_CATEGORY_BAR,  BUBBLE_CATEGORY_RESTAURANTS, BUBBLE_CATEGORY_RETAIL, nil];
    NSArray * arrImage = [[NSArray alloc] init];
    if(dropDownCategory == nil) {
        CGFloat f = 200;
        dropDownCategory = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down" type:DROP_DOWN_TYPE_TEXT selectedIndex:0];
        dropDownCategory.delegate = self;
    }
    else {
        [dropDownCategory hideDropDown:sender];
        dropDownCategory = nil;
    }
}
- (IBAction)onTouchBtnCompanyColor:(id)sender {
    if(dropDownCategory)
    {
        [dropDownCategory hideDropDown:btnBubbleCategory];
        dropDownCategory = nil;
    }

    NSArray * arr = [[NSArray alloc] init];
    arr = BUBBLE_COMPANY_COLORS;
    
    NSArray * arrImage = [[NSArray alloc] init];
    if(dropDownColor == nil) {
        CGFloat f = 200;
        dropDownColor = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down" type:DROP_DOWN_TYPE_COLOR selectedIndex:0];
        dropDownColor.delegate = self;
    }
    else {
        [dropDownColor hideDropDown:sender];
        dropDownColor = nil;
    }
}

- (IBAction)onTouchBtnSubmit:(id)sender {
    if([[GlobalData sharedData]isSessionExpired])
       [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
    [self checkBubbleFormValidationWithSuccessHandler:^{
        CustomIOSAlertView *alertView = [[GlobalData sharedData] createBusinessCreationNoticeViewWithTitle:@"Please be patient, approval may take up to 24 hours."];
        [alertView show];
        alertView.delegate = self;
    } FailureHandler:^{

    }];
}
- (void)submitBubbleCreation
{
    NSDate *date = [NSDate date];
    NSString *strfilename = [NSString stringWithFormat:@"Place%@%ld.jpg", [[GlobalData sharedData]mUserInfo].mId , (long)[date timeIntervalSince1970]];
    strfilename = [[GlobalData sharedData]urlencode:strfilename];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[_txtBubbleName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  forKey:@"bubblename"];
    [params setObject:_txtWebsite.text forKey:@"website"];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"creator_id"];
    [params setObject:self.mBubbleCategory forKey:@"category"];
    [params setObject:_txtLocation.text forKey:@"location"];
    [params setObject:[NSString stringWithFormat:@"%f",locationCoordinate.latitude] forKey:@"latitude"];
    [params setObject:[NSString stringWithFormat:@"%f",locationCoordinate.longitude] forKey:@"longitude"];
    [params setObject:_txtDescription.text forKey:@"description"];
    [params setObject:strfilename forKey:@"photo_filename"];
    [params setObject:_txtOwner.text forKey:@"owner"];
    [params setObject:_txtContactNumber.text forKey:@"contactnumber"];
    const CGFloat* components = CGColorGetComponents(self.mComanyColor.CGColor);
    [params setObject:[NSString stringWithFormat:@"%f",components[0]] forKey:@"color_red"];
    [params setObject:[NSString stringWithFormat:@"%f",components[1]] forKey:@"color_green"];
    [params setObject:[NSString stringWithFormat:@"%f",components[2]] forKey:@"color_blue"];
    [params setObject:[NSString stringWithFormat:@"%ld", self.mCurBubbleCompanyColorIndex] forKey:@"color_index"];
    [params setObject:[_txtPaypal.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] forKey:@"paypal"];
    [self showProgress:@"Please wait..."];
    [[BubbleService sharedData]createBubbleWithDictionary:params success:^(id _responseObject) {
        [[GlobalData sharedData] uploadPhotoWithName:strfilename oldname:@"" image:self.mImgBubble type:@"" completion:^{
            [self hideProgress];
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Submitted For Review Successfully." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
                //                    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_DATA object:nil];
                
                [[GlobalData sharedData] setIsBack:YES];
                [self.navigationController popViewControllerAnimated:YES];
                
            }];
        } FailureHandler:^{
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Image upload has been failed." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:^{
            }];
            [self hideProgress];
        }];
        
    } failure:^(NSInteger _errorCode) {
        [self hideProgress];
        if(_errorCode == 400)
        {
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:[NSString stringWithFormat:@"There is already existing bubble '%@'.",_txtBubbleName.text] BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
            return;
        }
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
    }];
}
-(void)customIOS7dialogButtonTouchUpInside:(id)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self submitBubbleCreation];
}

- (IBAction)onTouchBtnCategory:(id)sender {
    UIButton *btn = (UIButton *)sender;
    if([btn isEqual:_btnCategoryBars])
        self.mBubbleCategory = BUBBLE_CATEGORY_BAR;
    else if([btn isEqual:_btnCategoryRetail])
        self.mBubbleCategory = BUBBLE_CATEGORY_RETAIL;
    else if([btn isEqual:_btnCategoryRestaurants])
        self.mBubbleCategory = BUBBLE_CATEGORY_RESTAURANTS;
    
}

#pragma mark Setter & Getter
- (void)setMBubbleCategory:(NSString *)mBubbleCategory
{
    _mBubbleCategory = mBubbleCategory;
//    [btnBubbleCategory setTitle:mBubbleCategory forState:UIControlStateNormal];
    [_btnCategoryBars setBackgroundColor:[UIColor whiteColor]];
    _btnCategoryBars.selected = NO;
    [_btnCategoryRestaurants setBackgroundColor:[UIColor whiteColor]];
    _btnCategoryRestaurants.selected = NO;
    [_btnCategoryRetail setBackgroundColor:[UIColor whiteColor]];
    _btnCategoryRetail.selected = NO;
    
    if([mBubbleCategory isEqualToString:BUBBLE_CATEGORY_BAR])
    {
        [_btnCategoryBars setBackgroundColor:[UIColor blackColor]];
        _btnCategoryBars.selected = YES;
    }
    else if([mBubbleCategory isEqualToString:BUBBLE_CATEGORY_RESTAURANTS])
    {
        [_btnCategoryRestaurants setBackgroundColor:[UIColor blackColor]];
        _btnCategoryRestaurants.selected = YES;
    }
    else if([mBubbleCategory isEqualToString:BUBBLE_CATEGORY_RETAIL])
    {
        [_btnCategoryRetail setBackgroundColor:[UIColor blackColor]];
        _btnCategoryRetail.selected = YES;
    }
}
- (void)setMImgBubble:(UIImage *)mImgBubble
{
    _mImgBubble = mImgBubble;
    if(mImgBubble != nil)
    {
        [btnUploadLogo setBackgroundImage:mImgBubble forState:UIControlStateNormal];
        [btnUploadLogo setTitle:@"" forState:UIControlStateNormal];
    }
}
- (void)setMComanyColor:(UIColor *)mComanyColor
{
    _mComanyColor = mComanyColor;
    [btnCompanyColor setBackgroundColor:mComanyColor];
}
- (void)setMCurBubbleCompanyColorIndex:(NSInteger)mCurBubbleCompanyColorIndex
{
    _mCurBubbleCompanyColorIndex = mCurBubbleCompanyColorIndex;
    
}
#pragma mark Address Select Delegate
- (void)selectAddressViewController:(SearchAddressViewController *)viewController didSelectedLocation:(NSString *)address coordinate:(CLLocationCoordinate2D)coordinate
{
    locationCoordinate = coordinate;
    [_txtLocation setText:address];
}
#pragma mark UITextField Delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([textField isEqual:_txtLocation])
    {
        SearchAddressViewController *searchVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchAddressViewController"];
        searchVC.delegate = self;
        [self presentViewController:searchVC animated:YES completion:nil];
        return NO;
    }
    return YES;
}
-(void)niDropDownDelegateMethod:(NSInteger)index text:(NSObject *)text type:(NSInteger)type
{
    if(type == DROP_DOWN_TYPE_TEXT)
        self.mBubbleCategory = (NSString *)text;
    else if(type == DROP_DOWN_TYPE_COLOR)
    {
        self.mComanyColor = (UIColor *)text;
        self.mCurBubbleCompanyColorIndex = index;
    }
}
#pragma mark  actionsheet didDismissWithButtonIndex
- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ( buttonIndex == 2 )
        return;
    if ( actionSheet == photoActionSheet )
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.editing = YES;
        picker.videoMaximumDuration = kMaxVideoLenth;
        {
            if (buttonIndex == 0) {
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
            } else if (buttonIndex == 1) {
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            }
        }
        picker.mediaTypes = [NSArray arrayWithObjects: (NSString*)kUTTypeImage, nil];//[NSArray arrayWithObjects:(NSString*)kUTTypeMovie, (NSString*)kUTTypeImage, nil];
        [self presentViewController:picker animated:TRUE completion:nil ];
    }
}
#pragma mark didFinishPickingMediaWithInfo
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *imgtmp = [info objectForKey:UIImagePickerControllerOriginalImage];
    if(!imgtmp || imgtmp == nil)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"You can only use photo." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        return;
    }
    imgtmp = [GlobalData scaleAndRotateImage:imgtmp];
    [self dismissViewControllerAnimated:YES completion:^{

        PECropViewController *controller = [[PECropViewController alloc] init];
        controller.delegate = self;
        controller.image = imgtmp;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            controller.modalPresentationStyle = UIModalPresentationFormSheet;
        }
        [controller setKeepingCropAspectRatio:NO];
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        }
        [self presentViewController:navigationController animated:YES completion:NULL];
    }];
    

}
#pragma mark Cropper Delegate
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    self.mImgBubble = croppedImage;
}
- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark Other Methods
- (void)checkBubbleFormValidationWithSuccessHandler:(void (^)())successHandler FailureHandler:(void (^)())failureHandler
{
    if(dropDownColor)
    {
        [dropDownColor hideDropDown:btnCompanyColor];
        dropDownColor = nil;
    }
    if(dropDownCategory)
    {
        [dropDownCategory hideDropDown:btnBubbleCategory];
        dropDownCategory = nil;
    }
    if([[GlobalData sharedData]isEmpty:_txtBubbleName.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide bubble name." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if([[GlobalData sharedData]isEmpty:_txtLocation.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide address." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if([[GlobalData sharedData]isEmpty:_txtWebsite.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide website." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
  
    if([[GlobalData sharedData]isEmpty:_txtOwner.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide owner or authorized accociate." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if([[GlobalData sharedData]isEmpty:_txtContactNumber.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide contact number." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if(!self.mImgBubble)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide bubble image." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    successHandler();
    
}

@end
