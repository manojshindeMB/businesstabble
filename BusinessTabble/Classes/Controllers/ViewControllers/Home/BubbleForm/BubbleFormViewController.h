//
//  BubbleFormViewController.h
//  BusinessTabble
//
//  Created by Tian Ming on 14/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGLDropDownMenu.h"
#import "NIDropDown.h"

#import "SearchAddressViewController.h"

@interface BubbleFormViewController : UIViewController<NIDropDownDelegate,UITextFieldDelegate,UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, SelectAddressViewControllerDelegate, PECropViewControllerDelegate, CustomIOSAlertViewDelegate>
{
    SWRevealViewController *revealViewController;
    NSArray *arrBubbleCategory;
    UIActionSheet *photoActionSheet;
}
@property (weak, nonatomic) IBOutlet UILabel *lblCreatorName;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UILabel *lblVerifyDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnUploadLogo;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtBubbleName;
@property (strong, nonatomic) NSString *mBubbleName;


@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtWebsite;

@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtLocation;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtOwner;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtContactNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnBubbleCategory;
@property (weak, nonatomic) IBOutlet UIScrollView *svMain;
@property (weak, nonatomic) IBOutlet UIButton *btnCompanyColor;

@property (strong, nonatomic)  NIDropDown *dropDownCategory;
@property (strong, nonatomic)  NIDropDown *dropDownColor;
@property (nonatomic) CLLocationCoordinate2D locationCoordinate;

@property (weak, nonatomic) IBOutlet UIButton *btnCategoryBars;
@property (weak, nonatomic) IBOutlet UIButton *btnCategoryRestaurants;
@property (weak, nonatomic) IBOutlet UIButton *btnCategoryRetail;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtPaypal;

//Property
@property (nonatomic, strong) UIImage *mImgBubble;
@property (nonatomic, strong) NSString *mBubbleCategory;
@property (nonatomic, strong) UIColor *mComanyColor;
@property (nonatomic) NSInteger mCurBubbleCompanyColorIndex;
@end
