//
//  BubbleFormTypeViewController.h
//  Tabble Business
//
//  Created by Dong Jin Jo on 02/07/2018.
//  Copyright © 2018 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BubbleFormTypeViewController : UIViewController<UITextFieldDelegate,UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,  PECropViewControllerDelegate, UIScrollViewDelegate>
{
    SWRevealViewController *revealViewController;
    UIActionSheet *photoActionSheet;
}
@property (weak, nonatomic) IBOutlet UILabel *lblCreatorName;
@property (weak, nonatomic) IBOutlet UIScrollView *svMain;
@property (weak, nonatomic) IBOutlet UIView *vwBusinessType;
@property (weak, nonatomic) IBOutlet UIView *vwBusinessName;
@property (weak, nonatomic) IBOutlet UIView *vwCompanyLogo;
@property (weak, nonatomic) IBOutlet UIButton_Image *btnRetail;
@property (weak, nonatomic) IBOutlet UIButton_Image *btnBars;
@property (weak, nonatomic) IBOutlet UIButton *btnCkBars;
@property (weak, nonatomic) IBOutlet UIButton *btnCkRetail;
@property (nonatomic, strong) NSString *mBubbleCategory;
@property (weak, nonatomic) IBOutlet UIView *vwCKBarContainer;
@property (weak, nonatomic) IBOutlet UIView *vwCKRetailContainer;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtBubbleName;
@property (weak, nonatomic) IBOutlet UIButton *btnUploadLogo;
@property (nonatomic, strong) UIImage *mImgBubble;
@end
