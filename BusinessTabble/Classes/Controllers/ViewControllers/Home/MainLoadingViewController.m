//
//  MainLoadingViewController.m
//  Tabble Business
//
//  Created by Dong Jin Jo on 14/02/2018.
//  Copyright © 2018 Liu. All rights reserved.
//

#import "MainLoadingViewController.h"
#import "RevealMainViewController.h"
#import "HomeTabBarController.h"
@interface MainLoadingViewController ()

@end

@implementation MainLoadingViewController
@synthesize mCurrentProgressNumber = _mCurrentProgressNumber;
@synthesize timer;
@synthesize timerProgress;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_vwLogoContainer.layer setCornerRadius:_vwLogoContainer.frame.size.height / 2];
    [_vwLogoContainer.layer setBorderColor:MAIN_COLOR.CGColor];
    [_vwLogoContainer.layer setBorderWidth:3.f];
    if(![[GlobalData sharedData] isMainFirstLoading])
    {
        [self showMainHome];
        return;
    }
    [self removeNotifications];
    [self registerNotifications];
    [self syncDatabase];
    timer = [NSTimer scheduledTimerWithTimeInterval:10.0
                                             target:self
                                           selector:@selector(syncDatabase)
                                           userInfo:nil
                                            repeats:YES];
    
    [self showProgressView];
    [NSTimer scheduledTimerWithTimeInterval:5.f target:self selector:@selector(hideProgressView) userInfo:nil repeats:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Progress View
- (void)onProgressTick:(NSTimer *)timer
{
    
    self.mCurrentProgressNumber = (self.mCurrentProgressNumber + 1)  % 4;
    
}
- (void)showProgressView
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

    self.mCurrentProgressNumber = 0;
    
//    [_vwSplash setHidden:NO];
//    [UIView animateWithDuration:0.3 animations:^{
//        [_vwSplash setAlpha:1.f];
//    }];
    
    timerProgress = [NSTimer scheduledTimerWithTimeInterval: 0.5
                                                     target: self
                                                   selector:@selector(onProgressTick:)
                                                   userInfo: nil repeats:YES];
    
}
- (void)hideProgressView
{
//    [_vwSplash setHidden:NO];
//    [UIView animateWithDuration:0.3 animations:^{
//        [_vwSplash setAlpha:0.f];
//    }];
    
    self.mCurrentProgressNumber = 0;
    [timerProgress invalidate];
    [self showMainHome];
}
- (void)setMCurrentProgressNumber:(NSInteger)mCurrentProgressNumber
{
    _mCurrentProgressNumber = mCurrentProgressNumber;
    [_imgProgress1 setAlpha:0.6];
    [_imgProgress2 setAlpha:0.6f];
    [_imgProress3 setAlpha:0.6f];
    [_imgProgress4 setAlpha:0.6f];
    if(mCurrentProgressNumber == 0)
        [_imgProgress1 setAlpha:1.f];
    else if(mCurrentProgressNumber == 1)
        [_imgProgress2 setAlpha:1.f];
    else if(mCurrentProgressNumber == 2)
        [_imgProress3 setAlpha:1.f];
    else if(mCurrentProgressNumber == 3)
        [_imgProgress4 setAlpha:1.f];
    
}
- (void)showMainHome
{
    [[GlobalData sharedData]setIsMainFirstLoading:NO];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
    HomeTabBarController *homeTabbarVC = [storyboard instantiateViewControllerWithIdentifier:@"HomeTabBarController"];
    RevealMainViewController *revealViewController = [storyboard instantiateViewControllerWithIdentifier:@"RevealMainViewController"];
    [revealViewController setFrontViewController:homeTabbarVC];
    [revealViewController setRearViewController:mainViewController];
    [self.navigationController pushViewController:revealViewController animated:NO];
}
#pragma mark Notifications

- (void)registerNotifications
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getBussinessBubbleList:) name:NOTIFICATION_RELOAD_BUBBLE_DATA object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getBubbleMemberList:) name:NOTIFICATION_RELOAD_BUBBLE_MEMBER_LIST object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getBubbleMenuList:) name:NOTIFICATION_RELOAD_BUBBLE_MENU_LIST object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getBubbleMessageList:) name:NOTIFICATION_RELOAD_BUBBLE_MESSAGE_LIST object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getBubbleChatList:) name:NOTIFICATION_RELOAD_BUBBLE_CHAT_LIST object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getBubbleHidePosters:) name:NOTIFICATION_RELOAD_BUBBLE_CHAT_HIDE_POSTER_LIST object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getCouponUseList:) name:NOTIFICATION_RELOAD_COUPON_USE_LIST object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getActivityLikeList:) name:NOTIFICATION_RELOAD_ACTIVITY_LIKE_LIST object:nil];
}
- (void)removeNotifications
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NOTIFICATION_RELOAD_BUBBLE_DATA object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NOTIFICATION_RELOAD_BUBBLE_MEMBER_LIST object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NOTIFICATION_RELOAD_BUBBLE_MENU_LIST object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NOTIFICATION_RELOAD_BUBBLE_MESSAGE_LIST object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NOTIFICATION_RELOAD_BUBBLE_CHAT_LIST object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NOTIFICATION_RELOAD_BUBBLE_CHAT_HIDE_POSTER_LIST object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NOTIFICATION_RELOAD_COUPON_USE_LIST object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NOTIFICATION_RELOAD_ACTIVITY_LIKE_LIST object:nil];
}
#pragma mark Sync database
- (void)syncDatabase
{
    JUserInfo *myInfo = [[GlobalData sharedData]mUserInfo];
    if(![localRegistry objectForKey:kMyUserDict] || !myInfo)
    {
        [timer invalidate];
        return ;
    }
    [self getBussinessBubbleList:nil];
    [self getBubbleMemberList:nil];
    [self getBubbleMenuList:nil];
    [self getBubbleMessageList:nil];
    [self getBubbleChatList:nil];
    [self getBubbleHidePosters:nil];
    [self getCouponUseList:nil];
    [self getActivityLikeList:nil];
}
- (void)getBussinessBubbleList:(id)sender
{
    if([[GlobalData sharedData]isSessionExpired])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
        return;
    }
    [[MainService sharedData]getBussinessBubbleListWithSuccessHandler:^{
        
    } FailureHandler:^{
    }];
}
- (void)getBubbleMemberList:(id)sender
{
    if([[GlobalData sharedData]isSessionExpired])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
        return;
    }
    [[MainService sharedData] getBubbleMemberList:^{
        
    } FailureHandler:^{
        
    }];
}
- (void)getBubbleMenuList:(id)sender
{
    if([[GlobalData sharedData]isSessionExpired])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
        return;
    }
    [[MainService sharedData]fetchBubbleMenuList:[[GlobalData sharedData]mUserInfo] success:^(id _responseObject) {
        
    } failure:^(NSInteger _errorCode) {
        
    }];
}
- (void)getBubbleMessageList:(id)sender
{
    if([[GlobalData sharedData]isSessionExpired])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
        return;
    }
    [[MainService sharedData]getBubbleMessageListWithSuccessHandler:^{
        
    } FailureHandler:^{
        
    }];
}
- (void)getBubbleChatList:(id)sender
{
    if([[GlobalData sharedData]isSessionExpired])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
        return;
    }
    [[MainService sharedData]getBubbleChatListWithSuccessHandler:^(id _responseObject) {
        
    } FailureHandler:^{
        
    }];
}
- (void)getBubbleHidePosters:(id)sender
{
    if([[GlobalData sharedData]isSessionExpired])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
        return;
    }
    [[MainService sharedData]getBubbleChatHideListWithSuccessHandler:^(id _responseObject) {
    } FailureHandler:^{
    }];
}
- (void)getCouponUseList:(id)sender
{
    if([[GlobalData sharedData]isSessionExpired])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
        return;
    }
    
    
    [[MainService sharedData]getCouponUseListWithSuccessHandler:^(id _responseObject) {
    } FailureHandler:^{
    }];
}
- (void)getActivityLikeList:(id)sender
{
    if([[GlobalData sharedData]isSessionExpired])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
        return;
    }
    [[MainService sharedData]getActivityLikeListWithSuccessHandler:^(id _responseObject) {
    } FailureHandler:^{
    }];
}
@end
