//
//  HomeViewController.m
//  BusinessTabble
//
//  Created by Tian Ming on 09/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "HomeViewController.h"
#import "MyBubbleTableViewCell.h"
#import <MagicalRecord/MagicalRecord.h>
#import "PostViewController.h"
#import "MenuDetailViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController
@synthesize tvMyBubbleList;
@synthesize mArrBubbleList;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:_lblTitle.text];
    float spacing = 1.0f;

    [attributedString addAttribute:NSKernAttributeName
                             value:@(spacing)
                             range:NSMakeRange(0, [_lblTitle.text length])];
    _lblTitle.attributedText = attributedString;
    revealViewController = [self revealViewController];
    [revealViewController panGestureRecognizer];
    [revealViewController tapGestureRecognizer];
    mArrBubbleList = [[NSMutableArray alloc]init];
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
    [self.tableView reloadData];
    
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5.f];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:paragraphStyle forKey:NSParagraphStyleAttributeName];
    _lblEmptyDescription.attributedText = [[NSMutableAttributedString alloc] initWithString:_lblEmptyDescription.text attributes:attrsDictionary];
    
    paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:5.f];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    attrsDictionary = [NSDictionary dictionaryWithObject:paragraphStyle forKey:NSParagraphStyleAttributeName];
    _lblEmptyTitle.attributedText = [[NSMutableAttributedString alloc] initWithString:_lblEmptyTitle.text attributes:attrsDictionary];

    [_vwEmptyMask setHidden:NO];
    [self reloadDataWithCategoryName:@"" SearchString:@""];

}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self reloadDataWithCategoryName:@"" SearchString:@""];

    bubbleTimer = [NSTimer scheduledTimerWithTimeInterval:10.0
                                     target:self
                                   selector:@selector(reloadBubbleListFromServer:)
                                   userInfo:nil
                                    repeats:YES];
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [bubbleTimer invalidate];
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}
- (void)reloadDataWithCategoryName:(NSString *)strCategoryName
                      SearchString:(NSString *)strSearchString
{
    if(![localRegistry objectForKey:kMyUserDict])
        return;

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"reviewstatus = 1 AND field_deleted == 0 AND joined = 1 AND (NOT(category CONTAINS %@)) AND (NOT(category CONTAINS %@))", @"Activity", @"Event"];
    self.fetchedResultsController = [Bubble MR_fetchAllSortedBy:@"lastmsgtime" ascending:NO withPredicate:predicate groupBy:nil delegate:self];
    if([[self.fetchedResultsController fetchedObjects] count] > 0)
        [_vwEmptyMask setHidden:YES];
    else
        [_vwEmptyMask setHidden:NO];
        
    
}
- (void)reloadBubbleListFromServer:(id)sender
{
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_DATA object:nil];
    [self reloadDataWithCategoryName:@"" SearchString:@""];
}
- (float)getDistanceFromPlace :(Bubble *)currentBubble
{
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:[currentBubble.latitude floatValue] longitude:[currentBubble.longitude floatValue]];
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:[[GlobalData sharedData]currentLatitude] longitude:[[GlobalData sharedData]currentLongitude]];
    CLLocationDistance distance = [locA distanceFromLocation:locB];
    return distance;
}
#pragma mark Actions
- (IBAction)onTouchBtnLeftMenu:(id)sender {
    [revealViewController revealToggle:sender];
//
}
#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(SCREEN_HEIGHT < 568.0)
        return self.tableView.frame.size.height / 2;
    return self.tableView.frame.size.height / 3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Bubble *currentBubble = self.fetchedResultsController.fetchedObjects[indexPath.row];
    NSString *CellIdentifier = @"MyBubbleTableViewCell";
    MyBubbleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [cell.lblBubbleName setText:currentBubble.bubblename];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell.imgMessageIcon setHidden:YES];
    [cell.lblNewPhotoCoupon setHidden:YES];
    [cell.lblLastPost setHidden:NO];
    [cell.imgCouponIcon setHidden:YES];
    cell.mCurrentBubble = currentBubble;
    if([currentBubble.lastmsg_id integerValue] > 0)
    {
        if([currentBubble.coupon isEqualToNumber:@1])
        {
//            [cell.lblNewPhotoCoupon setText:@"Coupon Available"];
//            [cell.imgMessageIcon setHidden:NO];
//            [cell.lblNewPhotoCoupon setHidden:NO];
            [cell.imgCouponIcon setHidden:NO];
            [cell.lblLastPost setHidden:YES];
            [cell.imgMessageIcon setImage:[UIImage imageNamed:@"bubble_home_coupon"]];
        }
        else
        {
            if([[GlobalData sharedData]isEmpty:currentBubble.text])
            {
                [cell.lblNewPhotoCoupon setText:@"New Photo"];
//                cell.layout_constraint_message_left.constant = 20;
                [cell.imgMessageIcon setHidden:NO];
                [cell.lblNewPhotoCoupon setHidden:NO];
                [cell.lblLastPost setHidden:YES];
                [cell.imgMessageIcon setImage:[UIImage imageNamed:@"bubble_home_photo"]];
            }
            else
                [cell.lblLastPost setText:currentBubble.text];
        }
    }
    else {
        [cell.lblLastPost setText:@""];
    }
    if([currentBubble.lastmsgtime integerValue] > 0)
    {
        NSDate *lastMsgDate = [NSDate dateWithTimeIntervalSince1970:[currentBubble.lastmsgtime doubleValue]];
        [cell.lblPostDate setText:[NSString stringWithFormat:@"%@", [[GlobalData sharedData]dateTimeStringFromDate:lastMsgDate]]];
    }
    else
        [cell.lblPostDate setText:@""];
    
    NSString *photo_filename = [[GlobalData sharedData]urlencode:currentBubble.photo_filename];
    
    [[GlobalData sharedData]showImageWithImage:cell.imgBubble name:photo_filename placeholder:nil];
    [cell.vwBubbleBanner setBackgroundColor:[UIColor colorWithRed:[currentBubble.color_red floatValue] green:[currentBubble.color_green floatValue] blue:[currentBubble.color_blue floatValue] alpha:1]];
//    [cell.vwCellContent setBackgroundColor:cell.vwBubbleBanner.backgroundColor];
    cell.rightUtilityButtons = [self rightButtons];
    cell.delegate = self;
    [cell.btnChat setHidden:YES];
//    CGFloat distance = [self getDistanceFromPlace:currentBubble];
//    distance < CHAT_ESTABLISHMENT_RADIUS_IN_METER
    if([currentBubble.category isEqualToString:@"Bars/Restaurants"])
    {
        [cell.btnChat setHidden:NO];
    }
    
    // New Design
    [cell.btnChat setHidden:YES];
    [cell.vwBubbleBanner setBackgroundColor:[UIColor clearColor]];
    [cell.vwBannerSubHeader setBackgroundColor:[UIColor colorWithRed:[currentBubble.color_red floatValue] green:[currentBubble.color_green floatValue] blue:[currentBubble.color_blue floatValue] alpha:1]];
    [cell.vwBannerSubHeader.layer setCornerRadius:5.f];
    [cell.vwBannerSubContent setBackgroundColor:cell.vwBannerSubHeader.backgroundColor];
    [cell.vwContentArea.layer setBorderWidth:2.f];
    [cell.vwContentArea.layer setBorderColor:[UIColor colorWithRed:200/255.f green:200/255.f blue:200/255.f alpha:1.0].CGColor];
    [cell.vwContentArea.layer setMasksToBounds:YES];
    if(currentBubble.color_index && [currentBubble.color_index integerValue] >= 0)
    {
        if([currentBubble.color_index integerValue]== 0)
            [cell.imgRoof setImage:[UIImage imageNamed:@"home_page_roof_yellow"]];
        if([currentBubble.color_index integerValue]== 1)
            [cell.imgRoof setImage:[UIImage imageNamed:@"home_page_roof_black"]];
        if([currentBubble.color_index integerValue]== 2)
            [cell.imgRoof setImage:[UIImage imageNamed:@"home_page_roof_dark_blue"]];
        if([currentBubble.color_index integerValue]== 3)
            [cell.imgRoof setImage:[UIImage imageNamed:@"home_page_roof_purple"]];
        if([currentBubble.color_index integerValue]== 4)
            [cell.imgRoof setImage:[UIImage imageNamed:@"home_page_roof_red"]];
        if([currentBubble.color_index integerValue]== 5)
            [cell.imgRoof setImage:[UIImage imageNamed:@"home_page_roof_dark_main"]];
    }
    
//    CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
//    rotationAndPerspectiveTransform.m34 = 1.0 / 1000.0;
//    rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, M_PI * 0.6, 2.0f, 0.0f, 0.0f);
//    [UIView animateWithDuration:0.0 animations:^{
//        cell.vwBannerSubContent.layer.anchorPoint = CGPointMake(0.5, 0);
//        cell.vwBannerSubContent.layer.transform = rotationAndPerspectiveTransform;
//    } completion:^(BOOL finished){
//        // code to be executed when flip is completed
//    }];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Detail" bundle:nil];
    Bubble *currentBubble = self.fetchedResultsController.fetchedObjects[indexPath.row];
    MenuDetailViewController *postVC = [storyboard instantiateViewControllerWithIdentifier:@"MenuDetailViewController"];
    postVC.mCurrentBubble = currentBubble;    
    [self.navigationController pushViewController:postVC animated:YES];
}
- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];

    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                title:@"Delete"];
    return rightUtilityButtons;
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    MyBubbleTableViewCell *currentCell = (MyBubbleTableViewCell *)cell;
    Bubble *currentBubble = currentCell.mCurrentBubble;
    [cell hideUtilityButtonsAnimated:YES];
    if([currentBubble.creator_id isEqualToNumber:[NSNumber numberWithInteger:[[[GlobalData sharedData]mUserInfo].mId integerValue]]])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:[NSString stringWithFormat:@"This bubble was created by you. \n You can't delete the bubble this way."] BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        return;
    }
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]init];
    [activityIndicator setCenter:currentCell.vwCellContent.center];
    [currentCell.vwCellContent addSubview:activityIndicator];
    [activityIndicator startAnimating];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:currentBubble.bubble_id forKey:@"bubble_id"];
    [[BubbleService sharedData]unJoinBubbleWithDictionary:params success:^(id _responseObject) {
      
        currentBubble.joined = [NSNumber numberWithInt:0];
        currentBubble.membercount = [NSNumber numberWithInteger:[currentBubble.membercount integerValue] - 1];

        [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
            [self reloadDataWithCategoryName:@"" SearchString:@""];
        }];
        [localRegistry removeObjectForKey:kLastFetchTimeStampForBubbleMemberList];
        [localRegistry removeObjectForKey:kLastFetchTimeStampForBubbleMenuList];
        [localRegistry removeObjectForKey:kLastFetchTimeStampForBubbleMessageList];
        //[localRegistry removeObjectForKey:kLastFetchTimeStampForCouponList];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MEMBER_LIST object:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MENU_LIST object:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MESSAGE_LIST object:nil];
        //[[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_COUPON_USE_LIST object:nil];
        
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Successfully deleted." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
            [activityIndicator stopAnimating];
            [activityIndicator removeFromSuperview];
        }];
        
    } failure:^(NSInteger _errorCode) {
        [activityIndicator stopAnimating];
        [activityIndicator removeFromSuperview];
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:^{
        }];
    }];

}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{
    
}
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    return YES;
}
@end
