//
//  MyBubbleTableViewCell.h
//  BusinessTabble
//
//  Created by Tian Ming on 10/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
@protocol MyBubbleDelegate <NSObject>

@optional - (void) onTapBubbleMap:(Bubble* ) currentBubble;
@optional - (void) onTapBubbleCall:(Bubble* ) currentBubble;
@optional - (void) onTapBubblePhotosOrChat:(Bubble* ) currentBubble;
@optional - (void) onTapBubbleMenuOrInventory:(Bubble* ) currentBubble;
@optional - (void) onTapPhoto:(UIImage *)image;

@end
@interface MyBubbleNewTableViewCell : SWTableViewCell<UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgBubble;
@property (weak, nonatomic) IBOutlet UILabel *lblBubbleName;
@property (weak, nonatomic) IBOutlet UILabel *lblLastPost;
@property (weak, nonatomic) IBOutlet UILabel *lblPostDate;
@property (weak, nonatomic) IBOutlet UIView *vwCellContent;
@property (strong, nonatomic) Bubble *mCurrentBubble;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_message_left;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_last_msg_img_bottom_space;
@property (weak, nonatomic) IBOutlet UIView *vwBubbleBanner;
@property (weak, nonatomic) IBOutlet UIView *vwBubbleImageShadow;
@property (weak, nonatomic) IBOutlet UIView *vwContentArea;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_attachphoto_height;
@property (weak, nonatomic) IBOutlet UIImageView *imgLastMessagePhoto;
@property (weak, nonatomic) IBOutlet UIImageView *imgBuySellItem;
@property (weak, nonatomic) IBOutlet UILabel *lblSellItemPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblSellItemDeliverPrice;
@property (nonatomic, strong) id<MyBubbleDelegate> myBubbleDelegate;

@property (weak, nonatomic) IBOutlet UIButton *btnInfoMap;
@property (weak, nonatomic) IBOutlet UIButton *btnInfoCall;
@property (weak, nonatomic) IBOutlet UIButton *btnInfoPhotoOrChat;
@property (weak, nonatomic) IBOutlet UIButton *btnInfoMenu;


@end
