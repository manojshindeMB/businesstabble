//
//  RevealMainViewController.m
//  BusinessTabble
//
//  Created by macbook on 11/06/2017.
//  Copyright © 2017 Liu. All rights reserved.
//

#import "RevealMainViewController.h"
#import "LoginNavigationController.h"
@interface RevealMainViewController ()

@end

@implementation RevealMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
//    ((LoginNavigationController *)(self.navigationController)).tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(revealToggle:)];
//    [((LoginNavigationController *)(self.navigationController)).view addGestureRecognizer:((LoginNavigationController *)(self.navigationController)).tapGestureRecognizer];
//    ((LoginNavigationController *)(self.navigationController)).tapGestureRecognizer.enabled = NO;
//    [((LoginNavigationController *)(self.navigationController)).view addGestureRecognizer:self.panGestureRecognizer];
//    self.delegate = (LoginNavigationController *)(self.navigationController);
//
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onPushNotificationReceived:) name:NOTIFICATION_PUSH_NOTIFICATION_RECEIVED object:nil];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)onPushNotificationReceived:(NSNotification *)notification
{
    [[GlobalData sharedData]onOpenChatViewWhenPushReceived:self notification:notification];
}
@end
