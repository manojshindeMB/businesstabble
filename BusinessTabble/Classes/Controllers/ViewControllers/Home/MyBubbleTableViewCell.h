//
//  MyBubbleTableViewCell.h
//  BusinessTabble
//
//  Created by Tian Ming on 10/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"

@interface MyBubbleTableViewCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgBubble;
@property (weak, nonatomic) IBOutlet UILabel *lblBubbleName;
@property (weak, nonatomic) IBOutlet UILabel *lblLastPost;
@property (weak, nonatomic) IBOutlet UILabel *lblPostDate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_image_top;
@property (weak, nonatomic) IBOutlet UIView *vwCellContent;
@property (weak, nonatomic) IBOutlet UIImageView *imgMessageIcon;
@property (strong, nonatomic) Bubble *mCurrentBubble;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_message_width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_message_left;
@property (weak, nonatomic) IBOutlet UILabel *lblNewPhotoCoupon;
@property (weak, nonatomic) IBOutlet UIView *vwBubbleBanner;
@property (weak, nonatomic) IBOutlet UIView *vwBubbleImageShadow;
@property (weak, nonatomic) IBOutlet UIImageView *imgCouponIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnChat;

@property (weak, nonatomic) IBOutlet UIView *vwBannerSubHeader;
@property (weak, nonatomic) IBOutlet UIView *vwBannerSubContent;
@property (weak, nonatomic) IBOutlet UIView *vwContentArea;
@property (weak, nonatomic) IBOutlet UIImageView *imgRoof;


@end
