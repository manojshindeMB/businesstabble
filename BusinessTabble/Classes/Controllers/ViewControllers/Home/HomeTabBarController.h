//
//  TabBarController.h
//  Sticker
//
//  Created by Rixian on 04/08/15.
//  Copyright (c) 2015 AppDay Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YACustomTabbar.h"
@interface HomeTabBarController : UITabBarController <YACustomTabbarDelegate>

// Controls
@property (nonatomic, strong) YACustomTabbar *customTabBar;
// Variables
@property (nonatomic, readwrite) CustomTabbarButtonIndex activeButtonIndex;

@end
