//
//  SearchAddressViewController.h
//  BusinessTabble
//
//  Created by Liumin on 18/04/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPGooglePlacesAutocompleteQuery.h"
#import "SPGooglePlacesAutocompletePlace.h"
#import <GoogleMaps/GoogleMaps.h>
#define IOS_VERSION     [[[UIDevice currentDevice] systemVersion] floatValue]

@class SearchAddressViewController;

@protocol SelectAddressViewControllerDelegate <NSObject>

- (void) selectAddressViewController:(SearchAddressViewController *)viewController didSelectedLocation:(NSString *)address coordinate:(CLLocationCoordinate2D)coordinate;
- (void) selectAddressViewControllerDidCancel:(SearchAddressViewController *)viewController;

@end

@interface SearchAddressViewController : UIViewController<UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnCurrentLocation;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtAddress;
@property (weak, nonatomic) IBOutlet UITableView *tvAddressList;


@property (nonatomic, strong) NSString *address;
@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) GMSGeocoder *geocoder;
@property (nonatomic, strong) CLLocationManager     *locationManager;
@property (nonatomic, readwrite, getter=isLocked) BOOL isLocked;

@property (nonatomic, strong) NSMutableArray *arrAddress;
@property (nonatomic, strong) NSString     *curLocationStr;

@property (nonatomic, assign) id<SelectAddressViewControllerDelegate> delegate;
@property (nonatomic, strong) SPGooglePlacesAutocompleteQuery *searchQuery;
@end
