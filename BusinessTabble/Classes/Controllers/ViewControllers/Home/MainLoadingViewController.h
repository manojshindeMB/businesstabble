//
//  MainLoadingViewController.h
//  Tabble Business
//
//  Created by Dong Jin Jo on 14/02/2018.
//  Copyright © 2018 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainLoadingViewController : UIViewController
@property (nonatomic, strong) NSTimer *timer;
@property (strong, nonatomic) NSTimer *timerProgress;
@property (nonatomic)NSInteger mCurrentProgressNumber;
@property (weak, nonatomic) IBOutlet UIView *vwSplash;
@property (weak, nonatomic) IBOutlet UIImageView *imgProgress1;
@property (weak, nonatomic) IBOutlet UIImageView *imgProgress2;
@property (weak, nonatomic) IBOutlet UIImageView *imgProress3;
@property (weak, nonatomic) IBOutlet UIImageView *imgProgress4;
@property (weak, nonatomic) IBOutlet UIView *vwLogoContainer;

@end
