//
//  BusinessBubbleTableViewCell.h
//  BusinessTabble
//
//  Created by Haichen Song on 16/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusinessBubbleTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblBubbleName;
@property (weak, nonatomic) IBOutlet UIImageView *imgBubble;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UIButton_Image *btnActivityRate;
@property (weak, nonatomic) IBOutlet UILabel *lblActivityRate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_distance_bottom_space;
@property (weak, nonatomic) IBOutlet UIImageView *imgBanner;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_title_left_padding;


@end
