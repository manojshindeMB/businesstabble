//
//  SearchAddressTableViewCell.h
//  BusinessTabble
//
//  Created by Liumin on 18/04/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchAddressTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;

@end
