//
//  SearchAddressViewController.m
//  BusinessTabble
//
//  Created by Liumin on 14/04/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import "SearchAddressViewController.h"
#import "SPGooglePlacesAutocompleteQuery.h"
#import "SPGooglePlacesAutocompletePlace.h"
#import "SearchAddressViewController.h"

#import "SearchAddressTableViewCell.h"

#import <GoogleMaps/GoogleMaps.h>

@interface SearchAddressViewController ()

@end

@implementation SearchAddressViewController
@synthesize searchQuery;
@synthesize locationManager;
@synthesize arrAddress;
@synthesize curLocationStr;

- (void)viewDidLoad {
    [super viewDidLoad];
    [_btnCurrentLocation.layer setCornerRadius:5.0f];
    [_btnCurrentLocation.layer setBorderWidth:1.0f];
    [_btnCurrentLocation.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_txtAddress setKeyboardAppearance:UIKeyboardAppearanceDark];
    _txtAddress.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    searchQuery = [[SPGooglePlacesAutocompleteQuery alloc] initWithApiKey:kGoogleAPIKey];
    searchQuery.radius = 99999.f;
    searchQuery.location = self.coordinate;
    self.address = @"";
    [self handleSearchForSearchString:self.address];
    locationManager = [[CLLocationManager alloc]init];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (IOS_VERSION >= 8.0) {
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
            
        } else {
            
        }
    } else {
        
    }
    if(![CLLocationManager locationServicesEnabled]){
        [SVProgressHUD showErrorWithStatus:@"You should enable your location service."];
    }
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    [locationManager startUpdatingLocation];
    locationManager.delegate = self;
    
    float latitude = locationManager.location.coordinate.latitude;
    float longitude = locationManager.location.coordinate.longitude;
    NSLog(@" current latitude %f, current longitude %f", latitude, longitude);
}
#pragma mark - Base Proc
- (void)handleSearchForSearchString:(NSString *)searchString {
    [self showProgress:@"Please wait..."];
    searchQuery.input = searchString;
    [searchQuery fetchPlaces:^(NSArray *places, NSError *error) {
        [self hideProgress];
        if (error) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Could not fetch Places" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
            [alert show];

        } else {
            arrAddress = [NSMutableArray arrayWithArray:places];
            [_tvAddressList reloadData];
        }
    }];

}
#pragma mark Actions
- (IBAction)onTouchBtnBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)onTouchBtnCurrentLocation:(id)sender
{
    [self getLocationInfo:locationManager.location];
}


#pragma mark LocationMethods
-(void)getLocationInfo:(CLLocation*) location
{
    
    CLGeocoder *geoCoder = [[CLGeocoder alloc]init];
    [self showProgress:@"Please wait..."];
    [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         [self hideProgress];
         if (error == nil && [placemarks count] > 0)
         {
             CLPlacemark * placemark = [placemarks lastObject];
             
             // strAdd -> take bydefault value nil
             NSString *strAdd = nil;
             
             if ([placemark.subThoroughfare length] != 0)
                 strAdd = placemark.subThoroughfare;
             
             if ([placemark.thoroughfare length] != 0)
             {
                 // strAdd -> store value of current location
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark thoroughfare]];
                 else
                 {
                     // strAdd -> store only this value,which is not null
                     strAdd = placemark.thoroughfare;
                 }
             }
             if ([placemark.locality length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark locality]];
                 else
                     strAdd = placemark.locality;
             }
             
             if ([placemark.administrativeArea length] != 0)
             {
                 if ([strAdd length] != 0)
                     strAdd = [NSString stringWithFormat:@"%@, %@",strAdd,[placemark administrativeArea]];
                 else
                     strAdd = placemark.administrativeArea;
             }
             curLocationStr = strAdd;
             _txtAddress.text = curLocationStr;
             [self handleSearchForSearchString : curLocationStr];
         }
     }];
}


- (void) onLocationManagerNotification {
    
//    CLLocation *location = [LocationManager currentLocation];
    
    [CATransaction begin];
    [CATransaction setAnimationDuration:1.0f];
}
#pragma mark - UITextDelegate
- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    //	NSString *address = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //	[self handleSearchForSearchString:address];
    
    return YES;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [self handleSearchForSearchString:_txtAddress.text];
    return YES;
}


#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrAddress count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55.f;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"SearchAddressTableViewCell";
    SearchAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    SPGooglePlacesAutocompletePlace *place = arrAddress[indexPath.row];
    cell.lblAddress.text = place.name;
    [cell.lblAddress setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];
    [cell.lblAddress setNumberOfLines:0];

//
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    [self showProgress:@"Please wait..."];
    SPGooglePlacesAutocompletePlace *place = self.arrAddress[indexPath.row];
    [place resolveToPlacemark:^(CLPlacemark *placemark, NSString *addressString, NSError *error) {
        [self hideProgress];
        if (error) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Could not map selected Place" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
            [alert show];
        } else if (placemark) {
            NSString *strAddress = place.name;
            CLLocationCoordinate2D coordinate = placemark.location.coordinate;
            _txtAddress.text = strAddress;
            if([self.delegate respondsToSelector:@selector(selectAddressViewController:didSelectedLocation:coordinate:)])
            {
                [self.delegate selectAddressViewController:self didSelectedLocation:strAddress coordinate:coordinate];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }
    }];
}
@end
