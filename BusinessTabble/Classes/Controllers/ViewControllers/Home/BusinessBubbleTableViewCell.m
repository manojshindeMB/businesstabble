//
//  BusinessBubbleTableViewCell.m
//  BusinessTabble
//
//  Created by Haichen Song on 16/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "BusinessBubbleTableViewCell.h"

@implementation BusinessBubbleTableViewCell
@synthesize imgBubble, lblBubbleName, lblDistance;
@synthesize imgBanner;
- (void)awakeFromNib {
    [self layoutIfNeeded];
    [imgBubble.layer setCornerRadius:imgBubble.frame.size.width / 2];
    [imgBubble.layer setMasksToBounds:YES];
    [imgBubble.layer setMasksToBounds:YES];
     [_btnActivityRate centerImageAndTitle:2.f];
    [imgBubble setContentMode:UIViewContentModeScaleAspectFill];
    [imgBubble.layer setMasksToBounds:YES];
    [imgBanner.layer setCornerRadius:3.f];
    [imgBanner.layer setMasksToBounds:YES];
    [imgBanner.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [imgBanner.layer setBorderWidth:2.f];
    [imgBanner.layer setMasksToBounds:YES];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
