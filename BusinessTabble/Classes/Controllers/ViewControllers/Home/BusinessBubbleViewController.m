//
//  BusinessBubbleViewController.m
//  BusinessTabble
//
//  Created by Liumin on 15/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "BusinessBubbleViewController.h"
#import "BubbleService.h"

#import <MagicalRecord/MagicalRecord.h>
#import "BusinessBubbleTableViewCell.h"
#import "BusinessBubbleDetailViewController.h"
#import "ActivityDetailViewController.h"
#import "EventBubbleSectionHeaderCell.h"
#import "EventDetailViewController.h"
#import "ChatViewController.h"

@interface BusinessBubbleViewController ()

@end
@implementation BusinessBubbleViewController
@synthesize searchBar;
@synthesize mArrBubbleList;
@synthesize mBubbleCategory = _mBubbleCategory;
@synthesize btnBubbleTypeAll, btnBubbleTypeBars, btnBubbleTypeEvents, btnBubbleTypeRestaurants, btnBubbleTypeRetail;
@synthesize btnShare;
@synthesize arrCollapseStatus;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customizeSearchBar];

    [btnBubbleTypeAll setSelected:YES];
    self.mBubbleCategory = @"Bars/Restaurants";
   
    [btnBubbleTypeAll centerImageAndTitle:2.f];
    [btnBubbleTypeBars centerImageAndTitle:2.f];
    [btnBubbleTypeEvents centerImageAndTitle:2.f];
    [btnBubbleTypeRestaurants centerImageAndTitle:2.f];
    [btnBubbleTypeRetail centerImageAndTitle:2.f];
    [btnShare centerImageAndTitle:4.f];
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_DATA object:nil];
    [self onTouchBtnCategory:btnBubbleTypeBars];
//    [btn setSelected:YES];
    
    
    [self.tableView setContentInset:UIEdgeInsetsMake(0,0,200,0)];
    [self reloadDataWithCategoryName:self.mBubbleCategory SearchString:searchBar.text];

    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.tableView;
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(handleTableViewRefresh:) forControlEvents:UIControlEventValueChanged];
    tableViewController.refreshControl = self.refreshControl;

}
- (void)handleTableViewRefresh:(id)sender
{
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_DATA object:nil];
    [[MainService sharedData] getBussinessBubbleListWithSuccessHandler:^{
        [self.refreshControl endRefreshing];
    } FailureHandler:^{
        [self.refreshControl endRefreshing];
    }];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
  
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)reloadDataWithCategoryName:(NSString *)strCategoryName
                      SearchString:(NSString *)strSearchString
{
    [self.tableView setHidden:NO];
    [_vwDescriptionContainer setHidden:YES];
    
    NSPredicate *predicate = nil;
    if(![[GlobalData sharedData]isEmpty:strCategoryName] && ![[GlobalData sharedData]isEmpty:strSearchString])
        predicate = [NSPredicate predicateWithFormat:@"category = %@ AND bubblename contains[c] %@", strCategoryName, strSearchString];
    else if([[GlobalData sharedData]isEmpty:strCategoryName] && ![[GlobalData sharedData]isEmpty:strSearchString])
        predicate = [NSPredicate predicateWithFormat:@"bubblename contains[c] %@", strSearchString];
    else if(![[GlobalData sharedData]isEmpty:strCategoryName] && [[GlobalData sharedData]isEmpty:strSearchString])
        predicate = [NSPredicate predicateWithFormat:@"category = %@ ", strCategoryName];
    else
        predicate = [NSPredicate predicateWithFormat:@"reviewstatus = 1 AND field_deleted = 0 AND (joined = nil OR joined = 0)"];
    NSPredicate *suffixPredicate = [NSPredicate predicateWithFormat:@"reviewstatus = 1 AND field_deleted = 0 AND (joined = nil OR joined = 0) AND (distance <= %ld) ", MAX_BUBBLE_DISTANCE];
    
    NSCompoundPredicate *compoundPredicate;
    compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate, suffixPredicate]];
    if([strCategoryName isEqualToString:@"Event"])
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd";
        NSString *strToday =[formatter stringFromDate:[NSDate date]];
        NSPredicate *eventSuffixPredicate = [NSPredicate predicateWithFormat:@"event_startdate >=%@", strToday];
//        NSPredicate *eventSuffixPredicate = [NSPredicate predicateWithFormat:@"event_startdate >=%d", 0];
        compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[compoundPredicate, eventSuffixPredicate]];
    }
    NSString *groupBy = nil;
    NSString *sortBy = @"distance";
    if([strCategoryName isEqualToString:@"Event"])
    {
        groupBy = @"event_startdate";
        sortBy = @"event_startdate";
    }
    
    self.fetchedResultsController = [Bubble MR_fetchAllSortedBy:sortBy ascending:YES withPredicate:compoundPredicate groupBy:groupBy
                                                       delegate:self];
//    [self.tableView reloadData];
    if([strCategoryName isEqualToString:@"Event"])
    {
        if(!arrCollapseStatus)
        {
            NSInteger sectionCount = [[self.fetchedResultsController sections]count];
            arrCollapseStatus = [[NSMutableArray alloc]init];
            for(int i = 0; i < sectionCount; i++)
            {
                arrCollapseStatus[i] = @"1";
            }
        }
    }
    
//    if([strCategoryName isEqualToString:@"Bars/Restaurants"] || [strCategoryName isEqualToString:@"Restaurants"] || [strCategoryName isEqualToString:@"Retail"])
//    {
//        if([strCategoryName isEqualToString:@"Bars/Restaurants"])
//        {
//            _lblDescription.text = @"Search for a Bar/Restaurant";
//        }
//        else if([strCategoryName isEqualToString:@"Restaurants"])
//        {
//            _lblDescription.text = @"Search for a Favorite Restaurant and \n Join their Place!";
//        }
//        else if([strCategoryName isEqualToString:@"Retail"])
//        {
//            _lblDescription.text = @"Search for a Retail Store";
//        }
//        if([[GlobalData sharedData]isEmpty:strSearchString])
//        {
////            [_vwDescriptionContainer setHidden:NO];
////            [self.tableView setHidden:YES];
//        }
//
//
//    }
    else
    {
        [self.tableView setHidden:NO];
        [_vwDescriptionContainer setHidden:YES];
    }
    [self.tableView setHidden:NO];
    if([[self.fetchedResultsController fetchedObjects] count] > 0 ) //&& ![[GlobalData sharedData] isEmpty:strSearchString]
    {
        [_vwEmpty setHidden:YES];
    }
    else
    {
        if(_vwEmpty.isHidden &&  ![[GlobalData sharedData] isEmpty:strSearchString])
            [_vwEmpty setHidden:YES];
        else
        {
            [_vwEmpty setHidden:NO];
            [self.tableView setHidden:YES];
            [self.view bringSubviewToFront:_vwEmpty];
        }
    }
    
    
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [super controllerDidChangeContent:controller];
    [self.tableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)customizeSearchBar
{
    CGRect rect = self.searchBar.frame;
    UIView *lineView1 = [[UIView alloc]initWithFrame:CGRectMake(0, rect.size.height-2, rect.size.width * 2, 2)];
    UIView *lineView2 = [[UIView alloc]initWithFrame:CGRectMake(0, 0,rect.size.width * 2, 2)];
    lineView1.backgroundColor = searchBar.barTintColor;
    lineView2.backgroundColor = searchBar.barTintColor;
    [self.searchBar addSubview:lineView1];
    [self.searchBar addSubview:lineView2];
    
    for (id object in [[[searchBar subviews] objectAtIndex:0] subviews])
    {
        if ([object isKindOfClass:[UITextField class]])
        {
            UITextField *textFieldObject = (UITextField *)object;
            textFieldObject.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            textFieldObject.layer.borderWidth = 1.0;
            [textFieldObject setFont:[UIFont systemFontOfSize:14.f]];
            [textFieldObject.layer setCornerRadius:10.f];
            [textFieldObject.layer setMasksToBounds:YES];
            break;
        }
    }
}
#pragma mark Actions
- (IBAction)onTouchBtnCategory:(id)sender {
    
    btnBubbleTypeRetail.selected = NO;
    btnBubbleTypeAll.selected = NO;
    btnBubbleTypeBars.selected = NO;
    btnBubbleTypeEvents.selected = NO;
    btnBubbleTypeRestaurants.selected = NO;
    [((UIButton *)sender) setSelected:YES];
    NSString *txtCategory = ((UIButton *)sender).titleLabel.text;
    if([txtCategory isEqualToString:@"Activity"]) txtCategory = @"Activity";
    else if([txtCategory isEqualToString:@"Events"]) txtCategory = @"Event";
    self.mBubbleCategory = txtCategory;
    [self reloadDataWithCategoryName:txtCategory SearchString:searchBar.text];
    
}
- (IBAction)onTouchBtnCopyPaste:(id)sender {
    [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Copied." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{        
        
    }];
}
#pragma mark Getter & Setter
-(void)setMBubbleCategory:(NSString *)mBubbleCategory
{
    _mBubbleCategory = mBubbleCategory;
    if([mBubbleCategory isEqualToString:@"Event"])
        searchBar.placeholder = @"Search Events";
    else if([mBubbleCategory isEqualToString:@"Activity"])
        searchBar.placeholder = @"Search Activities";
    else if([mBubbleCategory isEqualToString:@"Retail"])
        searchBar.placeholder = @"Search Retail Store";
    else
        searchBar.placeholder = @"Search Bars/Restaurants";
}
#pragma mark Data Methods


#pragma mark Search Delegate
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar1
{
    if([searchBar isEqual:searchBar1])
    {
        [searchBar resignFirstResponder];
        searchBar.text = @"";
    }
    [self reloadDataWithCategoryName:self.mBubbleCategory SearchString:searchBar.text];
}
- (void)searchBar:(UISearchBar *)searchBar1 textDidChange:(NSString *)searchText
{
    [self reloadDataWithCategoryName:self.mBubbleCategory SearchString:searchBar.text];
}
#pragma mark TableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.f;
//
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Bubble *currentBubble = self.fetchedResultsController.fetchedObjects[indexPath.row];
    if([self.mBubbleCategory isEqualToString:@"Event"])
        currentBubble =     [[[[self.fetchedResultsController sections] objectAtIndex:indexPath.section] objects] objectAtIndex:indexPath.row];
    NSString *CellIdentifier = @"BusinessBubbleTableViewCell";
    BusinessBubbleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [cell.lblBubbleName setText:currentBubble.bubblename];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell.lblDistance setText:[NSString stringWithFormat:@"%.1f Mile",[currentBubble.distance doubleValue]]];
    if([currentBubble.distance doubleValue] > 1)
        [cell.lblDistance setText:[NSString stringWithFormat:@"%.1f Miles",[currentBubble.distance doubleValue]]];
    
        
    [cell.imgBubble setHidden:NO];
    [cell.imgBanner setHidden:YES];
    cell.layout_constraint_title_left_padding.constant = 10;
    if([self.mBubbleCategory isEqualToString:@"Activity"])
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"activity_id == %@ AND status = 1", currentBubble.bubble_id];
        NSFetchedResultsController *theFetchedUsersController = [Activity_Like MR_fetchAllSortedBy:@"user_id" ascending:NO withPredicate:predicate groupBy:nil delegate:nil];
        [cell.lblActivityRate setText:[NSString stringWithFormat:@"%ld", [[theFetchedUsersController fetchedObjects] count]]];
        cell.layout_constraint_distance_bottom_space.constant = 0;
        [cell.btnActivityRate setHidden:NO];
        [cell.lblActivityRate setHidden:NO];
    }
    else if([self.mBubbleCategory isEqualToString:@"Event"])
    {
        NSString *photo_filename = [[GlobalData sharedData]urlencode:currentBubble.banner_filename];
        [[GlobalData sharedData]showImageWithImage:cell.imgBanner name:photo_filename placeholder:nil];
        [cell.imgBubble setHidden:YES];
        [cell.imgBanner setHidden:NO];
        [cell.btnActivityRate setHidden:YES];
        [cell.lblActivityRate setHidden:YES];
        cell.layout_constraint_distance_bottom_space.constant = 3;
        cell.layout_constraint_title_left_padding.constant = 60;
    }
    else{
        cell.layout_constraint_distance_bottom_space.constant = 24;
        [cell.btnActivityRate setHidden:YES];
        [cell.lblActivityRate setHidden:YES];
    }
    [cell.imgBubble.layer setBorderWidth:1.f];
    [cell.imgBubble.layer setBorderColor:[UIColor colorWithWhite:0.1 alpha:0.3].CGColor];
    
    NSString *photo_filename = [[GlobalData sharedData]urlencode:currentBubble.photo_filename];
    [[GlobalData sharedData]showImageWithImage:cell.imgBubble name:photo_filename placeholder:nil];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Bubble *currentBubble = self.fetchedResultsController.fetchedObjects[indexPath.row];
    if([self.mBubbleCategory isEqualToString:@"Event"])
        currentBubble =     [[[[self.fetchedResultsController sections] objectAtIndex:indexPath.section] objects] objectAtIndex:indexPath.row];
    if(!currentBubble)
        return;
    if([currentBubble.category isEqualToString:@"Activity"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activity" bundle:nil];
        ActivityDetailViewController * detailVC = [storyboard instantiateViewControllerWithIdentifier:@"ActivityDetailViewController"];
        detailVC.mCurBubble = currentBubble;
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:detailVC];
        [navController setNavigationBarHidden:YES];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
//        [self.navigationController pushViewController:navController animated:YES];
        return;
    }
    else if([currentBubble.category isEqualToString:@"Event"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Event" bundle:nil];
        EventDetailViewController * detailVC = [storyboard instantiateViewControllerWithIdentifier:@"EventDetailViewController"];
        detailVC.mCurBubble = currentBubble;
        UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:detailVC];
        [navController setNavigationBarHidden:YES];
        [self.navigationController presentViewController:navController animated:YES completion:nil];
        return;
    }
    BusinessBubbleDetailViewController * detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BusinessBubbleDetailViewController"];
    detailVC.mCurrentBubble = currentBubble;
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return 0;
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if([self.mBubbleCategory isEqualToString:@"Event"])
    {
        NSInteger sectionCount = [[self.fetchedResultsController sections]count];
        return sectionCount;
    }
    return 1;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(![self.mBubbleCategory isEqualToString:@"Event"])
    {
        return nil;
    }
    NSString *CellIdentifier = @"EventBubbleSectionHeaderCell";
    EventBubbleSectionHeaderCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd";
    NSDate *dtStartDate = [formatter dateFromString:[[[self.fetchedResultsController sections] objectAtIndex:section]name]];
    formatter.dateStyle = NSDateFormatterMediumStyle;
    formatter.timeStyle = NSDateFormatterNoStyle;
    
    NSString *prefix = @"";
    NSDateComponents *dtStartDateComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:dtStartDate];
    NSDateComponents *today = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    if([today day] == [dtStartDateComponents day] &&
       [today month] == [dtStartDateComponents month] &&
       [today year] == [dtStartDateComponents year] &&
       [today era] == [dtStartDateComponents era]) {
        prefix = @"Today ";
    }
    
    headerView.lblHeaderTitle.text = [NSString stringWithFormat:@"%@%@", prefix, [formatter stringFromDate:dtStartDate]];
    
    
    NSInteger objectCount = [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects];
    UIButton *btnClick = [[UIButton alloc]initWithFrame:headerView.frame];
    [btnClick setBackgroundColor:[UIColor clearColor]];
    btnClick.tag = section + 1;
    [btnClick addTarget:self action:@selector(sectionTapped:) forControlEvents:UIControlEventTouchDown];
    [headerView addSubview:btnClick];
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(![self.mBubbleCategory isEqualToString:@"Event"])
        return 0;
    return 35.f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if([self.mBubbleCategory isEqualToString:@"Event"])
    {
        if ([arrCollapseStatus[section] isEqualToString:@"1"]) {
            return [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects] + 0;
        } else {
            return [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects] + 0;
//            return 0;
        }
    }
    return [[self.fetchedResultsController fetchedObjects]count];
}
- (void)sectionTapped:(UIButton*)btn {
    NSInteger section = btn.tag - 1;
    if([arrCollapseStatus[section] isEqualToString:@"0"])
        arrCollapseStatus[section] = @"1";
    else
        arrCollapseStatus[section] = @"1";
    
    [UIView transitionWithView:self.tableView
                      duration:0.35f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void)
     {
         [self.tableView reloadData];
     }
                    completion:nil];
}

@end
