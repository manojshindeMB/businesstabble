//
//  MyBubbleTableViewCell.m
//  BusinessTabble
//
//  Created by Tian Ming on 10/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "MyBubbleNewTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation MyBubbleNewTableViewCell
@synthesize imgBubble,lblBubbleName,lblLastPost,lblPostDate;
@synthesize myBubbleDelegate;
@synthesize layout_constraint_message_left;

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _vwBubbleImageShadow.bounds;
    gradient.colors = @[(id)[[UIColor whiteColor] CGColor],
                        (id)[[UIColor lightGrayColor] CGColor]];
    [self layoutIfNeeded];
    [_vwBubbleImageShadow.layer setCornerRadius:_vwBubbleImageShadow.frame.size.width / 2];
    [_vwBubbleImageShadow.layer setMasksToBounds:YES];
    [_vwBubbleImageShadow.layer insertSublayer:gradient atIndex:0];
    
    [self layoutIfNeeded];
    [imgBubble.layer setCornerRadius:imgBubble.frame.size.width / 2];
    [imgBubble.layer setMasksToBounds:YES];
    [imgBubble.layer setBorderWidth:1.0f];
    [imgBubble.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [imgBubble.layer setShadowColor:[UIColor whiteColor].CGColor];
    [imgBubble.layer setShadowOpacity:1.f];
    [imgBubble.layer setShadowRadius:1.0];
    [imgBubble.layer setShadowOffset:CGSizeMake(1.0, 1.0)];
    imgBubble.clipsToBounds = YES;
    [imgBubble setContentMode:UIViewContentModeScaleAspectFill];
    
    // Configure the view for the selected state
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:lblLastPost.text];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    [style setLineSpacing:2.f];
    [style setAlignment:NSTextAlignmentCenter];
    [attributedString addAttribute:NSParagraphStyleAttributeName
                             value:style
                             range:NSMakeRange(0, lblLastPost.text.length)];
    lblLastPost.attributedText = attributedString;
    
    UITapGestureRecognizer *gesturePhoto = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapPhoto:)];
    gesturePhoto.delegate = self;
    [_imgLastMessagePhoto addGestureRecognizer:gesturePhoto];
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

     [super setSelected:selected animated:animated];

    //[lblBubbleName sizeToFit];
}

- (IBAction)onTouchBtnMap:(id)sender {
    if(!_mCurrentBubble)
        return;
    if([myBubbleDelegate respondsToSelector:@selector(onTapBubbleMap:)])
    {
        [myBubbleDelegate onTapBubbleMap:_mCurrentBubble];
    }
}
- (IBAction)onTouchBtnCall:(id)sender {
    if(!_mCurrentBubble)
        return;
    if([myBubbleDelegate respondsToSelector:@selector(onTapBubbleCall:)])
    {
        [myBubbleDelegate onTapBubbleCall:_mCurrentBubble];
    }
}
- (IBAction)onTouchBtnPhotosOrChat:(id)sender {
    if(!_mCurrentBubble)
        return;
    if([myBubbleDelegate respondsToSelector:@selector(onTapBubblePhotosOrChat:)])
    {
        [myBubbleDelegate onTapBubblePhotosOrChat:_mCurrentBubble];
    }
}
- (IBAction)onTouchBtnMenuOrInventory:(id)sender {
    if(!_mCurrentBubble)
        return;
    if([myBubbleDelegate respondsToSelector:@selector(onTapBubbleMenuOrInventory:)])
    {
        [myBubbleDelegate onTapBubbleMenuOrInventory:_mCurrentBubble];
    }
}
- (void) onTapPhoto:(UIImage *)image
{
    if(!_mCurrentBubble)
        return;
    if(!_imgLastMessagePhoto.image)
        return;
    if([myBubbleDelegate respondsToSelector:@selector(onTapPhoto:)])
        [myBubbleDelegate onTapPhoto:_imgLastMessagePhoto.image];
}
@end
