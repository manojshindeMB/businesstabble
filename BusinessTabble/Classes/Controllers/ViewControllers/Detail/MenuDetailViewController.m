//
//  MenuDetailViewController.m
//  BusinessTabble
//
//  Created by Haichen Song on 21/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "MenuDetailViewController.h"
#import "PostViewController.h"
#import "PageViewController.h"

#import "CoreDataViewController.h"
#import "BubbleSettingViewController.h"
#import "BubbleMenuViewController.h"
#import <MagicalRecord/MagicalRecord.h>
#import "BubbleMember+CoreDataProperties.h"
#import "MemberDetailViewController.h"
#import "BubbleSettingNavigationController.h"
#import "ChatNavigationController.h"
#import "MemberPostViewController.h"
#import "BubbleSettingUpdateViewController.h"
#import "BubbleMapViewController.h"
#import "BubbleMenuFullViewController.h"
#import "InventoryViewController.h"
@interface MenuDetailViewController ()

@end

@implementation MenuDetailViewController
@synthesize pageMenu;
@synthesize mCurrentBubble;
@synthesize imgBubble, lblTitle, switchAlert;
@synthesize imgBtnOurMenu, imgBtnWhosHere, imgBtnCallUs, imgBtnSetting;
@synthesize pageViewController;
@synthesize postVC, menuVC, settingVC, whosHereVC, whosHereNav, settingNav,chatNav, chatVC;
@synthesize mStrBubbleDetailPageType = _mStrBubbleDetailPageType;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    [imgBubble.layer setCornerRadius:imgBubble.frame.size.width / 2];
    [imgBubble.layer setMasksToBounds:YES];
    [imgBubble.layer setBorderWidth:1.f];
    [imgBubble.layer setBorderColor:[UIColor colorWithWhite:1.f alpha:1.f].CGColor];
    
    
    [switchAlert setFrame:CGRectMake(switchAlert.frame.origin.x, switchAlert.frame.origin.y, switchAlert.frame.size.width, switchAlert.frame.size.height - 5)];
    switchAlert.transform = CGAffineTransformMakeScale(0.9, 0.7);
    switchAlert.layer.cornerRadius = 16.0; // you must import QuartzCore to do this
    [switchAlert.layer setMasksToBounds:YES];

    if (switchAlert.on) {
        [switchAlert setThumbTintColor:[UIColor whiteColor]];
        [switchAlert setBackgroundColor:[UIColor whiteColor]];
        [switchAlert setOnTintColor:[UIColor colorWithRed:56/256.0 green:128/256.0 blue:255/256.0 alpha:1]];
    }else{
        [switchAlert setTintColor:[UIColor clearColor]];
        [switchAlert setThumbTintColor:[UIColor whiteColor]];
        [switchAlert setBackgroundColor:[UIColor grayColor]];
    }
    [self configureMenuButtons];
    
    // PageView Contents
    whosHereVC =  [self.storyboard instantiateViewControllerWithIdentifier:@"WhosHereViewController"];
    whosHereNav = [[WhosHereNavController alloc]initWithRootViewController:whosHereVC];
    [whosHereNav setNavigationBarHidden:YES];
    postVC =  [self.storyboard instantiateViewControllerWithIdentifier:@"PostViewController"];
    menuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BubbleMenuViewController"];
    settingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BubbleSettingViewController"];
    settingNav = [[BubbleSettingNavigationController alloc]initWithRootViewController:settingVC];
    [settingNav setNavigationBarHidden:YES];
    chatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
    
    chatNav = [[ChatNavigationController alloc]initWithRootViewController:chatVC];
    [chatNav setNavigationBarHidden:YES];
    
    UITapGestureRecognizer *recognizerBubbleName = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTouchBubbleName:)];
    [lblTitle addGestureRecognizer:recognizerBubbleName];
    recognizerBubbleName.delegate = self;
    
    [self fillBublbeNameImage:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(fillBublbeNameImage:) name:NOTIFICATION_RELOAD_BUBBLE_NAME_IMAGE object:nil];
    
}
- (void)fillBublbeNameImage:(id)obj
{
    lblTitle.text = mCurrentBubble.bubblename;
    NSString *photo_filename = [[GlobalData sharedData]urlencode:mCurrentBubble.photo_filename];
    [[GlobalData sharedData]showImageWithImage:imgBubble name:photo_filename placeholder:nil];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    _layout_constraint_sub_menu_height.constant = 45.f;
    [_vwSubMenu setHidden:NO];
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_TAB_BAR_HIDE object:nil];
    
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_TAB_BAR_SHOW object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:NOTIFICATION_RELOAD_BUBBLE_NAME_IMAGE];
    [super viewWillDisappear:animated];
    
}
- (void)configureMenuButtons
{
    // Top Menu buttons
    [imgBtnWhosHere centerImageAndTitle:2.f];
    [imgBtnOurMenu centerImageAndTitle:2.f];
    [imgBtnCallUs centerImageAndTitle:2.f];
    [imgBtnSetting centerImageAndTitle:2.f];
    
    if([[[GlobalData sharedData]mUserInfo].mId isEqualToString:[NSString stringWithFormat:@"%@", mCurrentBubble.creator_id ]])
    {
        [imgBtnWhosHere.imageView setAlpha:0.f];
        [imgBtnWhosHere.imageView setHidden:YES];
        UILabel *lblBubbleMemberCount = [[UILabel alloc]initWithFrame:CGRectMake(imgBtnWhosHere.imageView.frame.origin.x - 1, imgBtnWhosHere.imageView.frame.origin.y - 3, imgBtnWhosHere.imageView.frame.size.width + 2, imgBtnWhosHere.imageView.frame.size.height + 4)];
        [imgBtnWhosHere addSubview:lblBubbleMemberCount];
        [lblBubbleMemberCount setBackgroundColor:MAIN_COLOR];
        [lblBubbleMemberCount setText:[NSString stringWithFormat:@"%@",mCurrentBubble.membercount]];
        [lblBubbleMemberCount setTextColor:[UIColor whiteColor]];
        [lblBubbleMemberCount setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14.f]];
        [lblBubbleMemberCount setTextAlignment:NSTextAlignmentCenter];
    }
    
    [self setMenuButtonRightCornerShadow:imgBtnWhosHere];
    [self setMenuButtonRightCornerShadow:imgBtnOurMenu];
    [self setMenuButtonRightCornerShadow:imgBtnCallUs];
    [self setMenuButtonRightCornerShadow:imgBtnSetting];
    if([mCurrentBubble.category isEqualToString:@"Retail"] || [mCurrentBubble.category isEqualToString:@"Events"])
    {
        [imgBtnOurMenu setImage:[UIImage imageNamed:@"btn_eventphoto"]  forState:UIControlStateNormal];
        if([mCurrentBubble.category isEqualToString:@"Retail"])
            [imgBtnOurMenu setTitle:@"Store Photo" forState:UIControlStateNormal];
        else
            [imgBtnOurMenu setTitle:@"Event Photo" forState:UIControlStateNormal];
    }
    else
    {
        [imgBtnOurMenu setImage:[UIImage imageNamed:@"btn_ourmenu"]  forState:UIControlStateNormal];
        [imgBtnOurMenu setTitle:@" Our Menu" forState:UIControlStateNormal];
    }
    
    if(![self isUserChatAllowed])
    {
        
        [imgBtnSetting setTitle:@"Settings" forState:UIControlStateNormal];
        [imgBtnSetting setImage:[UIImage imageNamed:@"btn_bubblesetting"]  forState:UIControlStateNormal];
        
        if([[[GlobalData sharedData]mUserInfo].mId isEqualToString:[NSString stringWithFormat:@"%@", mCurrentBubble.creator_id ]])
        {
            [imgBtnWhosHere setTitle:@"Who's Here" forState:UIControlStateNormal];
            [imgBtnWhosHere setImage:[UIImage imageNamed:@"btn_callus"]  forState:UIControlStateNormal];
        }
        else{
            [imgBtnWhosHere setTitle:@"      Map      " forState:UIControlStateNormal];
            [imgBtnWhosHere setImage:[UIImage imageNamed:@"btn_map"]  forState:UIControlStateNormal];
            
            if([mCurrentBubble.category isEqualToString:@"Retail"] || [mCurrentBubble.category isEqualToString:@"Events"])
            {
                [imgBtnSetting setTitle:@"Inventory  " forState:UIControlStateNormal];
                [imgBtnSetting setImage:[UIImage imageNamed:@"btn_info_inventory_white"]  forState:UIControlStateNormal];
                [imgBtnSetting setHidden:YES];
            }
        }

    }
    else
    {
        [imgBtnWhosHere setTitle:@"      Map      " forState:UIControlStateNormal];
        [imgBtnWhosHere setImage:[UIImage imageNamed:@"btn_map"]  forState:UIControlStateNormal];
        [imgBtnSetting setTitle:@"  Chat " forState:UIControlStateNormal];
        [imgBtnSetting setImage:[UIImage imageNamed:@"btn_chat2"]  forState:UIControlStateNormal];

    }
    
    
  
    

}
- (BOOL)isUserChatAllowed
{
    if([[[GlobalData sharedData]mUserInfo].mId isEqualToString:[NSString stringWithFormat:@"%@", mCurrentBubble.creator_id ]])
        return NO;
    if([mCurrentBubble.category isEqualToString:@"Bars/Restaurants"])
        return YES;
    return NO;
        
}
- (void) setMenuButtonRightCornerShadow:(UIButton *)button
{
    CALayer *upperBorder = [CALayer layer];
    upperBorder.backgroundColor = [UIColor colorWithRed:167.0/255.0 green:140.0/255.0 blue:98.0/255.0 alpha:0.25].CGColor;
    upperBorder.frame = CGRectMake(CGRectGetWidth(button.frame) - 1, 0, 1.0f, button.frame.size.height);
    [upperBorder setShadowColor:[UIColor grayColor].CGColor];
    [upperBorder setShadowOffset:CGSizeMake(1.f, 1.f)];
    [upperBorder setShadowOpacity:0.25];
    [button.layer addSublayer:upperBorder];
}
- (void)setMStrBubbleDetailPageType:(NSString *)mStrBubbleDetailPageType
{
    _mStrBubbleDetailPageType = mStrBubbleDetailPageType;
}
#pragma mark Actions
- (IBAction)onTouchBtnBack:(id)sender {
     lblTitle.text = mCurrentBubble.bubblename;
    _layout_constraint_sub_menu_height.constant = 45.f;
    [_vwSubMenu setHidden:NO];
   UIViewController *currenctActiveController =  [pageViewController.viewControllers objectAtIndex:0];
   
    if([self.mStrBubbleDetailPageType isEqualToString:BUBBLE_DETAIL_PAGE_WHOSHERE])
    {
        if([currenctActiveController isKindOfClass:[WhosHereNavController class]])
        {
            if([whosHereNav.visibleViewController isKindOfClass:[MemberDetailViewController class]])
                [[NSNotificationCenter defaultCenter]postNotificationName:BUBBLE_DETAIL_PAGE_BACK_WHOSHERE object:BUBBLE_DETAIL_PAGE_BACK_WHOSHERE];
            else
                [self showPostPageView];
        }
        else
            [self showPostPageView];
    }
    else if([self.mStrBubbleDetailPageType isEqualToString:BUBBLE_DETAIL_PAGE_BUBBLEMENU])
        [self showPostPageView];
    else if([self.mStrBubbleDetailPageType isEqualToString:BUBBLE_DETAIL_PAGE_CHAT])
    {
        [self showPostPageView];
    }
    else if([self.mStrBubbleDetailPageType isEqualToString:BUBBLE_DETAIL_PAGE_BUBBLESETTING])
    {
        if([settingNav.visibleViewController isKindOfClass:[BubbleSettingUpdateViewController class]])
        {
             [[NSNotificationCenter defaultCenter]postNotificationName:BUBBLE_DETAIL_PAGE_BUBBLESETTING_UPDATE_BACK object:nil];
        }
        else if([settingNav.visibleViewController isKindOfClass:[MemberPostViewController class]])
        {
            [[NSNotificationCenter defaultCenter]postNotificationName:BUBBLE_DETAIL_PAGE_BUBBLESETTING_MEMBER_BACK object:nil];
        }
        else if([settingNav.visibleViewController isKindOfClass:[MemberDetailViewController class]])
        {
            [[NSNotificationCenter defaultCenter]postNotificationName:BUBBLE_DETAIL_PAGE_BACK_WHOSHERE object:nil];
        }
        else
            [self showPostPageView];
    }
    else
        [self.navigationController popViewControllerAnimated:YES];
}
- (void)onTouchBubbleName:(UIGestureRecognizer *)recognizer
{
//    NSLog(@"%@, %@", mCurrentBubble.latitude , mCurrentBubble.longitude);
//    if ([[UIApplication sharedApplication] canOpenURL:
//         [NSURL URLWithString:@"comgooglemaps://"]]) {
//        NSString *url = [NSString stringWithFormat:@"comgooglemaps://?center=%@,%@&zoom=14&views=traffic", mCurrentBubble.latitude, mCurrentBubble.longitude];
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
//    } else {
//        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Can not open google map in your device." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
//    }
    
//    BubbleMapViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BubbleMapViewController"];
//    mapVC.mCurrentBubble = mCurrentBubble;
//    [self presentViewController:mapVC animated:YES completion:nil];
    
}
- (IBAction)onTouchBtnWhosHere:(id)sender {
    if([[[GlobalData sharedData]mUserInfo].mId isEqualToString:[NSString stringWithFormat:@"%@", mCurrentBubble.creator_id ]])
    {
        whosHereVC.mCurrentBubble = mCurrentBubble;
        [pageViewController setViewControllers:@[whosHereNav] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:^(BOOL finished) {
            self.mStrBubbleDetailPageType = BUBBLE_DETAIL_PAGE_WHOSHERE;
        }];
    }
    else
    {
        BubbleMapViewController *mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BubbleMapViewController"];
        mapVC.mCurrentBubble = mCurrentBubble;
        [self presentViewController:mapVC animated:YES completion:nil];

    }
    
    
}
- (IBAction)onTouchBtnOurMenu:(id)sender {
//    menuVC.mCurrentBubble = mCurrentBubble;
//    [pageViewController setViewControllers:@[menuVC] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:^(BOOL finished) {
//        self.mStrBubbleDetailPageType = BUBBLE_DETAIL_PAGE_BUBBLEMENU;
//    }];
    BubbleMenuFullViewController *fullMenuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BubbleMenuFullViewController"];
    fullMenuVC.mCurrentBubble = mCurrentBubble;    
    [self presentViewController:fullMenuVC animated:YES completion:nil];
//
    
}
- (IBAction)onTouchBtnCallUs:(id)sender {
    NSString *phoneNumber = mCurrentBubble.contactnumber;
    if([[GlobalData sharedData]isEmpty:mCurrentBubble.contactnumber])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"The phone number is not set yet." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        
    }
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneNumber]]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneNumber]]];
    }
    else {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"This Device Doesn't Support Call Functionality" BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
    }
    
}
- (IBAction)onTouchBtnBubbleSetting:(id)sender {
    if(![self isUserChatAllowed])
    {
        if(![[[GlobalData sharedData]mUserInfo].mId isEqualToString:[NSString stringWithFormat:@"%@", mCurrentBubble.creator_id ]])
        {
            if([mCurrentBubble.category isEqualToString:@"Retail"] || [mCurrentBubble.category isEqualToString:@"Events"])
            {
                InventoryViewController *inventoryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InventoryViewController"];
                inventoryVC.currentBubble = mCurrentBubble;
                [self.navigationController pushViewController:inventoryVC animated:YES];
                return;
            }
        }
        settingVC.mCurrentBubble = mCurrentBubble;
        [pageViewController setViewControllers:@[settingNav] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:^(BOOL finished) {
            pageViewController.delegate  =self;
            self.mStrBubbleDetailPageType = BUBBLE_DETAIL_PAGE_BUBBLESETTING;
        }];
        
       
    }
    else{
        
        
//        _layout_constraint_sub_menu_height.constant = 0;
//        [_vwSubMenu setHidden:YES];
//         lblTitle.text = [NSString stringWithFormat:@"%@ Chat", mCurrentBubble.bubblename];
//        chatVC.mCurrentBubble = mCurrentBubble;
//        [pageViewController setViewControllers:@[chatNav] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:^(BOOL finished) {
//            pageViewController.delegate  =self;
//            self.mStrBubbleDetailPageType = BUBBLE_DETAIL_PAGE_CHAT;
//        }];
        chatVC.mCurrentBubble = mCurrentBubble;
        [self.navigationController pushViewController:chatVC animated:YES];
        
        
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ( [segue.identifier isEqualToString:@"pageViewController"]) {
        pageViewController = segue.destinationViewController ;
        [self showPostPageView];
    }
}
- (void)showPostPageView
{
    postVC =  [self.storyboard instantiateViewControllerWithIdentifier:@"PostViewController"];
    postVC.mCurrentBubble = mCurrentBubble;
    [pageViewController setViewControllers:@[postVC] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:^(BOOL finished) {
        self.mStrBubbleDetailPageType = BUBBLE_DETAIL_PAGE_POST_VIEW;
    }];
}
-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed
{
    
}

#pragma mark Alert Switch
- (IBAction)onSwitchStatusChange:(id)sender {
    if (switchAlert.on) {
        [switchAlert setThumbTintColor:[UIColor whiteColor]];
        [switchAlert setBackgroundColor:[UIColor whiteColor]];
        [switchAlert setOnTintColor:[UIColor colorWithRed:56/256.0 green:128/256.0 blue:255/256.0 alpha:1]];
    }else{
        [switchAlert setTintColor:[UIColor clearColor]];
        [switchAlert setThumbTintColor:[UIColor whiteColor]];
        [switchAlert setBackgroundColor:[UIColor grayColor]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark APIs

@end
