//
//  BubbleMapViewController.m
//  BusinessTabble
//
//  Created by Liumin on 19/04/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import "BubbleMapViewController.h"

@interface BubbleMapViewController ()

@end

@implementation BubbleMapViewController
@synthesize mCurrentBubble;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    [_imgBubble.layer setCornerRadius:_imgBubble.frame.size.width / 2];
    [_imgBubble.layer setMasksToBounds:YES];
    _lblBubbleName.text = mCurrentBubble.bubblename;
    NSString *photo_filename = [[GlobalData sharedData]urlencode:mCurrentBubble.photo_filename];
    [[GlobalData sharedData]showImageWithImage:_imgBubble name:photo_filename placeholder:nil];
    
//    _mapView.mapType = MKMapTypeStandard;
    [_mapView setShowsUserLocation:YES];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([mCurrentBubble.latitude floatValue], [mCurrentBubble.longitude floatValue]);
    [self createAndAddAnnotationForCoordinate:coordinate];
    [self showOverlay:coordinate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
- (IBAction)onTouchBtnBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)showOverlay : (CLLocationCoordinate2D)coordinate
{
    [_mapView removeOverlays:self.mapView.overlays];
    //Creating and adding an overlay that covers the entire mapView
//    CLLocationCoordinate2D worldPoints[6] = { {90,0},{90,180},{-90,180},{-90,0},{-90,-180},{90,-180}};
//    MKPolygon *worldSquare = [MKPolygon polygonWithCoordinates:worldPoints count:6];
//    [_mapView addOverlay:worldSquare];
    
    CGFloat distancePerSec = 11.9335938;
    CGFloat radius = 20;
    radius = radius * distancePerSec;
    CGFloat zoomRadius = radius + (radius/ 1609.344f * 1000);
    MKCircle *updatedCircle = [MKCircle circleWithCenterCoordinate:coordinate radius:radius];
    MKCoordinateRegion midRegion = MKCoordinateRegionMakeWithDistance(coordinate, zoomRadius, zoomRadius);
    
//    [_mapView addOverlay:updatedCircle];
    [_mapView setRegion: midRegion animated: YES];
}
-(id) createAndAddAnnotationForCoordinate : (CLLocationCoordinate2D)coordinate{
    
    MKPointAnnotation* annotation = [[MKPointAnnotation alloc]init];
    annotation.coordinate = coordinate;
    [_mapView addAnnotation:annotation];
    return annotation;
}
#pragma mark map view delegate
//Overridden method that adds the overlay
-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay{
    if([overlay isKindOfClass:[MKPolygon class]]){
        MKPolygonRenderer *polygonView = [[MKPolygonRenderer alloc]initWithOverlay:overlay];
        polygonView.fillColor = [UIColor blackColor];
        polygonView.alpha = 0.15;
        return polygonView;
    }
    if([overlay isKindOfClass:[MKCircle class]]){
        MKCircleRenderer *circleView = [[MKCircleRenderer alloc]initWithOverlay:overlay];
        //strokecolor and linewidth have to do with border of circle
        circleView.strokeColor = [UIColor blackColor];
        circleView.lineWidth = 2.0f;
        circleView.fillColor = [UIColor whiteColor];
        circleView.alpha = 0.3;
        return circleView;
    }
    if([overlay isKindOfClass:[MKPolyline class]]){
        MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc]initWithOverlay:overlay];
        //renderer.strokeColor = [UIColor redColor];
        renderer.strokeColor = [UIColor colorWithRed:18/255.0 green:121/255.0 blue:240/255.0 alpha:1];
        renderer.lineWidth = 6.0f;
        return renderer;
    }
    else
        return nil;
}

@end
