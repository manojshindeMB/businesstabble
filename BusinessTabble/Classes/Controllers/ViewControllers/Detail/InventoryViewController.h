//
//  InventoryViewController.h
//  Tabble Business
//
//  Created by Dong Jin Jo on 22/02/2018.
//  Copyright © 2018 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InventoryViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgBubble;
@property (strong, nonatomic) Bubble *currentBubble;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtSearch;

@end
