//
//  BubbleMapViewController.h
//  BusinessTabble
//
//  Created by Liumin on 19/04/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <AddressBook/AddressBook.h>
@interface BubbleMapViewController : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgBubble;
@property (weak, nonatomic) IBOutlet UILabel *lblBubbleName;
@property (nonatomic, strong) Bubble *mCurrentBubble;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end
