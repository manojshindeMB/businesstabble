//
//  InventoryViewController.m
//  Tabble Business
//
//  Created by Liu Jo on 22/02/2018.
//  Copyright © 2018 Liu. All rights reserved.
//

#import "InventoryViewController.h"

@interface InventoryViewController ()

@end

@implementation InventoryViewController
@synthesize currentBubble;
- (void)viewDidLoad {
    [super viewDidLoad];
    [_imgBubble.layer setCornerRadius:_imgBubble.frame.size.width / 2];
    [_imgBubble.layer setMasksToBounds:YES];
    [_imgBubble.layer setBorderWidth:1.f];
    [_imgBubble.layer setBorderColor:[UIColor colorWithWhite:1.f alpha:1.f].CGColor];
    NSString *photo_filename = [[GlobalData sharedData]urlencode:currentBubble.photo_filename];
    [[GlobalData sharedData]showImageWithImage:_imgBubble name:photo_filename placeholder:nil];
    [_txtSearch.layer setCornerRadius:10.f];
    
    NSString *placeholderText = [NSString stringWithFormat:@"Search Inventory for %@", currentBubble.bubblename];
    if ([_txtSearch respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor colorWithRed:255/255.f green:255/255.f blue:255/255.f alpha:0.5f];
        _txtSearch.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholderText attributes:@{NSForegroundColorAttributeName: color}];
    } else {
  
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_TAB_BAR_HIDE object:nil];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_TAB_BAR_SHOW object:nil];
    [super viewWillDisappear:animated];
}
- (IBAction)onTouchBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
