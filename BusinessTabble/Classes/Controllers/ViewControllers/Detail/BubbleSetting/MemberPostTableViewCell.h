//
//  WhosHereTableViewCell.h
//  BusinessTabble
//
//  Created by Haichen Song on 21/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BubbleMember+CoreDataProperties.h"
@protocol MemberPostDelegate <NSObject>
- (void) onShowMemberDetail:(BubbleMember* ) currentBubbleMember;
- (void) onChangeEnableStatus:(BubbleMember* ) currentBubbleMember;
@end

@interface MemberPostTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgMember;
@property (strong, nonatomic) BubbleMember *mCurrentBubbleMember;
@property (weak, nonatomic) IBOutlet UIButton *btnMemberName;
@property (nonatomic, strong) id<MemberPostDelegate> delegate;
@property (nonatomic) BOOL postEnabled;
@property (weak, nonatomic) IBOutlet UIButton *btnPostEnabled;


@end
