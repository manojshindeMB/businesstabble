//
//  WhosHereViewController.m
//  BusinessTabble
//
//  Created by Haichen Song on 21/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "MemberPostViewController.h"
#import "MemberPostTableViewCell.h"
#import "MemberDetailViewController.h"

@interface MemberPostViewController ()

@end

@implementation MemberPostViewController
@synthesize mCurrentBubble;
@synthesize searchBar;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self reloadDataWithCategoryName:@"" SearchString:@""];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onTouchBtnBack:) name:BUBBLE_DETAIL_PAGE_BUBBLESETTING_MEMBER_BACK object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)reloadDataWithCategoryName:(NSString *)strCategoryName
                      SearchString:(NSString *)strSearchString
{

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"joined = 1 AND bubble_id = %@ AND member_id != %@", mCurrentBubble.bubble_id, mCurrentBubble.creator_id];
    if(![[GlobalData sharedData]isEmpty:strSearchString])
    {
        predicate = [NSPredicate predicateWithFormat:@"joined = %@ AND bubble_id = %@ AND username contains[c] %@ AND member_id != %@",@1, mCurrentBubble.bubble_id, strSearchString, mCurrentBubble.creator_id];
    }

    self.fetchedResultsController = [BubbleMember MR_fetchAllSortedBy:@"joinedAt" ascending:NO withPredicate:predicate groupBy:nil delegate:self];
}
#pragma mark TableView Delegate
- (IBAction)onTouchBtnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BubbleMember *currentBubbleMember = self.fetchedResultsController.fetchedObjects[indexPath.row];
    NSString *CellIdentifier = @"MemberPostTableViewCell";
    MemberPostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.delegate = self;
    cell.mCurrentBubbleMember = currentBubbleMember;
    cell.postEnabled = [currentBubbleMember.postenabled boolValue];
    [cell.btnPostEnabled setSelected:cell.postEnabled];
    [cell.btnMemberName setTitle:currentBubbleMember.username forState:UIControlStateNormal];
    NSString *photo_filename = [[GlobalData sharedData]urlencode:currentBubbleMember.photo_filename];
    [[GlobalData sharedData]showImageWithImage:cell.imgMember name:photo_filename placeholder:nil];
    return cell;

}
#pragma mark Search Delegate
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar1
{
    if([searchBar isEqual:searchBar1])
    {
        [searchBar resignFirstResponder];
        searchBar.text = @"";
    }
    [self reloadDataWithCategoryName:@"" SearchString:searchBar.text];
}
- (void)searchBar:(UISearchBar *)searchBar1 textDidChange:(NSString *)searchText
{
    [self reloadDataWithCategoryName:@"" SearchString:searchBar.text];
}
#pragma mark Member Post delegate
- (void)onShowMemberDetail:(BubbleMember *)currentBubbleMember
{
    if(currentBubbleMember)
    {
        MemberDetailViewController *memberVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MemberDetailViewController"];
        memberVC.mCurrentMember = currentBubbleMember;
        [self.navigationController pushViewController:memberVC animated:YES];
    }
}
- (void)onChangeEnableStatus:(BubbleMember *)currentBubbleMember
{
    if(!currentBubbleMember)
        return;
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:currentBubbleMember.member_id forKey:@"member_id"];
    [params setObject:mCurrentBubble.bubble_id forKey:@"bubble_id"];
    [params setObject:[NSNumber numberWithBool:![currentBubbleMember.postenabled boolValue]] forKey:@"postenabled"];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[BubbleService sharedData]setMemberPostEnabledForBubbleWithDictionary:params success:^(id _responseObject) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Successfully set." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
            //[self.navigationController popViewControllerAnimated:YES];
        }];
        currentBubbleMember.postenabled = [NSNumber numberWithBool:![currentBubbleMember.postenabled boolValue]];
        //Need to reload bubble model due to postenabled change?
        [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreAndWait];
    } failure:^(NSInteger _errorCode) {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:^{
        }];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];
//
//    
}
@end
