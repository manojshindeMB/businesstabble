//
//  BubbleSettingViewController.h
//  BusinessTabble
//
//  Created by Haichen Song on 21/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BubbleSettingViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgCreator;
@property (weak, nonatomic) IBOutlet UIImageView *imgBubble;
@property (weak, nonatomic) IBOutlet UILabel *lblCreatorName;
@property (weak, nonatomic) IBOutlet UILabel *lblBubbleName;
@property (weak, nonatomic) IBOutlet UILabel *lblWebUrl;
@property (weak, nonatomic) IBOutlet UILabel *lblCategory;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblCallUs;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) Bubble *mCurrentBubble;
@end
