//
//  WhosHereViewController.h
//  BusinessTabble
//
//  Created by Haichen Song on 21/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataViewController.h"
#import "BubbleMember+CoreDataProperties.h"
#import "Bubble+CoreDataProperties.h"
#import <MagicalRecord/MagicalRecord.h>
#import "MemberPostTableViewCell.h"
@interface MemberPostViewController : CoreDataViewController <UISearchBarDelegate, MemberPostDelegate>
@property (nonatomic, strong) Bubble * mCurrentBubble;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end
