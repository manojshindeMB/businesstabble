//
//  WhosHereTableViewCell.m
//  BusinessTabble
//
//  Created by Haichen Song on 21/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "MemberPostTableViewCell.h"

@implementation MemberPostTableViewCell
@synthesize imgMember, btnMemberName;
@synthesize delegate, btnPostEnabled;
@synthesize postEnabled, mCurrentBubbleMember;
- (void)awakeFromNib {
    // Initialization code
    [self layoutIfNeeded];
    [imgMember.layer setCornerRadius:imgMember.frame.size.width / 2];
    [imgMember.layer setMasksToBounds:YES];
    [btnPostEnabled setSelected:postEnabled];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}
- (IBAction)onTapBtnMemberName:(id)sender {
    if([delegate respondsToSelector:@selector(onShowMemberDetail:)])
    {
        [delegate onShowMemberDetail:mCurrentBubbleMember];        
    }
}
- (IBAction)onTapBtnPostEnabled:(id)sender {
    if([delegate respondsToSelector:@selector(onChangeEnableStatus:)])
    {
        [btnPostEnabled setSelected:!btnPostEnabled.selected];
        [delegate onChangeEnableStatus:mCurrentBubbleMember];
    }
}

@end
