//
//  BubbleFormViewController.m
//  BusinessTabble
//
//  Created by Tian Ming on 14/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "BubbleSettingUpdateViewController.h"
#import "NIDropDown.h"
#import "HomeViewController.h"
#import "HomeTabBarController.h"
#import <MagicalRecord/MagicalRecord.h>
#import "UIButton+Custom.h"
#import "MemberPostViewController.h"
#import <SDWebImage/UIButton+WebCache.h>

#define MENU_BTN_START_TAG_NUM 101
#define ACTIONSHEET_TYPE_PHOTO 1
#define ACTIONSHEET_TYPE_MENU 2

@interface BubbleSettingUpdateViewController ()

@end

@implementation BubbleSettingUpdateViewController
@synthesize lblCreatorName, btnUploadLogo,btnSubmit,lblVerifyDescription,svMain,btnBubbleCategory,dropDownCategory, dropDownColor, btnCompanyColor, btnAllowUserPost;
@synthesize mImgBubble = _mImgBubble;
@synthesize mBubbleCategory = _mBubbleCategory;
@synthesize mComanyColor = _mComanyColor;
@synthesize mCurrentBubble = _mCurrentBubble;
@synthesize vwMenuContainer;
@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize locationCoordinate;
@synthesize mCurBubbleCompanyColorIndex = _mCurBubbleCompanyColorIndex;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[GlobalData sharedData]setIsBack:NO];
    btnUploadLogo.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    btnUploadLogo.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.view layoutIfNeeded];
    [btnUploadLogo.layer setCornerRadius:btnUploadLogo.frame.size.width / 2];
    [btnUploadLogo.layer setMasksToBounds:YES];
    [btnUploadLogo.layer setBorderWidth:2.f];
    [btnUploadLogo.layer setBorderColor:MAIN_COLOR.CGColor];
    for(int i = MENU_BTN_START_TAG_NUM; i <= MENU_BTN_START_TAG_NUM + 7; i++)
    {
        UIButton * pMenuBtn = [self.view viewWithTag:i];
        [pMenuBtn.layer setMasksToBounds:YES];
    }
    lblVerifyDescription.font = [UIFont italicSystemFontOfSize:12.0f];
    [btnSubmit.layer setBorderWidth:2.f];
    [btnSubmit.layer setBorderColor:[UIColor colorWithRed:67/255.0 green:173/255.0 blue:50/255.0 alpha:1.0].CGColor];
    [btnSubmit.layer setMasksToBounds:YES];
    
    [btnBubbleCategory.layer setBorderWidth:2.f];
    [btnBubbleCategory.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [btnCompanyColor.layer setBorderWidth:2.f];
    [btnCompanyColor.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    photoActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel"  destructiveButtonTitle:nil otherButtonTitles:@"From Camera",@"From Photo Library", nil];
    menuActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel"  destructiveButtonTitle:nil otherButtonTitles:@"From Camera",@"From Photo Library", nil];
    
    bubbleImageChanged = NO;
    [btnAllowUserPost drawRectForUnderline:btnAllowUserPost.frame];
    
    [vwMenuContainer.layer setBorderColor:[UIColor whiteColor].CGColor];
    [vwMenuContainer.layer setBorderWidth:1.f];
    currentActionSheetType = ACTIONSHEET_TYPE_PHOTO;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onTouchBtnBack:) name:BUBBLE_DETAIL_PAGE_BUBBLESETTING_UPDATE_BACK object:nil];
    
    [_btnCategoryBars.layer  setBorderWidth:3.f];
    [_btnCategoryBars.layer setCornerRadius:_btnCategoryBars.frame.size.width / 2];
    [_btnCategoryBars.layer setMasksToBounds:YES];
    [_btnCategoryBars.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [_btnCategoryRestaurants.layer  setBorderWidth:3.f];
    [_btnCategoryRestaurants.layer setCornerRadius:_btnCategoryRestaurants.frame.size.width / 2];
    [_btnCategoryRestaurants.layer setMasksToBounds:YES];
    [_btnCategoryRestaurants.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [_btnCategoryRetail.layer  setBorderWidth:3.f];
    [_btnCategoryRetail.layer setCornerRadius:_btnCategoryBars.frame.size.width / 2];
    [_btnCategoryRetail.layer setMasksToBounds:YES];
    [_btnCategoryRetail.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [_btnRemoveMenu1 setContentMode:UIViewContentModeScaleAspectFill];
    [_btnRemoveMenu2 setContentMode:UIViewContentModeScaleAspectFill];
    [_btnRemoveMenu3 setContentMode:UIViewContentModeScaleAspectFill];
    [_btnRemoveMenu4 setContentMode:UIViewContentModeScaleAspectFill];
    [_btnRemoveMenu5 setContentMode:UIViewContentModeScaleAspectFill];
    [_btnRemoveMenu6 setContentMode:UIViewContentModeScaleAspectFill];
    [_btnRemoveMenu7 setContentMode:UIViewContentModeScaleAspectFill];
    [_btnRemoveMenu8 setContentMode:UIViewContentModeScaleAspectFill];
    
    [_btnRemoveMenu1.layer setMasksToBounds:YES];
    [_btnRemoveMenu2.layer setMasksToBounds:YES];
    [_btnRemoveMenu3.layer setMasksToBounds:YES];
    [_btnRemoveMenu4.layer setMasksToBounds:YES];
    [_btnRemoveMenu5.layer setMasksToBounds:YES];
    [_btnRemoveMenu6.layer setMasksToBounds:YES];
    [_btnRemoveMenu7.layer setMasksToBounds:YES];
    [_btnRemoveMenu8.layer setMasksToBounds:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(!self.mCurrentBubble)
        return;
    if([[GlobalData sharedData]isBack])
    {
        [[GlobalData sharedData]setIsBack:NO];
        return;
    }
    [self setDefaultValues];
    [self reloadBubbleMenus];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];


}

- (NSFetchedResultsController *)fetchedResultsController {
    // Set up the fetched results controller if needed.
    if (_fetchedResultsController == nil) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"item_deleted = %@ AND bubble_id = %@", @0, self.mCurrentBubble.bubble_id];;
        self.fetchedResultsController = [BubbleMenu MR_fetchAllSortedBy:@"number" ascending:YES withPredicate:predicate groupBy:nil delegate:self];
    }
    return _fetchedResultsController;
}
- (BubbleMenu *)getParticularFetchedMenu :(NSInteger )menuNum
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"item_deleted = %@ AND bubble_id = %@ AND number = %@", @0, self.mCurrentBubble.bubble_id, [NSNumber numberWithInteger:menuNum]];
    NSArray *arrMenus = [BubbleMenu MR_fetchAllSortedBy:@"number" ascending:YES withPredicate:predicate groupBy:nil delegate:self].fetchedObjects;
    if([arrMenus count] > 0)
        return  [arrMenus objectAtIndex:0];
    return nil;
}
- (void)setDefaultValues
{
    if(![[GlobalData sharedData]isEmpty:[[GlobalData sharedData]currentCity]])
        [_txtLocation setText:[NSString stringWithFormat:@"%@, %@, %@",[[GlobalData sharedData]currentCity], [[GlobalData sharedData]currentState], [[GlobalData sharedData]currentCountry]]];
    if(self.mCurrentBubble.latitude != nil)
        locationCoordinate = CLLocationCoordinate2DMake([self.mCurrentBubble.latitude floatValue], [self.mCurrentBubble.longitude floatValue]);
    _txtBubbleName.text = self.mCurrentBubble.bubblename;
    _txtWebsite.text = self.mCurrentBubble.website;
    _txtContactNumber.text = self.mCurrentBubble.contactnumber;
    _txtDescription.text = self.mCurrentBubble.content;
    _txtPaypal.text = self.mCurrentBubble.paypal;
    
    if(![[GlobalData sharedData]isEmpty:self.mCurrentBubble.location])
        _txtLocation.text = self.mCurrentBubble.location;
    _txtOwner.text = self.mCurrentBubble.owner;
    self.mComanyColor = [UIColor colorWithRed:[self.mCurrentBubble.color_red floatValue] green:[self.mCurrentBubble.color_green floatValue] blue:[self.mCurrentBubble.color_blue floatValue] alpha:1];
    self.mBubbleCategory = self.mCurrentBubble.category;
    self.mCurBubbleCompanyColorIndex = 5;// [self.mCurrentBubble.color_index integerValue];
    [lblCreatorName setText:[NSString stringWithFormat:@"Creator: %@",self.mCurrentBubble.username]];
    UIImageView *tempBubbleImage = [[UIImageView alloc]init]; //sd_setBackgroundImage for button
    NSString *photo_filename = [[GlobalData sharedData]urlencode:self.mCurrentBubble.photo_filename];
    [tempBubbleImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",g_storagePrefix,photo_filename]]  placeholderImage:[UIImage imageNamed:@"placeholder.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.mImgBubble = image;
    }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Actions
- (IBAction)onTouchBtnBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onTouchBtnUploadImage:(id)sender {
    currentActionSheetType = ACTIONSHEET_TYPE_PHOTO;
    [photoActionSheet showInView:[self.view window]];
}
- (IBAction)onTouchBtnCategorySelected:(id)sender {
    if(dropDownColor)
    {
        [dropDownColor hideDropDown:btnCompanyColor];
        dropDownColor = nil;
    }
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:BUBBLE_CATEGORY_BAR, BUBBLE_CATEGORY_EVENTS, BUBBLE_CATEGORY_RESTAURANTS, BUBBLE_CATEGORY_RETAIL, nil];
    NSArray * arrImage = [[NSArray alloc] init];
    if(dropDownCategory == nil) {
        CGFloat f = 200;
        dropDownCategory = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down" type:DROP_DOWN_TYPE_TEXT selectedIndex:0];
        dropDownCategory.delegate = self;
    }
    else {
        [dropDownCategory hideDropDown:sender];
        dropDownCategory = nil;
    }
}
- (IBAction)onTouchBtnCompanyColor:(id)sender {
    if(dropDownCategory)
    {
        [dropDownCategory hideDropDown:btnBubbleCategory];
        dropDownCategory = nil;
    }

    NSArray * arr = [[NSArray alloc] init];
    arr = BUBBLE_COMPANY_COLORS;
    
    NSArray * arrImage = [[NSArray alloc] init];
    if(dropDownColor == nil) {
        CGFloat f = 200;
        dropDownColor = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down" type:DROP_DOWN_TYPE_COLOR selectedIndex:0];
        dropDownColor.delegate = self;
    }
    else {
        [dropDownColor hideDropDown:sender];
        dropDownColor = nil;
    }
}

- (IBAction)onTouchBtnSubmit:(id)sender {
    if([[GlobalData sharedData]isSessionExpired])
       [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
    if(!self.mCurrentBubble)
    {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    [self checkBubbleFormValidationWithSuccessHandler:^{
        NSDate *date = [NSDate date];
        NSString *strfilename = [NSString stringWithFormat:@"profile_%@%ld.jpg", [[GlobalData sharedData]mUserInfo].mId , (long)[date timeIntervalSince1970]];
        strfilename = [[GlobalData sharedData]urlencode:strfilename];
        NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
        [params setObject:[_txtBubbleName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  forKey:@"bubblename"];
        [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"creator_id"];
        [params setObject:_txtWebsite.text forKey:@"website"];
        [params setObject:self.mBubbleCategory forKey:@"category"];
        [params setObject:_txtLocation.text forKey:@"location"];
        [params setObject:[NSString stringWithFormat:@"%f",locationCoordinate.latitude] forKey:@"latitude"];
        [params setObject:[NSString stringWithFormat:@"%f",locationCoordinate.longitude] forKey:@"longitude"];
        
        [params setObject:_txtDescription.text forKey:@"description"];
        [params setObject:strfilename forKey:@"photo_filename"];
        [params setObject:[NSNumber numberWithBool:bubbleImageChanged] forKey:@"photo_changed"];
        [params setObject:_txtOwner.text forKey:@"owner"];
        [params setObject:_txtContactNumber.text forKey:@"contactnumber"];
        [params setObject:_txtPaypal.text forKey:@"paypal"];
        
        const CGFloat* components = CGColorGetComponents(self.mComanyColor.CGColor);    
        [params setObject:[NSString stringWithFormat:@"%f",components[0]] forKey:@"color_red"];
        [params setObject:[NSString stringWithFormat:@"%f",components[1]] forKey:@"color_green"];
        [params setObject:[NSString stringWithFormat:@"%f",components[2]] forKey:@"color_blue"];
        [params setObject:[NSString stringWithFormat:@"%@",self.mCurrentBubble.bubble_id] forKey:@"bubble_id"];
        [params setObject:[NSString stringWithFormat:@"%ld", self.mCurBubbleCompanyColorIndex] forKey:@"color_index"];
        [self showProgress:@"Please wait..."];
        [[BubbleService sharedData]updateBubbleWithDictionary:params success:^(id _responseObject) {
            [self hideProgress];
                [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Successfully updated." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
                    //[[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_DATA object:nil];

                }];
    
            self.mCurrentBubble.bubblename = [_txtBubbleName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            self.mCurrentBubble.category = self.mBubbleCategory;
            self.mCurrentBubble.website = _txtWebsite.text;
            self.mCurrentBubble.location = _txtLocation.text;
            self.mCurrentBubble.content = _txtDescription.text;
            self.mCurrentBubble.owner = _txtOwner.text;
            self.mCurrentBubble.contactnumber = _txtContactNumber.text;
            self.mCurrentBubble.color_red = [NSNumber numberWithFloat:components[0]];
            self.mCurrentBubble.color_green = [NSNumber numberWithFloat:components[1]];
            self.mCurrentBubble.color_blue = [NSNumber numberWithFloat:components[2]];
            self.mCurrentBubble.latitude = [NSNumber numberWithFloat: locationCoordinate.latitude];
            self.mCurrentBubble.longitude =[NSNumber numberWithFloat: locationCoordinate.longitude];
            if(bubbleImageChanged)
            {
                NSString *oldPhotoName = self.mCurrentBubble.photo_filename;
                
                bubbleImageChanged = NO;
                [self showProgress:@"Uploading photo..."];
                [[GlobalData sharedData] uploadPhotoWithName:strfilename oldname:self.mCurrentBubble.photo_filename image:self.mImgBubble type:@"" completion:^{
                    [self hideProgress];
                    self.mCurrentBubble.photo_filename = strfilename;
                    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
//                        [self.navigationController popViewControllerAnimated:YES];
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SHOW_HOME object:nil];
                    }];
                } FailureHandler:^{
                    [self hideProgress];
                    self.mCurrentBubble.photo_filename = oldPhotoName;
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }
            else
            {
                [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
//                    [self.navigationController popViewControllerAnimated:YES];
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_SHOW_HOME object:nil];
                }];
            }
        } failure:^(NSInteger _errorCode) {
            [self hideProgress];
            if(_errorCode == 400)
            {
                [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:[NSString stringWithFormat:@"There is already existing bubble '%@'.",_txtBubbleName.text] BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
                return;
            }
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        }];
    } FailureHandler:^{

    }];
}
- (IBAction)onTouchBtnMemberPost:(id)sender {
    MemberPostViewController *memberPostVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MemberPostViewController"];
    memberPostVC.mCurrentBubble = self.mCurrentBubble;
    [self.navigationController pushViewController:memberPostVC animated:YES];
}
- (IBAction)onTouchBtnCategory:(id)sender {
    UIButton *btn = (UIButton *)sender;
    if([btn isEqual:_btnCategoryBars])
        self.mBubbleCategory = BUBBLE_CATEGORY_BAR;
    else if([btn isEqual:_btnCategoryRetail])
        self.mBubbleCategory = BUBBLE_CATEGORY_RETAIL;
    else if([btn isEqual:_btnCategoryRestaurants])
        self.mBubbleCategory = BUBBLE_CATEGORY_RESTAURANTS;

}

#pragma mark Setter & Getter
- (void)setMBubbleCategory:(NSString *)mBubbleCategory
{
    _mBubbleCategory = mBubbleCategory;
    //    [btnBubbleCategory setTitle:mBubbleCategory forState:UIControlStateNormal];
    [_btnCategoryBars setBackgroundColor:[UIColor whiteColor]];
    _btnCategoryBars.selected = NO;
    [_btnCategoryRestaurants setBackgroundColor:[UIColor whiteColor]];
    _btnCategoryRestaurants.selected = NO;
    [_btnCategoryRetail setBackgroundColor:[UIColor whiteColor]];
    _btnCategoryRetail.selected = NO;
    
    if([mBubbleCategory isEqualToString:BUBBLE_CATEGORY_BAR])
    {
        [_btnCategoryBars setBackgroundColor:[UIColor blackColor]];
        _btnCategoryBars.selected = YES;
    }
    else if([mBubbleCategory isEqualToString:BUBBLE_CATEGORY_RESTAURANTS])
    {
        [_btnCategoryRestaurants setBackgroundColor:[UIColor blackColor]];
        _btnCategoryRestaurants.selected = YES;
    }
    else if([mBubbleCategory isEqualToString:BUBBLE_CATEGORY_RETAIL])
    {
        [_btnCategoryRetail setBackgroundColor:[UIColor blackColor]];
        _btnCategoryRetail.selected = YES;
    }
}
- (void)setMImgBubble:(UIImage *)mImgBubble
{
    _mImgBubble = mImgBubble;
    if(mImgBubble != nil)
    {
        [btnUploadLogo setBackgroundImage:mImgBubble forState:UIControlStateNormal];
        [btnUploadLogo setTitle:@"" forState:UIControlStateNormal];
        [btnUploadLogo setContentMode:UIViewContentModeScaleAspectFill];
        [btnUploadLogo.layer setMasksToBounds:YES];
    }
}
- (void)setMComanyColor:(UIColor *)mComanyColor
{
    _mComanyColor = mComanyColor;
    [btnCompanyColor setBackgroundColor:mComanyColor];
}
 - (void)setMCurBubbleCompanyColorIndex:(NSInteger)mCurBubbleCompanyColorIndex
{
    _mCurBubbleCompanyColorIndex = mCurBubbleCompanyColorIndex;
}
- (void)setMCurrentBubble:(Bubble *)mCurrentBubble
{
    _mCurrentBubble = mCurrentBubble;
}
#pragma mark Address Select Delegate
- (void)selectAddressViewController:(SearchAddressViewController *)viewController didSelectedLocation:(NSString *)address coordinate:(CLLocationCoordinate2D)coordinate
{
    locationCoordinate = coordinate;
    [_txtLocation setText:address];
    [[GlobalData sharedData]setIsBack:YES];
}
#pragma mark UITextField Delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([textField isEqual:_txtLocation])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SearchAddressViewController *searchVC = [storyboard instantiateViewControllerWithIdentifier:@"SearchAddressViewController"];
        searchVC.delegate = self;
        [self presentViewController:searchVC animated:YES completion:nil];
        return NO;
    }
    return YES;
}

-(void)niDropDownDelegateMethod:(NSInteger)index text:(NSObject *)text type:(NSInteger)type
{
    if(type == DROP_DOWN_TYPE_TEXT)
        self.mBubbleCategory = (NSString *)text;
    else if(type == DROP_DOWN_TYPE_COLOR)
    {
        self.mComanyColor = (UIColor *)text;
        self.mCurBubbleCompanyColorIndex = index;
    }
}
#pragma mark  actionsheet didDismissWithButtonIndex
- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ( buttonIndex == 2 )
        return;
    if ( actionSheet == photoActionSheet || actionSheet == menuActionSheet)
    {
        [[GlobalData sharedData]setIsBack:YES];
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.editing = YES;
        picker.videoMaximumDuration = kMaxVideoLenth;
        {
            if (buttonIndex == 0) {
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
            } else if (buttonIndex == 1) {
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            }
        }
        picker.mediaTypes = [NSArray arrayWithObjects: (NSString*)kUTTypeImage, nil];//[NSArray arrayWithObjects:(NSString*)kUTTypeMovie, (NSString*)kUTTypeImage, nil];
        [self presentViewController:picker animated:TRUE completion:nil ];
    }

    
}
#pragma mark didFinishPickingMediaWithInfo
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *imgtmp = [info objectForKey:UIImagePickerControllerOriginalImage];
    [[GlobalData sharedData]setIsBack:YES];
    if(!imgtmp || imgtmp == nil)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"You can only use photo." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        return;
    }
    if(currentActionSheetType != ACTIONSHEET_TYPE_MENU)
        imgtmp = [GlobalData scaleAndRotateImage:imgtmp];
    [self dismissViewControllerAnimated:YES completion:^{
        if(currentActionSheetType == ACTIONSHEET_TYPE_PHOTO)
        {

            PECropViewController *controller = [[PECropViewController alloc] init];
            controller.delegate = self;
            controller.image = imgtmp;
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                controller.modalPresentationStyle = UIModalPresentationFormSheet;
            }
            [controller setKeepingCropAspectRatio:NO];
            
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
            }
            [self presentViewController:navigationController animated:YES completion:NULL];
            
        }
        else if(currentActionSheetType == ACTIONSHEET_TYPE_MENU)
        {
            [[GlobalData sharedData]setIsBack:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
            if(!imgtmp) return;
            [currentMenuButton setTitle:@"" forState:UIControlStateNormal];
            [currentMenuButton setBackgroundImage:imgtmp forState:UIControlStateNormal];
            [self uploadMenuFile:(currentMenuButton.tag - MENU_BTN_START_TAG_NUM + 1) image:imgtmp];

        }
   
    }];
}
#pragma mark Cropper Delegate
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    [[GlobalData sharedData]setIsBack:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    if(currentActionSheetType == ACTIONSHEET_TYPE_PHOTO)
    {
        self.mImgBubble = croppedImage;
        bubbleImageChanged = YES;
    }
    else if(currentActionSheetType == ACTIONSHEET_TYPE_MENU)
    {
        if(!croppedImage) return;
        bubbleImageChanged = NO;
        [currentMenuButton setTitle:@"" forState:UIControlStateNormal];
        [currentMenuButton setBackgroundImage:croppedImage forState:UIControlStateNormal];
        [self uploadMenuFile:(currentMenuButton.tag - MENU_BTN_START_TAG_NUM + 1) image:croppedImage];
    }
    
}
- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark Other Methods
- (void)checkBubbleFormValidationWithSuccessHandler:(void (^)())successHandler FailureHandler:(void (^)())failureHandler
{
    if(dropDownColor)
    {
        [dropDownColor hideDropDown:btnCompanyColor];
        dropDownColor = nil;
    }
    if(dropDownCategory)
    {
        [dropDownCategory hideDropDown:btnBubbleCategory];
        dropDownCategory = nil;
    }
    if([[GlobalData sharedData]isEmpty:_txtBubbleName.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide bubble name." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if([[GlobalData sharedData]isEmpty:_txtWebsite.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide website." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if([[GlobalData sharedData]isEmpty:_txtLocation.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide location." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if([[GlobalData sharedData]isEmpty:_txtOwner.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide owner or authorized accociate." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if([[GlobalData sharedData]isEmpty:_txtContactNumber.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide contact number." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if(!self.mImgBubble)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide bubble image." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    successHandler();
}

#pragma mark Menu
- (IBAction)onTouchBtnRemoveBubble:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    NSInteger menuNum = btn.tag - 250;
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[_txtBubbleName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  forKey:@"bubblename"];
    [params setObject:self.mCurrentBubble.bubble_id forKey:@"bubble_id"];
    [params setObject:[NSString stringWithFormat:@"%ld", menuNum] forKey:@"menu_number"];
     [self showProgress:@"Please wait..."];
    [[BubbleService sharedData]removeBubbleMenuWithDictionary:params success:^(id _responseObject) {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Image successfully removed." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:nil];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MENU_LIST object:nil];
        [btn setHidden:YES];
        UIButton * btnCurrentMenu = [vwMenuContainer viewWithTag:menuNum + MENU_BTN_START_TAG_NUM - 1];
        [btnCurrentMenu setBackgroundImage:nil forState:UIControlStateNormal];
        [btnCurrentMenu setTitle:@"Tap to upload" forState:UIControlStateNormal];

        
    } failure:^(NSInteger _errorCode) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
    }];


}

- (IBAction)onTouchBtnMenu:(id)sender {
    currentActionSheetType = ACTIONSHEET_TYPE_MENU;
    UIButton *btn = (UIButton *)sender;
    currentMenuButton = btn;//[vwMenuContainer viewWithTag:btn.tag];
    [menuActionSheet showInView:[self.view window]];
}
- (void)uploadMenuFile: (NSInteger )menuNumber image:(UIImage *)image
{
    NSDate *date = [NSDate date];
    NSString *strfilename = [NSString stringWithFormat:@"%@_menu%ld_%ld.jpg",[[GlobalData sharedData] mUserInfo].mUserName, menuNumber,  (long)[date timeIntervalSince1970]];
    strfilename = [[GlobalData sharedData]urlencode:strfilename];
    [self showProgress:@"Please wait..."];
    NSString *oldMenuFileName = @"";
    BubbleMenu *oldMenu = [self getParticularFetchedMenu:menuNumber];
    if(oldMenu)
        oldMenuFileName = oldMenu.menu_filename;
    [[GlobalData sharedData]uploadPhotoWithName:strfilename oldname:oldMenuFileName image:image type:@"" completion:^{
        NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
        [params setObject:[_txtBubbleName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  forKey:@"bubblename"];
        [params setObject:self.mCurrentBubble.bubble_id forKey:@"bubble_id"];
        [params setObject:[NSString stringWithFormat:@"%ld", menuNumber] forKey:@"menu_number"];
        [params setObject:strfilename forKey:@"menu_filename"];
        [self hideProgress];
        [[BubbleService sharedData]updateBubbleMenuWithDictionary:params success:^(id _responseObject) {
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Image successfully uploaded." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:nil];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            UIButton *btnRemove = [vwMenuContainer viewWithTag:250 +menuNumber];
            [btnRemove setHidden:NO];
            [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MENU_LIST object:nil];
        } failure:^(NSInteger _errorCode) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        }];
    } FailureHandler:^{
          [self hideProgress];
    }];
}
- (void)reloadBubbleMenus
{
    NSArray *array = [self.fetchedResultsController fetchedObjects];
    [_btnRemoveMenu1 setHidden:YES];
    [_btnRemoveMenu2 setHidden:YES];
    [_btnRemoveMenu3 setHidden:YES];
    [_btnRemoveMenu4 setHidden:YES];
    [_btnRemoveMenu5 setHidden:YES];
    [_btnRemoveMenu6 setHidden:YES];
    [_btnRemoveMenu7 setHidden:YES];
    [_btnRemoveMenu8 setHidden:YES];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) , ^{
        NSInteger index = 0;
        for(BubbleMenu *currentMenu in array)
        {
            index++;
            if(!currentMenu.menu_id)
                continue;
            if([currentMenu.item_deleted integerValue] == 1)
                continue;
            UIButton * btnCurrentMenu = [vwMenuContainer viewWithTag:[currentMenu.number integerValue] + MENU_BTN_START_TAG_NUM - 1];
            
            NSString *curMenuFileName = currentMenu.menu_filename;
            curMenuFileName = [[GlobalData sharedData]urlencode:curMenuFileName];
            if(!curMenuFileName || [curMenuFileName isEqualToString:@""])
                continue;
            [btnCurrentMenu setBackgroundImage:nil forState:UIControlStateNormal];
            dispatch_async(dispatch_get_main_queue(), ^(void){
                if([currentMenu.number integerValue] == 8)
                {
                    NSLog(@"current");
                }
                [[SDImageCache sharedImageCache] removeImageForKey:[NSString stringWithFormat:@"%@%@",g_storagePrefix,curMenuFileName] fromDisk:YES];
                
//                [btnCurrentMenu sd_setBackgroundImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",g_storagePrefix,curMenuFileName]]  forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder.png"] options:SDWebImageProgressiveDownload completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                    if(image)
//                    {
//                        UIButton *btnRemove = [vwMenuContainer viewWithTag:(250 + [currentMenu.number integerValue])];
//                        [btnRemove setHidden:NO];
//                        [btnCurrentMenu setTitle:@"" forState:UIControlStateNormal];
//                    }
//                }]; 
                
                [btnCurrentMenu sd_setBackgroundImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",g_storagePrefix,curMenuFileName]]  forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"placeholder.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if(image)
                    {
                        UIButton *btnRemove = [vwMenuContainer viewWithTag:(250 + [currentMenu.number integerValue])];
                        [btnRemove setHidden:NO];
                        [btnCurrentMenu setTitle:@"" forState:UIControlStateNormal];
                    }
                }];
            });
        }
    });
}

//Fetched Controller Delegates
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    NSLog(@"will change");
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    NSLog(@"did change %@", anObject);
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {

}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    NSLog(@"did change content");

}
@end
