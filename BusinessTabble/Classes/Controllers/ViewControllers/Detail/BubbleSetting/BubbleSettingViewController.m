//
//  BubbleSettingViewController.m
//  BusinessTabble
//
//  Created by Haichen Song on 21/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "BubbleSettingViewController.h"
#import "BubbleSettingUpdateViewController.h"
@interface BubbleSettingViewController ()

@end

@implementation BubbleSettingViewController
@synthesize lblBubbleName, lblCallUs, lblCategory, lblCreatorName, lblDescription, lblLocation, lblWebUrl, imgBubble, imgCreator, btnEdit;
@synthesize mCurrentBubble;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    [imgCreator.layer setCornerRadius:imgCreator.frame.size.width / 2];
    [imgCreator.layer setMasksToBounds:YES];
    [imgBubble.layer setCornerRadius:imgBubble.frame.size.width / 2];
    [imgBubble.layer setMasksToBounds:YES];
    
    [imgBubble.layer setBorderWidth:1.f];
    [imgBubble.layer setBorderColor:[UIColor colorWithWhite:0.1 alpha:0.3].CGColor];
    [imgCreator.layer setBorderWidth:1.f];
    [imgCreator.layer setBorderColor:[UIColor colorWithWhite:0.1 alpha:0.3].CGColor];
    [imgBubble setContentMode:UIViewContentModeScaleAspectFill];
    [imgCreator setContentMode:UIViewContentModeScaleAspectFill];
    
    if(!mCurrentBubble)
        return;
    if([[[GlobalData sharedData]mUserInfo].mId isEqualToString:[NSString stringWithFormat:@"%@", mCurrentBubble.creator_id ]])
        [btnEdit setHidden:NO];
    else
        [btnEdit setHidden:YES];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadBubbleData];
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_NAME_IMAGE object:nil];
}
- (void)loadBubbleData
{
    lblBubbleName.text = mCurrentBubble.bubblename;
    lblCreatorName.text = mCurrentBubble.username;
    lblCategory.text = mCurrentBubble.category;
    lblDescription.text = mCurrentBubble.content;
    lblCallUs.text = mCurrentBubble.contactnumber;
    lblLocation.text = mCurrentBubble.location;
    lblWebUrl.text = mCurrentBubble.website;
    NSString *photo_filename = [[GlobalData sharedData]urlencode:mCurrentBubble.user_photo_filename];
    [[GlobalData sharedData]showImageWithImage:imgCreator name:photo_filename placeholder:nil];
    NSString *photo_filename2 = [[GlobalData sharedData]urlencode:mCurrentBubble.photo_filename];
    [[GlobalData sharedData]showImageWithImage:imgBubble name:photo_filename2 placeholder:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)onTouchBtnEdit:(id)sender {
    BubbleSettingUpdateViewController *bubbleSettingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BubbleSettingUpdateViewController"];
    bubbleSettingVC.mCurrentBubble = mCurrentBubble;
    [self.navigationController pushViewController:bubbleSettingVC animated:YES];
}


@end
