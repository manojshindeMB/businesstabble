//
//  BubbleFormViewController.h
//  BusinessTabble
//
//  Created by Tian Ming on 14/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGLDropDownMenu.h"
#import "NIDropDown.h"

#import "SearchAddressViewController.h"


@interface BubbleSettingUpdateViewController : UIViewController<NIDropDownDelegate,UITextFieldDelegate,UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, NSFetchedResultsControllerDelegate, SelectAddressViewControllerDelegate, PECropViewControllerDelegate>
{

    NSArray *arrBubbleCategory;
    BOOL bubbleImageChanged;
    UIActionSheet *photoActionSheet;
    UIActionSheet *menuActionSheet;
    UIButton *currentMenuButton;
    NSInteger currentActionSheetType;
}
@property (weak, nonatomic) IBOutlet UILabel *lblCreatorName;

@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UILabel *lblVerifyDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnUploadLogo;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtBubbleName;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtWebsite;

@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtLocation;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtOwner;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtContactNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnBubbleCategory;
@property (weak, nonatomic) IBOutlet UIScrollView *svMain;
@property (weak, nonatomic) IBOutlet UIButton *btnCompanyColor;
@property (weak, nonatomic) IBOutlet UIButton *btnAllowUserPost;

@property (strong, nonatomic)  NIDropDown *dropDownCategory;
@property (strong, nonatomic)  NIDropDown *dropDownColor;
@property (nonatomic) CLLocationCoordinate2D locationCoordinate;
// Menu
@property (weak, nonatomic) IBOutlet UIView *vwMenuContainer;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtPaypal;

@property (weak, nonatomic) IBOutlet UIButton *btnCategoryBars;
@property (weak, nonatomic) IBOutlet UIButton *btnCategoryRestaurants;
@property (weak, nonatomic) IBOutlet UIButton *btnCategoryRetail;

@property (weak, nonatomic) IBOutlet UIButton *btnRemoveMenu1;
@property (weak, nonatomic) IBOutlet UIButton *btnRemoveMenu2;
@property (weak, nonatomic) IBOutlet UIButton *btnRemoveMenu3;
@property (weak, nonatomic) IBOutlet UIButton *btnRemoveMenu4;
@property (weak, nonatomic) IBOutlet UIButton *btnRemoveMenu5;
@property (weak, nonatomic) IBOutlet UIButton *btnRemoveMenu6;
@property (weak, nonatomic) IBOutlet UIButton *btnRemoveMenu7;
@property (weak, nonatomic) IBOutlet UIButton *btnRemoveMenu8;

//Property
@property (nonatomic, strong) UIImage *mImgBubble;
@property (nonatomic, strong) NSString *mBubbleCategory;
@property (nonatomic, strong) UIColor *mComanyColor;
@property (nonatomic, strong) Bubble *mCurrentBubble;
@property (nonatomic) NSInteger mCurBubbleCompanyColorIndex;
@end
