//
//  MenuDetailViewController.h
//  BusinessTabble
//
//  Created by Haichen Song on 21/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageViewController.h"
#import "PostViewController.h"
#import "BubbleMenuViewController.h"
#import "BubbleSettingViewController.h"
#import "WhosHereViewController.h"
#import "WhosHere/WhosHereNavController.h"
#import "BubbleSettingNavigationController.h"
#import "ChatNavigationController.h"
#import "ChatViewController.h"

@interface MenuDetailViewController : UIViewController<UIGestureRecognizerDelegate, UIPageViewControllerDelegate, UIPageViewControllerDataSource>
@property (nonatomic) CAPSPageMenu *pageMenu;
@property (nonatomic, strong) Bubble *mCurrentBubble;

@property (weak, nonatomic) IBOutlet UIImageView *imgBubble;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UISwitch *switchAlert;

@property (weak, nonatomic) IBOutlet UIButton_Image *imgBtnWhosHere;
@property (weak, nonatomic) IBOutlet UIButton_Image *imgBtnCallUs;
@property (weak, nonatomic) IBOutlet UIButton_Image *imgBtnSetting;

@property (weak, nonatomic) IBOutlet UIButton_Image *imgBtnOurMenu;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_sub_menu_height;
@property (weak, nonatomic) IBOutlet UIView *vwSubMenu;
@property (weak, nonatomic)  PageViewController *pageViewController;
@property (nonatomic, strong) PostViewController *postVC;
@property (nonatomic, strong) WhosHereViewController *whosHereVC;
@property (nonatomic, strong) WhosHereNavController *whosHereNav;
@property (nonatomic, strong) BubbleMenuViewController *menuVC;
@property (nonatomic, strong) BubbleSettingNavigationController *settingNav;
@property (nonatomic, strong) BubbleSettingViewController *settingVC;
@property (nonatomic, strong) ChatNavigationController *chatNav;
@property (nonatomic, strong) ChatViewController *chatVC;

@property (nonatomic, strong) NSString *mStrBubbleDetailPageType;
@end
