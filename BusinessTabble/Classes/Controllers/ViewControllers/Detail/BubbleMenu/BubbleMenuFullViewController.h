//
//  BubbleMenuViewController.h
//  BusinessTabble
//
//  Created by Haichen Song on 21/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RMDemoGalleryViewController.h"
#import "RMGalleryTransition.h"

@interface BubbleMenuFullViewController : UIViewController<UIViewControllerTransitioningDelegate, UIScrollViewDelegate>
@property (nonatomic, strong) Bubble *mCurrentBubble;
@property (weak, nonatomic) IBOutlet UIScrollView *svContainer;
@property (weak, nonatomic) IBOutlet UIView *vwMainContent;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong) NSMutableArray *mArrImgBubbles;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_vwMenuContainer_Width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_vwMenuContainer_Height;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;

@end
