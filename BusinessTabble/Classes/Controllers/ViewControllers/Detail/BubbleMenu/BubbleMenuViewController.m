//
//  BubbleMenuViewController.m
//  BusinessTabble
//
//  Created by Haichen Song on 21/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "BubbleMenuViewController.h"
#import "UIImageView+ProgressView.h"
#define MENU_IMAGE_START_TAG 101
@interface BubbleMenuViewController ()


@end

@implementation BubbleMenuViewController
@synthesize vwMainContent, svContainer;
@synthesize mCurrentBubble;
@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize layout_constraint_vwMenuContainer_Height, layout_constraint_vwMenuContainer_Width;
@synthesize mArrImgBubbles;

- (void)viewDidLoad {
    [super viewDidLoad];
    mArrImgBubbles = [[NSMutableArray alloc]init];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MENU_LIST object:nil];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self reloadBubbleMenus];
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (NSFetchedResultsController *)fetchedResultsController {
    // Set up the fetched results controller if needed.
    if (_fetchedResultsController == nil) {

        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"item_deleted = %@ AND bubble_id = %@", @"0", self.mCurrentBubble.bubble_id];;
        self.fetchedResultsController = [BubbleMenu MR_fetchAllSortedBy:@"number" ascending:YES withPredicate:predicate groupBy:nil delegate:nil];
    }
    return _fetchedResultsController;
}
- (void)reloadBubbleMenus
{
    
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) , ^{
    NSInteger nCurrentMenuIndex = 0;
    CGFloat menuHeight = vwMainContent.frame.size.height;
    if(!menuHeight)
        return;
    NSArray *array = [self.fetchedResultsController fetchedObjects];
    [[SDImageCache sharedImageCache] clearMemory];

    [mArrImgBubbles removeAllObjects];
    for(BubbleMenu *currentMenu in array)
    {
        if(!currentMenu.menu_id)
            continue;
        NSString *curMenuFileName = currentMenu.menu_filename;
        NSLog(@"current bubble id %@", currentMenu.bubble_id);
        
        curMenuFileName = [[GlobalData sharedData]urlencode:curMenuFileName];
        if(!curMenuFileName || [curMenuFileName isEqualToString:@""])
            continue;

        UIView *vwCurrentMenuContainer = [[UIView alloc]initWithFrame:CGRectMake(nCurrentMenuIndex * SCREEN_WIDTH, 0, SCREEN_WIDTH, menuHeight)];
        UIImageView *imgCurrentMenu = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, menuHeight)];
        
        [imgCurrentMenu.layer setBorderWidth:2.f];
        [imgCurrentMenu.layer setBorderColor:[UIColor whiteColor].CGColor];
        [vwCurrentMenuContainer addSubview:imgCurrentMenu];
        
        
        [imgCurrentMenu setUserInteractionEnabled:NO];
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(thumbnailTapGestureRecognized:)];
        [imgCurrentMenu addGestureRecognizer:tapGesture];
        imgCurrentMenu.tag = nCurrentMenuIndex + MENU_IMAGE_START_TAG;

        [vwMainContent addSubview:vwCurrentMenuContainer];

        UIProgressView *progressView = [[UIProgressView alloc]initWithFrame:CGRectMake(0,0,SCREEN_WIDTH / 2, 50)];
        [progressView setCenter:imgCurrentMenu.center];

        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",g_storagePrefix,curMenuFileName]];
            [imgCurrentMenu sd_setImageWithURL:url placeholderImage:nil options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [imgCurrentMenu setImage:image];
                [imgCurrentMenu setUserInteractionEnabled:YES];
                [mArrImgBubbles addObject:imgCurrentMenu.image];
                [progressView setHidden:YES];
            } usingProgressView:progressView];
            
        });

        nCurrentMenuIndex++;
    }

    
    if(nCurrentMenuIndex > 0)
        layout_constraint_vwMenuContainer_Width.constant = (nCurrentMenuIndex- 1) * SCREEN_WIDTH;

}

- (void)presentGalleryWithImageAtIndex:(NSUInteger)index
{
    RMDemoGalleryViewController *galleryViewController = [RMDemoGalleryViewController new];
    galleryViewController.mArrImages = mArrImgBubbles;    
    galleryViewController.galleryIndex = index;
    
    // The gallery is designed to be presented in a navigation controller or on its own.
    UIViewController *viewControllerToPresent;
#if RM_NAVIGATION_CONTROLLER
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:galleryViewController];
    navigationController.toolbarHidden = YES;
    
    // When using a navigation controller the tap gesture toggles the navigation bar and toolbar. A way to dismiss the gallery must be provided.
    galleryViewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissGallery:)];
    
    viewControllerToPresent = navigationController;
#else
    viewControllerToPresent = galleryViewController;
#endif
    
    // Set the transitioning delegate. This is only necessary if you want to use RMGalleryTransition.
    viewControllerToPresent.transitioningDelegate = self;
    viewControllerToPresent.modalPresentationStyle = UIModalPresentationFullScreen;
    
    [self presentViewController:viewControllerToPresent animated:YES completion:nil];
}
- (void)dismissGallery:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)thumbnailTapGestureRecognized:(UIGestureRecognizer*)gestureRecognizer
{
    UIView *imageView = gestureRecognizer.view;
    NSUInteger index = imageView.tag - MENU_IMAGE_START_TAG;

    [self presentGalleryWithImageAtIndex:index];
}

#pragma mark UIViewControllerTransitioningDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source
{
    RMGalleryTransition *transition = [RMGalleryTransition new];
    transition.delegate = self;
    return transition;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    RMGalleryTransition *transition = [RMGalleryTransition new];
    transition.delegate = self;
    return transition;
}

#pragma mark RMGalleryTransitionDelegate

- (UIImageView*)galleryTransition:(RMGalleryTransition*)transition transitionImageViewForIndex:(NSUInteger)index
{
    return [vwMainContent viewWithTag:index + MENU_IMAGE_START_TAG];
}

- (CGSize)galleryTransition:(RMGalleryTransition*)transition estimatedSizeForIndex:(NSUInteger)index
{ // If the transition image is different than the one displayed in the gallery we need to provide its size
    UIImageView *imageView = [vwMainContent viewWithTag:index + MENU_IMAGE_START_TAG];
    const CGSize thumbnailSize = imageView.image.size;
    
    // In this example the final images are about 25 times bigger than the thumbnail
    const CGSize estimatedSize = CGSizeMake(thumbnailSize.width * 25, thumbnailSize.height * 25);
    return estimatedSize;
}


@end
