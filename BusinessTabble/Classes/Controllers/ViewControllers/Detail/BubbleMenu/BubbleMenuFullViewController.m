//
//  BubbleMenuViewController.m
//  BusinessTabble
//
//  Created by Liumin on 21/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "BubbleMenuFullViewController.h"
#import "UIImageView+ProgressView.h"
#define MENU_IMAGE_START_TAG 101
@interface BubbleMenuFullViewController ()


@end

@implementation BubbleMenuFullViewController
@synthesize vwMainContent, svContainer;
@synthesize mCurrentBubble;
@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize layout_constraint_vwMenuContainer_Height, layout_constraint_vwMenuContainer_Width;
@synthesize mArrImgBubbles;

- (void)viewDidLoad {
    [super viewDidLoad];
    mArrImgBubbles = [[NSMutableArray alloc]init];
    [self.view layoutIfNeeded];
    [_btnClose.layer setCornerRadius:_btnClose.frame.size.width / 2];
    [_btnClose.layer setMasksToBounds:YES];
    [_btnClose setHidden:NO];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MENU_LIST object:nil];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self reloadBubbleMenus];
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)onTouchBtnClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (NSFetchedResultsController *)fetchedResultsController {
    // Set up the fetched results controller if needed.
    if (_fetchedResultsController == nil) {

        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"item_deleted = %@ AND bubble_id = %@", @"0", self.mCurrentBubble.bubble_id];;
        self.fetchedResultsController = [BubbleMenu MR_fetchAllSortedBy:@"number" ascending:YES withPredicate:predicate groupBy:nil delegate:nil];
    }
    return _fetchedResultsController;
}
- (void)reloadBubbleMenus
{
    
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) , ^{
    NSInteger nCurrentMenuIndex = 0;
    CGFloat menuHeight = vwMainContent.frame.size.height;
    if(!menuHeight)
        return;
    NSArray *array = [self.fetchedResultsController fetchedObjects];
    [[SDImageCache sharedImageCache] clearMemory];

    [mArrImgBubbles removeAllObjects];
    for(BubbleMenu *currentMenu in array)
    {
        if(!currentMenu.menu_id)
            continue;
        NSString *curMenuFileName = currentMenu.menu_filename;
        NSLog(@"current bubble id %@", currentMenu.bubble_id);
        
        curMenuFileName = [[GlobalData sharedData]urlencode:curMenuFileName];
        if(!curMenuFileName || [curMenuFileName isEqualToString:@""])
            continue;

        UIView *vwCurrentMenuContainer = [[UIView alloc]initWithFrame:CGRectMake(nCurrentMenuIndex * SCREEN_WIDTH, 0, SCREEN_WIDTH, menuHeight)];
        UIScrollView *subscrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, menuHeight)];
        subscrollView.delegate = self;
        subscrollView.maximumZoomScale = 4.0;
        [subscrollView setClipsToBounds:YES];


        UIImageView *imgCurrentMenu = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, menuHeight)];
        [imgCurrentMenu.layer setBorderWidth:2.f];
        [imgCurrentMenu.layer setBorderColor:[UIColor whiteColor].CGColor];
        
        [subscrollView addSubview:imgCurrentMenu];
        [vwCurrentMenuContainer addSubview:subscrollView];
        
        
//        [imgCurrentMenu setUserInteractionEnabled:NO];
//        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(thumbnailTapGestureRecognized:)];
//        [imgCurrentMenu addGestureRecognizer:tapGesture];
        imgCurrentMenu.tag = nCurrentMenuIndex + MENU_IMAGE_START_TAG;

        [vwMainContent addSubview:vwCurrentMenuContainer];

        UIProgressView *progressView = [[UIProgressView alloc]initWithFrame:CGRectMake(0,0,SCREEN_WIDTH / 2, 50)];
        [progressView setCenter:imgCurrentMenu.center];

        dispatch_async(dispatch_get_main_queue(), ^(void){
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",g_storagePrefix,curMenuFileName]];
            [imgCurrentMenu sd_setImageWithURL:url placeholderImage:nil options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if(!image)
                    return;
                [imgCurrentMenu setImage:image];
                [imgCurrentMenu setUserInteractionEnabled:YES];
                [mArrImgBubbles addObject:imgCurrentMenu.image];
                [progressView setHidden:YES];
            } usingProgressView:progressView];
            
        });

        nCurrentMenuIndex++;
    }

    
    if(nCurrentMenuIndex > 0)
        layout_constraint_vwMenuContainer_Width.constant = (nCurrentMenuIndex- 1) * SCREEN_WIDTH;
    [_btnClose bringSubviewToFront:self.view];

}
- (void)dismissGallery:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    UIImageView *curImageView;
    for(UIView *subview in scrollView.subviews)
    {
        if([subview isKindOfClass:[UIImageView class]] && subview.tag > 100)
            curImageView = (UIImageView *)subview;
    }
    return curImageView;
}

@end
