//
//  SellItemViewController.m
//  BusinessTabble
//
//  Created by macbook on 26/06/2017.
//  Copyright © 2017 Liu. All rights reserved.
//

#import "SellItemViewController.h"

#define CONDITION_TYPE_NEW 1
#define CONDITION_TYPE_USED 2

#define SHIPPING_TYPE_FREE 1
#define SHIPPING_TYPE_PRICE 2

#define CHECKOUT_TYPE_PAYPAL 1
#define CHECKOUT_TYPE_URL 2
@interface SellItemViewController ()
{
    NSMutableArray *arrQuantities;
}
@end

@implementation SellItemViewController
@synthesize mAttachedPhoto = _mAttachedPhoto;
@synthesize mConditionType = _mConditionType;
@synthesize selectedQuantity = _selectedQuantity;
@synthesize mPrice = _mPrice;
@synthesize mShippingType = _mShippingType;
@synthesize mShippingPrice = _mShippingPrice;
@synthesize mCheckoutType = _mCheckoutType;
- (void)viewDidLoad {
    [super viewDidLoad];

    
    _txtContent.textAlignment = NSTextAlignmentLeft;
    _layout_constraint_image_height.constant = SCREEN_WIDTH - 30;
    photoActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel"  destructiveButtonTitle:nil otherButtonTitles:@"From Camera",@"From Photo Library", nil];
        [self.view layoutIfNeeded];
    arrQuantities = [[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9", @"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20", nil];
    [self configureSubViews];
    self.mAttachedPhoto = nil;
    self.mConditionType = CONDITION_TYPE_NEW;
    self.selectedQuantity = 0;
    self.mShippingType = SHIPPING_TYPE_FREE;
    self.mCheckoutType = CHECKOUT_TYPE_PAYPAL;
    
//    UITapGestureRecognizer *gestureRec = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapMainView:)];
//    [_vwContentView addGestureRecognizer:gestureRec];
    [_vwContentView setUserInteractionEnabled:YES];
    [self.view setUserInteractionEnabled:YES];
    
}
- (void)configureSubViews
{
    [_vwTextContainer.layer setCornerRadius:10.f];
    [_vwTextContainer.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_vwTextContainer.layer setBorderWidth:1.f];
    
    [_imgAttachedPhoto setHidden:YES];
    [_imgAttachedPhoto.layer setBorderWidth:1.f];
    [_imgAttachedPhoto.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_imgAttachedPhoto.layer setMasksToBounds:YES];
    [_imgAttachedPhoto setContentMode:UIViewContentModeScaleAspectFill];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:_btnPhoto.titleLabel.text];
    float spacing = 1.0f;
    [attributedString addAttribute:NSKernAttributeName
                             value:@(spacing)
                             range:NSMakeRange(0, [_btnPhoto.titleLabel.text length])];
    _btnPhoto.titleLabel.attributedText = attributedString;
    [_btnPhoto centerImageAndTitle:0.5];

    
    [_btnQuantity.layer setBorderColor:[UIColor grayColor].CGColor];
    [_btnQuantity.layer setBorderWidth:1.f];
    [_btnQuantity.layer setCornerRadius:3.f];
    [_txtPrice.layer setBorderColor:[UIColor grayColor].CGColor];
    [_txtPrice.layer setCornerRadius:3.f];
    [_txtPriceFloat.layer setBorderColor:[UIColor grayColor].CGColor];
    [_txtPriceFloat.layer setCornerRadius:3.f];
    [_txtPrice.layer setBorderWidth:1.f];
    [_txtPriceFloat.layer setBorderWidth:1.f];
    UIView *paddingViewRight = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtPrice.rightView = paddingViewRight;
    _txtPrice.rightViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingViewLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtPriceFloat.leftView = paddingViewLeft;
    _txtPriceFloat.leftViewMode = UITextFieldViewModeAlways;
    
    [_txtShippingPrice.layer setBorderColor:[UIColor grayColor].CGColor];
    [_txtShippingPrice.layer setCornerRadius:3.f];
    [_txtShippingPriceFloat.layer setBorderColor:[UIColor grayColor].CGColor];
    [_txtShippingPriceFloat.layer setCornerRadius:3.f];
    [_txtShippingPrice.layer setBorderWidth:1.f];
    [_txtShippingPriceFloat.layer setBorderWidth:1.f];

    [_txtCheckoutURL.layer setCornerRadius:3.f];
    [_txtCheckoutURL.layer setBorderWidth:1.f];
    [_txtCheckoutURL.layer setBorderColor:[UIColor grayColor].CGColor];

    UIView *paddingShippingViewRight = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtShippingPrice.rightView = paddingShippingViewRight;
    _txtShippingPrice.rightViewMode = UITextFieldViewModeAlways;
    
    UIView *paddingShippingViewLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtShippingPriceFloat.leftView = paddingShippingViewLeft;
    _txtShippingPriceFloat.leftViewMode = UITextFieldViewModeAlways;
    UIView *paddingCheckoutViewLeft = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _txtCheckoutURL.leftView = paddingCheckoutViewLeft;
    _txtCheckoutURL.leftViewMode = UITextFieldViewModeAlways;
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if(!self.mAttachedPhoto)
    {
        _layout_constraint_image_height.constant = 0;
        [_imgAttachedPhoto setHidden:YES];
        [_btnRemovePhoto setHidden:YES];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touches began");
//    UITouch *touch = [touches anyObject];
    CGPoint location = [[touches anyObject] locationInView:_vwPriceContainer];
//    CGRect fingerRect = CGRectMake(location.x-5, location.y-5, 10, 10);
    CGRect dropArea = CGRectMake(_btnQuantity.frame.origin.x, _btnQuantity.frame.origin.y, _btnQuantity.frame.size.width, 300);
    isReadyQuantityDrop = NO;
    if(!CGRectContainsPoint(dropArea, location))
       [self onTapMainView:nil];
    else
    {
        isReadyQuantityDrop = YES;
//        [_svMainContainer setUserInteractionEnabled:YES];
    }
    
}
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    
}
- (void)onTapMainView:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    if(![recognizer.view isKindOfClass:[NIDropDown class]])
    if(_dropDownQuantity != nil)
    {
        [_dropDownQuantity hideDropDown:_btnQuantity];
        _dropDownQuantity = nil;
//        [_svMainContainer setUserInteractionEnabled:YES];
    }

}
- (IBAction)onTouchBtnBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (IBAction)onTouchBtnUploadImage:(id)sender {
    [photoActionSheet showInView:[self.view window]];
}
- (IBAction)onTouchBtnRemovePhoto:(id)sender {
    self.mAttachedPhoto = nil;
}

- (IBAction)onTouchBtnQuantity:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = arrQuantities;

    NSArray * arrImage = [[NSArray alloc] init];
    if(_dropDownQuantity == nil) {
        CGFloat f = 200;
        _dropDownQuantity = [[NIDropDown alloc]showDropDown:_btnQuantity :&f :arr :arrImage :@"down" type:DROP_DOWN_TYPE_TEXT selectedIndex:self.selectedQuantity];
        _dropDownQuantity.delegate = self;
        _dropDownQuantity.tag = 101;
        [_dropDownQuantity setUserInteractionEnabled:YES];
//        [_svMainContainer setUserInteractionEnabled:NO];
    }
    else {
//        [_svMainContainer setUserInteractionEnabled:YES];
        [_dropDownQuantity hideDropDown:sender];
        _dropDownQuantity = nil;
    }
}
- (IBAction)onTouchBtnCondition:(id)sender {
    if([sender isEqual:_btnConditionNew])
        self.mConditionType = CONDITION_TYPE_NEW;
    else
        self.mConditionType = CONDITION_TYPE_USED;
}
- (IBAction)onTouchBtnShippingPrice:(id)sender {
    if([sender isEqual:_btnShippingFree])
        self.mShippingType = SHIPPING_TYPE_FREE;
    else
        self.mShippingType = SHIPPING_TYPE_PRICE;
}
- (IBAction)onTouchBtnCheckoutType:(id)sender {
    if([sender isEqual:_btnCheckoutTypePaypal])
        self.mCheckoutType = CHECKOUT_TYPE_PAYPAL;
    else
        self.mCheckoutType = CHECKOUT_TYPE_URL;
}
- (IBAction)onTouchBtnPost:(id)sender {
    UIButton *btnPost = (UIButton *)sender;
    [self checkFormValidationWithSuccessHandler:^{
        if([btnPost isEqual:_btnPostLater])
        {
            [self configureDateSelection];
            [self presentViewController:dateSelectionController animated:YES completion:nil];
            return;
        }
        [self postMessage:btnPost postTime:nil];

    } FailureHandler:^{
        return;
    }];
    }

- (void)configureDateSelection
{
    RMActionControllerStyle style = RMActionControllerStyleDefault;
    dateSelectionController = [RMDateSelectionViewController actionControllerWithStyle:style];
    dateSelectionController.title = @"Placez";
    dateSelectionController.message = @"\nPlease choose a post time and press 'Select' or 'Cancel'.";
    
    //You can enable or disable blur, bouncing and motion effects
    dateSelectionController.disableBouncingEffects = NO;
    dateSelectionController.disableBackgroundTaps = NO;
    dateSelectionController.disableBlurEffectsForContentView = NO;
    dateSelectionController.disableBlurEffectsForBackgroundView = NO;
    dateSelectionController.disableMotionEffects = NO;
    dateSelectionController.disableBlurEffects = NO;
    
    //You can access the actual UIDatePicker via the datePicker property
    dateSelectionController.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    dateSelectionController.datePicker.minuteInterval = 5;
    dateSelectionController.datePicker.date = [NSDate date];
    
    RMAction<RMActionController<UIDatePicker *> *> *selectAction = [RMAction<RMActionController<UIDatePicker *> *> actionWithTitle:@"Select" style:RMActionStyleDone andHandler:^(RMActionController<UIDatePicker *> *controller) {
        NSLog(@"Successfully selected date: %@", controller.contentView.date);
        NSDate *selectedDate = controller.contentView.date;
        if([selectedDate compare: [NSDate date]] == NSOrderedAscending)
        {
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"You should not select the date prior the moment." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
            return;
        }
        [self postMessage:_btnPostLater postTime:selectedDate];
    }];
    RMAction<RMActionController<UIDatePicker *> *> *cancelAction = [RMAction<RMActionController<UIDatePicker *> *> actionWithTitle:@"Cancel" style:RMActionStyleCancel andHandler:^(RMActionController<UIDatePicker *> *controller) {
    }];
    
    
    [dateSelectionController addAction:selectAction];
    [dateSelectionController addAction:cancelAction];
    
    RMAction<RMActionController<UIDatePicker *> *> *in15MinAction = [RMAction<RMActionController<UIDatePicker *> *> actionWithTitle:@"15 Min" style:RMActionStyleAdditional andHandler:^(RMActionController<UIDatePicker *> *controller) {
        controller.contentView.date = [NSDate dateWithTimeIntervalSinceNow:15*60];
    }];
    in15MinAction.dismissesActionController = NO;
    
    RMAction<RMActionController<UIDatePicker *> *> *in30MinAction = [RMAction<RMActionController<UIDatePicker *> *> actionWithTitle:@"30 Min" style:RMActionStyleAdditional andHandler:^(RMActionController<UIDatePicker *> *controller) {
        controller.contentView.date = [NSDate dateWithTimeIntervalSinceNow:30*60];
    }];
    in30MinAction.dismissesActionController = NO;
    
    RMAction<RMActionController<UIDatePicker *> *> *in45MinAction = [RMAction<RMActionController<UIDatePicker *> *> actionWithTitle:@"45 Min" style:RMActionStyleAdditional andHandler:^(RMActionController<UIDatePicker *> *controller) {
        controller.contentView.date = [NSDate dateWithTimeIntervalSinceNow:45*60];
    }];
    in45MinAction.dismissesActionController = NO;
    
    RMAction<RMActionController<UIDatePicker *> *> *in60MinAction = [RMAction<RMActionController<UIDatePicker *> *> actionWithTitle:@"60 Min" style:RMActionStyleAdditional andHandler:^(RMActionController<UIDatePicker *> *controller) {
        controller.contentView.date = [NSDate dateWithTimeIntervalSinceNow:60*60];
    }];
    in60MinAction.dismissesActionController = NO;
    
    RMGroupedAction<RMActionController<UIDatePicker *> *> *groupedAction = [RMGroupedAction<RMActionController<UIDatePicker *> *> actionWithStyle:RMActionStyleAdditional andActions:@[in15MinAction, in30MinAction, in45MinAction, in60MinAction]];
    
    [dateSelectionController addAction:groupedAction];
    
    RMAction<RMActionController<UIDatePicker *> *> *nowAction = [RMAction<RMActionController<UIDatePicker *> *> actionWithTitle:@"Now" style:RMActionStyleAdditional andHandler:^(RMActionController<UIDatePicker *> * _Nonnull controller) {
        controller.contentView.date = [NSDate date];
        NSLog(@"Now button tapped");
    }];
    nowAction.dismissesActionController = NO;
    [dateSelectionController addAction:nowAction];
    
}

#pragma mark DropDown Delegate
-(void)niDropDownDelegateMethod:(NSInteger)index text:(NSObject *)text type:(NSInteger)type
{
    [_dropDownQuantity hideDropDown:_btnQuantity];
    if(type == DROP_DOWN_TYPE_TEXT)
        self.selectedQuantity = index;
    
}
#pragma mark  actionsheet didDismissWithButtonIndex
- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 2)
        return;
    if (actionSheet == photoActionSheet)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.editing = YES;
//        [picker setAllowsEditing:YES];
        picker.videoMaximumDuration = kMaxVideoLenth;
        {
            if (buttonIndex == 0) {
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
            } else if (buttonIndex == 1) {
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            }
        }
        picker.mediaTypes = [NSArray arrayWithObjects: (NSString*)kUTTypeImage, nil];//[NSArray arrayWithObjects:(NSString*)kUTTypeMovie, (NSString*)kUTTypeImage, nil];
        [self presentViewController:picker animated:TRUE completion:nil ];
    }
}
#pragma mark didFinishPickingMediaWithInfo
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *imgtmp = [info objectForKey:UIImagePickerControllerOriginalImage];
    if(!imgtmp || imgtmp == nil)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"You can only use photo." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        return;
    }
    imgtmp = [GlobalData scaleAndRotateImage:imgtmp];
    [self dismissViewControllerAnimated:YES completion:^{
        PECropViewController *controller = [[PECropViewController alloc] init];
        controller.delegate = self;
        controller.image = imgtmp;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            controller.modalPresentationStyle = UIModalPresentationFormSheet;
        }
        [controller setKeepingCropAspectRatio:NO];
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        }
        [self presentViewController:navigationController animated:YES completion:NULL];
    }];
}
#pragma mark Cropper Delegate
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    self.mAttachedPhoto = croppedImage;
}
- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark TextView Delegates
- (void)textViewDidChange:(UITextView *)textView
{
    
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if([textField isEqual:_txtPrice] && range.length + range.location > textField.text.length)
        return NO;
    if([textField isEqual:_txtPriceFloat] && range.length + range.location > textField.text.length)
        return NO;
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if([textField isEqual:_txtPrice])
        return newLength <= 5;
    if([textField isEqual:_txtPriceFloat])
        return newLength <= 2;
    if([textField isEqual:_txtShippingPrice])
        return newLength <= 3;
    if([textField isEqual:_txtShippingPriceFloat])
        return newLength <= 2;
    return YES;

}
#pragma mark Property Getter & Setter
- (void)setMAttachedPhoto:(UIImage *)mAttachedPhoto
{
    _mAttachedPhoto = mAttachedPhoto;
    if(self.mAttachedPhoto)
    {
        _layout_constraint_image_height.constant = SCREEN_WIDTH - 30;
        [_imgAttachedPhoto setHidden:NO];
        [_imgAttachedPhoto setImage:mAttachedPhoto];
        [_btnRemovePhoto setHidden:NO];
    }
    else
    {
        _layout_constraint_image_height.constant = 0;
        [_imgAttachedPhoto setHidden:YES];
        [_btnRemovePhoto setHidden:YES];
        [_imgAttachedPhoto setImage:nil];
    }
}
-(void)setMConditionType:(NSInteger)mConditionType
{
    _mConditionType = mConditionType;
    if(mConditionType == CONDITION_TYPE_NEW)
    {
        [_btnConditionNew setSelected:YES];
        [_btnConditionUsed setSelected:NO];
    }
    else if(mConditionType == CONDITION_TYPE_USED)
    {
        [_btnConditionUsed setSelected:YES];
        [_btnConditionNew setSelected:NO];
    }
}
- (void)setMShippingType:(NSInteger)mShippingType
{
    _mShippingType = mShippingType;
    if(mShippingType == SHIPPING_TYPE_FREE)
    {
        [_btnShippingFree setSelected:YES];
        [_btnShippingCost setSelected:NO];
    }
    else if(mShippingType == SHIPPING_TYPE_PRICE)
    {
        [_btnShippingFree setSelected:NO];
        [_btnShippingCost setSelected:YES];
    }
}
- (void)setMCheckoutType:(NSInteger)mCheckoutType
{
    _mCheckoutType = mCheckoutType;
    if(mCheckoutType == CHECKOUT_TYPE_PAYPAL)
    {
        [_btnCheckoutTypePaypal setSelected:YES];
        [_btnCheckoutTypeURL setSelected:NO];
    }
    else if(mCheckoutType == CHECKOUT_TYPE_URL)
    {
        [_btnCheckoutTypePaypal setSelected:NO];
        [_btnCheckoutTypeURL setSelected:YES];
    }
}
- (void)setSelectedQuantity:(NSInteger )selectedQuantity
{
    _selectedQuantity = selectedQuantity;
    [_btnQuantity setTitle:[arrQuantities objectAtIndex:selectedQuantity] forState:UIControlStateNormal];
}
- (void)setMPrice:(float)mPrice
{
    _mPrice = mPrice;
}
- (void)setMShippingPrice:(float)mShippingPrice
{
    _mShippingPrice = mShippingPrice;
}

#pragma mark Other Methods
- (void)checkFormValidationWithSuccessHandler:(void (^)())successHandler FailureHandler:(void (^)())failureHandler
{
    if(_dropDownQuantity)
    {
        [_dropDownQuantity hideDropDown:_btnQuantity];
        _dropDownQuantity = nil;
    }
    if([[GlobalData sharedData]isEmpty: [_txtContent.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]] && !self.mAttachedPhoto)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please write or attach photo." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if([[GlobalData sharedData]isEmpty:_txtPrice.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide price." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if(self.mShippingType == SHIPPING_TYPE_PRICE && [[GlobalData sharedData]isEmpty:_txtShippingPrice.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide shipping cost." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if(self.mCheckoutType == CHECKOUT_TYPE_URL && [[GlobalData sharedData]isEmpty:_txtCheckoutURL.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide website URL." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    successHandler();
    
}
- (void)postMessage:(UIButton *)button postTime:(NSDate *)postTime
{
    
    NSString *postTimeStamp = @"0";
    NSString *bApproved = @"1";
    NSString *photo_filename = @"";
    NSString *coupon = @"0";
    if([button isEqual:_btnPostNow])
    {
        bApproved = @"1";
        NSTimeInterval interval  = [[NSDate date] timeIntervalSince1970] ;
        postTimeStamp = [NSString stringWithFormat:@"%f", interval];
    }
    else if([button isEqual:_btnPostNo])
        bApproved = @"-1";
    else if([button isEqual:_btnPostLater])
    {
        bApproved = @"0";
        NSTimeInterval interval  = [postTime timeIntervalSince1970] ;
        postTimeStamp = [NSString stringWithFormat:@"%f", interval];
    }
    if(self.mAttachedPhoto)
    {
        NSDate *date = [NSDate date];
        NSString *strfilename = [NSString stringWithFormat:@"sellitem%@%ld.jpg", [[GlobalData sharedData]mUserInfo].mId , (long)[date timeIntervalSince1970]];
        photo_filename = [[GlobalData sharedData]urlencode:strfilename];
    }
    if(_mPostType == POST_TYPE_SELL_ITEM)
        coupon = @"2";
    
    float shippingCost = 0;
    float price = [_txtPrice.text floatValue];
    
    if(![[GlobalData sharedData]isEmpty:_txtPriceFloat.text])
    {
        price = price + [[NSString stringWithFormat:@"0.%@", _txtPriceFloat.text] floatValue];
    }
    if(self.mShippingType == SHIPPING_TYPE_PRICE)
    {
        shippingCost = [_txtShippingPrice.text floatValue];
        if(![[GlobalData sharedData]isEmpty:_txtShippingPriceFloat.text])
        {
            shippingCost = shippingCost + [[NSString stringWithFormat:@"0.%@", _txtShippingPriceFloat.text] floatValue];
        }
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:bApproved forKey:@"approved"];
    [params setObject:[_txtContent.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"text"];
    [params setObject:postTimeStamp forKey:@"approveFrom"];
    [params setObject:coupon forKey:@"coupon"];
    
    [params setObject:[NSString stringWithFormat:@"%f", price] forKey:@"price"];
    [params setObject:[NSString stringWithFormat:@"%f", shippingCost] forKey:@"shipping"];
    [params setObject:[NSString stringWithFormat:@"%ld", self.mCheckoutType] forKey:@"checkout"];
    [params setObject:_txtCheckoutURL.text forKey:@"checkout_url"];
    [params setObject:@"1" forKey:@"sell_item"];
    [params setObject:[NSString stringWithFormat:@"%@", [arrQuantities objectAtIndex:self.selectedQuantity] ] forKey:@"quantity"];
    
    [params setObject:self.mCurrentBubble.bubble_id forKey:@"bubble_id"];
    [params setObject:photo_filename forKey:@"photo_filename"];
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self showProgress:@"Please wait..."];
    [[MessageService sharedData]createMessageWithDictionary:params success:^(id _responseObject) {
        if([button isEqual:_btnPostNo])
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"This post has been disabled." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:nil];
        else
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Successfully created" BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:nil];
        [self hideProgress];
        if(self.mAttachedPhoto)
        {
             [self showProgress:@"Uploading photo..."];
            [[GlobalData sharedData]uploadPhotoWithName:photo_filename oldname:@"" image:self.mAttachedPhoto type:@"" completion:^{
                [self onSuccessfulCreatingNewPost];
            } FailureHandler:^{
                [[GlobalData sharedData]uploadPhotoWithName:photo_filename oldname:@"" image:self.mAttachedPhoto type:@"" completion:^{
                    [self onSuccessfulCreatingNewPost];
                } FailureHandler:^{
                     [self onSuccessfulCreatingNewPost];
                }];
            }];
        }
        else
        {
              [self hideProgress];
            [self onSuccessfulCreatingNewPost];
        }
        
        
        
        return;
    } failure:^(NSInteger _errorCode) {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
          [self hideProgress];
    }];
    
}
- (void)onSuccessfulCreatingNewPost
{
//    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    [self hideProgress];
    
    [[MainService sharedData]getBubbleMessageListWithSuccessHandler:^{
        [[MainService sharedData]getBussinessBubbleListWithSuccessHandler:^{
            [self dismissViewControllerAnimated:YES completion:nil];
        } FailureHandler:^{
        }];
    } FailureHandler:^{
    }];
}
@end
