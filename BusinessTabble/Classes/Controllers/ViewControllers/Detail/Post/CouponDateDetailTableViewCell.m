//
//  CouponDateDetailTableViewCell.m
//  BusinessTabble
//
//  Created by Tian Ming on 05/01/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import "CouponDateDetailTableViewCell.h"

@implementation CouponDateDetailTableViewCell
@synthesize imgUserPhoto;
- (void)awakeFromNib {
    [self layoutIfNeeded];
    [imgUserPhoto.layer setCornerRadius:imgUserPhoto.frame.size.width / 2];
    [imgUserPhoto.layer setMasksToBounds:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
