//
//  WEPopoverContentViewController.h
//  WEPopover
//
//  Created by Werner Altewischer on 06/11/10.
//  Copyright 2010 Werner IT Consultancy. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol JPopOverDelegate
@optional;
    -(void) popoverItemSelected: (NSInteger )index;
@end


@interface WEPopoverContentViewController : UITableViewController {

}
@property (nonatomic, assign) id<JPopOverDelegate>   delegate;
@property (nonatomic, strong) NSMutableArray *mArrCurrencies;
@property (nonatomic) NSInteger     mCellNumber;

@end
