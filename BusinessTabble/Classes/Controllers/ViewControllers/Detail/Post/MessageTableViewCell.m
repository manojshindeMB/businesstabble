//
//  MessageTableViewCell.m
//  BusinessTabble
//
//  Created by Tian Ming on 30/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "MessageTableViewCell.h"

@implementation MessageTableViewCell
@synthesize lblApproveFrom, lblText, lblUserName, imgPhoto, imgUserPhoto, layout_constraint_message_image_width;
@synthesize delegate, mCurrentMessage, mCurrentIndexPath;
@synthesize cell;
@synthesize messageDelegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    [self layoutIfNeeded];
    [imgUserPhoto.layer setCornerRadius:imgUserPhoto.frame.size.width / 2];
    [imgUserPhoto.layer setMasksToBounds:YES];
    layout_constraint_message_image_width.constant = SCREEN_WIDTH - 20;
    
    [imgPhoto.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [imgPhoto.layer setBorderWidth:1.f];
    [imgPhoto.layer setMasksToBounds:YES];
    [_btnExportCouponData.layer setCornerRadius:15.f];
    [_btnExportCouponData.layer setMasksToBounds:YES];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapTableViewCellContent:)];
    [self.contentView addGestureRecognizer:tapGesture];
    
    UITapGestureRecognizer *tapGesturePhoto = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapPhoto:)];
    [self.imgPhoto addGestureRecognizer:tapGesturePhoto];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state

}

- (IBAction)onTouchBtnExportCouponData:(id)sender {
    if([messageDelegate respondsToSelector:@selector(onExportCouponData:indexPath:cell:)])
    {
        [messageDelegate onExportCouponData:mCurrentMessage indexPath:mCurrentIndexPath cell:self];
    }
}
- (IBAction)onTouchBtnUseCoupon:(id)sender {
    if([messageDelegate respondsToSelector:@selector(onUseCoupon:indexPath:cell:)])
    {
        [messageDelegate onUseCoupon:mCurrentMessage indexPath:mCurrentIndexPath cell:self];
    }
}
- (IBAction)onTouchBtnBuySellItem:(id)sender {
    if([messageDelegate respondsToSelector:@selector(onTapBuySellItem:)])
    {
        [messageDelegate onTapBuySellItem:self];
    }
}

- (void)onTapTableViewCellContent:(id)sender
{
    if([messageDelegate respondsToSelector:@selector(onTapCurrentCell:)])
    {
        [messageDelegate onTapCurrentCell:self];
    }
}
- (void)onTapPhoto:(id)sender
{
    if([messageDelegate respondsToSelector:@selector(onTapPhoto:)])
    {
        [messageDelegate onTapPhoto:self.imgPhoto.image];
    }
}
@end
