//
//  PostViewController.h
//  BusinessTabble
//
//  Created by Haichen Song on 19/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageButton+Shadow.h"
#import "CoreDataViewController.h"
#import "MessageTableViewCell.h"
#import "SWTableViewCell.h"

#import "PayPalMobile.h"
#import "PayPalConfiguration.h"
#import "PayPalPaymentViewController.h"

@interface PostViewController : CoreDataViewController<MessageDelegate, SWTableViewCellDelegate, PayPalPaymentDelegate>
@property (strong, nonatomic) IBOutlet UIView *vwWritePostContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_writepostcontainer_height;
@property (strong, nonatomic) Bubble *mCurrentBubble;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_writepostbutton_width;
@property (weak, nonatomic) IBOutlet UIButton *btnSellItem;
@property (strong, nonatomic) PayPalConfiguration *paypalConfiguration;

@end
