//
//  WritePhotoViewController.m
//  BusinessTabble
//
//  Created by Haichen Song on 25/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "WritePhotoViewController.h"




@interface WritePhotoViewController ()

@end

@implementation WritePhotoViewController
@synthesize btnPhoto, txtContent, btnRemovePhoto, imgTitleIcon, lblTitle;
@synthesize mAttachedPhoto = _mAttachedPhoto;
@synthesize imgAttachedPhoto, layout_constraint_image_width, layout_constraint_image_height;
@synthesize btnPostLater, btnPostNo, btnPostNow;
@synthesize mCurrentBubble;
@synthesize mPostType;
- (void)viewDidLoad {
    [super viewDidLoad];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:btnPhoto.titleLabel.text];
    
    float spacing = 1.0f;
    [attributedString addAttribute:NSKernAttributeName
                             value:@(spacing)
                             range:NSMakeRange(0, [btnPhoto.titleLabel.text length])];
    btnPhoto.titleLabel.attributedText = attributedString;   
    [btnPhoto centerImageAndTitle:0.5];
    [self.view layoutIfNeeded];
    [_vwTextContainer.layer setCornerRadius:10.f];
    [_vwTextContainer.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_vwTextContainer.layer setBorderWidth:1.f];
//    txtContent.contentInset = UIEdgeInsetsMake(10,12,0,0);
    txtContent.textAlignment = NSTextAlignmentLeft;
    
    layout_constraint_image_width.constant = 0;
    [imgAttachedPhoto setHidden:YES];
    self.mAttachedPhoto = nil;
    layout_constraint_image_height.constant = SCREEN_WIDTH - 30;
    [imgAttachedPhoto.layer setBorderWidth:1.f];
    [imgAttachedPhoto.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [imgAttachedPhoto setContentMode:UIViewContentModeScaleAspectFill];
    [imgAttachedPhoto.layer setMasksToBounds:YES];
    photoActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel"  destructiveButtonTitle:nil otherButtonTitles:@"From Camera",@"From Photo Library", nil];

    if(mPostType == POST_TYPE_COUPON)
    {
        [imgTitleIcon setImage:[UIImage imageNamed:@"coupon_icon"]];
        [btnPhoto setTitle:@"Coupon" forState:UIControlStateNormal];
        [lblTitle setText:@"Send Coupon"];
    }
    
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if(!self.mAttachedPhoto)
    {
        layout_constraint_image_height.constant = 0;
        [imgAttachedPhoto setHidden:YES];
        [btnRemovePhoto setHidden:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];


}
#pragma mark Actions

- (IBAction)onTouchBtnBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)onTouchBtnUploadImage:(id)sender {
    [photoActionSheet showInView:[self.view window]];
}
- (IBAction)onTouchBtnRemovePhoto:(id)sender {
    self.mAttachedPhoto = nil;
}
- (IBAction)onTouchBtnPost:(id)sender
{
    UIButton *btnPost = (UIButton *)sender;
    if([[GlobalData sharedData]isEmpty: [txtContent.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]] && !self.mAttachedPhoto)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please write or attach photo." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        return;
    }
    if([btnPost isEqual:btnPostLater])
    {
        [self configureDateSelection];
        [self presentViewController:dateSelectionController animated:YES completion:nil];
        return;
    }
    [self postMessage:btnPost postTime:nil];
}
#pragma mark Property Getter & Setter
- (void)setMAttachedPhoto:(UIImage *)mAttachedPhoto
{
    _mAttachedPhoto = mAttachedPhoto;
    if(self.mAttachedPhoto)
    {
        layout_constraint_image_height.constant = SCREEN_WIDTH - 30;
        [imgAttachedPhoto setHidden:NO];
        [imgAttachedPhoto setImage:mAttachedPhoto];
        [btnRemovePhoto setHidden:NO];
    }
    else
    {
        layout_constraint_image_height.constant = 0;
        [imgAttachedPhoto setHidden:YES];
        [btnRemovePhoto setHidden:YES];
        [imgAttachedPhoto setImage:nil];
    }
}
#pragma mark  actionsheet didDismissWithButtonIndex
- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ( buttonIndex == 2 )
        return;
    if ( actionSheet == photoActionSheet )
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.editing = YES;
        picker.videoMaximumDuration = kMaxVideoLenth;
        {
            if (buttonIndex == 0) {
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
            } else if (buttonIndex == 1) {
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            }
        }
        picker.mediaTypes = [NSArray arrayWithObjects: (NSString*)kUTTypeImage, nil];//[NSArray arrayWithObjects:(NSString*)kUTTypeMovie, (NSString*)kUTTypeImage, nil];

        [self presentViewController:picker animated:TRUE completion:nil ];
    }
}
#pragma mark didFinishPickingMediaWithInfo
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *imgtmp = [info objectForKey:UIImagePickerControllerOriginalImage];
    if(!imgtmp || imgtmp == nil)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"You can only use photo." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        return;
    }
    imgtmp = [GlobalData scaleAndRotateImage:imgtmp];
    [self dismissViewControllerAnimated:YES completion:^{
        PECropViewController *controller = [[PECropViewController alloc] init];
        controller.delegate = self;
        controller.image = imgtmp;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            controller.modalPresentationStyle = UIModalPresentationFormSheet;
        }
        [controller setKeepingCropAspectRatio:NO];
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        }
        [self presentViewController:navigationController animated:YES completion:NULL];
    }];
}
#pragma mark Cropper Delegate
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    self.mAttachedPhoto = croppedImage;
}
- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark Other Methods
- (void)postMessage:(UIButton *)button postTime:(NSDate *)postTime
{
    
    NSString *postTimeStamp = @"0";
    NSString *bApproved = @"1";
    NSString *photo_filename = @"";
    NSString *coupon = @"0";
    if([button isEqual:btnPostNow])
    {
        bApproved = @"1";
        NSTimeInterval interval  = [[NSDate date] timeIntervalSince1970] ;
        postTimeStamp = [NSString stringWithFormat:@"%f", interval];
    }
    else if([button isEqual:btnPostNo])
        bApproved = @"-1";
    else if([button isEqual:btnPostLater])
    {
        bApproved = @"0";
        NSTimeInterval interval  = [postTime timeIntervalSince1970] ;
        postTimeStamp = [NSString stringWithFormat:@"%f", interval];
    }
    if(self.mAttachedPhoto)
    {
        NSDate *date = [NSDate date];
        NSString *strfilename = [NSString stringWithFormat:@"writephoto%@%ld.jpg", [[GlobalData sharedData]mUserInfo].mId , (long)[date timeIntervalSince1970]];
        photo_filename = [[GlobalData sharedData]urlencode:strfilename];
    }
    if(mPostType == POST_TYPE_COUPON)
        coupon = @"1";
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:bApproved forKey:@"approved"];
    [params setObject:[txtContent.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"text"];
    [params setObject:postTimeStamp forKey:@"approveFrom"];
    [params setObject:coupon forKey:@"coupon"];
    [params setObject:self.mCurrentBubble.bubble_id forKey:@"bubble_id"];
    [params setObject:photo_filename forKey:@"photo_filename"];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[MessageService sharedData]createMessageWithDictionary:params success:^(id _responseObject) {
        if([button isEqual:btnPostNo])
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"This post has been disabled." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:nil];
        else
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Successfully created" BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:nil];
        if(self.mAttachedPhoto)
        {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [[GlobalData sharedData] uploadPhotoWithName:photo_filename oldname:@"" image:self.mAttachedPhoto type:@"" completion:^{
                [self onSuccessfulCreatingNewPost];
            } FailureHandler:^{
                [[GlobalData sharedData] uploadPhotoWithName:photo_filename oldname:@"" image:self.mAttachedPhoto type:@"" completion:^{
                    [self onSuccessfulCreatingNewPost];
                } FailureHandler:^{
                    [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"There is problem uploading photo." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
                    [self onSuccessfulCreatingNewPost];
                }];
            }];
        }
        else
        {
            [self onSuccessfulCreatingNewPost];
        }
        
        
        
        return;
    } failure:^(NSInteger _errorCode) {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }];

}
- (void)onSuccessfulCreatingNewPost
{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    
    [[MainService sharedData]getBubbleMessageListWithSuccessHandler:^{
        [[MainService sharedData]getBussinessBubbleListWithSuccessHandler:^{
            [self dismissViewControllerAnimated:YES completion:nil];
        } FailureHandler:^{
        }];
    } FailureHandler:^{
    }];
}
- (void)configureDateSelection
{
    RMActionControllerStyle style = RMActionControllerStyleDefault;
    dateSelectionController = [RMDateSelectionViewController actionControllerWithStyle:style];
    dateSelectionController.title = @"Placez";
    dateSelectionController.message = @"\nPlease choose a post time and press 'Select' or 'Cancel'.";
    
    //You can enable or disable blur, bouncing and motion effects
    dateSelectionController.disableBouncingEffects = NO;
    dateSelectionController.disableBackgroundTaps = NO;
    dateSelectionController.disableBlurEffectsForContentView = NO;
    dateSelectionController.disableBlurEffectsForBackgroundView = NO;
    dateSelectionController.disableMotionEffects = NO;
    dateSelectionController.disableBlurEffects = NO;
    
    //You can access the actual UIDatePicker via the datePicker property
    dateSelectionController.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    dateSelectionController.datePicker.minuteInterval = 5;
    dateSelectionController.datePicker.date = [NSDate date];
    
    RMAction<RMActionController<UIDatePicker *> *> *selectAction = [RMAction<RMActionController<UIDatePicker *> *> actionWithTitle:@"Select" style:RMActionStyleDone andHandler:^(RMActionController<UIDatePicker *> *controller) {
        NSLog(@"Successfully selected date: %@", controller.contentView.date);
        NSDate *selectedDate = controller.contentView.date;
        if([selectedDate compare: [NSDate date]] == NSOrderedAscending)
        {
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"You should not select the date prior the moment." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
            return;
        }
        [self postMessage:btnPostLater postTime:selectedDate];
    }];
    RMAction<RMActionController<UIDatePicker *> *> *cancelAction = [RMAction<RMActionController<UIDatePicker *> *> actionWithTitle:@"Cancel" style:RMActionStyleCancel andHandler:^(RMActionController<UIDatePicker *> *controller) {
    }];
   
    
    [dateSelectionController addAction:selectAction];
    [dateSelectionController addAction:cancelAction];
    
    RMAction<RMActionController<UIDatePicker *> *> *in15MinAction = [RMAction<RMActionController<UIDatePicker *> *> actionWithTitle:@"15 Min" style:RMActionStyleAdditional andHandler:^(RMActionController<UIDatePicker *> *controller) {
        controller.contentView.date = [NSDate dateWithTimeIntervalSinceNow:15*60];
    }];
    in15MinAction.dismissesActionController = NO;
    
    RMAction<RMActionController<UIDatePicker *> *> *in30MinAction = [RMAction<RMActionController<UIDatePicker *> *> actionWithTitle:@"30 Min" style:RMActionStyleAdditional andHandler:^(RMActionController<UIDatePicker *> *controller) {
        controller.contentView.date = [NSDate dateWithTimeIntervalSinceNow:30*60];
    }];
    in30MinAction.dismissesActionController = NO;
    
    RMAction<RMActionController<UIDatePicker *> *> *in45MinAction = [RMAction<RMActionController<UIDatePicker *> *> actionWithTitle:@"45 Min" style:RMActionStyleAdditional andHandler:^(RMActionController<UIDatePicker *> *controller) {
        controller.contentView.date = [NSDate dateWithTimeIntervalSinceNow:45*60];
    }];
    in45MinAction.dismissesActionController = NO;
    
    RMAction<RMActionController<UIDatePicker *> *> *in60MinAction = [RMAction<RMActionController<UIDatePicker *> *> actionWithTitle:@"60 Min" style:RMActionStyleAdditional andHandler:^(RMActionController<UIDatePicker *> *controller) {
        controller.contentView.date = [NSDate dateWithTimeIntervalSinceNow:60*60];
    }];
    in60MinAction.dismissesActionController = NO;
    
    RMGroupedAction<RMActionController<UIDatePicker *> *> *groupedAction = [RMGroupedAction<RMActionController<UIDatePicker *> *> actionWithStyle:RMActionStyleAdditional andActions:@[in15MinAction, in30MinAction, in45MinAction, in60MinAction]];
    
    [dateSelectionController addAction:groupedAction];
    
    RMAction<RMActionController<UIDatePicker *> *> *nowAction = [RMAction<RMActionController<UIDatePicker *> *> actionWithTitle:@"Now" style:RMActionStyleAdditional andHandler:^(RMActionController<UIDatePicker *> * _Nonnull controller) {
        controller.contentView.date = [NSDate date];
        NSLog(@"Now button tapped");
    }];
    nowAction.dismissesActionController = NO;
    [dateSelectionController addAction:nowAction];

}
- (void)textViewDidChange:(UITextView *)textView
{
//    txtContent.contentInset = UIEdgeInsetsMake(10,12,0,0);
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
//    txtContent.contentInset = UIEdgeInsetsMake(10,12,0,0);
    return YES;
}
@end
