//
//  CouponDateDetailTableViewCell.h
//  BusinessTabble
//
//  Created by Tian Ming on 05/01/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CouponDateDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblUsers;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserPhoto;

@end
