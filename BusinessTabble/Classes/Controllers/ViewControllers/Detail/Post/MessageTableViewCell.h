//
//  MessageTableViewCell.h
//  BusinessTabble
//
//  Created by Tian Ming on 30/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
@protocol MessageDelegate <NSObject>
@optional - (void) onTapPhoto:(UIImage *)image;
@optional - (void) onUseCoupon:(Message* ) currentCoupon indexPath:(NSIndexPath *)indexPath cell:(id)cell;
@optional - (void) onExportCouponData:(Message* ) currentCoupon indexPath:(NSIndexPath *)indexPath cell:(id)cell;
@optional - (void) onTapCurrentCell:(id)cell;
@optional - (void) onTapBuySellItem:(id)cell;
@end

@interface MessageTableViewCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblApproveFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblText;
@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserPhoto;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_message_image_width;
@property (weak, nonatomic) IBOutlet UIButton *btnExportCouponData;
@property (weak, nonatomic) IBOutlet UIButton *btnUseCoupon;
@property (weak, nonatomic) IBOutlet UIButton *btnUsed;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_button_export_height;
@property (weak, nonatomic) IBOutlet UIView *vwCellItemContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_cell_item_container_height;

@property (weak, nonatomic) IBOutlet UILabel *lblSellItemQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lblSellItemShippingPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblSellItemPrice;

@property (nonatomic, strong) id<MessageDelegate> messageDelegate;
@property (strong, nonatomic) Message *mCurrentMessage;
@property (strong, nonatomic) Chat *mCurrentChat;
@property (strong, nonatomic) NSIndexPath* mCurrentIndexPath;
@property (strong, nonatomic) id cell;
@end
