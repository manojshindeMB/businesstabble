//
//  CouponTrackerViewController.h
//  BusinessTabble
//
//  Created by Tian Ming on 05/01/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataViewController.h"
@interface CouponTrackerViewController : CoreDataViewController<UITableViewDataSource, UITableViewDelegate>
{
    Boolean sortedByDateASC;
    Boolean sortedByUsersASC;
}
@property(nonatomic, strong) Message *mCurrentCoupon;
@property(nonatomic, strong) Bubble *mCurrentBubble;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIButton *btnTitleDate;
@property (weak, nonatomic) IBOutlet UIButton *btnTitleUsers;
@property (nonatomic, strong) NSMutableArray *mArrRowCollapseStatus;
@end
