//
//  WritePhotoViewController.h
//  BusinessTabble
//
//  Created by Haichen Song on 25/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WYPopoverController.h"
#import "RMDateSelectionViewController.h"
@interface WritePhotoViewController : UIViewController<UITextViewDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, WYPopoverControllerDelegate, PECropViewControllerDelegate>
{
    UIActionSheet *photoActionSheet;
    UIDatePicker *datepicker;
    WYPopoverController *popover;
    RMDateSelectionViewController *dateSelectionController;
}
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgTitleIcon;
@property (weak, nonatomic) IBOutlet UITextView *txtContent;
@property (weak, nonatomic) IBOutlet UIButton_Image *btnPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *imgAttachedPhoto;
@property (weak, nonatomic) IBOutlet UIButton *btnRemovePhoto;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_image_width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_image_height;

@property (weak, nonatomic) IBOutlet UIView *vwTextContainer;
@property (weak, nonatomic) IBOutlet UIButton *btnPostNow;
@property (weak, nonatomic) IBOutlet UIButton *btnPostLater;
@property (weak, nonatomic) IBOutlet UIButton *btnPostNo;

@property (strong, nonatomic) UIImage *mAttachedPhoto;
@property (strong, nonatomic) Bubble *mCurrentBubble;
@property (nonatomic) NSInteger mPostType;

@end
