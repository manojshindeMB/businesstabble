//
//  BubbleMenuViewController.m
//  BusinessTabble
//
//  Created by Liumin on 21/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "PhotoFullViewController.h"
#import "UIImageView+ProgressView.h"
#define MENU_IMAGE_START_TAG 101
@interface PhotoFullViewController ()


@end

@implementation PhotoFullViewController
@synthesize vwMainContent, svContainer;
@synthesize mImage;

@synthesize layout_constraint_vwMenuContainer_Height, layout_constraint_vwMenuContainer_Width;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view layoutIfNeeded];
//    [_btnClose.layer setCornerRadius:_btnClose.frame.size.width / 2];
//    [_btnClose.layer setMasksToBounds:YES];
    [_btnClose setHidden:NO];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(mImage)
        [self reloadBubbleMenus];
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)onTouchBtnClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)reloadBubbleMenus
{
    //dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) , ^{
    CGFloat menuHeight = vwMainContent.frame.size.height;
    if(!menuHeight)
        return;
    float imageHeight = mImage.size.height;
    imageHeight = MIN(mImage.size.width / SCREEN_WIDTH * imageHeight + 300, menuHeight - 200);
//    imageHeight = MIN(mImage.size.width / SCREEN_WIDTH * imageHeight, menuHeight - 200);
    
    UIScrollView *subscrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, imageHeight)];
    subscrollView.delegate = self;
    subscrollView.maximumZoomScale = 4.0;
    [subscrollView setClipsToBounds:YES];
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH,  imageHeight)];
    [imgView.layer setBorderWidth:0.f];
    [imgView.layer setBorderColor:[UIColor whiteColor].CGColor];
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    [imgView.layer setMasksToBounds:YES];
    imgView.image = mImage;
    imgView.tag =  MENU_IMAGE_START_TAG;
    [subscrollView addSubview:imgView];
    subscrollView.center = vwMainContent.center;
    [vwMainContent addSubview:subscrollView];
    
//    [subscrollView setBackgroundColor:[UIColor grayColor]];
    [_btnClose bringSubviewToFront:self.view];

}
- (void)dismissGallery:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    UIImageView *curImageView;
    for(UIView *subview in scrollView.subviews)
    {
        if([subview isKindOfClass:[UIImageView class]] && subview.tag > 100)
            curImageView = (UIImageView *)subview;
    }
    return curImageView;
}

@end
