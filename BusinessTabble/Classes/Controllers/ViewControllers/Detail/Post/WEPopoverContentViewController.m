//
//  WEPopoverContentViewController.m
//  WEPopover
//
//  Created by Werner Altewischer on 06/11/10.
//  Copyright 2010 Werner IT Consultancy. All rights reserved.
//

#import "WEPopoverContentViewController.h"

@implementation WEPopoverContentViewController
@synthesize delegate,mArrCurrencies;
@synthesize mCellNumber;

#pragma mark -
#pragma mark Initialization

- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if ((self = [super initWithStyle:style])) {
        
        if ([self respondsToSelector:@selector(setPreferredContentSize:)]) {
            self.preferredContentSize = CGSizeMake(130, 2 * 35 - 1);
        } else {
            self.contentSizeForViewInPopover = CGSizeMake(130, 2 * 35 - 1);
        }
    }
    return self;
}

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.separatorColor = [UIColor clearColor];
    
	self.tableView.rowHeight = 35.0;
	self.view.backgroundColor = [UIColor clearColor];
    [self.view.layer setBorderColor:[UIColor blackColor].CGColor];

    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}



#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [mArrCurrencies count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    NSString *content = [mArrCurrencies objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [cell setBackgroundColor:[UIColor clearColor]];
//    UIImageView *imgBackground = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"contentbackground"]];
//    [imgBackground setAlpha:1.f];
//    [cell setBackgroundView:imgBackground];
    // Configure the cell...
    cell.textLabel.textAlignment = NSTextAlignmentCenter;

    cell.textLabel.textColor = [UIColor whiteColor];
    
    [cell.textLabel setFont:[UIFont fontWithName:@"Avenir-Medium" size:12]];

//    cell.imageView.image = [UIImage imageNamed:[dict objectForKey:@"icon"]];
    cell.textLabel.text = [NSString stringWithFormat:@"%@", content];
    
//    UIImageView *selectedImgBackground = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"contentbackground"]];
//    [selectedImgBackground setAlpha:0.9];
//    cell.selectedBackgroundView =selectedImgBackground;
    
    return cell;
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	
    if ([(id)delegate respondsToSelector: @selector(popoverItemSelected:)])
    {
        [delegate popoverItemSelected:indexPath.row];
    }
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}




@end

