//
//  CouponHeaderTableViewCell.h
//  BusinessTabble
//
//  Created by Tian Ming on 05/01/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CouponHeaderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblUserCount;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@end
