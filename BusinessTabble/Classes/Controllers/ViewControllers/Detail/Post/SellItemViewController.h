//
//  SellItemViewController.h
//  BusinessTabble
//
//  Created by macbook on 26/06/2017.
//  Copyright © 2017 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WYPopoverController.h"
#import "RMDateSelectionViewController.h"
#import "IGLDropDownMenu.h"
#import "NIDropDown.h"
//#import "WEPopoverContentViewController.h"
//#import "WEPopoverController.h"

@interface SellItemViewController : UIViewController<UITextViewDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, PECropViewControllerDelegate, WYPopoverControllerDelegate, UITextFieldDelegate, NIDropDownDelegate, UIGestureRecognizerDelegate>//, WEPopoverControllerDelegate, JPopOverDelegate

{
    UIActionSheet *photoActionSheet;
    UIDatePicker *datepicker;
    BOOL isReadyQuantityDrop;
    Class popoverClass;
    RMDateSelectionViewController *dateSelectionController;
//    WEPopoverController *popoverController;
}

@property (weak, nonatomic) IBOutlet UIView *vwTextContainer;
@property (weak, nonatomic) IBOutlet UITextView *txtContent;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_image_height;
@property (weak, nonatomic) IBOutlet UIButton_Image *btnPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *imgAttachedPhoto;
@property (weak, nonatomic) IBOutlet UIButton *btnRemovePhoto;
@property (weak, nonatomic) IBOutlet UIButton *btnConditionNew;
@property (weak, nonatomic) IBOutlet UIButton *btnConditionUsed;
@property (weak, nonatomic) IBOutlet UIButton *btnQuantity;
@property (weak, nonatomic) IBOutlet UIView *vwContentView;
@property (weak, nonatomic) IBOutlet UITextField *txtPrice;
@property (weak, nonatomic) IBOutlet UITextField *txtPriceFloat;

@property (weak, nonatomic) IBOutlet UIScrollView *svMainContainer;
@property (weak, nonatomic) IBOutlet UIView *vwPriceContainer;
@property (weak, nonatomic) IBOutlet UIButton *btnShippingFree;
@property (weak, nonatomic) IBOutlet UIButton *btnShippingCost;
@property (weak, nonatomic) IBOutlet UITextField *txtShippingPrice;
@property (weak, nonatomic) IBOutlet UITextField *txtShippingPriceFloat;

@property (weak, nonatomic) IBOutlet UIButton *btnCheckoutTypePaypal;
@property (weak, nonatomic) IBOutlet UIButton *btnCheckoutTypeURL;
@property (weak, nonatomic) IBOutlet UITextField *txtCheckoutURL;
@property (weak, nonatomic) IBOutlet UIButton *btnPostNow;
@property (weak, nonatomic) IBOutlet UIButton *btnPostLater;
@property (weak, nonatomic) IBOutlet UIButton *btnPostNo;

@property (weak, nonatomic) IBOutlet UITextField *txtQuantity;
@property (strong, nonatomic)  NIDropDown *dropDownQuantity;


@property (strong, nonatomic) Bubble *mCurrentBubble;
@property (strong, nonatomic) UIImage *mAttachedPhoto;
@property (nonatomic) NSInteger mPostType;
@property (nonatomic) NSInteger mConditionType;
@property (nonatomic) NSInteger mShippingType;
@property (nonatomic) NSInteger mCheckoutType;
@property (nonatomic) NSInteger selectedQuantity;

@property (nonatomic) float mPrice;
@property (nonatomic) float mShippingPrice;



@end
