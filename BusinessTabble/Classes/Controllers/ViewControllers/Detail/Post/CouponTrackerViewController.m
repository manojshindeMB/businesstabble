//
//  CouponTrackerViewController.m
//  BusinessTabble
//
//  Created by Tian Ming on 05/01/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import "CouponTrackerViewController.h"
#import "CouponDateDetailTableViewCell.h"
#import "CouponHeaderTableViewCell.h"
@interface CouponTrackerViewController ()

@end

@implementation CouponTrackerViewController
@synthesize mCurrentBubble, mCurrentCoupon;
@synthesize txtEmail, btnSend, btnTitleDate, btnTitleUsers;
@synthesize mArrRowCollapseStatus;
- (void)viewDidLoad {
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_COUPON_USE_LIST object:nil];
    
    [super viewDidLoad];
    [txtEmail.layer setBorderWidth:1.f];
    [txtEmail.layer setCornerRadius:15.f];
    [txtEmail.layer setBorderColor:[UIColor lightGrayColor].CGColor];
   
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, btnTitleDate.frame.size.height - 3, btnTitleDate.frame.size.width, 1)];
    lineView.backgroundColor = [UIColor darkGrayColor];
    [btnTitleDate addSubview:lineView];
    
    lineView = [[UIView alloc] initWithFrame:CGRectMake(0, btnTitleUsers.frame.size.height - 3, btnTitleUsers.frame.size.width, 1)];
    lineView.backgroundColor = [UIColor darkGrayColor];
    [btnTitleUsers addSubview:lineView];
    sortedByUsersASC = NO;
    sortedByDateASC = NO;
    [self reloadDataWithCategoryName:@"" SearchString:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}
- (IBAction)onTouchBtnBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)reloadDataWithCategoryName:(NSString *)strCategoryName
                      SearchString:(NSString *)strSearchString
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"coupon_id = %@ AND status = 1", mCurrentCoupon.message_id];
    self.fetchedResultsController = [CouponUse MR_fetchAllSortedBy:@"updatedAt" ascending:sortedByDateASC withPredicate:predicate groupBy:@"usedDate" delegate:self];
    if(!mArrRowCollapseStatus)
    {
        mArrRowCollapseStatus = [[NSMutableArray alloc]init];
        NSInteger sectionCount = [[self.fetchedResultsController sections]count];
        for(int i = 0; i < sectionCount; i++)
        {
            mArrRowCollapseStatus[i] = @"0";
        }
    }
    
}
- (IBAction)onTouchBtnSortByDate:(id)sender {
//    sortedByDateASC = !sortedByDateASC;
//    [self reloadDataWithCategoryName:@"" SearchString:@""];
}
- (IBAction)onTouchBtnSortByUsers:(id)sender {
    
}
- (IBAction)onTouchBtnSendEmail:(id)sender {
    if([self.fetchedResultsController.fetchedObjects count] < 1)
    {
         [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"No users found." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        return;
    }
    if(![[GlobalData sharedData]validateEmailWithString:txtEmail.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide valid email." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        return;
    }
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]init];
    [btnSend addSubview:activityIndicator];
    [activityIndicator setColor:[UIColor blackColor]];
    [activityIndicator setCenter:CGPointMake(btnSend.frame.size.width / 2, btnSend.frame.size.height / 2)];
    [activityIndicator startAnimating];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:txtEmail.text forKey:@"email"];
    [params setObject:mCurrentCoupon.message_id forKey:@"coupon_id"];

    [[MessageService sharedData]emailCouponUsersWithDictionary:params success:^(id _responseObject) {
        [activityIndicator removeFromSuperview];
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Email has been sent." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:nil];
        return;
    } failure:^(NSInteger _errorCode) {
        [activityIndicator removeFromSuperview];
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
    }];

}

#pragma mark - Table view data source
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    CouponUse *currentCouponUse = self.fetchedResultsController.fetchedObjects[indexPath.section * indexPath.row];
    CouponUse *currentCouponUse =     [[[[self.fetchedResultsController sections] objectAtIndex:indexPath.section] objects] objectAtIndex:indexPath.row];
    NSString *CellIdentifier = @"CouponDateDetailTableViewCell";
    CouponDateDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.lblDate setText: [NSString stringWithFormat:@"%@", currentCouponUse.username]];
    [cell.lblUsers setText: [NSString stringWithFormat:@"%@", currentCouponUse.usedTime]];
    NSString *photo_filename = [[GlobalData sharedData]urlencode:currentCouponUse.user_photo_filename];
    [[GlobalData sharedData]showImageWithImage:cell.imgUserPhoto name:photo_filename placeholder:nil];
    return cell;
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return 0;
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger sectionCount = [[self.fetchedResultsController sections]count];

    return sectionCount;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *CellIdentifier = @"CouponHeaderTableViewCell";
    CouponHeaderTableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    headerView.lblDate.text = [[[self.fetchedResultsController sections] objectAtIndex:section]name];
    NSInteger objectCount = [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects];
    headerView.lblUserCount.text = [NSString stringWithFormat:@"%ld Users", objectCount];
    UIButton *btnClick = [[UIButton alloc]initWithFrame:headerView.frame];
    [btnClick setBackgroundColor:[UIColor clearColor]];
    btnClick.tag = section + 1;
    [btnClick addTarget:self action:@selector(sectionTapped:) forControlEvents:UIControlEventTouchDown];
    [headerView addSubview:btnClick];
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35.f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([mArrRowCollapseStatus[section] isEqualToString:@"1"]) {
        return [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects] + 0;
    } else {
        return 0;
    }
}
- (void)sectionTapped:(UIButton*)btn {
    NSInteger section = btn.tag - 1;
    if([mArrRowCollapseStatus[section] isEqualToString:@"0"])
        mArrRowCollapseStatus[section] = @"1";
    else
        mArrRowCollapseStatus[section] = @"0";

    [UIView transitionWithView:self.tableView
                      duration:0.35f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^(void)
     {
         [self.tableView reloadData];
     }
                    completion:nil];
}
@end
