//
//  PostViewController.m
//  BusinessTabble
//
//  Created by Haichen Song on 19/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "PostViewController.h"
#import "WritePhotoViewController.h"
#import "MessageTableViewCell.h"
#import "CouponTrackerViewController.h"
#import "SWTableViewCell.h"
#import "NYAlertViewController.h"
#import "SellItemViewController.h"
#import "PhotoFullViewController.h"
//#import "BraintreeCore.h"
//#import "BraintreePayPal.h"


@import SafariServices;
@interface PostViewController ()  //BTAppSwitchDelegate, BTViewControllerPresentingDelegate
//@property (nonatomic, strong) BTAPIClient *braintreeClient;
//@property (nonatomic, strong) BTPayPalDriver *payPalDriver;
@end

@implementation PostViewController
@synthesize vwWritePostContainer;
@synthesize mCurrentBubble;
@synthesize layout_constraint_writepostcontainer_height;


- (void)viewDidLoad {
    [super viewDidLoad];
    [_btnSellItem setHidden:YES];
    if(![mCurrentBubble.postenabled isEqualToNumber:[NSNumber numberWithInteger:1]])
    {
        layout_constraint_writepostcontainer_height.constant = 0;
        [vwWritePostContainer setHidden:YES];
    }
    else{
        _layout_constraint_writepostbutton_width.constant = SCREEN_WIDTH / 2;
        if([mCurrentBubble.creator_id integerValue] == [[[GlobalData sharedData]mUserInfo].mId integerValue])
        {
            _layout_constraint_writepostbutton_width.constant = SCREEN_WIDTH / 3;
            [_btnSellItem setHidden:NO];
        }
    }
    [self.view layoutIfNeeded];
    CALayer *upperBorder = [CALayer layer];
    upperBorder.backgroundColor = [[UIColor lightGrayColor] CGColor];
    upperBorder.frame = CGRectMake(0, vwWritePostContainer.frame.size.height - 1, CGRectGetWidth(vwWritePostContainer.frame), 1.0f);
    [vwWritePostContainer.layer addSublayer:upperBorder];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44.0f;
    
    
    _paypalConfiguration = [[PayPalConfiguration alloc]init];
    _paypalConfiguration.acceptCreditCards = YES;
    _paypalConfiguration.merchantName = mCurrentBubble.owner;
    _paypalConfiguration.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _paypalConfiguration.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
    _paypalConfiguration.languageOrLocale = [NSLocale preferredLanguages][0];
    _paypalConfiguration.payPalShippingAddressOption = PayPalShippingAddressOptionNone; //PayPalShippingAddressOption;
    NSLog(@"Paypal SDK:%@", [PayPalMobile libraryVersion]);
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self reloadDataWithCategoryName:@"" SearchString:@""];

    [[GlobalData sharedData]setIsMessageViewOpened:YES];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [[GlobalData sharedData]setIsMessageViewOpened:NO];
    [super viewWillDisappear:animated];
}
- (void)reloadDataWithCategoryName:(NSString *)strCategoryName
                      SearchString:(NSString *)strSearchString
{
//    NSDate *oneWeekAgo = [[GlobalData sharedData]getAddDatedFromDate:[NSDate date] dayNum:- 7];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bubble_id = %@ AND item_deleted = 0 AND (updatedTime > %@)", mCurrentBubble.bubble_id, oneWeekAgo];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bubble_id = %@ AND item_deleted = 0", mCurrentBubble.bubble_id];
    self.fetchedResultsController.fetchRequest.returnsDistinctResults = YES;
    
    
    NSFetchRequest *userRequest = [Message MR_requestAllSortedBy:@"updatedAt" ascending:NO withPredicate:predicate];
    [userRequest setFetchLimit:25];
    NSFetchedResultsController *theFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:userRequest managedObjectContext:[NSManagedObjectContext MR_defaultContext] sectionNameKeyPath:nil cacheName:nil];
    self.fetchedResultsController = theFetchedResultsController;
}

#pragma mark Actions
- (IBAction)onTouchBtnWritePhoto:(id)sender {
    WritePhotoViewController *writeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WritePhotoViewController"];
    writeVC.mCurrentBubble = mCurrentBubble;
    writeVC.mPostType = POST_TYPE_WRITE_PHOTO;
    [self presentViewController:writeVC animated:YES completion:^{
        
    }];
}
- (IBAction)onTouchBtnPostPostCoupon:(id)sender
{
    WritePhotoViewController *writeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WritePhotoViewController"];
    writeVC.mCurrentBubble = mCurrentBubble;
    writeVC.mPostType = POST_TYPE_COUPON;
    [self presentViewController:writeVC animated:YES completion:^{
        
    }];
}
- (IBAction)onTouchBtnSellItem:(id)sender {
    SellItemViewController *sellItemVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SellItemViewController"];
    sellItemVC.mCurrentBubble = mCurrentBubble;
    sellItemVC.mPostType = POST_TYPE_SELL_ITEM;
    [self presentViewController:sellItemVC animated:YES completion:^{
        
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"MessageTableViewCell";
    MessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell = [self configureBasicCell:cell indexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (MessageTableViewCell *)configureBasicCell:(MessageTableViewCell *) cell indexPath:(NSIndexPath *)indexPath
{
    Message *currentMessage = self.fetchedResultsController.fetchedObjects[indexPath.row];
    cell.lblUserName.text = currentMessage.username;
    cell.lblText.text = currentMessage.text;
    cell.mCurrentMessage = currentMessage;
//    NSLog(@"current cell index %ld   message_id %ld", indexPath.row,[currentMessage.message_id integerValue]);
    if(currentMessage.approvefrom > 0)
    {
        NSDate *approveFrom = [NSDate dateWithTimeIntervalSince1970:[currentMessage.approvefrom doubleValue]];
        [cell.lblApproveFrom setText:[NSString stringWithFormat:@"%@", [[GlobalData sharedData]dateTimeStringFromDate:approveFrom]]];
    }
    if(![[GlobalData sharedData]isEmpty:currentMessage.photo_filename])
    {
        NSString *photo_filename = [[GlobalData sharedData]urlencode:currentMessage.photo_filename];
        [[GlobalData sharedData]showImageWithImage:cell.imgPhoto name:photo_filename placeholder:nil];
        [cell.imgPhoto.layer setMasksToBounds:YES];
        [cell.imgPhoto setContentMode:UIViewContentModeScaleAspectFill];
        cell.layout_constraint_message_image_width.constant = SCREEN_WIDTH - 25;
    }
    else
        cell.layout_constraint_message_image_width.constant = 0;
    
    [cell.vwCellItemContainer setHidden:YES];
    cell.layout_constraint_cell_item_container_height.constant = 0;
    if([currentMessage.coupon isEqualToNumber:@1])
    {
        cell.mCurrentIndexPath = indexPath;
        cell.messageDelegate = self;
        cell.layout_constraint_button_export_height.constant = 35.f;
        cell.layout_constraint_cell_item_container_height.constant = 43.f;
        [self showCouponStatus:currentMessage index:indexPath cell:cell];
    }
    else
    {
        cell.layout_constraint_button_export_height.constant = 0;
        if([currentMessage.coupon isEqualToNumber:@2]) //Sell Item
        {
            cell.messageDelegate = self;
            [cell.vwCellItemContainer setHidden:NO];
            cell.layout_constraint_cell_item_container_height.constant = 43.f;
            cell.lblSellItemPrice.text = [NSString stringWithFormat:@"$%@", currentMessage.price];
            if([currentMessage.shipping_price floatValue] > 0)
                cell.lblSellItemShippingPrice.text = [NSString stringWithFormat:@"$%@", currentMessage.shipping_price];
            else
                cell.lblSellItemShippingPrice.text = @"FREE";
            cell.lblSellItemQuantity.text = [NSString stringWithFormat:@"%@", currentMessage.sell_quantity];
            
        }
    }
    NSString *photo_filename = [[GlobalData sharedData]urlencode:currentMessage.user_photo_filename];
    [[GlobalData sharedData]showImageWithImage:cell.imgUserPhoto name:photo_filename placeholder:nil];
    [cell.imgUserPhoto.layer setBorderColor:[UIColor colorWithWhite:0.1 alpha:0.3].CGColor];
    [cell.imgUserPhoto.layer setBorderWidth:1.f];
    
    // Right Utility Buttons
    cell.delegate = self;
    if([currentMessage.poster_id isEqualToNumber:[NSNumber numberWithInteger:[[[GlobalData sharedData]mUserInfo].mId integerValue]]])
    {
        cell.rightUtilityButtons = [self rightButtons];
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}
- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                title:@"Delete"];
    return rightUtilityButtons;
}
-(BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    MessageTableViewCell *currentCell =(MessageTableViewCell *) cell;
    Message *currentMessage = currentCell.mCurrentMessage;
    if([currentMessage.poster_id isEqualToNumber:[NSNumber numberWithInteger:[[[GlobalData sharedData]mUserInfo].mId integerValue]]])
    {
        return YES;
    }
    return NO;
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    MessageTableViewCell *currentCell = (MessageTableViewCell *)cell;
    Message *currentMessage = currentCell.mCurrentMessage;
    [cell hideUtilityButtonsAnimated:YES];
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]init];
    [activityIndicator setCenter:currentCell.contentView.center];
    [currentCell.contentView addSubview:activityIndicator];
    [activityIndicator setColor:[UIColor blackColor]];
    [activityIndicator startAnimating];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:currentMessage.message_id forKey:@"message_id"];
    [[MessageService sharedData]deleteMessageWithDictionary:params success:^(id _responseObject) {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Successfully deleted." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
        }];
        currentMessage.item_deleted = [NSNumber numberWithInt:1];

        [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
//            [self reloadDataWithCategoryName:@"" SearchString:@""];
        }];

        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_DATA object:nil];
        [activityIndicator stopAnimating];
        [activityIndicator removeFromSuperview];
        
    } failure:^(NSInteger _errorCode) {
        [activityIndicator stopAnimating];
        [activityIndicator removeFromSuperview];
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:^{
        }];
    }];
}
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    return YES;
}

#pragma mark Message Delegates
- (void)onTapBuySellItem:(id)cell
{
    if(!cell)
        return;
    MessageTableViewCell *curCell = (MessageTableViewCell *)cell;
    Message *msg = curCell.mCurrentMessage;
    if(!msg)
        return;
    if([msg.checkout integerValue] == 2 && ![[GlobalData sharedData] isEmpty:msg.checkout_url])
    {        
        
        NSURL *url = [[NSURL alloc]initWithString:msg.checkout_url];
        if ([SFSafariViewController class]) {
            SFSafariViewController *viewController = [[SFSafariViewController alloc] initWithURL:url];
            [self presentViewController:viewController animated:YES completion:nil];
        } else {
            [[UIApplication sharedApplication] openURL:url];
        }
    }
    else if([msg.checkout integerValue] == 1) // Direct Paypal
    {
        NSDecimalNumber *itemPrice = [[NSDecimalNumber alloc] initWithString:[NSString stringWithFormat:@"%@", msg.price]];
        PayPalItem *item1 = [PayPalItem itemWithName:msg.text withQuantity:[msg.sell_quantity integerValue] withPrice:itemPrice withCurrency:@"USD" withSku:@"SKU"];
        NSArray *items = @[item1];
        NSDecimalNumber *subTotal = [PayPalItem totalPriceForItems:items];
        NSDecimalNumber *shippingCost = [[NSDecimalNumber alloc] initWithString:[NSString stringWithFormat:@"%@", msg.shipping_price]];
        NSDecimalNumber *taxPrice =[[NSDecimalNumber alloc] initWithString:@"0.0"];
        
        PayPalPaymentDetails *paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subTotal withShipping:shippingCost withTax:taxPrice];
        NSDecimalNumber *total = [[subTotal decimalNumberByAdding:shippingCost] decimalNumberByAdding:taxPrice];
        
        PayPalPayment * payment = [[PayPalPayment alloc]init];
        payment.amount = total;
        payment.currencyCode = @"USD";
        payment.shortDescription =[NSString stringWithFormat:@"Total Cost: "];
        payment.paymentDetails = paymentDetails;
        if(!payment.processable)
        {
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Payment is not possible. Please check again!" BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        }
        PayPalPaymentViewController *paymentVC = [[PayPalPaymentViewController alloc]initWithPayment:payment configuration:_paypalConfiguration delegate:self];
        [self presentViewController:paymentVC animated:YES completion:^{
        }];
    }
}
- (void)onExportCouponData:(Message *)currentCoupon indexPath:(NSIndexPath *)indexPath cell:(id)cell
{
    CouponTrackerViewController *trackVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CouponTrackerViewController"];
    trackVC.mCurrentBubble = mCurrentBubble;
    trackVC.mCurrentCoupon = currentCoupon;
    
    [self presentViewController:trackVC animated:YES completion:nil];
    
}
- (void)onUseCoupon:(Message *)currentCoupon indexPath:(NSIndexPath *)indexPath cell:(id)cell
{
    if(!currentCoupon)
        return;
    NYAlertViewController *alertViewController = [[GlobalData sharedData]createNewAlertViewWithTitle:@"Are you sure you want to use the coupon? \n Please present to associate prior to use." view:self.view];
    [alertViewController addAction:[NYAlertAction actionWithTitle:NSLocalizedString(@"Ok", nil)
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                              MessageTableViewCell *currentCell;
                                                              UIActivityIndicatorView *activityIndicator;
                                                              if([cell isKindOfClass:[MessageTableViewCell class]])
                                                              {
                                                                  currentCell = cell;
                                                                  activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
                                                                  [activityIndicator setCenter:currentCell.btnUseCoupon.center];
                                                                  
                                                                  [activityIndicator startAnimating];
                                                                  [activityIndicator setColor:[UIColor blackColor]];
                                                                  [activityIndicator setTintColor:[UIColor blackColor]];
                                                                  [currentCell.btnUseCoupon setEnabled:NO];
                                                                  [currentCell addSubview:activityIndicator];
                                                              }
                                                              NSString *myId = [[GlobalData sharedData]mUserInfo].mId;
                                                              NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
                                                              [params setObject:myId forKey:@"user_id"];
                                                              [params setObject:currentCoupon.message_id forKey:@"message_id"];
                                                              [[MessageService sharedData]useCouponWithDictionary:params success:^(id _responseObject) {
                                                                  [activityIndicator stopAnimating];
                                                                  [activityIndicator removeFromSuperview];
                                                                  NSLog(@"coupon successfully used");
                                                                  [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_COUPON_USE_LIST object:nil];
                                                                  [self showCouponStatus:currentCoupon index:indexPath cell:cell];
                                                                  [currentCell.btnUseCoupon setHidden:YES];
                                                                  [currentCell.btnUsed setHidden:NO];
                                                                  [currentCell.btnUseCoupon setEnabled:YES];
                                                              } failure:^(NSInteger _errorCode) {
                                                                  [activityIndicator stopAnimating];
                                                                  [activityIndicator removeFromSuperview];
                                                                  [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
                                                                  [currentCell.btnUseCoupon setHidden:NO];
                                                                  [currentCell.btnUsed setHidden:YES];
                                                                  [currentCell.btnUseCoupon setEnabled:YES];
                                                                  NSLog(@"coupon failed to use");
                                                              }];
                                                          }]];
    [alertViewController addAction:[NYAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                          }]];
    [self presentViewController:alertViewController animated:YES completion:nil];
//    
}
-(void)onTapCurrentCell:(id)cell
{
    
}
-(void)onTapPhoto:(UIImage *)image
{
    if(!image)
        return;
    PhotoFullViewController *fullMenuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoFullViewController"];
    fullMenuVC.mImage = image;
    [self presentViewController:fullMenuVC animated:YES completion:nil];
}
- (void)showCouponStatus :(Message *)currentMessage index:(NSIndexPath *)indexPath cell:(MessageTableViewCell *)cell
{
    if(!cell)
        return;
    [cell.btnExportCouponData setHidden:YES];
    [cell.btnUsed setHidden:YES];
    [cell.btnUseCoupon setHidden:YES];
    NSNumberFormatter *numberFormat = [[NSNumberFormatter alloc] init];
    numberFormat.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *myId = [numberFormat numberFromString:[[GlobalData sharedData]mUserInfo].mId];
    
    if([currentMessage.poster_id isEqualToNumber:myId])
    {
        [cell.btnExportCouponData setHidden:NO];
    }
    else
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"coupon_id = %@ AND status = 1 AND user_id = %@", currentMessage.message_id, myId];
        NSArray *arrCouponUseForThisMessage = [CouponUse MR_fetchAllSortedBy:@"updatedAt" ascending:NO withPredicate:predicate groupBy:nil delegate:self].fetchedObjects;
        if([arrCouponUseForThisMessage count] > 0)
        {
            [cell.btnUsed setHidden:NO];
        }
        else
        {
            [cell.btnUseCoupon setHidden:NO];
        }
    }
}
#pragma mark Paypal
- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController
{
    NSLog(@"paypal payment canceled");
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment
{
    NSLog(@"paypal payment success!!!");
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
