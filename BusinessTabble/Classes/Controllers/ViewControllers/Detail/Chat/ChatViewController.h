//
//  ChatViewController.h
//  BusinessTabble
//
//  Created by Liumin on 27/10/2016.
//  Copyright © 2016 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataViewController.h"
#import "MessageTableViewCell.h"
#import <CoreLocation/CoreLocation.h>

@interface ChatViewController : CoreDataViewController<SWTableViewCellDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate,  MessageDelegate>
// UITableViewDataSource, UITableViewDelegate, //CLLocationManagerDelegate,
{   
    UIActivityIndicatorView *activityIndiatorView;
}

@property (weak, nonatomic) IBOutlet UIView *vwChatNotEstablished;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIView *vwTextView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_text_view_bottom_space;
@property (weak, nonatomic) IBOutlet UIButton *btnRefreshLocation;
@property (weak, nonatomic) IBOutlet UIImageView *imgBubble;
@property (weak, nonatomic) IBOutlet UILabel *lblBubbleName;

//@property (strong, nonatomic) IBOutlet UITableView *tableView;
//@property (strong, nonatomic)NSFetchedResultsController *fetchedResultsController;
@property (nonatomic) BOOL isChatSaving;
@property (strong, nonatomic) Bubble *mCurrentBubble;
@end
