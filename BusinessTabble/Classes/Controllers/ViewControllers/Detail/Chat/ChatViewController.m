
//
//  ChatViewController.m
//  BusinessTabble
//
//  Created by Liumin on 27/10/2016.
//  Copyright © 2016 Liu. All rights reserved.
//

#import "ChatViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "MessageTableViewCell.h"
#import "IQKeyboardManager.h"
@interface ChatViewController ()

@end

@implementation ChatViewController
@synthesize mCurrentBubble;
@synthesize isChatSaving;
- (void)viewDidLoad {
    [super viewDidLoad];
    isChatSaving = NO;
    [_vwChatNotEstablished setHidden:NO];
    [_vwChatNotEstablished.superview bringSubviewToFront:_vwChatNotEstablished];
    
    [self.tableView setHidden:YES];
    [self.tableView setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapTableViewGesture:)];
    [self.tableView addGestureRecognizer:tapGesture];
    tapGesture.delegate = self;
    [self.view endEditing:YES];
    
    [_imgBubble.layer setCornerRadius:_imgBubble.frame.size.width / 2];
    [_imgBubble.layer setMasksToBounds:YES];
    [_imgBubble.layer setBorderWidth:1.f];
    [_imgBubble.layer setBorderColor:[UIColor colorWithWhite:1.f alpha:1.f].CGColor];
    
    _lblBubbleName.text = [NSString stringWithFormat:@"%@ Chat", mCurrentBubble.bubblename];
    NSString *photo_filename = [[GlobalData sharedData]urlencode:mCurrentBubble.photo_filename];
    [[GlobalData sharedData]showImageWithImage:_imgBubble name:photo_filename placeholder:nil];
        self.automaticallyAdjustsScrollViewInsets = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onUserLocationReceived:) name:kStrNotif_UserLocationReceived object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(refreshChatHistory) name:NOTIFICATION_CHAT_LIST_RECEIVED object:nil];
    
    [self onUserLocationReceived:nil];
    [self reloadDataWithCategoryName:@"" SearchString:@""];
    
  
    [[IQKeyboardManager sharedManager] setEnable:NO];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_TAB_BAR_HIDE object:nil];
    [[GlobalData sharedData]setIsChatViewOpened:YES];
    isChatSaving = NO;
    self.revealViewController.panGestureRecognizer.enabled = NO;

}
- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]postNotificationName:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kStrNotif_UserLocationReceived object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NOTIFICATION_CHAT_LIST_RECEIVED object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    
    [[IQKeyboardManager sharedManager] setEnable:YES];
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_TAB_BAR_SHOW object:nil];
    [[GlobalData sharedData]setIsChatViewOpened:NO];
    self.revealViewController.panGestureRecognizer.enabled = YES;
    [super viewWillDisappear:animated];
    
}
#pragma mark
#pragma mark Keyboard Event

- (void)onKeyboardShow:(NSNotification *)paramNotification
{
    NSDictionary* info = [paramNotification userInfo];
    CGSize kbSizeNew = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    _layout_constraint_text_view_bottom_space.constant = kbSizeNew.height ; //- tabBarHeight
    [self.view layoutIfNeeded];
    [self moveTableViewScrollToBottom];
}

- (void)onKeyboardHide:(NSNotification *)paramNotification
{
    _layout_constraint_text_view_bottom_space.constant = 0;
    [self.view layoutIfNeeded];
}
- (void)moveTableViewScrollToBottom
{
    NSInteger lastIndex = [[self.fetchedResultsController fetchedObjects] count];
    if(lastIndex > 3)
    {
        if(lastIndex > 20)
            lastIndex = 20;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:lastIndex - 1 inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath
                              atScrollPosition:UITableViewScrollPositionTop
                                      animated:YES];
    }
}

#pragma mark
#pragma mark  Chat Table Filtering
- (void)refreshChatHistory
{
    [self reloadDataWithCategoryName:@"" SearchString:@""];
}
- (void)reloadDataWithCategoryName:(NSString *)strCategoryName
                      SearchString:(NSString *)strSearchString
{
    NSPredicate *predicate = nil;
    predicate = [NSPredicate predicateWithFormat:@"user_id=%@ AND status=1", [[GlobalData sharedData]mUserInfo].mId];
    NSFetchedResultsController *fetchedControllerForHidePosts = [Chat_hideposter MR_fetchAllSortedBy:@"updatedAt" ascending:NO withPredicate:predicate groupBy:nil delegate:self];
    NSMutableArray *arrHiddenPosts = [[NSMutableArray alloc]init];
    for(Chat_hideposter *ch in fetchedControllerForHidePosts.fetchedObjects)
    {
        [arrHiddenPosts addObject:ch.poster_id];
    }
   
    predicate = [NSPredicate predicateWithFormat:@"bubble_id = %@ AND item_deleted = 0 AND NOT(poster_id IN %@)", mCurrentBubble.bubble_id, arrHiddenPosts];
    NSFetchRequest *fetchRequest = [Chat MR_requestAllSortedBy:@"updatedAt" ascending:YES withPredicate:predicate];
    NSError *error;
    NSInteger count = [[NSManagedObjectContext MR_defaultContext] countForFetchRequest:fetchRequest /*the one you have above but without limit */ error:&error];
    NSInteger size = 20;
    count -= size;
    [fetchRequest setFetchOffset:count>0?count:0];
    [fetchRequest setFetchLimit:size];
    NSFetchedResultsController *theFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[NSManagedObjectContext MR_defaultContext] sectionNameKeyPath:nil cacheName:nil];
    self.fetchedResultsController = theFetchedResultsController;

    BOOL success = [self.fetchedResultsController performFetch:&error];
    if(success)
    {
        [self.tableView reloadData];
        [self moveTableViewScrollToBottom];
    }
//    self.fetchedResultsController = [Chat MR_fetchAllSortedBy:@"updatedAt" ascending:YES withPredicate:predicate groupBy:nil delegate:self];
//    [self moveTableViewScrollToBottom];
}

#pragma mark
#pragma mark Actions

- (void)onTapTableViewGesture:(UIGestureRecognizer *)gestureRecognizer
{
    [self.view endEditing:YES];
}
- (void)onTapCurrentCell:(id)cell
{
    [self.view endEditing:YES];
    
}

- (IBAction)onTouchBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onTouchBtnSend:(id)sender {
    if([[GlobalData sharedData]isEmpty: [_txtMessage.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]])
    {
        //        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please write or attach photo." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        return;
    }
    [self sendMsg];
    
}
- (IBAction)onTouchBtnRefresh:(id)sender {
    activityIndiatorView = [[UIActivityIndicatorView alloc]init];
    [activityIndiatorView setFrame:CGRectMake(0, 0, 40, 40)];
    activityIndiatorView.center =CGPointMake(_btnRefreshLocation.center.x, _btnRefreshLocation.center.y - 6);
    [activityIndiatorView setColor:[UIColor blackColor]];
    [_vwChatNotEstablished addSubview:activityIndiatorView];
    [activityIndiatorView startAnimating];
    [_btnRefreshLocation setEnabled:NO];
    [NSTimer scheduledTimerWithTimeInterval:1.6 target:self selector:@selector(refreshLocation:) userInfo:nil repeats:NO];
}
- (void)refreshLocation:(id)sender
{
    [_btnRefreshLocation setEnabled:YES];
    [activityIndiatorView stopAnimating];
    [activityIndiatorView removeFromSuperview];
    [self onUserLocationReceived:nil];
}
-(void)onUserLocationReceived:(id)sender
{
    NSLog(@"current latitude = %f, current longitude = %f", [[GlobalData sharedData]currentLatitude],[[GlobalData sharedData]currentLongitude]);
    [self checkUserLocationUpdateForChatEstablishmentInWithSuccessHandler:^{
        [_vwChatNotEstablished setHidden:YES];
        [self.tableView setHidden:NO];
    } FailureHandler:^{
        [_vwChatNotEstablished setHidden:NO];
        [self.tableView setHidden:YES];
        
    }];
}
#pragma mark
#pragma mark Textfield Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= CHAT_POST_MAX_LENGTH;
}
#pragma mark
#pragma mark TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger totalNum = [[self.fetchedResultsController fetchedObjects] count];
//    if(totalNum > 20)
//        return 20;
    return totalNum;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 105.f;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"MessageTableViewCell";
    MessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    // Right Utility Buttons
//    if(!cell.mCurrentIndexPath)
    cell = [self configureBasicCell:cell indexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (MessageTableViewCell *)configureBasicCell:(MessageTableViewCell *) cell indexPath:(NSIndexPath *)indexPath
{
//    Chat *currentMessage = self.fetchedResultsController.fetchedObjects[[[self.fetchedResultsController fetchedObjects] count] - indexPath.row - 1];
    Chat * currentMessage = self.fetchedResultsController.fetchedObjects[indexPath.row];
    cell.lblUserName.text = currentMessage.username;
    cell.lblText.text = currentMessage.text;
    cell.mCurrentChat = currentMessage;
    cell.mCurrentIndexPath = indexPath;
    if(currentMessage.approvefrom > 0)
    {
        NSDate *approveFrom = [NSDate dateWithTimeIntervalSince1970:[currentMessage.approvefrom doubleValue]];
        [cell.lblApproveFrom setText:[NSString stringWithFormat:@"%@", [[GlobalData sharedData]dateTimeStringFromDate:approveFrom]]];
    }
    if(![[GlobalData sharedData]isEmpty:currentMessage.photo_filename])
    {
        NSString *photo_filename = [[GlobalData sharedData]urlencode:currentMessage.photo_filename];
        [[GlobalData sharedData]showImageWithImage:cell.imgPhoto name:photo_filename placeholder:nil];
        cell.layout_constraint_message_image_width.constant = SCREEN_WIDTH - 25;
    }
    else
        cell.layout_constraint_message_image_width.constant = 0;
    cell.layout_constraint_button_export_height.constant = 0;

    NSString *photo_filename = [[GlobalData sharedData]urlencode:currentMessage.user_photo_filename];
    [[GlobalData sharedData]showImageWithImage:cell.imgUserPhoto name:photo_filename placeholder:nil];
    [cell.imgUserPhoto.layer setBorderColor:[UIColor colorWithWhite:0.1 alpha:0.3].CGColor];
    [cell.imgUserPhoto.layer setBorderWidth:1.f];
    
    cell.rightUtilityButtons = [self rightButtonsWithIndexPath:indexPath cell:cell];
    cell.delegate = self;
    cell.messageDelegate = self;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark
#pragma mark Swipable TableView Delegates

- (NSArray *)rightButtonsWithIndexPath:(NSIndexPath *)indexPath cell:(MessageTableViewCell *)cell
{
  
//self.fetchedResultsController.fetchedObjects[[[self.fetchedResultsController fetchedObjects] count] - cell.mCurrentIndexPath.row  - 1];
    Chat *currentMessage = cell.mCurrentChat;

    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    NSString *string = @"Hide User";
    if([currentMessage.poster_id integerValue] == [[[GlobalData sharedData]mUserInfo].mId integerValue])
    {
        string = @"Delete";
    }
    NSLog(@"text %@", currentMessage.text);
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentCenter];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:string];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [string length])];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0] range:NSMakeRange(0, [string length])];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [string length])];
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor darkGrayColor] attributedTitle:attributedString];
    return rightUtilityButtons;
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell scrollingToState:(SWCellState)state
{

    
}
- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    return YES;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    MessageTableViewCell *currentCell = (MessageTableViewCell *)cell;
    if(!currentCell.mCurrentIndexPath)
        return;
    Chat *currentChat = currentCell.mCurrentChat;
    [cell hideUtilityButtonsAnimated:YES];
    if([currentChat.poster_id isEqualToNumber:[NSNumber numberWithInteger:[[[GlobalData sharedData]mUserInfo].mId integerValue]]])
    {
        [self deleteChat:currentChat currentCell:currentCell];
        return;
    }
    [self hideUser:currentChat currentCell:currentCell];
    
}

#pragma mark
#pragma mark Other Methods

- (void)checkUserLocationUpdateForChatEstablishmentInWithSuccessHandler:(void (^)())successHandler
                                                         FailureHandler:(void (^)())failureHandler
{
    if(!mCurrentBubble)
    {
        failureHandler();
        return;
    }
    float distance = [self getDistanceBetweenLocations];
    if(distance < CHAT_ESTABLISHMENT_RADIUS_IN_METER)
    {
        successHandler();
        return;
    }
    failureHandler();
}
- (float)getDistanceBetweenLocations
{
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:[mCurrentBubble.latitude floatValue] longitude:[mCurrentBubble.longitude floatValue]];
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:[[GlobalData sharedData]currentLatitude] longitude:[[GlobalData sharedData]currentLongitude]];
    //     NSLog(@"current latitude = %f, current longitude = %f", [mCurrentBubble.latitude floatValue],[mCurrentBubble.longitude floatValue]);
    CLLocationDistance distance = [locA distanceFromLocation:locB];
    return distance;
}

#pragma mark
#pragma mark API  Methods
- (void)sendMsg
{
    NSTimeInterval interval  = [[NSDate date] timeIntervalSince1970] ;
    NSString *postTimeStamp = [NSString stringWithFormat:@"%f", interval];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:@"1" forKey:@"approved"];
    [params setObject:[_txtMessage.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] forKey:@"text"];
    [params setObject:postTimeStamp forKey:@"approveFrom"];
    [params setObject:@"0" forKey:@"coupon"];
    [params setObject:self.mCurrentBubble.bubble_id forKey:@"bubble_id"];
    [params setObject:@"" forKey:@"photo_filename"];
    [self showProgress:@"Sending..."];
    isChatSaving = YES;
    [[ChatService sharedData]createMessageWithDictionary:params success:^(id _responseObject) {
        [self hideProgress];
        isChatSaving = NO;
        [self hideProgress];
        [_txtMessage setText:@""];
        [self.view endEditing:YES];
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_CHAT_LIST object:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_CHAT_HIDE_POSTER_LIST object:nil];
    } failure:^(NSInteger _errorCode) {
        isChatSaving = YES;
        [[ChatService sharedData]createMessageWithDictionary:params success:^(id _responseObject) {
            [self hideProgress];
            isChatSaving = NO;
            [self hideProgress];
            [_txtMessage setText:@""];
            [self.view endEditing:YES];
            [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_CHAT_LIST object:nil];
            [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_CHAT_HIDE_POSTER_LIST object:nil];
        } failure:^(NSInteger _errorCode) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            [self hideProgress];
        }];
        
    }];
}
- (void)deleteChat:(Chat *)currentChat currentCell:(MessageTableViewCell *)currentCell
{
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]init];
    [activityIndicator setCenter:currentCell.contentView.center];
    [currentCell.contentView addSubview:activityIndicator];
    [activityIndicator startAnimating];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:[NSString stringWithFormat:@"%ld", [currentChat.message_id integerValue]] forKey:@"chat_id"];
    isChatSaving = YES;
    [[ChatService sharedData]deleteMessageWithDictionary:params success:^(id _responseObject) {
        
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_CHAT_LIST object:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_CHAT_HIDE_POSTER_LIST object:nil];
        isChatSaving = NO;
        [self hideProgress];
        
       
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Successfully deleted." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
            [activityIndicator stopAnimating];
            [activityIndicator removeFromSuperview];
        }];
        
    } failure:^(NSInteger _errorCode) {
        isChatSaving = NO;
        [activityIndicator stopAnimating];
        [activityIndicator removeFromSuperview];
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:^{
        }];
    }];
    
}
- (void)hideUser:(Chat *)currentChat currentCell:(MessageTableViewCell *)currentCell
{
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]init];
    [activityIndicator setCenter:currentCell.contentView.center];
    [currentCell.contentView addSubview:activityIndicator];
    [activityIndicator startAnimating];
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:[NSString stringWithFormat:@"%ld", [currentChat.message_id integerValue]] forKey:@"chat_id"];
    [params setObject:[NSString stringWithFormat:@"%ld", [currentChat.poster_id integerValue]] forKey:@"poster_id"];
    isChatSaving = YES;
    [[ChatService sharedData]hideUser:params success:^(id _responseObject) {
        
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_CHAT_LIST object:nil];
        [[MainService sharedData] getBubbleChatHideListWithSuccessHandler:^(id _responseObject) {
            [self reloadDataWithCategoryName:@"" SearchString:@""];
            [self moveTableViewScrollToBottom];
        } FailureHandler:^{
            
        }];
        isChatSaving = NO;
        [self hideProgress];
        
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Successfully deleted." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
            [activityIndicator stopAnimating];
            [activityIndicator removeFromSuperview];
        }];
        
    } failure:^(NSInteger _errorCode) {
        isChatSaving = NO;
        [self hideProgress];
        
        [activityIndicator stopAnimating];
        [activityIndicator removeFromSuperview];
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:^{
        }];
    }];
}
@end
