//
//  WhosHereTableViewCell.m
//  BusinessTabble
//
//  Created by Haichen Song on 21/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "WhosHereTableViewCell.h"

@implementation WhosHereTableViewCell
@synthesize imgMember, lblMemberName;
- (void)awakeFromNib {
    // Initialization code
    [self layoutIfNeeded];
    [imgMember.layer setCornerRadius:imgMember.frame.size.width / 2];
    [imgMember.layer setMasksToBounds:YES];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
