//
//  MemberDetailViewController.h
//  BusinessTabble
//
//  Created by Haichen Song on 22/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BubbleMember+CoreDataProperties.h"
@interface MemberDetailViewController : UIViewController<UIGestureRecognizerDelegate>
@property (nonatomic, strong) BubbleMember *mCurrentMember;
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserLocation;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_photo_width;

@end
