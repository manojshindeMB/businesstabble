//
//  WhosHereTableViewCell.h
//  BusinessTabble
//
//  Created by Haichen Song on 21/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
@interface WhosHereTableViewCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgMember;
@property (weak, nonatomic) IBOutlet UILabel *lblMemberName;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@end
