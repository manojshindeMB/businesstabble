//
//  WhosHereViewController.m
//  BusinessTabble
//
//  Created by Haichen Song on 21/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "WhosHereViewController.h"
#import "WhosHereTableViewCell.h"
#import "MemberDetailViewController.h"
@interface WhosHereViewController ()

@end

@implementation WhosHereViewController
@synthesize mCurrentBubble;
@synthesize searchBar;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self reloadDataWithCategoryName:@"" SearchString:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)reloadDataWithCategoryName:(NSString *)strCategoryName
                      SearchString:(NSString *)strSearchString
{

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"joined = 1 AND bubble_id = %@", mCurrentBubble.bubble_id];
    if(![[GlobalData sharedData]isEmpty:strSearchString])
    {
        predicate = [NSPredicate predicateWithFormat:@"joined = %@ AND bubble_id = %@ AND username contains[c] %@",@1, mCurrentBubble.bubble_id, strSearchString];
    }

    self.fetchedResultsController = [BubbleMember MR_fetchAllSortedBy:@"joinedAt" ascending:NO withPredicate:predicate groupBy:nil delegate:self];
}
#pragma mark TableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.f;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BubbleMember *currentBubbleMember = self.fetchedResultsController.fetchedObjects[indexPath.row];
    NSString *CellIdentifier = @"WhosHereTableViewCell";
    WhosHereTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.lblMemberName.text = currentBubbleMember.username;
    NSString *photo_filename = [[GlobalData sharedData]urlencode:currentBubbleMember.photo_filename];
    [[GlobalData sharedData]showImageWithImage:cell.imgMember name:photo_filename placeholder:nil];
    
    [cell.imgMember.layer setBorderWidth:1.f];
    [cell.imgMember.layer setBorderColor:[UIColor whiteColor].CGColor];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BubbleMember *currentMember = self.fetchedResultsController.fetchedObjects[indexPath.row];
    MemberDetailViewController *memberVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MemberDetailViewController"];
    memberVC.mCurrentMember = currentMember;
    [self.navigationController pushViewController:memberVC animated:YES];
    
    //    NSLog(@"selected %@", [menuItems objectAtIndex:indexPath.row]);
}
#pragma mark Search Delegate
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar1
{
    if([searchBar isEqual:searchBar1])
    {
        [searchBar resignFirstResponder];
        searchBar.text = @"";
    }
    [self reloadDataWithCategoryName:@"" SearchString:searchBar.text];
}
- (void)searchBar:(UISearchBar *)searchBar1 textDidChange:(NSString *)searchText
{
    [self reloadDataWithCategoryName:@"" SearchString:searchBar.text];
}

@end
