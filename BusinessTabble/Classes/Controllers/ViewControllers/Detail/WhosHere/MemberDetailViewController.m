//
//  MemberDetailViewController.m
//  BusinessTabble
//
//  Created by Haichen Song on 22/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "MemberDetailViewController.h"

@interface MemberDetailViewController ()
{
    BOOL bLargePhoto;
}

@end

@implementation MemberDetailViewController
@synthesize mCurrentMember;
@synthesize lblUserLocation, lblUserName, imgUser;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    [imgUser.layer setCornerRadius:imgUser.frame.size.width / 2];
    [imgUser.layer setMasksToBounds:YES];
     NSString *photo_filename = [[GlobalData sharedData]urlencode:mCurrentMember.photo_filename];
    [[GlobalData sharedData]showImageWithImage:imgUser name:photo_filename placeholder:nil];
    lblUserName.text = mCurrentMember.username;
    lblUserLocation.text = [NSString stringWithFormat:@"%@ %@", mCurrentMember.city, mCurrentMember.state];
    bLargePhoto = NO;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onPhotoTapped:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.delegate =self;
    [imgUser addGestureRecognizer:tapGesture];
    [imgUser setContentMode:UIViewContentModeScaleAspectFill];
    [imgUser.layer setMasksToBounds:YES];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onTouchBtnGoBack:) name:BUBBLE_DETAIL_PAGE_BACK_WHOSHERE object:nil];
    
    
}

- (void)onPhotoTapped:(UITapGestureRecognizer *)gesture
{
    if(bLargePhoto == NO)
    {
        CGFloat estimateCorner =  (SCREEN_WIDTH - 40) / 2;
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"cornerRadius"];
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
        animation.fromValue = @(imgUser.layer.cornerRadius);
        animation.toValue = @(estimateCorner);
        animation.duration = 0.1;

        [imgUser.layer setCornerRadius:estimateCorner];
        [imgUser.layer addAnimation:animation forKey:@"cornerRadius"];
        [imgUser.layer setMasksToBounds:YES];
        [imgUser setClipsToBounds:YES];
        
        [UIView animateWithDuration:0.1 animations:^{
          
            
            _layout_constraint_photo_width.constant = SCREEN_WIDTH - 40;

            
            [imgUser layoutIfNeeded];
            [self.view layoutIfNeeded];

        }];
//        [UIView animateWithDuration:0.0 animations:^{
//              _layout_constraint_photo_width.constant = SCREEN_WIDTH - 40;
//            [imgUser.layer setCornerRadius:imgUser.frame.size.width / 2];
//            [imgUser.layer setMasksToBounds:YES];
//            [imgUser setClipsToBounds:YES];
//            [self.view layoutIfNeeded];
//        } completion:^(BOOL finished) {
//            [imgUser.layer setCornerRadius:imgUser.frame.size.width / 2];
//        }];
    }
    else
    {
        [UIView animateWithDuration:0.0 animations:^{
            _layout_constraint_photo_width.constant = 150;
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            [self.view layoutIfNeeded];
            [imgUser.layer setCornerRadius:imgUser.frame.size.width / 2];
        }];
    }
    bLargePhoto = !bLargePhoto;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onTouchBtnGoBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
