//
//  EventBubbleSectionHeaderCell.h
//  BusinessTabble
//
//  Created by Liumin on 11/07/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventBubbleSectionHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblHeaderTitle;

@end
