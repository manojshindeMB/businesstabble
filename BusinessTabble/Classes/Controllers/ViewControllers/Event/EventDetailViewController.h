//
//  ActivityDetailViewController.h
//  BusinessTabble
//
//  Created by Liumin on 08/07/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton_Image *btnSetting;
@property (weak, nonatomic) IBOutlet UIImageView *imgBanner;


@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNumLikes;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;


@property (weak, nonatomic) IBOutlet UILabel *lblStartTime;
@property (weak, nonatomic) IBOutlet UILabel *lblEndTime;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;

@property (strong, nonatomic) Bubble * mCurBubble;

@property (nonatomic) BOOL isReviewByAdmin;
@property (weak, nonatomic) IBOutlet UIButton *btnReject;
@property (weak, nonatomic) IBOutlet UIButton *btnApprove;

@end
