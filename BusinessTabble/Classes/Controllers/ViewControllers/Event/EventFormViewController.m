//
//  BubbleFormViewController.m
//  BusinessTabble
//
//  Created by Tian Ming on 14/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "EventFormViewController.h"
#import "NIDropDown.h"
#import "HomeViewController.h"
#import "HomeTabBarController.h"
#import "SearchAddressViewController.h"
#import "ChatViewController.h"
#define CURRENT_PAGE_TYPE_ADD 01
#define CURRENT_PAGE_TYPE_EDIT 02

#define CURRENT_PHOTO_MODE_LOGO 01
#define CURRENT_PHOTO_MODE_BANNER 02
#define CURRENT_PHOTO_MODE_PHOTO1 03
#define CURRENT_PHOTO_MODE_PHOTO2 04

#define DATE_PICKIER_MODE_FROM_TAG 01
#define DATE_PICKIER_MODE_TO_TAG 02


@interface EventFormViewController ()

@end

@implementation EventFormViewController
@synthesize btnSubmit,svMain,locationCoordinate;
@synthesize btnBanner;
@synthesize mImgBanner = _mImgBanner;
@synthesize mCurrentPhotoMode;
@synthesize mCurrentActivity;
@synthesize mCurrentPageType;
@synthesize isBannerChanged;
@synthesize mStartTime = _mStartTime;
@synthesize mEndTime = _mEndTime;
@synthesize datePicker;



- (void)viewDidLoad {
    [super viewDidLoad];
    revealViewController = [self revealViewController];
    [revealViewController panGestureRecognizer];
    [revealViewController tapGestureRecognizer];

    if ([_txtStartDate respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor colorWithWhite:1.f alpha:0.7];
        _txtStartDate.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: color}];
        _txtEndDate.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"" attributes:@{NSForegroundColorAttributeName: color}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
    }
    
    btnBanner.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    btnBanner.titleLabel.textAlignment = NSTextAlignmentCenter;
    [btnBanner.layer setCornerRadius:5.f];
    [btnBanner.layer setMasksToBounds:YES];
    [btnBanner.layer setBorderWidth:5.f];
    [btnBanner.layer setBorderColor:[UIColor whiteColor].CGColor];
    [btnBanner setContentMode:UIViewContentModeScaleAspectFill];
    
    [btnSubmit.layer setBorderWidth:0.f];
    [btnSubmit.layer setBorderColor:[UIColor whiteColor].CGColor];
    [btnSubmit.layer setMasksToBounds:YES];
    
    photoActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel"  destructiveButtonTitle:nil otherButtonTitles:@"From Camera",@"From Photo Library", nil];
    
//    [self setDefaultValues];
    isBannerChanged = NO;
    [_btnBack setHidden:YES];
    [_btnMenu setHidden:NO];
    
    datePicker = [[IQActionSheetPickerView alloc] initWithTitle:@"Select date" delegate:self];
    [datePicker setActionSheetPickerStyle:IQActionSheetPickerStyleDateTimePicker];
    [_btnRemove setHidden:YES];
    if(mCurrentPageType == CURRENT_PAGE_TYPE_EDIT)
    {
        _txtBubbleName.text = mCurrentActivity.bubblename;
        _txtDescription.text = mCurrentActivity.content;
        _txtContactNumber.text = mCurrentActivity.contactnumber;
        _txtLocation.text = mCurrentActivity.location;
        _txtWebsite.text = mCurrentActivity.website;
        [btnSubmit setTitle:@"Save" forState:UIControlStateNormal];
        locationCoordinate = CLLocationCoordinate2DMake([mCurrentActivity.latitude floatValue], [mCurrentActivity.longitude floatValue]);
        self.mStartTime = [NSDate dateWithTimeIntervalSince1970:[mCurrentActivity.event_starttime doubleValue]];
        self.mEndTime = [NSDate dateWithTimeIntervalSince1970:[mCurrentActivity.event_endtime doubleValue]];
        UIImageView *tempBannerImage = [[UIImageView alloc]init]; //sd_setBackgroundImage for button
        [tempBannerImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",g_storagePrefix,self.mCurrentActivity.banner_filename]]  placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            self.mImgBanner = image;
        }];
        [_btnBack setHidden:NO];
        [_btnMenu setHidden:YES];
        [_btnRemove setHidden:NO];
    }
    
}
- (void)setDefaultValues
{
    if(![[GlobalData sharedData]isEmpty:[[GlobalData sharedData]currentCity]])
    {
        locationCoordinate = CLLocationCoordinate2DMake([[GlobalData sharedData]currentLatitude], [[GlobalData sharedData]currentLongitude]);
        
        [_txtLocation setText:[NSString stringWithFormat:@"%@, %@, %@",[[GlobalData sharedData]currentCity], [[GlobalData sharedData]currentState], [[GlobalData sharedData]currentCountry]]];
    }

}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Actions
- (IBAction)onTouchBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onTouchBtnMenu:(id)sender {
    [revealViewController revealToggle:sender];
}

- (IBAction)onTouchBtnBanner:(id)sender {
    [self.view endEditing:YES];
    mCurrentPhotoMode = CURRENT_PHOTO_MODE_BANNER;
    [photoActionSheet showInView:[self.view window]];
}
- (IBAction)onTouchBtnRemoveEvent:(id)sender {
    NYAlertViewController *alertViewController = [[GlobalData sharedData]createNewAlertViewWithTitle:@"\n Are you sure? \n" view:self.view];
    [alertViewController addAction:[NYAlertAction actionWithTitle:NSLocalizedString(@"Ok", nil)
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                              [self removeEvent];
                                                          }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                          }]];
    
    [self presentViewController:alertViewController animated:YES completion:nil];
    

}
- (void)removeEvent
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:mCurrentActivity.bubble_id forKey:@"bubble_id"];
    NSString *successStr = @"Successfully removed.";
    [self showProgress:@"Please wait..."];
    [[BubbleService sharedData]removeBubbleWithDictionary:params success:^(id _responseObject) {
        [self hideProgress];
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:successStr BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
            [[GlobalData sharedData]setIsBack:YES];
            [self onTouchBtnBack:nil];
        }];
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_DATA object:nil];
    } failure:^(NSInteger _errorCode) {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:^{
        }];
        [self hideProgress];
    }];
    
}

- (IBAction)onTouchBtnSubmit:(id)sender {
    if([[GlobalData sharedData]isSessionExpired])
       [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
    [self checkBubbleFormValidationWithSuccessHandler:^{
        if(mCurrentPageType == CURRENT_PAGE_TYPE_ADD)
        {
            CustomIOSAlertView *alertView = [[GlobalData sharedData] createBusinessCreationNoticeViewWithTitle:@"Please be patient, approval may take up to 24 hours."];
            [alertView show];
            alertView.delegate = self;            
            return;
        }
        else
            [self submitBubbleCreation];
    } FailureHandler:^{
        
    }];
}
- (void)customIOS7dialogButtonTouchUpInside:(id)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self submitBubbleCreation];
}
- (void)submitBubbleCreation
{
    
    NSDate *date = [NSDate date];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[_txtBubbleName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  forKey:@"bubblename"];
    [params setObject:_txtWebsite.text forKey:@"website"];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"creator_id"];
    [params setObject:@"Event" forKey:@"category"];
    [params setObject:_txtLocation.text forKey:@"location"];
    [params setObject:[NSString stringWithFormat:@"%f",locationCoordinate.latitude] forKey:@"latitude"];
    [params setObject:[NSString stringWithFormat:@"%f",locationCoordinate.longitude] forKey:@"longitude"];
    [params setObject:_txtDescription.text forKey:@"description"];
    
    
    if(mCurrentPageType == CURRENT_PAGE_TYPE_EDIT)
        [params setObject:[NSString stringWithFormat:@"%ld", [mCurrentActivity.bubble_id integerValue]] forKey:@"bubble_id"];
    
    
    
    NSString *strfilenameBanner = [NSString stringWithFormat:@"%@%ld_banner.jpg", [[GlobalData sharedData]mUserInfo].mId , (long)[date timeIntervalSince1970]];
    strfilenameBanner = [[GlobalData sharedData]urlencode:strfilenameBanner];
    if(mCurrentPageType == CURRENT_PAGE_TYPE_EDIT && !isBannerChanged)
        strfilenameBanner = mCurrentActivity.banner_filename;
    [params setObject:strfilenameBanner forKey:@"banner_filename"];
    
    [params setObject:@"" forKey:@"owner"];
    [params setObject:_txtContactNumber.text forKey:@"contactnumber"];
    [params setObject:[NSString stringWithFormat:@"%f",0.f] forKey:@"color_red"];
    [params setObject:[NSString stringWithFormat:@"%f",0.f] forKey:@"color_green"];
    [params setObject:[NSString stringWithFormat:@"%f",0.f] forKey:@"color_blue"];
    [params setObject:@"" forKey:@"photo_filename"];
    [params setObject:@"" forKey:@"photo_changed"];
    [params setObject:[NSString stringWithFormat:@"%d", (int)[self.mStartTime timeIntervalSince1970]] forKey:@"event_starttime"];
    [params setObject:[NSString stringWithFormat:@"%d", (int)[self.mEndTime timeIntervalSince1970]] forKey:@"event_endtime"];
    
    
    NSString *strfilenamePhoto1 = @"";
    [params setObject:strfilenamePhoto1 forKey:@"activity_filename1"];
    
    NSString *strfilenamePhoto2 = @"";
    [params setObject:strfilenamePhoto2 forKey:@"activity_filename2"];
    
    if(mCurrentPageType == CURRENT_PAGE_TYPE_ADD)
    {
        [self createNewActivityWithParams:params success:^(id _responseObject) {
            [self hideProgress];
            [[GlobalData sharedData] uploadPhotoWithName:strfilenameBanner oldname:@"" image:self.mImgBanner type:@"" completion:^{
                [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Submitted For Review Successfully." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
                    //            [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_DATA object:nil];
                }];
                [self goToMainPage];
            } FailureHandler:^{
                
            }];
            
        } failure:^(NSInteger _errorCode) {
            [self hideProgress];
            if(_errorCode == 400)
            {
                [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:[NSString stringWithFormat:@"There is already existing event '%@'.",_txtBubbleName.text] BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
                return;
            }
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        }];
    }
    else if(mCurrentPageType == CURRENT_PAGE_TYPE_EDIT)
    {
        [self updateActivityWithParams:params success:^(id _responseObject) {
            [self hideProgress];
            [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_DATA object:nil];
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Event Updated Successfully." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
                
                
            }];
            mCurrentActivity.bubblename = [_txtBubbleName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            mCurrentActivity.category = @"Event";
            mCurrentActivity.website = _txtWebsite.text;
            mCurrentActivity.location = _txtLocation.text;
            mCurrentActivity.content = _txtDescription.text;
            mCurrentActivity.event_starttime = [NSNumber numberWithInt:(int)[self.mStartTime timeIntervalSince1970]] ;
            mCurrentActivity.event_endtime = [NSNumber numberWithInt:(int)[self.mEndTime timeIntervalSince1970]] ;
            mCurrentActivity.contactnumber = _txtContactNumber.text;
            mCurrentActivity.latitude = [NSNumber numberWithFloat: locationCoordinate.latitude];
            mCurrentActivity.longitude =[NSNumber numberWithFloat: locationCoordinate.longitude];
            [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
                
            }];
            if(isBannerChanged)
            {
                NSString *oldPhotoName = mCurrentActivity.banner_filename;
                isBannerChanged = NO;
                [[GlobalData sharedData] uploadPhotoWithName:strfilenameBanner oldname:mCurrentActivity.banner_filename image:self.mImgBanner type:@"" completion:^{
                    mCurrentActivity.banner_filename = strfilenameBanner;
                    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
                        
                    }];
                } FailureHandler:^{
                    mCurrentActivity.banner_filename = oldPhotoName;
                }];
            }
        } failure:^(NSInteger _errorCode) {
            [self hideProgress];
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
            
        }];
    }
}
- (void)goToMainPage
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HomeTabBarController *pVCHome = [storyboard instantiateViewControllerWithIdentifier:@"HomeTabBarController"];
    [revealViewController revealToggle:nil];
    [revealViewController pushFrontViewController:pVCHome animated:YES];

}
- (void)createNewActivityWithParams : (NSMutableDictionary *)params
                             success:(void (^)())_success
                             failure:(void (^)(NSInteger _errorCode))_failure

{
    [self showProgress:@"Please wait..."];
    [[BubbleService sharedData]createBubbleWithDictionary:params success:^(id _responseObject) {
       
        
        _success();
        
        
    } failure:^(NSInteger _errorCode) {
        
        
        _failure(_errorCode);
    }];
}
- (void)updateActivityWithParams : (NSMutableDictionary *)params
                             success:(void (^)())_success
                             failure:(void (^)())_failure

{
    [self showProgress:@"Please wait..."];
    [[BubbleService sharedData]updateBubbleWithDictionary:params success:^(id _responseObject) {
         _success();
      
        
        
    } failure:^(NSInteger _errorCode) {
//        [self hideProgress];
        _failure();
    }];
}

#pragma mark Setter & Getter
- (void)setMStartTime:(NSDate *)mStartTime
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [_iconStartTime setHidden:YES];
    _mStartTime = mStartTime;
    [_txtStartDate setText: [formatter stringFromDate:mStartTime]];
}
- (void)setMEndTime:(NSDate *)mEndTime
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [_iconEndTime setHidden:YES];
    _mEndTime = mEndTime;
    [_txtEndDate setText: [formatter stringFromDate:mEndTime]];
}

- (void)setMImgBanner:(UIImage *)mImgBanner
{
    _mImgBanner = mImgBanner;
    if(mImgBanner != nil)
    {
        [btnBanner setBackgroundImage:mImgBanner forState:UIControlStateNormal];
        [btnBanner setTitle:@"" forState:UIControlStateNormal];
    }
}
#pragma mark Address Select Delegate
- (void)selectAddressViewController:(SearchAddressViewController *)viewController didSelectedLocation:(NSString *)address coordinate:(CLLocationCoordinate2D)coordinate
{
    locationCoordinate = coordinate;
    [_txtLocation setText:address];
}
#pragma mark UITextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([textField isEqual:_txtLocation])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SearchAddressViewController *searchVC = [storyboard instantiateViewControllerWithIdentifier:@"SearchAddressViewController"];
        searchVC.delegate = self;
        [self presentViewController:searchVC animated:YES completion:nil];
        return NO;
    }
    if([textField isEqual:_txtStartDate] || [textField isEqual:_txtEndDate])
    {
        [self.view endEditing:YES];
        if([textField isEqual:_txtStartDate])
        {
            [datePicker setTag:DATE_PICKIER_MODE_FROM_TAG];
        }
        else
        {
            [datePicker setTag:DATE_PICKIER_MODE_TO_TAG];
        }
        [datePicker show];
        return NO;
    }
    return YES;
}
#pragma mark ActionSheet Delegates
-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectTitles:(NSArray *)titles
{
    
    
}
-(void)actionSheetPickerView:(IQActionSheetPickerView *)pickerView didSelectDate:(NSDate *)date
{

    
    if(pickerView.tag == DATE_PICKIER_MODE_FROM_TAG)
    {
        
        self.mStartTime = date;
        
    }
    else if(pickerView.tag == DATE_PICKIER_MODE_TO_TAG)
        self.mEndTime = date;
    //        [_txtDateTo setText:[formatter stringFromDate:date]];
    
}

#pragma mark  actionsheet didDismissWithButtonIndex
- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ( buttonIndex == 2 )
        return;
    if ( actionSheet == photoActionSheet )
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.editing = YES;
        picker.videoMaximumDuration = kMaxVideoLenth;
        {
            if (buttonIndex == 0) {
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
            } else if (buttonIndex == 1) {
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            }
        }
        picker.mediaTypes = [NSArray arrayWithObjects: (NSString*)kUTTypeImage, nil];//[NSArray arrayWithObjects:(NSString*)kUTTypeMovie, (NSString*)kUTTypeImage, nil];
        [self presentViewController:picker animated:TRUE completion:nil ];
    }
}
#pragma mark didFinishPickingMediaWithInfo
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *imgtmp = [info objectForKey:UIImagePickerControllerOriginalImage];
    if(!imgtmp || imgtmp == nil)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"You can only use photo." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        return;
    }
    imgtmp = [GlobalData scaleAndRotateImage:imgtmp];
    [self dismissViewControllerAnimated:YES completion:^{
        PECropViewController *controller = [[PECropViewController alloc] init];
        controller.delegate = self;
        controller.image = imgtmp;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            controller.modalPresentationStyle = UIModalPresentationFormSheet;
        }
        [controller setKeepingCropAspectRatio:NO];
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        }
        [self presentViewController:navigationController animated:YES completion:NULL];

    }];
}
#pragma mark Cropper Delegate
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    self.mImgBanner = croppedImage;
    isBannerChanged = YES;
}
- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}
#pragma mark Other Methods
- (void)checkBubbleFormValidationWithSuccessHandler:(void (^)())successHandler FailureHandler:(void (^)())failureHandler
{
    if([[GlobalData sharedData]isEmpty:_txtBubbleName.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide bubble name." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if([[GlobalData sharedData]isEmpty:_txtWebsite.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide website." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if([[GlobalData sharedData]isEmpty:_txtLocation.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide location." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    
    if([[GlobalData sharedData]isEmpty:_txtContactNumber.text] )
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide contact number." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if(!self.mImgBanner && self.mCurrentPageType != CURRENT_PAGE_TYPE_EDIT)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide event image." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if([self.mStartTime compare:self.mEndTime] != NSOrderedAscending)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please add valid start and end time." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    

    successHandler();
    
}


@end
