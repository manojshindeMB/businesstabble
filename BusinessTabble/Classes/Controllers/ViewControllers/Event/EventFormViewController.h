//
//  BubbleFormViewController.h
//  BusinessTabble
//
//  Created by Tian Ming on 14/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGLDropDownMenu.h"
#import "NIDropDown.h"

#import "SearchAddressViewController.h"
#import "IQActionSheetPickerView.h"

@interface EventFormViewController : UIViewController<UITextFieldDelegate,UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,SelectAddressViewControllerDelegate, IQActionSheetPickerViewDelegate, PECropViewControllerDelegate, CustomIOSAlertViewDelegate>
{
    SWRevealViewController *revealViewController;
    NSArray *arrBubbleCategory;
    UIActionSheet *photoActionSheet;
}
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@property (weak, nonatomic) IBOutlet UITextField *txtStartDate;
@property (weak, nonatomic) IBOutlet UITextField *txtEndDate;
@property (weak, nonatomic) IBOutlet UIImageView *iconStartTime;
@property (weak, nonatomic) IBOutlet UIImageView *iconEndTime;

@property (weak, nonatomic) IBOutlet UIButton *btnRemove;

@property (weak, nonatomic) IBOutlet UIButton *btnBanner;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtBubbleName;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtWebsite;

@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtLocation;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;

@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtContactNumber;


@property (weak, nonatomic) IBOutlet UIScrollView *svMain;

@property (nonatomic) CLLocationCoordinate2D locationCoordinate;
@property (nonatomic, strong) Bubble *mCurrentActivity;
@property (nonatomic) NSInteger mCurrentPageType;

@property (nonatomic) BOOL isBannerChanged;
@property (nonatomic, strong) NSDate *mStartTime;
@property (nonatomic, strong) NSDate *mEndTime;
@property (nonatomic, strong) NSDate *mStartDate;
@property (nonatomic, strong) NSDate *mEndDate;
@property (nonatomic, strong) IQActionSheetPickerView *datePicker;

//Property
@property (nonatomic, strong) UIImage *mImgBanner;
@property (nonatomic) NSInteger mCurrentPhotoMode;

@end
