//
//  ActivityDetailViewController.m
//  BusinessTabble
//
//  Created by Liumin on 08/07/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import "EventDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "BubbleMapViewController.h"
#import "EventFormViewController.h"

@interface EventDetailViewController ()

@end

@implementation EventDetailViewController
@synthesize mCurBubble;
@synthesize isReviewByAdmin;
- (void)viewDidLoad {
    [super viewDidLoad];
    [_btnSetting centerImageAndTitle:2.f];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([[GlobalData sharedData]isBack])
    {
        [[GlobalData sharedData]setIsBack:NO];
        [self onTouchBtnBack:nil];
        return;
    }
    if(mCurBubble)
    {
        if(![[GlobalData sharedData]isEmpty: mCurBubble.banner_filename])
        {
            NSString *banner_filename = [[GlobalData sharedData]urlencode:mCurBubble.banner_filename];
            [[GlobalData sharedData]showImageWithImage:_imgBanner name:banner_filename placeholder:nil];
        }
        
        NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc]initWithString:mCurBubble.content];
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        [style setLineSpacing:5];
        [attrString addAttribute:NSParagraphStyleAttributeName
                           value:style
                           range:NSMakeRange(0, mCurBubble.content.length)];
        _lblDescription.attributedText = attrString;
        _lblTitle.text = mCurBubble.bubblename;
        if([mCurBubble.creator_id integerValue] != [[[GlobalData sharedData]mUserInfo].mId integerValue])
            [_btnSetting setHidden:YES];
        _lblLocation.text = mCurBubble.location;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        
        _lblStartTime.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate: [NSDate dateWithTimeIntervalSince1970:[mCurBubble.event_starttime doubleValue]]]];
        _lblEndTime.text = [NSString stringWithFormat:@"%@", [formatter stringFromDate: [NSDate dateWithTimeIntervalSince1970:[mCurBubble.event_endtime doubleValue]]]];
        
        if(isReviewByAdmin)
        {
            [_btnReject setHidden:NO];
            [_btnApprove setHidden:NO];
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
#pragma mark Actions
- (IBAction)onTouchBtnBack:(id)sender {
    if(isReviewByAdmin)
    {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)onTouchBtnApprove:(id)sender {
    NSString *successStr = @"Successfully approved.";
    NSString *type = @"1";
    if([sender isEqual:_btnReject])
    {
        successStr = @"Successfully rejected.";
        type = @"-1";
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:mCurBubble.bubble_id forKey:@"bubble_id"];
    [params setObject:type forKey:@"type"];
    [self showProgress:@"Please wait..."];
    [[BubbleService sharedData]approveBubbleWithDictionary:params success:^(id _responseObject) {
        [self hideProgress];
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:successStr BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
        if([sender isEqual:_btnReject])
            mCurBubble.reviewstatus = [NSNumber numberWithInt:-1];
        else
            mCurBubble.reviewstatus = [NSNumber numberWithInt:1];
        [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreAndWait];
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MEMBER_LIST object:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MENU_LIST object:nil];
        
    } failure:^(NSInteger _errorCode) {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:^{
        }];
        [self hideProgress];
    }];
    

}


- (IBAction)onTouchBtnSetting:(id)sender {
    
    EventFormViewController * formVC = [self.storyboard instantiateViewControllerWithIdentifier:@"EventFormViewController"];
    formVC.mCurrentPageType = 2;
    formVC.mCurrentActivity = mCurBubble;
    [self.navigationController pushViewController:formVC animated:YES];
}
- (IBAction)onTouchBtnCall:(id)sender {
    NSString *phoneNumber = mCurBubble.contactnumber;
    if([[GlobalData sharedData]isEmpty:mCurBubble.contactnumber])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"The phone number is not set yet." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        
    }
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneNumber]]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneNumber]]];
    }
    else {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"This Device Doesn't Support Call Functionality" BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        
    }
    
   
}
- (IBAction)onTouchBtnLocation:(id)sender {
    
//    if ([[UIApplication sharedApplication] canOpenURL:
//         [NSURL URLWithString:@"comgooglemaps://"]]) {
//        NSString *url = [NSString stringWithFormat:@"comgooglemaps://?center=%@,%@&zoom=14&views=traffic", mCurBubble.latitude, mCurBubble.longitude];
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
//    } else {
//        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Can not open google map in your device." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
//    }
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Detail" bundle:nil];
    BubbleMapViewController *mapVC = [storyboard instantiateViewControllerWithIdentifier:@"BubbleMapViewController"];
    mapVC.mCurrentBubble = mCurBubble;
    [self presentViewController:mapVC animated:YES completion:nil];

}
- (IBAction)onTouchBtnWebsite:(id)sender {
    NSString *strUrl = [mCurBubble.website stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if(![strUrl hasPrefix:@"http"])
    {
        strUrl = [NSString stringWithFormat:@"http://%@", strUrl];
    }
    NSURL *url = [NSURL URLWithString:strUrl];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

@end
