//
//  LoginNavigationController.h
//  BusinessTabble
//
//  Created by Tian Ming on 09/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface LoginNavigationController : UINavigationController<UIGestureRecognizerDelegate, SWRevealViewControllerDelegate>
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@end
