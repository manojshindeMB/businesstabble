//
//  BusinessBubbleDetailViewController.m
//  BusinessTabble
//
//  Created by Haichen Song on 17/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "ReviewBubbleDetailViewController.h"
#import <MagicalRecord/MagicalRecord.h>

@interface ReviewBubbleDetailViewController ()

@end

@implementation ReviewBubbleDetailViewController
@synthesize mCurrentBubble;
@synthesize btnApprove, btnReject;
@synthesize lblBubbleDescription,lblBubbleLocation,lblBubbleName,lblBubbleOwner, imgBubble,layout_constraint_lbldescription_height;
- (void)viewDidLoad {
    [super viewDidLoad];
    if(![[GlobalData sharedData]isEmpty:mCurrentBubble.content])
    {
        [lblBubbleDescription setText:mCurrentBubble.content];
    }
    
    NSString *photo_filename = [[GlobalData sharedData]urlencode:mCurrentBubble.photo_filename];
    if([mCurrentBubble.category isEqualToString:@"Event"])
        photo_filename = [[GlobalData sharedData]urlencode:mCurrentBubble.banner_filename];

    [[GlobalData sharedData]showImageWithImage:imgBubble name:photo_filename placeholder:nil];
    [self.view layoutIfNeeded];
    [imgBubble.layer setCornerRadius:imgBubble.frame.size.width / 2];
    [imgBubble.layer setMasksToBounds:YES];
    [imgBubble.layer setBorderWidth:1.f];
    [imgBubble.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [imgBubble setContentMode:UIViewContentModeScaleAspectFill];
    [imgBubble.layer setMasksToBounds:YES];
    
    lblBubbleName.text = mCurrentBubble.bubblename;
    lblBubbleLocation.text = mCurrentBubble.location;
    
    lblBubbleOwner.text = [NSString stringWithFormat:@"Created By %@ (%@)", mCurrentBubble.username, mCurrentBubble.contactnumber];
}
- (void)viewDidLayoutSubviews
{
    if(![[GlobalData sharedData]isEmpty:mCurrentBubble.content])
    {
        CGSize cz = [[GlobalData sharedData]messageSize:lblBubbleDescription.text font:lblBubbleDescription.font width:lblBubbleDescription.frame.size.width];
        layout_constraint_lbldescription_height.constant = cz.height;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Actions
- (IBAction)onTouchBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)onTouchBtnApproveBubble:(id)sender {
    NSString *successStr = @"Successfully approved.";
    NSString *type = @"1";
    if([sender isEqual:btnReject])
    {
        successStr = @"Successfully rejected.";
        type = @"-1";
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:mCurrentBubble.bubble_id forKey:@"bubble_id"];
    [params setObject:type forKey:@"type"];
    [self showProgress:@"Please wait..."];
    [[BubbleService sharedData]approveBubbleWithDictionary:params success:^(id _responseObject) {
        [self hideProgress];
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:successStr BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
        if([sender isEqual:btnReject])
            mCurrentBubble.reviewstatus = [NSNumber numberWithInt:-1];
        else
            mCurrentBubble.reviewstatus = [NSNumber numberWithInt:1];
        [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreAndWait];
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MEMBER_LIST object:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MENU_LIST object:nil];
        
    } failure:^(NSInteger _errorCode) {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:^{
        }];
        [self hideProgress];
    }];

}


@end
