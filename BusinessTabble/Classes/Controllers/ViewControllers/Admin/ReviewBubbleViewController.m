//
//  BusinessBubbleViewController.m
//  BusinessTabble
//
//  Created by Tian Ming on 15/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "ReviewBubbleViewController.h"
#import "BubbleService.h"
#import "ReviewBubbleDetailViewController.h"

#import <MagicalRecord/MagicalRecord.h>
#import "BusinessBubbleTableViewCell.h"
#import "BusinessBubbleDetailViewController.h"
#import "ActivityDetailViewController.h"
#import "EventBubbleSectionHeaderCell.h"
#import "EventDetailViewController.h"
@interface ReviewBubbleViewController ()

@end
@implementation ReviewBubbleViewController
@synthesize searchBar;
@synthesize mArrBubbleList;
@synthesize mBubbleCategory = _mBubbleCategory;
@synthesize btnBubbleTypeAll, btnBubbleTypeBars, btnBubbleTypeEvents, btnBubbleTypeRestaurants, btnBubbleTypeRetail;
@synthesize arrCollapseStatus;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customizeSearchBar];
    
    [btnBubbleTypeAll setSelected:YES];
    self.mBubbleCategory = @"Activity";
    
    [btnBubbleTypeAll centerImageAndTitle:2.f];
    [btnBubbleTypeBars centerImageAndTitle:2.f];
    [btnBubbleTypeEvents centerImageAndTitle:2.f];
    [btnBubbleTypeRestaurants centerImageAndTitle:2.f];
    [btnBubbleTypeRetail centerImageAndTitle:2.f];
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_DATA object:nil];
    
}

- (void)reloadDataWithCategoryName:(NSString *)strCategoryName
                      SearchString:(NSString *)strSearchString
{
    NSString *strDefaultPredicate = @"reviewstatus = 0 AND field_deleted = 0 "; //AND (joined = nil OR joined = 0)
    NSPredicate *predicate = [NSPredicate predicateWithFormat:strDefaultPredicate];
    if(![[GlobalData sharedData]isEmpty:strCategoryName] && ![[GlobalData sharedData]isEmpty:strSearchString])
        predicate = [NSPredicate predicateWithFormat:@"reviewstatus = 0 AND field_deleted = 0  AND category = %@ AND bubblename contains[c] %@", strCategoryName, strSearchString]; //AND (joined = nil OR joined = 0)
    else if([[GlobalData sharedData]isEmpty:strCategoryName] && ![[GlobalData sharedData]isEmpty:strSearchString])
        predicate = [NSPredicate predicateWithFormat:@"reviewstatus = 0 AND field_deleted = 0  AND  bubblename contains[c] %@", strSearchString]; //AND (joined = nil OR joined = 0)
    else if(![[GlobalData sharedData]isEmpty:strCategoryName] && [[GlobalData sharedData]isEmpty:strSearchString])
        predicate = [NSPredicate predicateWithFormat:@"reviewstatus = 0 AND field_deleted = 0  AND category = %@ ", strCategoryName];//AND (joined = nil OR joined = 0)
    NSString *groupBy = nil;
    
    
    
    if([strCategoryName isEqualToString:@"Event"])
        groupBy = @"event_startdate";
    
    self.fetchedResultsController = [Bubble MR_fetchAllSortedBy:@"distance" ascending:YES withPredicate:predicate groupBy:nil delegate:self];
    [self.fetchedResultsController.fetchRequest setReturnsDistinctResults:YES];
    [self.fetchedResultsController.fetchRequest setResultType:NSDictionaryResultType];
    [self.fetchedResultsController.fetchRequest setPropertiesToFetch:[NSArray arrayWithObject:@"bubblename"]];
    
 
    
//    if([strCategoryName isEqualToString:@"Event"])
//    {
//        if(!arrCollapseStatus)
//        {
//            NSInteger sectionCount = [[self.fetchedResultsController sections]count];
//            arrCollapseStatus = [[NSMutableArray alloc]init];
//            for(int i = 0; i < sectionCount; i++)
//            {
//                arrCollapseStatus[i] = @"0";
//            }
//        }
//    }
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self onTouchBtnCategory:btnBubbleTypeAll];
    [btnBubbleTypeAll setSelected:YES];
    [self reloadDataWithCategoryName:@"Activity" SearchString:searchBar.text];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (void)customizeSearchBar
{
    CGRect rect = self.searchBar.frame;
    UIView *lineView1 = [[UIView alloc]initWithFrame:CGRectMake(0, rect.size.height-2, rect.size.width * 2, 2)];
    UIView *lineView2 = [[UIView alloc]initWithFrame:CGRectMake(0, 0,rect.size.width * 2, 2)];
    lineView1.backgroundColor = searchBar.barTintColor;
    lineView2.backgroundColor = searchBar.barTintColor;
    [self.searchBar addSubview:lineView1];
    [self.searchBar addSubview:lineView2];
    
    for (id object in [[[searchBar subviews] objectAtIndex:0] subviews])
    {
        if ([object isKindOfClass:[UITextField class]])
        {
            UITextField *textFieldObject = (UITextField *)object;
            textFieldObject.layer.borderColor = [[UIColor lightGrayColor] CGColor];
            textFieldObject.layer.borderWidth = 1.0;
            [textFieldObject.layer setCornerRadius:10.f];
            [textFieldObject.layer setMasksToBounds:YES];
            break;
        }
    }
}
#pragma mark Actions
- (IBAction)onTouchBtnMenu:(id)sender {
    [self.revealViewController revealToggle:sender];
}

- (IBAction)onTouchBtnCategory:(id)sender {
    btnBubbleTypeRetail.selected = NO;
    btnBubbleTypeAll.selected = NO;
    btnBubbleTypeBars.selected = NO;
    btnBubbleTypeEvents.selected = NO;
    btnBubbleTypeRestaurants.selected = NO;
    [((UIButton *)sender) setSelected:YES];
    NSString *txtCategory = ((UIButton *)sender).titleLabel.text;
    if([txtCategory isEqualToString:@"Activity"]) txtCategory = @"Activity";
    else if([txtCategory isEqualToString:@"Events"]) txtCategory = @"Event";
    self.mBubbleCategory = txtCategory;
    [self reloadDataWithCategoryName:txtCategory SearchString:searchBar.text];
}
#pragma mark Getter & Setter
-(void)setMBubbleCategory:(NSString *)mBubbleCategory
{
    _mBubbleCategory = mBubbleCategory;
}
#pragma mark Data Methods
//- (void)getBussinessBubbleList
//{
//    if([[GlobalData sharedData]isSessionExpired])
//        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
//    NSString *lastTimeStamp = [localRegistry objectForKey:kLastFetchTimeStampForBusinessBubbleList];
//    if(!lastTimeStamp) lastTimeStamp = @"";
//    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
//    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
//    [params setObject:lastTimeStamp forKey:@"last_time_stamp"];
//    [params setObject:[NSString stringWithFormat:@"%f", [[GlobalData sharedData]currentLatitude]] forKey:@"latitude"];
//    [params setObject:[NSString stringWithFormat:@"%f", [[GlobalData sharedData]currentLongitude]] forKey:@"longitude"];
//    
//    [[BubbleService sharedData]getBubbleListWithDictionary:params success:^(id _responseObject) {
//        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
//            [localRegistry setObject:TimeStamp forKey:kLastFetchTimeStampForBusinessBubbleList];
//            [Bubble MR_importFromArray:_responseObject inContext:localContext];
//        }];
//        //        dispatch_async(dispatch_get_main_queue(), ^{
//        //            [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
//        //            }];
//        //        });
//    } failure:^(NSInteger _errorCode) {
//        
//    }];
//    
//}
#pragma mark Search Delegate
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar1
{
    if([searchBar isEqual:searchBar1])
    {
        [searchBar resignFirstResponder];
        searchBar.text = @"";
    }
    [self reloadDataWithCategoryName:self.mBubbleCategory SearchString:searchBar.text];
}
- (void)searchBar:(UISearchBar *)searchBar1 textDidChange:(NSString *)searchText
{
    [self reloadDataWithCategoryName:self.mBubbleCategory SearchString:searchBar.text];
}
#pragma mark TableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.f;
    //
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Bubble *currentBubble = self.fetchedResultsController.fetchedObjects[indexPath.row];
//    if([self.mBubbleCategory isEqualToString:@"Event"])
//        currentBubble =     [[[[self.fetchedResultsController sections] objectAtIndex:indexPath.section] objects] objectAtIndex:indexPath.row];
    NSString *CellIdentifier = @"BusinessBubbleTableViewCell";
    BusinessBubbleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [cell.lblBubbleName setText:currentBubble.bubblename];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell.lblDistance setText:[NSString stringWithFormat:@"%.1f Mile",[currentBubble.distance doubleValue]]];
    [cell.imgBubble setHidden:NO];
    [cell.imgBanner setHidden:YES];
    if([self.mBubbleCategory isEqualToString:@"Activity"])
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"activity_id == %@ AND status = 1", currentBubble.bubble_id];
        NSFetchedResultsController *theFetchedUsersController = [Activity_Like MR_fetchAllSortedBy:@"user_id" ascending:NO withPredicate:predicate groupBy:nil delegate:nil];
        [cell.lblActivityRate setText:[NSString stringWithFormat:@"%ld", [[theFetchedUsersController fetchedObjects] count]]];
        cell.layout_constraint_distance_bottom_space.constant = 0;
        [cell.btnActivityRate setHidden:NO];
        [cell.lblActivityRate setHidden:NO];
    }
    else if([self.mBubbleCategory isEqualToString:@"Event"])
    {
        NSString *photo_filename = [[GlobalData sharedData]urlencode:currentBubble.banner_filename];
        [[GlobalData sharedData]showImageWithImage:cell.imgBanner name:photo_filename placeholder:nil];
        [cell.imgBubble setHidden:YES];
        [cell.imgBanner setHidden:NO];
        [cell.btnActivityRate setHidden:YES];
        [cell.lblActivityRate setHidden:YES];
        cell.layout_constraint_distance_bottom_space.constant = 3;
    }
    else{
        cell.layout_constraint_distance_bottom_space.constant = 24;
        [cell.btnActivityRate setHidden:YES];
        [cell.lblActivityRate setHidden:YES];
    }
    NSString *photo_filename = [[GlobalData sharedData]urlencode:currentBubble.photo_filename];
    [[GlobalData sharedData]showImageWithImage:cell.imgBubble name:photo_filename placeholder:nil];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Bubble *currentBubble = self.fetchedResultsController.fetchedObjects[indexPath.row];
    if(!currentBubble)
        return;
    if([currentBubble.category isEqualToString:@"Activity"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Activity" bundle:nil];
        ActivityDetailViewController * detailVC = [storyboard instantiateViewControllerWithIdentifier:@"ActivityDetailViewController"];
        detailVC.mCurBubble = currentBubble;
        detailVC.isReviewByAdmin = YES;
        [self.navigationController pushViewController:detailVC animated:YES];
        return;
    }
    else if([currentBubble.category isEqualToString:@"Event"])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Event" bundle:nil];
        EventDetailViewController * detailVC = [storyboard instantiateViewControllerWithIdentifier:@"EventDetailViewController"];
        detailVC.mCurBubble = currentBubble;
        detailVC.isReviewByAdmin = YES;
        [self.navigationController pushViewController:detailVC animated:YES];
        return;
    }
    ReviewBubbleDetailViewController * detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ReviewBubbleDetailViewController"];
    detailVC.mCurrentBubble = currentBubble;
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return 0;
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return nil;
}
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    if([self.mBubbleCategory isEqualToString:@"Event"])
//    {
//        NSInteger sectionCount = [[self.fetchedResultsController sections]count];
//        return sectionCount;
//    }
//    return 1;
//}
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    if(![self.mBubbleCategory isEqualToString:@"Event"])
//    {
//        return nil;
//    }
//    NSString *CellIdentifier = @"EventBubbleSectionHeaderCell";
//    EventBubbleSectionHeaderCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (headerView == nil){
//        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
//    }
//    headerView.lblHeaderTitle.text = [[[self.fetchedResultsController sections] objectAtIndex:section]name];
//    NSInteger objectCount = [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects];
//    
//    UIButton *btnClick = [[UIButton alloc]initWithFrame:headerView.frame];
//    [btnClick setBackgroundColor:[UIColor clearColor]];
//    btnClick.tag = section + 1;
//    [btnClick addTarget:self action:@selector(sectionTapped:) forControlEvents:UIControlEventTouchDown];
//    [headerView addSubview:btnClick];
//    return headerView;
//}
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    if(![self.mBubbleCategory isEqualToString:@"Event"])
//        return 0;
//    return 35.f;
//}
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if([self.mBubbleCategory isEqualToString:@"Event"])
//    {
//        if ([arrCollapseStatus[section] isEqualToString:@"1"]) {
//            return [[[self.fetchedResultsController sections] objectAtIndex:section] numberOfObjects] + 0;
//        } else {
//            return 0;
//        }
//    }
//    return [[self.fetchedResultsController fetchedObjects]count];
//}
//- (void)sectionTapped:(UIButton*)btn {
//    NSInteger section = btn.tag - 1;
//    if([arrCollapseStatus[section] isEqualToString:@"0"])
//        arrCollapseStatus[section] = @"1";
//    else
//        arrCollapseStatus[section] = @"0";
//    
//    [UIView transitionWithView:self.tableView
//                      duration:0.35f
//                       options:UIViewAnimationOptionTransitionCrossDissolve
//                    animations:^(void)
//     {
//         [self.tableView reloadData];
//     }
//                    completion:nil];
//}

@end
