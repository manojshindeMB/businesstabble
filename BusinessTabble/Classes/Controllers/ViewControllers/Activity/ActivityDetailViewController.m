//
//  ActivityDetailViewController.m
//  BusinessTabble
//
//  Created by Liumin on 08/07/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import "ActivityDetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "BubbleMapViewController.h"
#import "ActivityFormViewController.h"

@interface ActivityDetailViewController ()

@end

@implementation ActivityDetailViewController
@synthesize mCurBubble;
@synthesize isReviewByAdmin;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_btnSetting centerImageAndTitle:2.f];
    [_btnLike centerImageAndTitle:2.f];
    [self.view layoutIfNeeded];
    [_imgLogo.layer setCornerRadius:_imgLogo.frame.size.width / 2];
    [_imgLogo.layer setMasksToBounds:YES];
    [_imgLogo.layer setBorderWidth:1.0f];
    [_imgLogo.layer setBorderColor:[UIColor whiteColor].CGColor];
    _imgLogo.clipsToBounds = YES;
    [_imgLogo setContentMode:UIViewContentModeScaleAspectFill];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _vwLogoContainer.bounds;
    gradient.colors = @[(id)[[UIColor whiteColor] CGColor],
                        (id)[[UIColor lightGrayColor] CGColor]];
    [_vwLogoContainer.layer setCornerRadius:_vwLogoContainer.frame.size.width / 2];
    [_vwLogoContainer.layer setMasksToBounds:YES];
    [_vwLogoContainer.layer insertSublayer:gradient atIndex:0];
    
    [_imgActivityPhoto1.layer setCornerRadius:5.f];
    [_imgActivityPhoto1.layer setBorderWidth:3.f];
    [_imgActivityPhoto1.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_imgActivityPhoto1 setContentMode:UIViewContentModeScaleAspectFill];
    
    [_imgActivityPhoto2.layer setCornerRadius:5.f];
    [_imgActivityPhoto2.layer setBorderWidth:3.f];
    [_imgActivityPhoto2.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_imgActivityPhoto2 setContentMode:UIViewContentModeScaleAspectFill];
    [_imgActivityPhoto1.layer setMasksToBounds:YES];
    [_imgActivityPhoto2.layer setMasksToBounds:YES];
    [_btnPreviewClose.layer setCornerRadius:_btnPreviewClose.frame.size.width / 2];
    [_btnPreviewClose.layer  setMasksToBounds:YES];
    
    [_vwPreviewMask setAlpha:0.f];
    [_vwPhotoPreviewContainer setHidden:YES];
    [_imgPreview.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_imgPreview.layer setBorderWidth:5.f];
    [_imgPreview.layer setCornerRadius:5.f];
    [_imgPreview.layer setMasksToBounds:YES];
    [_imgPreview setContentMode:UIViewContentModeScaleAspectFill];
    
    [self.view bringSubviewToFront:_vwPhotoPreviewContainer];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if([[GlobalData sharedData]isBack])
    {
        [[GlobalData sharedData]setIsBack:NO];
        [self onTouchBtnBack:nil];
        return;
    }
    if(mCurBubble)
    {
        if(![[GlobalData sharedData]isEmpty: mCurBubble.banner_filename])
        {
            NSString *banner_filename = [[GlobalData sharedData]urlencode:mCurBubble.banner_filename];
            [[GlobalData sharedData]showImageWithImage:_imgBanner name:banner_filename placeholder:nil];
        }
        if(![[GlobalData sharedData]isEmpty: mCurBubble.photo_filename])
        {
            NSString *photo_filename = [[GlobalData sharedData]urlencode:mCurBubble.photo_filename];
            [[GlobalData sharedData]showImageWithImage:_imgLogo name:photo_filename placeholder:nil];
        }
        if(![[GlobalData sharedData]isEmpty: mCurBubble.activity_filename1])
        {
            NSString *activity_filename1 = [[GlobalData sharedData]urlencode:mCurBubble.activity_filename1];
            [[GlobalData sharedData]showImageWithImage:_imgActivityPhoto1 name:activity_filename1 placeholder:nil];
        }
        else
            [_imgActivityPhoto1 setHidden:YES];
        
        if(![[GlobalData sharedData]isEmpty: mCurBubble.activity_filename2])
        {
            NSString *activity_filename2 = [[GlobalData sharedData]urlencode:mCurBubble.activity_filename2];
            [[GlobalData sharedData]showImageWithImage:_imgActivityPhoto2 name:activity_filename2 placeholder:nil];
        }
        else
            [_imgActivityPhoto2 setHidden:YES];
        
        if(![[GlobalData sharedData]isEmpty: mCurBubble.photo_filename])
        {
            NSString *photo_filename = [[GlobalData sharedData]urlencode:mCurBubble.photo_filename];
            [[GlobalData sharedData]showImageWithImage:_imgLogo name:photo_filename placeholder:nil];
        }
        
        
        NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc]initWithString:mCurBubble.content];
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        [style setLineSpacing:5];
        [attrString addAttribute:NSParagraphStyleAttributeName
                           value:style
                           range:NSMakeRange(0, mCurBubble.content.length)];
        _lblDescription.attributedText = attrString;
        _lblTitle.text = mCurBubble.bubblename;
        if([mCurBubble.creator_id integerValue] != [[[GlobalData sharedData]mUserInfo].mId integerValue])
            [_btnSetting setHidden:YES];
        
        
        if(isReviewByAdmin)
        {
            [_btnSetting setHidden:YES];
            [_btnLike setHidden:YES];
            [_btnApprove setHidden:NO];
            [_btnReject setHidden:NO];
        }
        UITapGestureRecognizer *tapGuesture1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapPhoto1:)];
        tapGuesture1.delegate = self;
        [_imgActivityPhoto1 addGestureRecognizer:tapGuesture1];
        
        UITapGestureRecognizer *tapGuesture2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapPhoto2:)];
        tapGuesture2.delegate = self;
        [_imgActivityPhoto2 addGestureRecognizer:tapGuesture2];
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user_id == %@ AND activity_id == %@", [[GlobalData sharedData]mUserInfo].mId, mCurBubble.bubble_id];
    NSFetchedResultsController *theFetchedUsersController = [Activity_Like MR_fetchAllSortedBy:@"user_id" ascending:NO withPredicate:predicate groupBy:nil delegate:nil];
    
    _btnLike.selected = false;
    if([theFetchedUsersController.fetchedObjects count] > 0)
    {
        Activity_Like *activityLike = [theFetchedUsersController.fetchedObjects objectAtIndex:0];
        if([activityLike.status integerValue] == 1)
            _btnLike.selected = true;
    }
    [self showActivityLike];
    
}
- (void)showActivityLike
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"activity_id == %@ AND status == 1", mCurBubble.bubble_id];
    NSFetchedResultsController *theFetchedUsersController = [Activity_Like MR_fetchAllSortedBy:@"user_id" ascending:NO withPredicate:predicate groupBy:nil delegate:nil];
    [_lblNumLikes setText:[NSString stringWithFormat:@"%ld Likes", [[theFetchedUsersController fetchedObjects] count]]];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
#pragma makr Gesture Recognizer
- (void)onTapPhoto1 :(UITapGestureRecognizer *)recognizer
{
    
    [UIView animateWithDuration:0.5 animations:^{
        if(_layout_constraint_activity_photo1_width.constant == 100)
        {
            [_vwPhotoPreviewContainer setHidden:NO];
            [_vwPreviewMask setAlpha:0.8f];
            [_imgPreview setImage:_imgActivityPhoto1.image];
            _layout_constraint_activity_photo1_width.constant = 200;
        }
        else
            _layout_constraint_activity_photo1_width.constant = 100;
        _layout_constraint_activity_photo2_width.constant = 100;
        [_imgActivityPhoto1 layoutIfNeeded];
        [self.view layoutIfNeeded];
        
    }completion:^(BOOL finished) {
        
    }];
}
- (void)onTapPhoto2 :(UITapGestureRecognizer *)recognizer
{
    [UIView animateWithDuration:0.5 animations:^{
        if(_layout_constraint_activity_photo2_width.constant == 100)
        {
            [_vwPhotoPreviewContainer setHidden:NO];
            [_vwPreviewMask setAlpha:0.8f];
            [_imgPreview setImage:_imgActivityPhoto2.image];
            _layout_constraint_activity_photo2_width.constant = 200;
        }
        else
            _layout_constraint_activity_photo2_width.constant = 100;
        _layout_constraint_activity_photo1_width.constant = 100;
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}
- (IBAction)onTouchBtnClosePreview:(id)sender {
    [_vwPhotoPreviewContainer setHidden:YES];
    [_vwPreviewMask setAlpha:0.f];
}

#pragma mark Actions
- (IBAction)onTouchBtnBack:(id)sender {
    if(isReviewByAdmin)
    {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onTouchBtnSetting:(id)sender {
    
    ActivityFormViewController * formVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ActivityFormViewController"];
    formVC.mCurrentPageType = 2;
    formVC.mCurrentActivity = mCurBubble;
    [self.navigationController pushViewController:formVC animated:YES];
}
- (IBAction)onTouchBtnApprove:(id)sender {
    NSString *successStr = @"Successfully approved.";
    NSString *type = @"1";
    if([sender isEqual:_btnReject])
    {
        successStr = @"Successfully rejected.";
        type = @"-1";
    }
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:mCurBubble.bubble_id forKey:@"bubble_id"];
    [params setObject:type forKey:@"type"];
    [self showProgress:@"Please wait..."];
    [[BubbleService sharedData]approveBubbleWithDictionary:params success:^(id _responseObject) {
        [self hideProgress];
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:successStr BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
            [self.navigationController popViewControllerAnimated:YES];
        }];
        if([sender isEqual:_btnReject])
            mCurBubble.reviewstatus = [NSNumber numberWithInt:-1];
        else
            mCurBubble.reviewstatus = [NSNumber numberWithInt:1];
        [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreAndWait];
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MEMBER_LIST object:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_MENU_LIST object:nil];
        
    } failure:^(NSInteger _errorCode) {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:^{
        }];
        [self hideProgress];
    }];

}
- (IBAction)onTouchBtnReject:(id)sender {
}

- (IBAction)onTouchBtnLike:(id)sender {
    NSString *alertTitle = @"Like this Activity?";
    if(_btnLike.selected)
    {
        alertTitle = @"Unlike this Activity?";
    }
    NYAlertViewController *alertViewController = [[GlobalData sharedData]createNewAlertViewWithTitle:alertTitle view:self.view];
    alertViewController.maximumWidth = 200;
    [alertViewController addAction:[NYAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil)
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                              [self setActivityLike];
                                                          }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:NSLocalizedString(@"No", nil)
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                          }]];
    
    [self presentViewController:alertViewController animated:YES completion:nil];
    
}
- (void)setActivityLike
{
    NSString *status = @"1";
    if(_btnLike.selected)
        status = @"0";
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:status forKey:@"status"];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:[NSString stringWithFormat:@"%ld", [mCurBubble.bubble_id integerValue]] forKey:@"activity_id"];
    [[BubbleService sharedData]setActivityLikeListWithDictionary:params success:^(id _responseObject) {
        _btnLike.selected = !_btnLike.selected;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"activity_id == %@ AND status == 1", mCurBubble.bubble_id];
        NSFetchedResultsController *theFetchedUsersController = [Activity_Like MR_fetchAllSortedBy:@"user_id" ascending:NO withPredicate:predicate groupBy:nil delegate:nil];
        if([status isEqualToString:@"0"])
            [_lblNumLikes setText:[NSString stringWithFormat:@"%ld Likes", [[theFetchedUsersController fetchedObjects] count] - 1]];
        else
            [_lblNumLikes setText:[NSString stringWithFormat:@"%ld Likes", [[theFetchedUsersController fetchedObjects] count] + 1]];        
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_ACTIVITY_LIKE_LIST object:nil];
        
        
    } failure:^(NSInteger _errorCode) {
        
    }];

}
- (IBAction)onTouchBtnCall:(id)sender {
    NSString *phoneNumber = mCurBubble.contactnumber;
    if([[GlobalData sharedData]isEmpty:mCurBubble.contactnumber])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"The phone number is not set yet." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        
    }
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneNumber]]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phoneNumber]]];
    }
    else {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"This Device Doesn't Support Call Functionality" BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        
    }
    
   
}
- (IBAction)onTouchBtnLocation:(id)sender {
    
//    if ([[UIApplication sharedApplication] canOpenURL:
//         [NSURL URLWithString:@"comgooglemaps://"]]) {
//        NSString *url = [NSString stringWithFormat:@"comgooglemaps://?center=%@,%@&zoom=14&views=traffic", mCurBubble.latitude, mCurBubble.longitude];
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
//    } else {
//        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Can not open google map in your device." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
//    }
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Detail" bundle:nil];
    BubbleMapViewController *mapVC = [storyboard instantiateViewControllerWithIdentifier:@"BubbleMapViewController"];
    mapVC.mCurrentBubble = mCurBubble;
    [self presentViewController:mapVC animated:YES completion:nil];

}
- (IBAction)onTouchBtnWebsite:(id)sender {
    NSString *strUrl = [mCurBubble.website stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if(![strUrl hasPrefix:@"http"])
    {
        strUrl = [NSString stringWithFormat:@"http://%@", strUrl];
    }
    NSURL *url = [NSURL URLWithString:strUrl];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }

}

@end
