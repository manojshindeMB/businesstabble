//
//  BubbleFormViewController.h
//  BusinessTabble
//
//  Created by Tian Ming on 14/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGLDropDownMenu.h"
#import "NIDropDown.h"

#import "SearchAddressViewController.h"

@interface ActivityFormViewController : UIViewController<UITextFieldDelegate,UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, SelectAddressViewControllerDelegate, PECropViewControllerDelegate, CustomIOSAlertViewDelegate>
{
    SWRevealViewController *revealViewController;
    NSArray *arrBubbleCategory;
    UIActionSheet *photoActionSheet;
}
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@property (weak, nonatomic) IBOutlet UIButton *btnUploadLogo;
@property (weak, nonatomic) IBOutlet UIButton *btnActivityPhoto1;
@property (weak, nonatomic) IBOutlet UIButton *btnActivityPhoto2;

@property (weak, nonatomic) IBOutlet UIButton *btnBanner;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtBubbleName;
@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtWebsite;

@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtLocation;
@property (weak, nonatomic) IBOutlet UITextView *txtDescription;

@property (weak, nonatomic) IBOutlet TextFieldWithBorderAndBound *txtContactNumber;

@property (weak, nonatomic) IBOutlet UIButton *btnRemoveActivity;

@property (weak, nonatomic) IBOutlet UIScrollView *svMain;

@property (nonatomic) CLLocationCoordinate2D locationCoordinate;
@property (nonatomic, strong) Bubble *mCurrentActivity;
@property (nonatomic) NSInteger mCurrentPageType;

@property (nonatomic) BOOL isBannerChanged;
@property (nonatomic) BOOL isLogoChanged;
@property (nonatomic) BOOL isActivityPhoto1Changed;
@property (nonatomic) BOOL isActivityPhoto2Changed;

//Property
@property (nonatomic, strong) UIImage *mImgBubble;
@property (nonatomic, strong) UIImage *mImgBanner;
@property (nonatomic, strong) UIImage *mImgActivityPhoto1;
@property (nonatomic, strong) UIImage *mImgActivityPhoto2;
@property (nonatomic) NSInteger mCurrentPhotoMode;



@end
