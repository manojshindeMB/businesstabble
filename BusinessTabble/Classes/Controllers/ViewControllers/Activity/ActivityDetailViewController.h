//
//  ActivityDetailViewController.h
//  BusinessTabble
//
//  Created by Liumin on 08/07/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityDetailViewController : UIViewController<UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UIButton_Image *btnSetting;
@property (weak, nonatomic) IBOutlet UIImageView *imgBanner;

@property (weak, nonatomic) IBOutlet UIView *vwLogoContainer;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNumLikes;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIButton_Image *btnLike;
@property (weak, nonatomic) IBOutlet UIImageView *imgActivityPhoto1;
@property (weak, nonatomic) IBOutlet UIImageView *imgActivityPhoto2;

@property (strong, nonatomic) Bubble * mCurBubble;

@property (nonatomic) BOOL isReviewByAdmin;
@property (weak, nonatomic) IBOutlet UIButton *btnReject;
@property (weak, nonatomic) IBOutlet UIButton *btnApprove;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_activity_photo1_width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layout_constraint_activity_photo2_width;

@property (weak, nonatomic) IBOutlet UIView *vwPhotoPreviewContainer;
@property (weak, nonatomic) IBOutlet UIImageView *imgPreview;
@property (weak, nonatomic) IBOutlet UIButton *btnPreviewClose;
@property (strong, nonatomic) IBOutlet UIView *vwPreviewMask;

@end
