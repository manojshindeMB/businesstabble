//
//  BubbleFormViewController.m
//  BusinessTabble
//
//  Created by Tian Ming on 14/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "ActivityFormViewController.h"
#import "NIDropDown.h"
#import "HomeViewController.h"
#import "HomeTabBarController.h"
#import "SearchAddressViewController.h"
#import "ChatViewController.h"
#define CURRENT_PAGE_TYPE_ADD 01
#define CURRENT_PAGE_TYPE_EDIT 02

#define CURRENT_PHOTO_MODE_LOGO 01
#define CURRENT_PHOTO_MODE_BANNER 02
#define CURRENT_PHOTO_MODE_PHOTO1 03
#define CURRENT_PHOTO_MODE_PHOTO2 04


@interface ActivityFormViewController ()

@end

@implementation ActivityFormViewController
@synthesize btnUploadLogo,btnSubmit,svMain,locationCoordinate;
@synthesize btnBanner;
@synthesize btnActivityPhoto1;
@synthesize btnActivityPhoto2;
@synthesize mImgBubble = _mImgBubble;
@synthesize mImgBanner = _mImgBanner;
@synthesize mImgActivityPhoto1 = _mImgActivityPhoto1;
@synthesize mImgActivityPhoto2 = _mImgActivityPhoto2;
@synthesize mCurrentPhotoMode;
@synthesize mCurrentActivity;
@synthesize mCurrentPageType;
@synthesize isLogoChanged;
@synthesize isBannerChanged;
@synthesize isActivityPhoto1Changed;
@synthesize isActivityPhoto2Changed;

- (void)viewDidLoad {
    [super viewDidLoad];
    revealViewController = [self revealViewController];
    [revealViewController panGestureRecognizer];
    [revealViewController tapGestureRecognizer];

    btnUploadLogo.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    btnUploadLogo.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.view layoutIfNeeded];
    [btnUploadLogo.layer setCornerRadius:btnUploadLogo.frame.size.width / 2];
    [btnUploadLogo.layer setMasksToBounds:YES];
    [btnUploadLogo.layer setBorderWidth:2.f];
    [btnUploadLogo.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    btnBanner.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    btnBanner.titleLabel.textAlignment = NSTextAlignmentCenter;
    [btnBanner.layer setCornerRadius:5.f];
    [btnBanner.layer setMasksToBounds:YES];
    [btnBanner.layer setBorderWidth:5.f];
    [btnBanner.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    btnActivityPhoto1.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    btnActivityPhoto1.titleLabel.textAlignment = NSTextAlignmentCenter;
    [btnActivityPhoto1.layer setCornerRadius:5.f];
    [btnActivityPhoto1.layer setMasksToBounds:YES];
    [btnActivityPhoto1.layer setBorderWidth:5.f];
    [btnActivityPhoto1.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    btnActivityPhoto2.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    btnActivityPhoto2.titleLabel.textAlignment = NSTextAlignmentCenter;
    [btnActivityPhoto2.layer setCornerRadius:5.f];
    [btnActivityPhoto2.layer setMasksToBounds:YES];
    [btnActivityPhoto2.layer setBorderWidth:5.f];
    [btnActivityPhoto2.layer setBorderColor:[UIColor whiteColor].CGColor];
    
    [btnActivityPhoto1 setContentMode:UIViewContentModeScaleAspectFill];
    [btnActivityPhoto2 setContentMode:UIViewContentModeScaleAspectFill];
    [btnUploadLogo.layer setMasksToBounds:YES];
    [btnBanner.layer setMasksToBounds:YES];
    [btnUploadLogo setContentMode:UIViewContentModeScaleAspectFill];
    [btnBanner setContentMode:UIViewContentModeScaleAspectFill];
    
    [btnSubmit.layer setBorderWidth:0.f];
    [btnSubmit.layer setBorderColor:[UIColor whiteColor].CGColor];
    [btnSubmit.layer setMasksToBounds:YES];
    
    photoActionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel"  destructiveButtonTitle:nil otherButtonTitles:@"From Camera",@"From Photo Library", nil];
    
//    [self setDefaultValues];
    
    isLogoChanged = NO;
    isBannerChanged = NO;
    isActivityPhoto1Changed = NO;
    isActivityPhoto2Changed = NO;
    [_btnBack setHidden:YES];
    [_btnMenu setHidden:NO];
    [_btnRemoveActivity setHidden:YES];
    if(mCurrentPageType == CURRENT_PAGE_TYPE_EDIT)
    {
        [_btnRemoveActivity setHidden:NO];
        _txtBubbleName.text = mCurrentActivity.bubblename;
        _txtDescription.text = mCurrentActivity.content;
        _txtContactNumber.text = mCurrentActivity.contactnumber;
        _txtLocation.text = mCurrentActivity.location;
        _txtWebsite.text = mCurrentActivity.website;
        [btnSubmit setTitle:@"Save" forState:UIControlStateNormal];
        locationCoordinate = CLLocationCoordinate2DMake([mCurrentActivity.latitude floatValue], [mCurrentActivity.longitude floatValue]);
        
        UIImageView *tempBubbleImage = [[UIImageView alloc]init]; //sd_setBackgroundImage for button
        [tempBubbleImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",g_storagePrefix,self.mCurrentActivity.photo_filename]]  placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            self.mImgBubble = image;
        }];
        UIImageView *tempBannerImage = [[UIImageView alloc]init]; //sd_setBackgroundImage for button
        [tempBannerImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",g_storagePrefix,self.mCurrentActivity.banner_filename]]  placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            self.mImgBanner = image;
        }];
        if(![[GlobalData sharedData]isEmpty:mCurrentActivity.activity_filename1])
        {
            UIImageView *tempPhoto = [[UIImageView alloc]init]; //sd_setBackgroundImage for button
            [tempPhoto sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",g_storagePrefix,self.mCurrentActivity.activity_filename1]]  placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                self.mImgActivityPhoto1 = image;
            }];
        }
        if(![[GlobalData sharedData]isEmpty:mCurrentActivity.activity_filename2])
        {
            UIImageView *tempPhoto = [[UIImageView alloc]init]; //sd_setBackgroundImage for button
            [tempPhoto sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",g_storagePrefix,self.mCurrentActivity.activity_filename2]]  placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                self.mImgActivityPhoto2 = image;
            }];
        }
        [_btnBack setHidden:NO];
        [_btnMenu setHidden:YES];
    }
    
}
- (void)setDefaultValues
{
    if(![[GlobalData sharedData]isEmpty:[[GlobalData sharedData]currentCity]])
    {
        locationCoordinate = CLLocationCoordinate2DMake([[GlobalData sharedData]currentLatitude], [[GlobalData sharedData]currentLongitude]);
        [_txtLocation setText:[NSString stringWithFormat:@"%@, %@, %@",[[GlobalData sharedData]currentCity], [[GlobalData sharedData]currentState], [[GlobalData sharedData]currentCountry]]];
    }

}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Actions
- (IBAction)onTouchBtnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onTouchBtnMenu:(id)sender {
    [revealViewController revealToggle:sender];
}
- (IBAction)onTouchBtnUploadImage:(id)sender {
    [self.view endEditing:YES];
    mCurrentPhotoMode = CURRENT_PHOTO_MODE_LOGO;
    [photoActionSheet showInView:[self.view window]];
}
- (IBAction)onTouchBtnActivityPhoto1:(id)sender {
    [self.view endEditing:YES];
    [self.view endEditing:YES];
    mCurrentPhotoMode = CURRENT_PHOTO_MODE_PHOTO1;
    [photoActionSheet showInView:[self.view window]];
}
- (IBAction)onTouchBtnActivityPhoto2:(id)sender {
    [self.view endEditing:YES];
    mCurrentPhotoMode = CURRENT_PHOTO_MODE_PHOTO2;
    [photoActionSheet showInView:[self.view window]];
}
- (IBAction)onTouchBtnBanner:(id)sender {
    [self.view endEditing:YES];
    mCurrentPhotoMode = CURRENT_PHOTO_MODE_BANNER;
    [photoActionSheet showInView:[self.view window]];
}
- (IBAction)onTouchBtnRemoveActivity:(id)sender {
    
    
    NYAlertViewController *alertViewController = [[GlobalData sharedData]createNewAlertViewWithTitle:@"\n Are you sure? \n" view:self.view];
    [alertViewController addAction:[NYAlertAction actionWithTitle:NSLocalizedString(@"Ok", nil)
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                              [self removeActivity];
                                                          }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(NYAlertAction *action) {
                                                              [self dismissViewControllerAnimated:YES completion:nil];
                                                          }]];
    
    [self presentViewController:alertViewController animated:YES completion:nil];
   
    
    
}
- (void)removeActivity
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:mCurrentActivity.bubble_id forKey:@"bubble_id"];
    NSString *successStr = @"Successfully removed.";
    [self showProgress:@"Please wait..."];
    [[BubbleService sharedData]removeBubbleWithDictionary:params success:^(id _responseObject) {
        [self hideProgress];
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:successStr BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
            
        }];
        
        mCurrentActivity.field_deleted = [NSNumber numberWithInt:1];
        [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreAndWait];
        [[GlobalData sharedData]setIsBack:YES];
        
        [self onTouchBtnBack:nil];
        
//        [[MainService sharedData]getBussinessBubbleListWithSuccessHandler:^{
//            [[GlobalData sharedData]setIsBack:YES];
//            [self onTouchBtnBack:nil];
//        } FailureHandler:^{
//            
//        }];
        
        
    } failure:^(NSInteger _errorCode) {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:^{
        }];
        [self hideProgress];
    }];

}
- (IBAction)onTouchBtnSubmit:(id)sender {
    if([[GlobalData sharedData]isSessionExpired])
       [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
    [self checkBubbleFormValidationWithSuccessHandler:^{
        if(mCurrentPageType == CURRENT_PAGE_TYPE_ADD)
        {
            CustomIOSAlertView *alertView = [[GlobalData sharedData] createBusinessCreationNoticeViewWithTitle:@"Please be patient, approval may take up to 24 hours."];
            [alertView show];
            alertView.delegate = self;
            return ;
        }
        [self submitBubbleCreation];
    } FailureHandler:^{
        
    }];
}
- (void)customIOS7dialogButtonTouchUpInside:(id)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self submitBubbleCreation];
}
- (void)submitBubbleCreation
{
    NSDate *date = [NSDate date];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[_txtBubbleName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]  forKey:@"bubblename"];
    [params setObject:_txtWebsite.text forKey:@"website"];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"creator_id"];
    [params setObject:@"Activity" forKey:@"category"];
    [params setObject:_txtLocation.text forKey:@"location"];
    [params setObject:[NSString stringWithFormat:@"%f",locationCoordinate.latitude] forKey:@"latitude"];
    [params setObject:[NSString stringWithFormat:@"%f",locationCoordinate.longitude] forKey:@"longitude"];
    [params setObject:_txtDescription.text forKey:@"description"];
    
    if(mCurrentPageType == CURRENT_PAGE_TYPE_EDIT)
        [params setObject:[NSString stringWithFormat:@"%ld", [mCurrentActivity.bubble_id integerValue]] forKey:@"bubble_id"];
    NSString *strfilename = [NSString stringWithFormat:@"Activity%@%ld.jpg", [[GlobalData sharedData]mUserInfo].mId , (long)[date timeIntervalSince1970]];
    strfilename = [[GlobalData sharedData]urlencode:strfilename];
    if(mCurrentPageType == CURRENT_PAGE_TYPE_EDIT && !isLogoChanged)
        strfilename = mCurrentActivity.photo_filename;
    
    [params setObject:strfilename forKey:@"photo_filename"];
    [params setObject:[NSNumber numberWithBool:isLogoChanged] forKey:@"photo_changed"];
    
    NSString *strfilenameBanner = [NSString stringWithFormat:@"%@%ld_banner.jpg", [[GlobalData sharedData]mUserInfo].mId , (long)[date timeIntervalSince1970]];
    strfilenameBanner = [[GlobalData sharedData]urlencode:strfilenameBanner];
    if(mCurrentPageType == CURRENT_PAGE_TYPE_EDIT && !isBannerChanged)
        strfilenameBanner = mCurrentActivity.banner_filename;
    [params setObject:strfilenameBanner forKey:@"banner_filename"];
    
    [params setObject:@"" forKey:@"owner"];
    [params setObject:_txtContactNumber.text forKey:@"contactnumber"];
    [params setObject:[NSString stringWithFormat:@"%f",0.f] forKey:@"color_red"];
    [params setObject:[NSString stringWithFormat:@"%f",0.f] forKey:@"color_green"];
    [params setObject:[NSString stringWithFormat:@"%f",0.f] forKey:@"color_blue"];
    
    NSString *strfilenamePhoto1 = @"";
    if(self.mImgActivityPhoto1)
    {
        if(mCurrentPageType == CURRENT_PAGE_TYPE_ADD)
        {
            strfilenamePhoto1 = [NSString stringWithFormat:@"%@%ld_activity_photo1.jpg", [[GlobalData sharedData]mUserInfo].mId , (long)[date timeIntervalSince1970]];
            strfilenamePhoto1 = [[GlobalData sharedData]urlencode:strfilenamePhoto1];
        }
        else if(mCurrentPageType == CURRENT_PAGE_TYPE_EDIT)
        {
            if(isActivityPhoto1Changed)
            {
                strfilenamePhoto1 = [NSString stringWithFormat:@"%@%ld_activity_photo1.jpg", [[GlobalData sharedData]mUserInfo].mId , (long)[date timeIntervalSince1970]];
                strfilenamePhoto1 = [[GlobalData sharedData]urlencode:strfilenamePhoto1];
            }
            else
                strfilenamePhoto1 = mCurrentActivity.activity_filename1;
        }
    }
    [params setObject:strfilenamePhoto1 forKey:@"activity_filename1"];
    
    NSString *strfilenamePhoto2 = @"";
    if(self.mImgActivityPhoto2)
    {
        if(mCurrentPageType == CURRENT_PAGE_TYPE_ADD)
        {
            strfilenamePhoto2 = [NSString stringWithFormat:@"%@%ld_activity_photo2.jpg", [[GlobalData sharedData]mUserInfo].mId , (long)[date timeIntervalSince1970]];
            strfilenamePhoto2 = [[GlobalData sharedData]urlencode:strfilenamePhoto2];
        }
        else if(mCurrentPageType == CURRENT_PAGE_TYPE_EDIT)
        {
            if(isActivityPhoto2Changed)
            {
                strfilenamePhoto2 = [NSString stringWithFormat:@"%@%ld_activity_photo2.jpg", [[GlobalData sharedData]mUserInfo].mId , (long)[date timeIntervalSince1970]];
                strfilenamePhoto2 = [[GlobalData sharedData]urlencode:strfilenamePhoto2];
            }
            else
                strfilenamePhoto2 = mCurrentActivity.activity_filename2;
        }
    }
    [params setObject:strfilenamePhoto2 forKey:@"activity_filename2"];
    
    if(mCurrentPageType == CURRENT_PAGE_TYPE_ADD)
    {
        [self createNewActivityWithParams:params success:^(id _responseObject) {
            [self hideProgress];
            
            [[GlobalData sharedData] uploadPhotoWithName:strfilename oldname:@"" image:self.mImgBubble type:@"" completion:^{
            } FailureHandler:^{
            }];
            [[GlobalData sharedData] uploadPhotoWithName:strfilenameBanner oldname:@"" image:self.mImgBanner type:@"" completion:^{
            } FailureHandler:^{
            }];
            if(self.mImgActivityPhoto1)
                [[GlobalData sharedData] uploadPhotoWithName:strfilenamePhoto1 oldname:@"" image:self.mImgActivityPhoto1 type:@"" completion:^{
                } FailureHandler:^{
                }];
            if(self.mImgActivityPhoto2)
                [[GlobalData sharedData] uploadPhotoWithName:strfilenamePhoto2 oldname:@"" image:self.mImgActivityPhoto2 type:@"" completion:^{
                } FailureHandler:^{
                }];
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Submitted For Review Successfully." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
                [self goToMainPage];
            }];
            
        } failure:^(NSInteger _errorCode) {
            if(_errorCode == 400)
            {
                [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:[NSString stringWithFormat:@"There is already existing bubble '%@'.",_txtBubbleName.text] BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
                return;
            }
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        }];
    }
    else if(mCurrentPageType == CURRENT_PAGE_TYPE_EDIT)
    {
        [self updateActivityWithParams:params success:^(id _responseObject) {
            //                [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_DATA object:nil];
            mCurrentActivity.bubblename = [_txtBubbleName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            mCurrentActivity.category = @"Activity";
            mCurrentActivity.website = _txtWebsite.text;
            mCurrentActivity.location = _txtLocation.text;
            mCurrentActivity.content = _txtDescription.text;
            
            mCurrentActivity.contactnumber = _txtContactNumber.text;
            mCurrentActivity.latitude = [NSNumber numberWithFloat: locationCoordinate.latitude];
            mCurrentActivity.longitude =[NSNumber numberWithFloat: locationCoordinate.longitude];
            [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
                
            }];
            [self hideProgress];
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Activity Updated Successfully." BackgroundColor:TOAST_SUCCESS_COLOR] completionBlock:^{
                
            }];
            
            if(isLogoChanged)
            {
                NSString *oldPhotoName = mCurrentActivity.photo_filename;
                
                isLogoChanged = NO;
                [[GlobalData sharedData] uploadPhotoWithName:strfilename oldname:mCurrentActivity.photo_filename image:self.mImgBubble type:@"" completion:^{
                    mCurrentActivity.photo_filename = strfilename;
                    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
                        
                    }];
                } FailureHandler:^{
                    mCurrentActivity.photo_filename = oldPhotoName;
                }];
                //                    [[GlobalData sharedData]uploadImageWithName:strfilename oldname:mCurrentActivity.photo_filename  image:self.mImgBubble completion:^{
                //
                //                    } FailureHandler:^{
                //
                //                    }];
            }
            if(isBannerChanged)
            {
                NSString *oldPhotoName = mCurrentActivity.banner_filename;
                isBannerChanged = NO;
                [[GlobalData sharedData]uploadPhotoWithName:strfilenameBanner oldname:mCurrentActivity.banner_filename image:self.mImgBanner type:@"" completion:^{
                    mCurrentActivity.banner_filename = strfilenameBanner;
                    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
                        
                    }];
                } FailureHandler:^{
                    mCurrentActivity.banner_filename = oldPhotoName;
                }];
                
            }
            if(isActivityPhoto1Changed)
            {
                NSString *oldPhotoName = mCurrentActivity.activity_filename1;
                isActivityPhoto1Changed = NO;
                [[GlobalData sharedData]uploadPhotoWithName:strfilenamePhoto1 oldname:oldPhotoName image:self.mImgActivityPhoto1 type:@"" completion:^{
                    mCurrentActivity.activity_filename1 = strfilenamePhoto1;
                    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
                        
                    }];
                } FailureHandler:^{
                    mCurrentActivity.activity_filename1 = oldPhotoName;
                }];
                
            }
            if(isActivityPhoto2Changed)
            {
                NSString *oldPhotoName = mCurrentActivity.activity_filename2;
                isActivityPhoto1Changed = NO;
                [[GlobalData sharedData]uploadPhotoWithName:strfilenamePhoto2 oldname:oldPhotoName image:self.mImgActivityPhoto2 type:@"" completion:^{
                    mCurrentActivity.activity_filename2 = strfilenamePhoto2;
                    [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
                        
                    }];
                } FailureHandler:^{
                    mCurrentActivity.activity_filename2 = oldPhotoName;
                }];
            }
            
            
        } failure:^(NSInteger _errorCode) {
            if(_errorCode == 400)
            {
                //            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:[NSString stringWithFormat:@"There is already existing bubble '%@'.",_txtBubbleName.text] BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
                //            return;
            }
            [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:MSG_NETWORK_CONNECTION_FAILED BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
            
        }];
    }
}
- (void)goToMainPage
{
    //            [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_RELOAD_BUBBLE_DATA object:nil];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HomeTabBarController *pVCHome = [storyboard instantiateViewControllerWithIdentifier:@"HomeTabBarController"];
    [revealViewController revealToggle:nil];
    [revealViewController pushFrontViewController:pVCHome animated:YES];

}
- (void)createNewActivityWithParams : (NSMutableDictionary *)params
                             success:(void (^)())_success
                             failure:(void (^)(NSInteger _errorCode))_failure

{
    [self showProgress:@"Please wait..."];
    [[BubbleService sharedData]createBubbleWithDictionary:params success:^(id _responseObject) {

//
        _success();
        
        
    } failure:^(NSInteger _errorCode) {
        [self hideProgress];
     
        _failure(_errorCode);
    }];
}
- (void)updateActivityWithParams : (NSMutableDictionary *)params
                             success:(void (^)())_success
                             failure:(void (^)(NSInteger _errorCode))_failure

{
    [self showProgress:@"Please wait..."];
    [[BubbleService sharedData]updateBubbleWithDictionary:params success:^(id _responseObject) {
        _success();
        
    } failure:^(NSInteger _errorCode) {
        
        _failure(_errorCode);
    }];
}

#pragma mark Setter & Getter

- (void)setMImgBubble:(UIImage *)mImgBubble
{
    _mImgBubble = mImgBubble;
    if(mImgBubble != nil)
    {
        [btnUploadLogo setBackgroundImage:mImgBubble forState:UIControlStateNormal];
        [btnUploadLogo setTitle:@"" forState:UIControlStateNormal];
    }
}
- (void)setMImgBanner:(UIImage *)mImgBanner
{
    _mImgBanner = mImgBanner;
    if(mImgBanner != nil)
    {
        [btnBanner setBackgroundImage:mImgBanner forState:UIControlStateNormal];
        [btnBanner setTitle:@"" forState:UIControlStateNormal];
    }
}
- (void)setMImgActivityPhoto1:(UIImage *)mImgActivityPhoto1
{
    _mImgActivityPhoto1 = mImgActivityPhoto1;
    if(mImgActivityPhoto1 != nil)
    {
        [btnActivityPhoto1 setBackgroundImage:mImgActivityPhoto1 forState:UIControlStateNormal];
        [btnActivityPhoto1 setTitle:@"" forState:UIControlStateNormal];
    }
}
- (void)setMImgActivityPhoto2:(UIImage *)mImgActivityPhoto2
{
    _mImgActivityPhoto2 = mImgActivityPhoto2;
    if(mImgActivityPhoto2 != nil)
    {
        [btnActivityPhoto2 setBackgroundImage:mImgActivityPhoto2 forState:UIControlStateNormal];
        [btnActivityPhoto2 setTitle:@"" forState:UIControlStateNormal];
    }
}
#pragma mark Address Select Delegate
- (void)selectAddressViewController:(SearchAddressViewController *)viewController didSelectedLocation:(NSString *)address coordinate:(CLLocationCoordinate2D)coordinate
{
    locationCoordinate = coordinate;
    [_txtLocation setText:address];
}
#pragma mark UITextField Delegate
-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 1;
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        
        [nextResponder becomeFirstResponder];
    } else {        
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if([textField isEqual:_txtLocation])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SearchAddressViewController *searchVC = [storyboard instantiateViewControllerWithIdentifier:@"SearchAddressViewController"];
        searchVC.delegate = self;
        [self presentViewController:searchVC animated:YES completion:nil];
        return NO;
    }
    return YES;
}

#pragma mark  actionsheet didDismissWithButtonIndex
- (void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ( buttonIndex == 2 )
        return;
    if ( actionSheet == photoActionSheet )
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.editing = YES;
        picker.videoMaximumDuration = kMaxVideoLenth;
        {
            if (buttonIndex == 0) {
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
            } else if (buttonIndex == 1) {
                picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            }
        }
        picker.mediaTypes = [NSArray arrayWithObjects: (NSString*)kUTTypeImage, nil];//[NSArray arrayWithObjects:(NSString*)kUTTypeMovie, (NSString*)kUTTypeImage, nil];
        [self presentViewController:picker animated:TRUE completion:nil ];
    }
}
#pragma mark didFinishPickingMediaWithInfo

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *imgtmp = [info objectForKey:UIImagePickerControllerOriginalImage];
    if(!imgtmp || imgtmp == nil)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"You can only use photo." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        return;
    }
    imgtmp = [GlobalData scaleAndRotateImage:imgtmp];
    [self dismissViewControllerAnimated:YES completion:^{
        PECropViewController *controller = [[PECropViewController alloc] init];
        controller.delegate = self;
        controller.image = imgtmp;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            controller.modalPresentationStyle = UIModalPresentationFormSheet;
        }
        [controller setKeepingCropAspectRatio:NO];
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
        }
        [self presentViewController:navigationController animated:YES completion:NULL];

    }];
}
#pragma mark Cropper Delegate
- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    if(mCurrentPhotoMode == CURRENT_PHOTO_MODE_LOGO)
    {
        self.mImgBubble = croppedImage;
        isLogoChanged = YES;
    }
    else if(mCurrentPhotoMode == CURRENT_PHOTO_MODE_BANNER)
    {
        self.mImgBanner = croppedImage;
        isBannerChanged = YES;
    }
    else if(mCurrentPhotoMode == CURRENT_PHOTO_MODE_PHOTO1)
    {
        self.mImgActivityPhoto1 = croppedImage;
        isActivityPhoto1Changed = YES;
    }
    else if(mCurrentPhotoMode == CURRENT_PHOTO_MODE_PHOTO2)
    {
        self.mImgActivityPhoto2 = croppedImage;
        isActivityPhoto2Changed = YES;
    }
    

}
- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark Other Methods
- (void)checkBubbleFormValidationWithSuccessHandler:(void (^)())successHandler FailureHandler:(void (^)())failureHandler
{
    if([[GlobalData sharedData]isEmpty:_txtBubbleName.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide bubble name." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if([[GlobalData sharedData]isEmpty:_txtWebsite.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide website." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if([[GlobalData sharedData]isEmpty:_txtLocation.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide location." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    
    if([[GlobalData sharedData]isEmpty:_txtContactNumber.text])
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide contact number." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if(!self.mImgBubble && self.mCurrentPageType != CURRENT_PAGE_TYPE_EDIT)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide bubble image." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    if(!self.mImgBanner && self.mCurrentPageType != CURRENT_PAGE_TYPE_EDIT)
    {
        [CRToastManager showNotificationWithOptions:[[GlobalData sharedData] dictForToastWithMessage:@"Please provide banner image." BackgroundColor:TOAST_ERROR_COLOR] completionBlock:nil];
        failureHandler();
        return;
    }
    successHandler();
    
}



@end
