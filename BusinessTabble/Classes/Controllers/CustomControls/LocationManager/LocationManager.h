//
//  LocationManager.h
//  TipTow
//
//  Created by Vasily Shorin on 18/12/14.
//  Copyright (c) 2014 KouKou. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
#import <MapKit/MapKit.h>

#define kLocationManagerDidUpdateLocationNotification					@"kLocationManagerDidUpdateLocationNotification"

@class Location;

typedef void (^LocationManagerCallback)(Location *location);
typedef void (^ETACallback)(BOOL success, NSTimeInterval duration);
typedef void (^DistanceCallback)(CGFloat distance);

@interface LocationManager : NSObject

@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain) CLLocation *currentLocation;

@property (nonatomic, readwrite, getter=isAvailable, setter=setIsAvailable:) BOOL bAvailable;

+ (instancetype) sharedInstance;

+ (void) loadCoordinateByAddress:(NSString *)address completionHandler:(LocationManagerCallback)completionHandler;
+ (void) loadAddressByLatitude:(CGFloat)latitude longitude:(CGFloat)longitude completionHandler:(LocationManagerCallback)completionHandler;
+ (void) loadAddressByCoordinate:(CLLocationCoordinate2D)coordinate completionHandler:(LocationManagerCallback)completionHandler;

+ (void) calculateETAByCoordinate:(CLLocationCoordinate2D)source target:(CLLocationCoordinate2D)target completionHandler:(ETACallback)completionHandler;
+ (void) calculateETA:(CLLocation *)source target:(CLLocation *)target completionHandler:(ETACallback)completionHandler;

+ (void) calculateDistanceByCoordinate:(CLLocationCoordinate2D)source target:(CLLocationCoordinate2D)target completionHandler:(DistanceCallback)completionHandler;
+ (void) calculateDistance:(CLLocation *)source target:(CLLocation *)target completionHandler:(DistanceCallback)completionHandler;

+ (CLLocation *) currentLocation;
+ (CLLocationDegrees) latitude;
+ (CLLocationDegrees) longitude;

- (void) startLocationManager;
- (void) stopLocationManager;

- (void) getDistanceByLocation:(CLLocation *)locationA and:(CLLocation *)locationB completionHandler:(DistanceCallback)completionHandler;
- (void) getDistanceByCoordinate:(CLLocationCoordinate2D)locationA and:(CLLocationCoordinate2D)locationB completionHandler:(DistanceCallback)completionHandler;

@end


@interface Location : NSObject

@property (nonatomic, strong) NSString *address;
@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;

+ (instancetype) instance;
+ (instancetype) instance:(NSString *)address coordinate:(CLLocationCoordinate2D)coordinate;

@end