//
//  LocationManager.m
//  TipTow
//
//  Created by Vasily Shorin on 18/12/14.
//  Copyright (c) 2014 KouKou. All rights reserved.
//

#import "LocationManager.h"
#import "SPGooglePlacesAutocompleteQuery.h"

#import "SPGooglePlacesAutocompletePlace.h"
#import "LRouteController.h"
#import "AppDelegate.h"

LocationManager *g_locationManager = nil;

@interface LocationManager () <CLLocationManagerDelegate>

@property (nonatomic, retain) LRouteController *routeController;

@end

@implementation LocationManager

+ (instancetype) sharedInstance {
	if ( !g_locationManager ) {
		g_locationManager = [[LocationManager alloc] init];
	}
	
	return g_locationManager;
}

+ (void) loadCoordinateByAddress:(NSString *)address completionHandler:(LocationManagerCallback)completionHandler {
	if ( !address ) {
		dispatch_async(kMainQueue, ^{
			if ( completionHandler )
				completionHandler(nil);
		});
	}
	
	NSString *strAddress = [address stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
	NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?address=%@", strAddress];
//	NSLog(@"%@", strUrl);
	
	dispatch_async(kBgQueue, ^{
		NSData* responseData = [NSData dataWithContentsOfURL:[NSURL URLWithString:strUrl]];
		if ( responseData.length > 0 ) {
			NSError* error;
			NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData
																 options:kNilOptions
																   error:&error];
			
			if ( error ) {
				[AppDelegate errorHandler:error.localizedDescription];
				return;
			}
			
			NSArray *results = json[@"results"];
			if ( results && [results count] > 0 ) {
				NSDictionary *geometry = results[0][@"geometry"];
				if ( geometry ) {
					
					NSDictionary *bounds = geometry[@"bounds"];
					if ( bounds ) {
						
						NSDictionary *northeast = bounds[@"northeast"];
						if ( northeast ) {
							CGFloat lat = [northeast[@"lat"] floatValue];
							CGFloat lng = [northeast[@"lng"] floatValue];
							
							dispatch_async(kMainQueue, ^{
								if ( completionHandler )
									completionHandler([Location instance:address coordinate:CLLocationCoordinate2DMake(lat, lng)]);
							});
							
							return;
						}
					}
					
					NSDictionary *locationInfo = geometry[@"location"];
					if ( locationInfo ) {
						CGFloat lat = [locationInfo[@"lat"] floatValue];
						CGFloat lng = [locationInfo[@"lng"] floatValue];
						
						dispatch_async(kMainQueue, ^{
							if ( completionHandler )
								completionHandler([Location instance:address coordinate:CLLocationCoordinate2DMake(lat, lng)]);
						});
						
						return;
					}
				}
			}
		}
	});
}

+ (void) loadAddressByLatitude:(CGFloat)latitude longitude:(CGFloat)longitude completionHandler:(LocationManagerCallback)completionHandler {
	[LocationManager loadAddressByCoordinate:CLLocationCoordinate2DMake(latitude, longitude) completionHandler:completionHandler];
}

+ (void) loadAddressByCoordinate:(CLLocationCoordinate2D)coordinate completionHandler:(LocationManagerCallback)completionHandler {
	
	GMSGeocoder *geocoder = [GMSGeocoder geocoder];
	[geocoder reverseGeocodeCoordinate:coordinate completionHandler:^(GMSReverseGeocodeResponse *response, NSError *error) {
		
		Location *location = nil;
		CLLocation *baseLocation = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
		for (GMSAddress *address in response.results) {
			CLLocation *tmpLocation = [[CLLocation alloc] initWithLatitude:address.coordinate.latitude longitude:address.coordinate.longitude];
			if ( [baseLocation distanceFromLocation:tmpLocation] < 10 ) {
				NSMutableString *locHeading = [NSMutableString stringWithFormat:@"%@", [[address lines] firstObject] ];
				if ([[address lines] count] > 1) {
					NSString *snippet = [[address lines] objectAtIndex:1];
					[locHeading appendString:snippet];
				}
				
				location = [Location instance:locHeading coordinate:address.coordinate];
				break;
			}
		}
		
		if ( !location ) {
			GMSAddress *address = response.firstResult;
			NSMutableString *locHeading = [NSMutableString stringWithFormat:@"%@", [[address lines] firstObject] ];
			if ([[address lines] count] > 1) {
				NSString *snippet = [[address lines] objectAtIndex:1];
				[locHeading appendString:snippet];
			}
			
			location = [Location instance:locHeading coordinate:address.coordinate];
		}
		
		dispatch_async(kMainQueue, ^{
			if ( completionHandler )
				completionHandler(location);
		});
	}];
}

+ (void) calculateETAByCoordinate:(CLLocationCoordinate2D)source target:(CLLocationCoordinate2D)target completionHandler:(ETACallback)completionHandler {
	[LocationManager calculateETA:[[CLLocation alloc] initWithLatitude:source.latitude longitude:source.longitude]
						   target:[[CLLocation alloc] initWithLatitude:target.latitude longitude:target.longitude] completionHandler:completionHandler];
}

+ (void) calculateETA:(CLLocation *)source target:(CLLocation *)target completionHandler:(ETACallback)completionHandler {
	MKPlacemark *sourcePlaceMark = [[MKPlacemark alloc] initWithCoordinate:source.coordinate addressDictionary:nil];
	MKMapItem *sourceItem = [[MKMapItem alloc] initWithPlacemark:sourcePlaceMark];
	
	MKPlacemark *targetPlaceMark = [[MKPlacemark alloc] initWithCoordinate:target.coordinate addressDictionary:nil];
	MKMapItem *targetItem = [[MKMapItem alloc] initWithPlacemark:targetPlaceMark];
	
	MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
	[request setSource:sourceItem];
	[request setDestination:targetItem];
	[request setTransportType:MKDirectionsTransportTypeAny];
	[request setRequestsAlternateRoutes:YES];
	
	MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
	[directions calculateETAWithCompletionHandler:^(MKETAResponse *response, NSError *error) {
		
		if ( completionHandler ) {
			dispatch_async(kMainQueue, ^{
				if ( response )
					completionHandler(YES, response.expectedTravelTime);
				else
					completionHandler(NO, 0);
			});
		}
		
//		NSLog(@"eta = %f", response.expectedTravelTime);
	}];
}

+ (void) calculateDistanceByCoordinate:(CLLocationCoordinate2D)source target:(CLLocationCoordinate2D)target completionHandler:(DistanceCallback)completionHandler {

#if 0
	MKPlacemark *sourcePlaceMark = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(40.1374, 124.403) addressDictionary:nil];
	MKMapItem *sourceItem = [[MKMapItem alloc] initWithPlacemark:sourcePlaceMark];
	
	MKPlacemark *targetPlaceMark = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake(40.137, 124.404) addressDictionary:nil];
	MKMapItem *targetItem = [[MKMapItem alloc] initWithPlacemark:targetPlaceMark];
#else
	MKPlacemark *sourcePlaceMark = [[MKPlacemark alloc] initWithCoordinate:source addressDictionary:nil];
	MKMapItem *sourceItem = [[MKMapItem alloc] initWithPlacemark:sourcePlaceMark];
	
	MKPlacemark *targetPlaceMark = [[MKPlacemark alloc] initWithCoordinate:target addressDictionary:nil];
	MKMapItem *targetItem = [[MKMapItem alloc] initWithPlacemark:targetPlaceMark];
#endif
	
	MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
	[request setSource:sourceItem];
	[request setDestination:targetItem];
	[request setTransportType:MKDirectionsTransportTypeAny];
	[request setRequestsAlternateRoutes:YES];
	
	MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
	[directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
		if ( completionHandler ) {
			dispatch_async(kMainQueue, ^{
				if ( error ) {
					[AppDelegate errorHandler:error.localizedDescription];
					completionHandler(0);
					return;
				}
				
				if ( !response ) {
					completionHandler(0);
					return;
				}
				
				CGFloat minDistance = 0;
				for (MKRoute *route in response.routes) {
					
					CGFloat distance = 0;
					for (MKRouteStep *step in route.steps) {
						distance += step.distance;
					}
					
					if ( distance > 0 ) {
						if ( minDistance == 0 )
							minDistance = distance;
						else
							minDistance = MIN(minDistance, distance);
					}
				}
				
				completionHandler(minDistance);
			});
		}
	}];
}

+ (void) calculateDistance:(CLLocation *)source target:(CLLocation *)target completionHandler:(DistanceCallback)completionHandler {
	[LocationManager calculateDistanceByCoordinate:source.coordinate target:target.coordinate completionHandler:completionHandler];
}

+ (CLLocation *) currentLocation {
	return [LocationManager sharedInstance].currentLocation;
}

+ (CLLocationDegrees) latitude {
	return [LocationManager sharedInstance].currentLocation.coordinate.latitude;
}

+ (CLLocationDegrees) longitude {
	return [LocationManager sharedInstance].currentLocation.coordinate.longitude;
}

- (void) startLocationManager {
	self.locationManager = [[CLLocationManager alloc] init];
	self.locationManager.delegate = self;
	self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
	self.locationManager.distanceFilter = 1.0f;
	if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
		[self.locationManager requestAlwaysAuthorization];
	}
	
	[self.locationManager startUpdatingLocation];
}

- (void) stopLocationManager {
	[self.locationManager stopUpdatingLocation];
}

- (void) getDistanceByLocation:(CLLocation *)locationA and:(CLLocation *)locationB completionHandler:(DistanceCallback)completionHandler {
	if ( self.routeController ) {
		[self.routeController abortRequest];
		self.routeController = nil;
	}
	
	self.routeController = [[LRouteController alloc] init];
#if 1
	NSArray *locations = @[locationA, locationB];
#else
	NSArray *locations = @[[[CLLocation alloc] initWithLatitude:42.2852 longitude:-71.0409], [[CLLocation alloc] initWithLatitude:42.2853 longitude:-71.03]];
#endif
	[self.routeController getRoutes:locations travelMode:TravelModeDriving andCompletitionBlock:^(NSArray *routes) {
		CGFloat distance = 0;
		for (int i = 0; i < [routes count] - 1; i++) {
			CLLocation *a = routes[i];
			CLLocation *b = routes[i+1];
			distance += [a distanceFromLocation:b];
		}
		
//		completionHandler(distance);
		completionHandler(1);
	}];
}

- (void) getDistanceByCoordinate:(CLLocationCoordinate2D)locationA and:(CLLocationCoordinate2D)locationB completionHandler:(DistanceCallback)completionHandler {
	[self getDistanceByLocation:[[CLLocation alloc] initWithLatitude:locationA.latitude longitude:locationA.longitude]
							and:[[CLLocation alloc] initWithLatitude:locationB.latitude longitude:locationB.longitude]
			  completionHandler:completionHandler];
}

#pragma mark - CLLocationManagerDelegate
- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
	
	CLLocation *newLocation = [locations lastObject];
	self.currentLocation = newLocation;
	
//	[[User sharedInstance] submitLocationInBackground];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:kLocationManagerDidUpdateLocationNotification object:self];
	
	NSLog(@"Location Manager : %f %f", self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude);
	
	[self setIsAvailable:YES];
}

- (void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {

	self.currentLocation = newLocation;
	
//	[[User sharedInstance] submitLocationInBackground];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:kLocationManagerDidUpdateLocationNotification object:self];
	
	NSLog(@"Location Manager : %f %f", self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude);
	
	[self setIsAvailable:YES];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
	
		//	double osversion = [[UIDevice currentDevice]systemVersion].doubleValue;
	switch (status) {
		case kCLAuthorizationStatusAuthorizedAlways:
			NSLog(@"Got authorization, start tracking location");
			
			break;
		case kCLAuthorizationStatusAuthorizedWhenInUse:
			NSLog(@"Got authorization, start tracking location");
			
			break;
		case kCLAuthorizationStatusNotDetermined:
			break;
		default:
			break;
	}
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
	
	NSString *strError;
	[manager stopUpdatingLocation];
	
	NSLog(@"Error: %@",[error localizedDescription]);
	switch ( [error code] ) {
		case kCLErrorDenied:
				//Access denied by user
			strError = @"Access to Location Services denied by user";
				//Do something...
//			[[[UIAlertView alloc] initWithTitle:@"Message："
//										message:@"Please enable location permissions!"
//									   delegate:nil
//							  cancelButtonTitle:nil
//							  otherButtonTitles:@"OK", nil] show];
			break;
		case kCLErrorLocationUnknown:
				//Probably temporary...
			strError = @"Location data unavailable";
//			[[[UIAlertView alloc] initWithTitle:@"Message："
//										message:@"Location data unavailable!"
//									   delegate:nil
//							  cancelButtonTitle:nil
//							  otherButtonTitles:@"OK", nil] show];
				//Do something else...
			break;
		default:
			strError = @"An unknown error has occurred";
//			[[[UIAlertView alloc] initWithTitle:@"Message："
//										message:@"An unknown error has occurred!"
//									   delegate:nil
//							  cancelButtonTitle:nil
//							  otherButtonTitles:@"OK", nil] show];
			break;
	}
	
	self.currentLocation = nil;
	
	[NSTimer scheduledTimerWithTimeInterval:2.f target:self selector:@selector(startLocationManager) userInfo:self repeats:NO];
	
	if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
		NSLog(@"Please authorize location services");
		return;
	}
	
	NSLog(@"Location Manager Error : %@", strError);
	
	[self setIsAvailable:NO];
}

@end


@implementation Location

+ (instancetype) instance {
	return [[Location alloc] init];
}

+ (instancetype) instance:(NSString *)address coordinate:(CLLocationCoordinate2D)coordinate {
	Location *location = [Location instance];
	
	location.address = address;
	location.coordinate = coordinate;
	
	return location;
}

@end