//
//  YACustomTabbar.h
//  FeedMe
//
//  Created by AnYong on 16/3/14.
//  Copyright (c) 2014 Mobile Application Developer. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol YACustomTabbarDelegate <NSObject>
@optional
- (void) DidClickTabbarButtonWithIndex:(CustomTabbarButtonIndex)buttonIndex;
@end

@interface YACustomTabbar : UIView

// Controls
//@property (nonatomic, assign) IBOutlet UIImageView *img_tabbar_background;

// Variables
@property (nonatomic, assign) id<YACustomTabbarDelegate> delegate;
@property (nonatomic, readwrite) CustomTabbarButtonIndex activeButtonIndex;

// Button Delegates
- (IBAction)onClickTabbarButton:(id)sender;

@end
