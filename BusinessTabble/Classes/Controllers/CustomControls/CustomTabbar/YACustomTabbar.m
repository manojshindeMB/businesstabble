//
//  YACustomTabbar.m
//  FeedMe
//
//  Created by Rixian on 16/3/14.
//  Copyright (c) 2014 Mobile Application Developer. All rights reserved.
//

#import "YACustomTabbar.h"
#import "UIButton+VerticalLayout.h"

#define BUTTON_NUM 5

@implementation YACustomTabbar

// Controls
// @synthesize img_tabbar_background;

// Variables
@synthesize activeButtonIndex = _activeButtonIndex;
@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    for (int i = 1; i <= 2; i++) {
        UIButton *button = (UIButton *)[self viewWithTag:i];
        [button centerVerticallyWithPadding:-1.f];
    }
}
#pragma mark - Button Delegates
- (IBAction)onClickTabbarButton:(id)sender
{
    UIButton *btnClicked = (UIButton *)sender;
    [self.delegate DidClickTabbarButtonWithIndex:(CustomTabbarButtonIndex)btnClicked.tag];
}
#pragma mark - Property Methods
- (CustomTabbarButtonIndex)activeButtonIndex
{
    return _activeButtonIndex;
}
- (void)setActiveButtonIndex:(CustomTabbarButtonIndex)activeButtonIndex
{
    _activeButtonIndex = activeButtonIndex;
    for (int i = 1; i <= 2; i++) {
        UIButton *button = (UIButton *)[self viewWithTag:i];
        NSString *strButtonImageNameActive = [[self tabbarActiveImgs] objectAtIndex:i - 1];
        NSString *strButtonImageNameDeactive = [[self tabbarInactiveImgs] objectAtIndex:i - 1];
        if (_activeButtonIndex == (CustomTabbarButtonIndex)button.tag) {
            [button setAdjustsImageWhenHighlighted:NO];
            [button setImage:[UIImage imageNamed:strButtonImageNameActive] forState:UIControlStateNormal];
            [button setTitleColor:MAIN_COLOR forState:UIControlStateNormal];
        } else {
            [button setAdjustsImageWhenHighlighted:YES];
            [button setImage:[UIImage imageNamed:strButtonImageNameDeactive] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor lightGrayColor]  forState:UIControlStateNormal];
        }
    }
}
- (NSArray *)tabbarInactiveImgs
{
    static NSArray *_titles;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _titles = @[@"btn_home@3x.png",
                    @"btn_place@3x.png"];
    });
    return _titles;
}
- (NSArray *)tabbarActiveImgs
{
    static NSArray *_titles;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _titles = @[@"btn_home_selected@3x.png",
                    @"btn_place_selected@3x.png"];
    });
    return _titles;
}
@end
