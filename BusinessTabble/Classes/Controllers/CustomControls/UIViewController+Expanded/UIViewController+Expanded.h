//
//  UIViewController+Expanded.h
//  Intuition Journal
//
//  Created by DaRong Cai on 7/16/13.
//  Copyright (c) 2013 DaRong Cai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface UIView (UIViewController_Expanded)

- (void) showWithAnimation:(BOOL)bShow;

- (UIImage *) screenshot;

@end

@interface UIViewController (UIViewController_Expanded)

- (void) setNavigationTitle:(NSString *)title;

- (void) showProgress:(NSString *)message;
- (void) showProgressWithTitle:(NSString *)title message:(NSString *)message;
- (void) hideProgress;
- (void) hideProgressAfter:(CGFloat)second;

@end
