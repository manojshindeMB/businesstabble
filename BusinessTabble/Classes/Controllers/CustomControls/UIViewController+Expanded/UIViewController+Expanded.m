//
//  UIViewController+Expanded.m
//  Intuition Journal
//
//  Created by DaRong Cai on 7/16/13.
//  Copyright (c) 2013 DaRong Cai. All rights reserved.
//

#import "UIViewController+Expanded.h"

@implementation UIView (UIViewController_Expanded)

- (void) showWithAnimation:(BOOL)bShow
{
	if ( bShow )
	{
		[self setHidden:NO];
		[UIView animateWithDuration:0.3f animations:^{
			[self setAlpha:1];
		}];
	}
	else
	{
		[UIView animateWithDuration:0.3f animations:^{
			[self setAlpha:0];
		} completion:^(BOOL finished) {
			[self setHidden:YES];
		}];
	}
}

- (UIImage *) screenshot
{
	UIGraphicsBeginImageContextWithOptions(self.frame.size, self.opaque, 0.0);
	[self.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return image;
}

@end

@implementation UIViewController (UIViewController_Expanded)

- (void) setNavigationTitle:(NSString *)title
{
//	NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
//											   [UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:(51.0f/255.0f)], UITextAttributeTextColor,
//											   [UIColor whiteColor], UITextAttributeTextShadowColor,
//											   [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0f], UITextAttributeFont,
//											   [NSValue valueWithUIOffset:UIOffsetMake(-1, 0)], UITextAttributeTextShadowOffset, nil];
//	
//	[[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
//	[[UINavigationBar appearance] setTitleVerticalPositionAdjustment:-4 forBarMetrics:UIBarMetricsDefault];
	
//	CGRect titleFrame = self.navigationItem.titleView.frame;
	CGRect titleFrame = CGRectMake(0, 10, 190, 48);
	
	UILabel *titleLabel = [[UILabel alloc] initWithFrame:titleFrame];
	[titleLabel setBackgroundColor:[UIColor clearColor]];
	[titleLabel setTextAlignment:NSTextAlignmentCenter];
//	[titleLabel setTextColor:[UIColor colorWithRed:(51.0f/255.0f) green:(51.0f/255.0f) blue:(51.0f/255.0f) alpha:(51.0f/255.0f)]];
	[titleLabel setTextColor:[UIColor blackColor]];
	[titleLabel setFont:[UIFont fontWithName:@"Broadway" size:16.0f]];
//	[titleLabel setFont:[UIFont fontWithName:@"OpenSans" size:14.0f]];
	[titleLabel setShadowColor:[UIColor whiteColor]];
	[titleLabel setShadowOffset:CGSizeMake(0, 0)];
	[titleLabel setText:title];
	self.navigationItem.titleView = titleLabel;
	
	[self.navigationController.navigationBar setTitleVerticalPositionAdjustment:1 forBarMetrics:UIBarMetricsDefault];
}

#pragma mark - MBProgressHUD
- (void) showProgress:(NSString *)message {
	MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	hud.mode = MBProgressHUDModeIndeterminate;
	hud.removeFromSuperViewOnHide = YES;
	hud.labelText = message;
	hud.labelFont = [UIFont fontWithName:@"OpenSans" size:hud.labelFont.pointSize];
}

- (void) showProgressWithTitle:(NSString *)title message:(NSString *)message {
	MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	hud.mode = MBProgressHUDModeIndeterminate;
	hud.removeFromSuperViewOnHide = YES;
	hud.labelText = title;
	hud.detailsLabelText = message;
	hud.labelFont = [UIFont fontWithName:@"OpenSans" size:hud.labelFont.pointSize];
}

- (void) showConfirmationProgress:(NSString *)message {
	MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
	hud.mode = MBProgressHUDModeCustomView;
	hud.removeFromSuperViewOnHide = YES;
	hud.labelText = message;
	hud.labelFont = [UIFont fontWithName:@"OpenSans" size:hud.labelFont.pointSize];
	[self hideProgressAfter:2.f];
}

- (void) hideProgress {
	[MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void) hideProgressAfter:(CGFloat)second {
	dispatch_async(kBgQueue, ^{
		[NSThread sleepForTimeInterval:second];
		dispatch_async(kMainQueue, ^{
			[MBProgressHUD hideHUDForView:self.view animated:YES];
		});
	});
}

@end
