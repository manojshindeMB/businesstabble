//
//  ImageCropper.h
//  Created by http://github.com/iosdeveloper
//

#import <UIKit/UIKit.h>
#import "PECropViewController.h"
//#import <AdobeCreativeSDKCore/AdobeCreativeSDKCore.h>
//#import <AdobeCreativeSDKFoundation/AdobeCreativeSDKFoundation.h>



@protocol ImageCropperDelegate;

@interface ImageCropper : UIViewController <UIScrollViewDelegate, PECropViewControllerDelegate> //AVYPhotoEditorControllerDelegate{
{
    
    UIScrollView *scrollView;
    UIImageView *imageView;
}

@property (nonatomic, retain) UIScrollView *scrollView;
@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic) Boolean bHideCropView;
@property (nonatomic, assign) id <ImageCropperDelegate> delegate;

- (id)initWithImage:(UIImage *)image width:(float)width height:(float)height;
+ (ImageCropper *)instance;
@end

@protocol ImageCropperDelegate <NSObject>
- (void)imageCropper:(ImageCropper *)cropper didFinishCroppingWithImage:(UIImage *)image;
- (void)imageCropperDidCancel:(ImageCropper *)cropper;
@end

