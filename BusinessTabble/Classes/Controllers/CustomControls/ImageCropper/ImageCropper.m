//
//  ImageCropper.m
//  Created by http://github.com/iosdeveloper
//



#import "ImageCropper.h"


@interface ImageCropper()
{
    UIImage *m_cropped;
}
@end
@implementation ImageCropper

@synthesize scrollView, imageView;
@synthesize delegate;
@synthesize bHideCropView;
-(BOOL)prefersStatusBarHidden
{
    return YES;
}
+ (ImageCropper *)instance
{
    __strong static ImageCropper* sharedController = nil ;
    static dispatch_once_t onceToken ;
    
    dispatch_once( &onceToken, ^{
        sharedController = [ [ ImageCropper alloc ] initWithNibName : @"ImageCropper" bundle : nil ] ;
    } ) ;
    return sharedController ;
}

- (id)initWithImage:(UIImage *)image width:(float)width height:(float)height {
    self = [super init];
    if (self) {
        int nHeightOfScreen = ( [UIScreen mainScreen].bounds.size.height - [UIScreen mainScreen].bounds.size.width / 320.0 * 44 ) / 2 - [UIScreen mainScreen].bounds.size.width / 2;
        
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, nHeightOfScreen + [UIScreen mainScreen].bounds.size.width / 320.0 * 44, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width)];
        [scrollView setBackgroundColor:[UIColor blackColor]];
        [scrollView setDelegate:self];
        [scrollView setShowsHorizontalScrollIndicator:YES];
        [scrollView setShowsVerticalScrollIndicator:YES];
        [scrollView setUserInteractionEnabled:YES];
        [scrollView setMaximumZoomScale:5.0];
        [scrollView setScrollEnabled:YES];
        
        CGRect rect ;
        rect.size = image.size;
        if (image.size.width > image.size.height) {
            rect.size.width = (float)rect.size.width / ( (float)rect.size.height / [UIScreen mainScreen].bounds.size.width );
            rect.size.height = [UIScreen mainScreen].bounds.size.width;
        } else {
            rect.size.height = (float)rect.size.height / ( (float)rect.size.width / [UIScreen mainScreen].bounds.size.width );
            rect.size.width = [UIScreen mainScreen].bounds.size.width;
        }
        imageView = [[UIImageView alloc] initWithFrame:rect];
        
        [imageView setContentMode:UIViewContentModeScaleToFill];
        imageView.image = image;
        
        [scrollView setContentSize:rect.size];
        [scrollView setMinimumZoomScale:1.0f];
        [scrollView setZoomScale:[scrollView minimumZoomScale]];
        
        [scrollView setZoomScale:1.0f];
        [scrollView setContentMode:UIViewContentModeScaleToFill];
        [scrollView addSubview:imageView];
        [[self view] addSubview:scrollView];
        UINavigationBar *navigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0.0, 0.0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.width / 320.0 * 44)];
        UINavigationItem *aNavigationItem = [[UINavigationItem alloc] initWithTitle:NSLocalizedString(@"Move and Scale", nil)];
        [aNavigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelCropping)] ];
        [aNavigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(photoChosenWithoutEditor:)] ];
        
        [navigationBar setItems:[NSArray arrayWithObject:aNavigationItem]];
        [[self view] addSubview:navigationBar];
        
        UIButton *btnShowEditor = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 150, SCREEN_HEIGHT - 50, 150, 50)];
        [btnShowEditor setTitle:NSLocalizedString(@"Use Editor", nil) forState:UIControlStateNormal];
        [btnShowEditor setTitleColor:btnShowEditor.tintColor forState:UIControlStateNormal];
        btnShowEditor.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [[self view]  addSubview:btnShowEditor];
        btnShowEditor.center = CGPointMake(SCREEN_WIDTH / 2, SCREEN_HEIGHT - btnShowEditor.frame.size.height / 2 - 5);
        [btnShowEditor addTarget:self action:@selector(finishCropping) forControlEvents:UIControlEventTouchUpInside];
        [btnShowEditor setHidden:YES];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)cancelCropping {
    [delegate imageCropperDidCancel:self];
}

- (void)finishCropping {
    float zoomScale = 1.0 / [scrollView zoomScale];
    CGRect rect;
    float fFactor = MIN(imageView.image.size.width, imageView.image.size.height) / [scrollView frame].size.height;
    rect.origin.x = [scrollView contentOffset].x * zoomScale * fFactor;
    rect.origin.y = [scrollView contentOffset].y * zoomScale * fFactor;
    rect.size.width = MIN(imageView.image.size.width, imageView.image.size.height) * zoomScale;
    rect.size.height = MIN(imageView.image.size.width, imageView.image.size.height) * zoomScale;
    CGImageRef cr = CGImageCreateWithImageInRect([[imageView image] CGImage], rect);
    
    m_cropped = [UIImage imageWithCGImage:cr];
    CGImageRelease(cr);
    
    
    //    [self displayEditorForImage:m_cropped];
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = m_cropped;
    //            CGFloat width = image.size.width;
    //            CGFloat height = image.size.height;
    //        CGFloat length = MIN(width, height);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        controller.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    [controller setKeepingCropAspectRatio:NO];
    
    //        controller.imageCropRect = CGRectMake((width - length) / 2,
    //                                              (height - length) / 2,
    //                                              length,
    //                                              length);
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    [self presentViewController:navigationController animated:YES completion:NULL];
}


- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return imageView;
}


#pragma mark - PECropViewControllerDelegate methods

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    [delegate imageCropper:self didFinishCroppingWithImage:croppedImage];
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void)photoChosenWithoutEditor:(id)sender
{
    if(bHideCropView)
    {
        [self dismissViewControllerAnimated:YES completion:NULL];
        [delegate imageCropper:self didFinishCroppingWithImage:imageView.image];
        return;
    }
    //    [self dismissViewControllerAnimated:YES completion:NULL];
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = imageView.image;
    //            CGFloat width = image.size.width;
    //            CGFloat height = image.size.height;
    //        CGFloat length = MIN(width, height);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        controller.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    [controller setKeepingCropAspectRatio:NO];
    
    //        controller.imageCropRect = CGRectMake((width - length) / 2,
    //                                              (height - length) / 2,
    //                                              length,
    //                                              length);
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    [self presentViewController:navigationController animated:YES completion:NULL];
    
}
@end

