//
//  CoreDataViewController.h
//  Business Tabble
//
//  Created by user on 12/8/15.
//  Copyright © 2015 Chris. All rights reserved.
//

#import <UIKit/UIKit.h>

@import CoreData;

@interface CoreDataViewController : UIViewController <NSFetchedResultsControllerDelegate, UITableViewDataSource>

#pragma mark - Controls
@property (strong, nonatomic) IBOutlet UITableView *tableView;

#pragma mark - Variables
// The controller (this class fetches nothing if this is not set).
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;

// Set to YES to get some debugging output in the console.
@property BOOL debug;

#pragma mark - Methods
// Causes the fetchedResultsController to refetch the data.
// You almost certainly never need to call this.
// The NSFetchedResultsController class observes the context
//  (so if the objects in the context change, you do not need to call performFetch
//   since the NSFetchedResultsController will notice and update the table automatically).
// This will also automatically be called if you change the fetchedResultsController @property.
- (void)performFetch;

@end
