//
//  AppDelegate.h
//  BusinessTabble
//
//  Created by Liumin on 06/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <GooglePlus/GooglePlus.h>
#import <CoreLocation/CoreLocation.h>
#import <AddressBook/AddressBook.h>
#import <Google/SignIn.h>
#import <UserNotifications/UNUserNotificationCenter.h>
#import <AVFoundation/AVAudioPlayer.h>
#import "PayPalMobile.h"
#import "LocationTracker.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate,  GIDSignInDelegate, UNUserNotificationCenterDelegate, UIAlertViewDelegate> //,GPPDeepLinkDelegate
{


}
@property (strong, nonatomic) UIWindow *window;
@property LocationTracker * locationTracker;
@property (nonatomic) NSTimer* locationUpdateTimer;
+ (AppDelegate *) SharedDelegate;
@end

