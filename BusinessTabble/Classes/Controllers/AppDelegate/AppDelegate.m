//
//  AppDelegate.m
//  BusinessTabble
//
//  Created by Tian Ming on 06/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "AppDelegate.h"
#import <SDWebImage/SDWebImageManager.h>
#import "IQKeyboardManager.h"
#import "Global.h"

#import <MagicalRecord/MagicalRecord.h>
#import <GoogleMaps/GoogleMaps.h>
#import <OneSignal/OneSignal.h>
#import "UserService.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

//#import "BraintreeCore.h"

#define GOOGLE_ID @"828372290361-b7c3o33rtmremmmprvi59gohgn9l3u3o.apps.googleusercontent.com"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@interface AppDelegate ()

@end

@implementation AppDelegate
+ (AppDelegate *) SharedDelegate
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[GlobalData sharedData] loadInitData];
    
    SDWebImageManager.sharedManager.cacheKeyFilter = ^(NSURL *url) {
        url = [[NSURL alloc] initWithScheme:url.scheme host:url.host path:url.path];
        return [url absoluteString];
    };
    
    [self initializeOneSignal:launchOptions];
    if( SYSTEM_VERSION_LESS_THAN( @"10.0" ) )
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound |    UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];

    }
    else
    {
        UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert + UNAuthorizationOptionSound)
                              completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                  // Enable or disable features based on authorization.
                              }];
        center.delegate = self;
    }
    
    
    
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent];
    
    //#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)  if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {  ];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showStatusBar:) name:NOTIFICATION_SHOW_STATUS_BAR object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(hideStatusBar:) name:NOTIFICATION_HIDE_STATUS_BAR object:nil];
    
    [GMSServices provideAPIKey:@"AIzaSyBlrLZztgEwq1VAUUXVr310FduDG0OKL4I"];
   
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GIDSignIn sharedInstance].delegate = self;
    [[IQKeyboardManager sharedManager]setEnableAutoToolbar:NO];
    
    
//    [BTAppSwitch setReturnURLScheme:@"com.chris.businesstabble.payments"];
    //Paypal
    //    [PayPalMobile
    //     initializeWithClientIdsForEnvironments:@{PayPalEnvironmentSandbox : @"AfxA-7FOAz-MPUN98jJ0Ov6U-JLf957mU7BKsld8Xr8mVZrqQfFl_I82upkOfcVdmm3TCDwlgqOlS3PY"}];
    
    [PayPalMobile
     initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction :@"AeIBeLWLFpx3n-n-dnzSLD2s_VKVyeMlguPCHC-joAiG_uuIW_ApOJtdIyesVj1Dc9Bik7enodPSezeU",
                                              PayPalEnvironmentSandbox :   @"AfxA-7FOAz-MPUN98jJ0Ov6U-JLf957mU7BKsld8Xr8mVZrqQfFl_I82upkOfcVdmm3TCDwlgqOlS3PY"}];
    [self doLocationUpdate];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    return YES;
}
- (void)doLocationUpdate
{
//    UIAlertView * alert;
    if([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusDenied){
//        alert = [[UIAlertView alloc]initWithTitle:@""
//                                          message:@"The app doesn't work without the Background App Refresh enabled. To turn it on, go to Settings > General > Background App Refresh"
//                                         delegate:nil
//                                cancelButtonTitle:@"Ok"
//                                otherButtonTitles:nil, nil];
//        [alert show];
    }else if([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusRestricted){
        
//        alert = [[UIAlertView alloc]initWithTitle:@""
//                                          message:@"The functions of this app are limited because the Background App Refresh is disable."
//                                         delegate:nil
//                                cancelButtonTitle:@"Ok"
//                                otherButtonTitles:nil, nil];
//        [alert show];
        
    }else{
        self.locationTracker = [[LocationTracker alloc]init];
        [self.locationTracker startLocationTracking];
        self.locationUpdateTimer =
        [NSTimer scheduledTimerWithTimeInterval:LOCATION_UPDATE_TIME_INTERVAL
                                         target:self
                                       selector:@selector(updateLocation)
                                       userInfo:nil
                                        repeats:YES];
    }
}
-(void)updateLocation {
    [self.locationTracker updateLocationToServer];
}

- (void)initializeOneSignal:(NSDictionary*)launchOptions {
    [OneSignal initWithLaunchOptions:launchOptions appId:kOneSignalKey handleNotificationReceived:^(OSNotification *notification) {
        
        
    } handleNotificationAction:^(OSNotificationOpenedResult *result) {
        // This block gets called when the user reacts to a notification received
        OSNotificationPayload* payload = result.notification.payload;
        
        NSString* messageTitle = @"OneSignal Example";
        NSString* fullMessage = [payload.body copy];
        
        if (payload.additionalData) {
            
            if(payload.title)
                messageTitle = payload.title;
            
            if (result.action.actionID)
                fullMessage = [fullMessage stringByAppendingString:[NSString stringWithFormat:@"\nPressed ButtonId:%@", result.action.actionID]];
            
        }
        if([[GlobalData sharedData]isChatViewOpened])
            return;
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_PUSH_NOTIFICATION_RECEIVED object:payload.additionalData];
        
    }  settings:@{kOSSettingsKeyInAppAlerts: @NO, kOSSettingsKeyAutoPrompt: @YES}];
    
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler
{
    NSLog(@"notification  -- %@", notification);
}
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler
{
    NSLog(@"response identifier  -- %@", response.actionIdentifier);
    NSLog(@"response content %@", response.notification.request.content);
    NSLog(@"req content %@", response.notification);
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    NSLog(@"received notification info %@",userInfo);
}

-(void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void
                                                                                                                               (^)(UIBackgroundFetchResult))completionHandler
{
    if( [UIApplication sharedApplication].applicationState == UIApplicationStateInactive
       || [UIApplication sharedApplication].applicationState == UIApplicationStateBackground)
    {
       
        NSLog( @"INACTIVE" );
        [self handleReceivedPushNotification:userInfo applicationIsForeground:NO];
        completionHandler( UIBackgroundFetchResultNewData );
    }
    else  
    {  
        NSLog( @"FOREGROUND" );
        [self handleReceivedPushNotification:userInfo applicationIsForeground:YES];
        completionHandler( UIBackgroundFetchResultNewData );  
    }
}
- (void)handleReceivedPushNotification:(NSDictionary *)userInfo applicationIsForeground:(BOOL)applicationIsForeground
{
    NSString *type = @"";
    NSString *extra = @"";
    NSString *identifier = @"";
    NSString *msg = @"";
    NSInteger message_id = 0;
    if(![userInfo objectForKey:@"custom"] || ![userInfo objectForKey:@"aps"])
        return;
    if([[userInfo objectForKey:@"custom"] objectForKey:@"a"])
    {
        NSDictionary *dictExtra = [[userInfo objectForKey:@"custom"] objectForKey:@"a"];
        if(dictExtra && [dictExtra objectForKey:@"type"])
        {
            type = [dictExtra objectForKey:@"type"];
            if([dictExtra objectForKey:@"extra"])
                message_id = [[dictExtra objectForKey:@"extra"] integerValue];
            //[[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_PUSH_NOTIFICATION_RECEIVED object:dictExtra];
        }
    }
    if([[userInfo objectForKey:@"custom"] objectForKey:@"i"])
        identifier = [[userInfo objectForKey:@"custom"] objectForKey:@"i"];
    if([[userInfo objectForKey:@"aps"] objectForKey:@"alert"])
        msg = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    
    if(![[GlobalData sharedData]isEmpty:identifier])
    {
        //Saving to coredata
        NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
        Alert *alert = [Alert MR_createEntityInContext:context];
        alert.identifier = identifier;
        alert.message = msg;
        alert.type = type;
        alert.arrived = TimeStamp;
        alert.message_id = [NSNumber numberWithInteger:message_id];
        [[NSManagedObjectContext MR_defaultContext]MR_saveToPersistentStoreAndWait];
        [[GlobalData sharedData]setMUnreadPushNotifications:[[GlobalData sharedData] mUnreadPushNotifications] + 1];
        if(applicationIsForeground)
        {
            NSMutableDictionary * params = [[NSMutableDictionary alloc]init];
            [params setObject:type forKey:@"type"];
            [params setObject:[NSString stringWithFormat:@"%ld", message_id] forKey:@"extra"];
            if([type isEqualToString:@"chat"])
            {
                if([[GlobalData sharedData]isChatViewOpened])
                    return;
                [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_PUSH_NOTIFICATION_RECEIVED object:params];
            }
            if([type isEqualToString:@"message"])
            {
                if([[GlobalData sharedData]isMessageViewOpened])
                    return;
                [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_PUSH_NOTIFICATION_RECEIVED object:params];
            }
            NSString *title = @"Chat";
            if([type isEqualToString:@"message"])
                title = @"";
            else
            {
                if(message_id && message_id > 0)
                {
                    Bubble *bubble = [Bubble getBubbleById:message_id];
                    title =bubble.bubblename;
                }
            }
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:msg delegate:self cancelButtonTitle:@"Okay" otherButtonTitles:nil   ];
            AudioServicesPlaySystemSound(1002);
            [alertView show];
        }
    }
}
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    //    if ([identifier isEqualToString:@"declineAction"]){
    //    }
    //    else if ([identifier isEqualToString:@"answerAction"]){
    //    }
}
// for parse push
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    
}

- (void)clearBadge
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    [GlobalData clearTmpDirectory];
}
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [self clearBadge];
    [[NSNotificationCenter defaultCenter]postNotificationName:kStrNotif_UserLocationReceived object:nil];
    
    [GlobalData setBadgeOfNewNotif];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
//    [MagicalRecord cleanUp];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
//    if(localRegistry && [localRegistry objectForKey:kMyUserDict])
//    {
//        [MagicalRecord setupCoreDataStackWithStoreNamed:[NSString stringWithFormat:@"Tabble_%@", [[localRegistry objectForKey:kMyUserDict] objectForKey:@"id"]]];
//    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [MagicalRecord cleanUp];
}
#pragma mark notifications
- (void)showStatusBar:(id)sender
{
    [UIView animateWithDuration:0.1 animations:^{
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
    }];
    
}
- (void)hideStatusBar:(id)sender
{
    [UIView animateWithDuration:0.1 animations:^{
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }];
}
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            sourceApplication:(NSString *)sourceApplication
            annotation:(id)annotation
{
    NSString* scheme = [url scheme] ;
    
    // Facebook ;
    if([scheme hasPrefix : [NSString stringWithFormat: @"fb%@", @"1392550177520904"]])
    {
//        return [FBSession.activeSession handleOpenURL:url];
        BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                      openURL:url
                                                            sourceApplication:sourceApplication
                                                                   annotation:annotation
                        ];
        
        return handled;
    }
    else {
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:sourceApplication
                                          annotation:annotation];
    
    }
    return YES;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return NO;
#ifdef kPF_FACEBOOK_SDK_PURE
    return [FBSession.activeSession handleOpenURL:url];
    return [PFFacebookUtils handleOpenURL:url];
#endif
}



+ (AppDelegate *) instance
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}
#pragma mark - Google Plus
//- ( void ) didReceiveDeepLink : (GPPDeepLink*) _deepLink
//{
//    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"Deep-link Data"
//                                                        message: [_deepLink deepLinkID]
//                                                       delegate: nil
//                                              cancelButtonTitle: @"OK"
//                                              otherButtonTitles: nil];
//    [alertView show];
//}
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
//    NSString *userId = user.userID;                  // For client-side use only!
//    NSString *idToken = user.authentication.idToken; // Safe to send to the server
//    NSString *fullName = user.profile.name;
//    NSString *givenName = user.profile.givenName;
//    NSString *familyName = user.profile.familyName;
//    NSString *email = user.profile.email;

}
- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
}

#pragma mark Notifications

@end
