//
//  ProfileClient.m
//
//  Created by iDeveloper on 7/6/13.
//  Copyright (c) 2013 iDevelopers. All rights reserved.
//

#import "ProfileClient.h"
#import "NetAPIClient.h"

@implementation ProfileClient

+ (ProfileClient *)sharedClient
{
    static ProfileClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (_sharedClient == nil)
            _sharedClient = [[self alloc] init];
    });
    return _sharedClient;
}
////get user token by email address and password
//+ (void) getUserTokenbyEmail:(NSString *)email
//                 AndPassword:(NSString *)password
//                   successed:(void (^)(id responseObject))success
//                     failure:(void (^)(NSError* error))failure
//{
//    //set Parameters
//    NSMutableDictionary* params = [NSMutableDictionary dictionary];
//    
//    [params setObject:email     forKey:@"email"];
//    [params setObject:password  forKey:@"password"];
//    
//    NSString *serviceAPIURL = [NSString stringWithFormat:WEBAPILOGIN_URL, WEBSITE_URL];
//    
//    //-------------call Web Service ---------------------
//    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIURL
//                                              params:params
//                                             success:success
//                                             failure:failure];
//}

@end
