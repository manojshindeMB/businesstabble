//
//  APIClient.m
//  TwinPoint
//
//  Created Liu on 12/05/15.
//  Copyright (c) 2014 Bernie Brondum. All rights reserved.
//
// --- Headers ---;
#import "APIClient.h"



@implementation APIClient

// Functions;
#pragma mark - Shared Client
+ (instancetype)sharedAPIClient
{
    static APIClient *_sharedClient;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedClient = [[APIClient alloc] init];
        
        // Set;
//        _sharedClient.responseSerializer = [AFJSONResponseSerializer serializer];
//        _sharedClient.responseSerializer.acceptableContentTypes =[NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"image/jpeg", nil];
        //[NSSet setWithObject: @"application/json"];
    });
    
    return _sharedClient;
}



-(void)uploadPhotoWithFileName : (NSString *)strfilename imageData:(NSData*)imgData completionHandler:(CompletionHandler)completionHandler{
    
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:FBALLR_WEB_SERVICES_URL]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:@"posts" forKey:@"type"];
    [parameters setValue:@"post_video" forKey:@"cmd"];
    [parameters setValue:strfilename forKey:@"filename"];
    AFHTTPRequestOperation *op = [manager POST:@"webservice.php" parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //do not put image inside parameters dictionary as I did, but append it!
        [formData appendPartWithFileData:imgData name:@"file" fileName:strfilename mimeType:@"image/jpeg"];
//        [formData appendPartWithFileData:imageData name:@"photo" fileName:@"photo.jpg" mimeType:@"image/jpeg"];

    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
    }];
    [op start];
}
-(void)uploadPhotoWithFileName : (NSString *)strfilename oldfilename:(NSString *)oldfilename imageData:(NSData*)imgData completion:(void (^)())successHandler FailureHandler:(void (^)())failureHandler;
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:FBALLR_WEB_SERVICES_URL]];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setValue:@"posts" forKey:@"type"];
    [parameters setValue:@"update_photo" forKey:@"cmd"];
    [parameters setValue:strfilename forKey:@"filename"];
    [parameters setValue:oldfilename forKey:@"oldfilename"];

    AFHTTPRequestOperation *op = [manager POST:@"webservice.php" parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:imgData name:@"file" fileName:strfilename mimeType:@"image/jpeg"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        successHandler();
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failureHandler();
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
    }];
    [op start];
}

@end
