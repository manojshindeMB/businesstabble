//
//  LoginService.h
//  Jogging
//
//  Created by Liu on 19/10/15.
//  Copyright (c) 2015 Chris Register. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserService : NSObject
+ (id)sharedData;
- (void)signupRequestByEmail:(NSString *)email
                      params:(NSMutableDictionary*)_params
                     success:(void (^)(id _responseObject, NSString *result_code))_success
                     failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)updateUserRequestById:(NSString *)userid email:(NSString *)email
                       params:(NSMutableDictionary*)_params
                      success:(void (^)(id _responseObject, NSString *result_code))_success
                      failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)loginRequestBySocialMediaWithParams:(NSMutableDictionary*)_params
                                    success:(void (^)(id _responseObject, NSInteger result_code))_success
                                    failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)loginRequestByUserNameWithParams:(NSMutableDictionary*)_params
                                 success:(void (^)(id _responseObject))_success
                                 failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)signUpRequestByUserNameWithParams:(NSMutableDictionary*)_params
                                     type:(NSInteger)type
                                 success:(void (^)(id _responseObject))_success
                                 failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)passwordRecoveryWithParams:(NSMutableDictionary*)_params
                                  success:(void (^)(id _responseObject))_success
                                  failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)updateUserLocationWithParams:(NSMutableDictionary*)_params
                             success:(void (^)(id _responseObject))_success
                             failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)checkUserNameAvailabilityWithParams:(NSMutableDictionary*)_params
                             success:(void (^)(id _responseObject))_success
                             failure:(void (^)(NSInteger  _errorCode))_failure;
@end
