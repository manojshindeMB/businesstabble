//
//  LoginService.m
//  Jogging
//
//  Created by Liu on 19/10/15.
//  Copyright (c) 2015 Chris Register. All rights reserved.
//

#import "BubbleService.h"


@implementation BubbleService
+(id) sharedData
{
    BubbleService *_SharedData = nil;
    @synchronized(self)
    {
        if (_SharedData == nil)
        {
            _SharedData = [[self alloc] init]; // assignment not done here
        }
    }
    return _SharedData;
}
- (void)createBubbleWithDictionary:(NSMutableDictionary*)_params
                                  success:(void (^)(id _responseObject))_success
                                  failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/bubble/create/"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSDictionary *dict=[_responseObject objectForKey:@"data"];
        _success(dict);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)updateBubbleWithDictionary:(NSMutableDictionary*)_params
                           success:(void (^)(id _responseObject))_success
                           failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/bubble/update/"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSDictionary *dict=[_responseObject objectForKey:@"data"];
        _success(dict);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)getBubbleListWithDictionary:(NSMutableDictionary*)_params
                           success:(void (^)(id _responseObject))_success
                           failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/bubble/bubbles/"];
    [[NetAPIClient sharedClient] sendToServiceByGET:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSMutableArray *arrResult=[_responseObject objectForKey:@"data"];
        _success(arrResult);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}

- (void)getMyBubbleListWithDictionary:(NSMutableDictionary*)_params
                            success:(void (^)(id _responseObject))_success
                            failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/bubble/mybubbles/"];
    [[NetAPIClient sharedClient] sendToServiceByGET:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSMutableArray *arrResult=[_responseObject objectForKey:@"data"];
        _success(arrResult);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)getAllBubbleListWithDictionary:(NSMutableDictionary*)_params
                            success:(void (^)(id _responseObject))_success
                            failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/bubble/allbubbles/"];
    [[NetAPIClient sharedClient] sendToServiceByGET:serviceAPIUrl params:_params success:^(id _responseObject) {
//        NSMutableArray *arrResult=[_responseObject objectForKey:@"data"];
        _success(_responseObject);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}


- (void)joinBubbleWithDictionary:(NSMutableDictionary*)_params
                         success:(void (^)(id _responseObject))_success
                         failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/bubble/join/"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSMutableArray *arrResult=[_responseObject objectForKey:@"data"];
        _success(arrResult);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)unJoinBubbleWithDictionary:(NSMutableDictionary*)_params
                         success:(void (^)(id _responseObject))_success
                         failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/bubble/unjoin/"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSMutableArray *arrResult=[_responseObject objectForKey:@"data"];
        _success(arrResult);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)approveBubbleWithDictionary:(NSMutableDictionary*)_params
                         success:(void (^)(id _responseObject))_success
                         failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/bubble/approve/"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSMutableArray *arrResult=[_responseObject objectForKey:@"data"];
        _success(arrResult);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)removeBubbleWithDictionary:(NSMutableDictionary*)_params
                            success:(void (^)(id _responseObject))_success
                            failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/bubble/remove/"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSMutableArray *arrResult=[_responseObject objectForKey:@"data"];
        _success(arrResult);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
// Activity Like
- (void)getActivityLikeListWithDictionary:(NSMutableDictionary*)_params
                                  success:(void (^)(id _responseObject))_success
                                  failure:(void (^)(NSInteger  _errorCode))_failure;

{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/bubble/activitylike/"];
    [[NetAPIClient sharedClient] sendToServiceByGET:serviceAPIUrl params:_params success:^(id _responseObject) {
        _success(_responseObject);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)setActivityLikeListWithDictionary:(NSMutableDictionary*)_params
                                  success:(void (^)(id _responseObject))_success
                                  failure:(void (^)(NSInteger  _errorCode))_failure;

{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/bubble/activitylike/"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        _success(_responseObject);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
// Bubble Member
- (void)getBubbleMemberListWithDictionary:(NSMutableDictionary*)_params
                                  success:(void (^)(id _responseObject))_success
                                  failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/bubble/members/"];
    [[NetAPIClient sharedClient] sendToServiceByGET:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSMutableArray *arrResult=[_responseObject objectForKey:@"data"];
        _success(arrResult);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)setMemberPostEnabledForBubbleWithDictionary:(NSMutableDictionary*)_params
                                            success:(void (^)(id _responseObject))_success
                                            failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/bubble/allowUserPost/"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSMutableArray *arrResult=[_responseObject objectForKey:@"data"];
        _success(arrResult);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}

// Bubble Menu
- (void)getBubbleMenuListWithDictionary:(NSMutableDictionary*)_params
                                  success:(void (^)(id _responseObject))_success
                                  failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/bubble/bubblemenus/"];
    [[NetAPIClient sharedClient] sendToServiceByGET:serviceAPIUrl params:_params success:^(id _responseObject) {
//        NSMutableArray *arrResult=[_responseObject objectForKey:@"data"];
        _success(_responseObject);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)updateBubbleMenuWithDictionary:(NSMutableDictionary*)_params
                           success:(void (^)(id _responseObject))_success
                           failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/bubble/bubblemenus/"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSDictionary *dict=[_responseObject objectForKey:@"data"];
        _success(dict);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)removeBubbleMenuWithDictionary:(NSMutableDictionary*)_params
                               success:(void (^)(id _responseObject))_success
                               failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/bubble/removebubblemenu/"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSDictionary *dict=[_responseObject objectForKey:@"data"];
        _success(dict);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
@end
