//
//  OtherService.m
//  BusinessTabble
//
//  Created by Haichen Song on 25/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "OtherService.h"

@implementation OtherService
+ (id)sharedData
{
    OtherService *_SharedData = nil;
    @synchronized(self)
    {
        if (_SharedData == nil)
        {
            _SharedData = [[self alloc] init]; // assignment not done here
        }
    }
    return _SharedData;
}
- (void)sendFeedbackWithDictionary:(NSMutableDictionary*)_params
                           success:(void (^)(id _responseObject))_success
                           failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/other/feedback/"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSDictionary *dict=[_responseObject objectForKey:@"data"];
        _success(dict);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];

}
- (void)sendReportWithDictionary:(NSMutableDictionary*)_params
                           success:(void (^)(id _responseObject))_success
                           failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/other/report/"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSDictionary *dict=[_responseObject objectForKey:@"data"];
        _success(dict);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
    
}
@end
