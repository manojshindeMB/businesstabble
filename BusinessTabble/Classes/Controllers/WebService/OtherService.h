//
//  OtherService.h
//  BusinessTabble
//
//  Created by Haichen Song on 25/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OtherService : NSObject
+ (id)sharedData;
- (void)sendFeedbackWithDictionary:(NSMutableDictionary*)_params
                           success:(void (^)(id _responseObject))_success
                           failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)sendReportWithDictionary:(NSMutableDictionary*)_params
                         success:(void (^)(id _responseObject))_success
                         failure:(void (^)(NSInteger  _errorCode))_failure;
@end
