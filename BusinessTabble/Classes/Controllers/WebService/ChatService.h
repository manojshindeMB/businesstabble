//
//  MessageService.h
//  BusinessTabble
//
//  Created by Tian Ming on 29/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatService : NSObject
+ (id)sharedData;
- (void)createMessageWithDictionary:(NSMutableDictionary*)_params
                           success:(void (^)(id _responseObject))_success
                           failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)getMessageListWithDictionary:(NSMutableDictionary*)_params
                             success:(void (^)(id _responseObject))_success
                             failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)getChatHidePosterListWithDictionary:(NSMutableDictionary*)_params
                               success:(void (^)(id _responseObject))_success
                               failure:(void (^)(NSInteger  _errorCode))_failure;

- (void)hideUser:(NSMutableDictionary*)_params
         success:(void (^)(id _responseObject))_success
         failure:(void (^)(NSInteger  _errorCode))_failure;

- (void)deleteMessageWithDictionary:(NSMutableDictionary*)_params
                        success:(void (^)(id _responseObject))_success
                        failure:(void (^)(NSInteger  _errorCode))_failure;
//- (void)emailCouponUsersWithDictionary:(NSMutableDictionary*)_params
//                               success:(void (^)(id _responseObject))_success
//                               failure:(void (^)(NSInteger  _errorCode))_failure;

@end
