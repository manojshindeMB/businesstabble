//
//  OtherService.m
//  BusinessTabble
//
//  Created by Haichen Song on 25/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "MainService.h"

@implementation MainService

+ (id)sharedData
{
    MainService *_SharedData = nil;
    @synchronized(self)
    {
        if (_SharedData == nil)
        {
            _SharedData = [[self alloc] init]; // assignment not done here
        }
    }
    return _SharedData;
}
- (void)getBussinessBubbleListWithSuccessHandler:(void (^)())successHandler
                                  FailureHandler:(void (^)())failureHandler
{
    if([[GlobalData sharedData]isSessionExpired])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
        return;
    }
    
    if([[GlobalData sharedData]isBubbleLoading])
    {
        failureHandler();
        return;
    }
    NSString *lastTimeStamp = [localRegistry objectForKey:kLastFetchTimeStampForBusinessBubbleList];
    if(!lastTimeStamp) lastTimeStamp = @"";
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:lastTimeStamp forKey:@"last_time_stamp"];
    [params setObject:[NSString stringWithFormat:@"%f", [[GlobalData sharedData]currentLatitude]] forKey:@"latitude"];
    [params setObject:[NSString stringWithFormat:@"%f", [[GlobalData sharedData]currentLongitude]] forKey:@"longitude"];
    
    [[GlobalData sharedData]setIsBubbleLoading:YES];
    [[BubbleService sharedData]getAllBubbleListWithDictionary:params success:^(id _responseObject) {
        if(![localRegistry objectForKey:kMyUserDict])
        {
            [[GlobalData sharedData]setIsBubbleLoading:NO];
            return ;
        }
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            [localRegistry setObject:[_responseObject objectForKey:@"timestamp"] forKey:kLastFetchTimeStampForBusinessBubbleList];
            NSMutableArray *arrResult=[_responseObject objectForKey:@"data"];
            [Bubble MR_importFromArray:arrResult inContext:localContext];
        } completion:^(BOOL contextDidSave, NSError *error) {
            [[GlobalData sharedData]setIsBubbleLoading:NO];
            successHandler();
        }];
    } failure:^(NSInteger _errorCode) {
        [[GlobalData sharedData]setIsBubbleLoading:NO];
        failureHandler();
    }];
    
}
- (void)getBubbleMemberList:(void (^)())successHandler
             FailureHandler:(void (^)())failureHandler
{
    if([[GlobalData sharedData]isSessionExpired])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
        return;
    }
    NSString *lastTimeStamp = [localRegistry objectForKey:kLastFetchTimeStampForBubbleMemberList];
    if(!lastTimeStamp) lastTimeStamp = @"";
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    
    [params setObject:lastTimeStamp forKey:@"last_time_stamp"];
    [[BubbleService sharedData]getBubbleMemberListWithDictionary:params success:^(id _responseObject) {
        if(![localRegistry objectForKey:kMyUserDict])
            return ;
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            [localRegistry setObject:TimeStamp forKey:kLastFetchTimeStampForBubbleMemberList];
            [BubbleMember MR_importFromArray:_responseObject inContext:localContext];
        } completion:^(BOOL contextDidSave, NSError *error) {
            [[GlobalData sharedData]setIsBubbleLoading:NO];
            successHandler();
        }];
        
    } failure:^(NSInteger _errorCode) {
        failureHandler();
    }];
}
- (void)fetchBubbleMenuList:(JUserInfo *)myInfo
                          success:(void (^)(id _responseObject))_success
                          failure:(void (^)(NSInteger  _errorCode))_failure
{
    if([[GlobalData sharedData]isSessionExpired])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
        return;
    }
    NSString *lastTimeStamp = [localRegistry objectForKey:kLastFetchTimeStampForBubbleMenuList];
    if(!lastTimeStamp) lastTimeStamp = @"";
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:lastTimeStamp forKey:@"last_time_stamp"];
     [[BubbleService sharedData]getBubbleMenuListWithDictionary:params success:^(id _responseObject) {
         if(![localRegistry objectForKey:kMyUserDict])
             return ;
         if([_responseObject objectForKey:@"timestamp"])
             [localRegistry setObject:[_responseObject objectForKey:@"timestamp"] forKey:kLastFetchTimeStampForBubbleMenuList];
         else
             [localRegistry setObject:TimeStamp forKey:kLastFetchTimeStampForBubbleMenuList];
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext) {
            [BubbleMenu  MR_importFromArray:[_responseObject objectForKey:@"data"]  inContext:localContext];
        } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
            _success(_responseObject);
        }];
    } failure:^(NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)getBubbleMessageListWithSuccessHandler:(void (^)())successHandler
                                FailureHandler:(void (^)())failureHandler
{
//    if([[GlobalData sharedData]isMessageLoading])
//    {
//        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC);
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//            [self getBubbleMessageActualLoadingListWithSuccessHandler:^{
//                successHandler();
//                return;
//            } FailureHandler:^{
//                failureHandler();
//                return;
//            }];
//        });
//      
//    }
    if([[GlobalData sharedData]isSessionExpired])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
        return;
    }
    [self getBubbleMessageActualLoadingListWithSuccessHandler:^{
        successHandler();
    } FailureHandler:^{
        failureHandler();
    }];
}
- (void)getBubbleMessageActualLoadingListWithSuccessHandler:(void (^)())successHandler
                                FailureHandler:(void (^)())failureHandler
{
    if([[GlobalData sharedData]isMessageLoading])
    {
        failureHandler();
        return;
    }
    NSString *lastTimeStamp = [localRegistry objectForKey:kLastFetchTimeStampForBubbleMessageList];
    if(!lastTimeStamp) lastTimeStamp = @"";
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:lastTimeStamp forKey:@"last_time_stamp"];
    [[GlobalData sharedData]setIsMessageLoading:YES];
    [[MessageService sharedData]getMessageListWithDictionary:params success:^(id _responseObject) {
        if(![localRegistry objectForKey:kMyUserDict])
        {
            [[GlobalData sharedData]setIsMessageLoading:NO];
            return;
        }
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            NSMutableArray *arrResult=[_responseObject objectForKey:@"data"];
            [localRegistry setObject:[_responseObject objectForKey:@"timestamp"] forKey:kLastFetchTimeStampForBubbleMessageList];
            [Message MR_importFromArray:arrResult inContext:localContext];
        } completion:^(BOOL contextDidSave, NSError *error) {
            [[GlobalData sharedData]setIsMessageLoading:NO];
            successHandler();
        }];
        
    } failure:^(NSInteger _errorCode) {
        [[GlobalData sharedData]setIsMessageLoading:NO];
        failureHandler();
    }];
}
- (void)getBubbleChatListWithSuccessHandler:( void (^)( id _responseObject ) )successHandler
                                FailureHandler:(void (^)())failureHandler
{
    if([[GlobalData sharedData]isChatLoading])
    {
        failureHandler();
        return;
    }
    if([[GlobalData sharedData]isSessionExpired])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
        return;
    }
    NSString *lastTimeStamp = [localRegistry objectForKey:kLastFetchTimeStampForBubbleChatList];
    if(!lastTimeStamp) lastTimeStamp = @"";
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:lastTimeStamp forKey:@"last_time_stamp"];
    
    [[GlobalData sharedData]setIsChatLoading:YES];
    [[ChatService sharedData]getMessageListWithDictionary:params success:^(id _responseObject) {
        if(![localRegistry objectForKey:kMyUserDict])
        {
            [[GlobalData sharedData]setIsChatLoading:NO];
            return ;
        }
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            NSMutableArray *arrResult=[_responseObject objectForKey:@"data"];
            
            [Chat MR_importFromArray:arrResult inContext:localContext];

        } completion:^(BOOL contextDidSave, NSError *error) {
            [localRegistry setObject:[_responseObject objectForKey:@"timestamp"] forKey:kLastFetchTimeStampForBubbleChatList];
            [[GlobalData sharedData]setIsChatLoading:NO];
            if([[_responseObject objectForKey:@"data"] count] > 0)
                [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_CHAT_LIST_RECEIVED object:nil];
            successHandler([_responseObject objectForKey:@"data"]);
            
        }];        
    } failure:^(NSInteger _errorCode) {
        [[GlobalData sharedData]setIsChatLoading:NO];
    }];
}
- (void)getBubbleChatHideListWithSuccessHandler:( void (^)( id _responseObject ) )successHandler
                             FailureHandler:(void (^)())failureHandler
{
    if([[GlobalData sharedData]isSessionExpired])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
        return;
    }
    NSString *lastTimeStamp = [localRegistry objectForKey:BubbleHidePosterListLastTimeStamp];
    if(!lastTimeStamp) lastTimeStamp = @"";
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:lastTimeStamp forKey:@"last_time_stamp"];
    
    [[ChatService sharedData]getChatHidePosterListWithDictionary:params success:^(id _responseObject) {
        if(![localRegistry objectForKey:kMyUserDict])
            return ;
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            NSMutableArray *arrResult=[_responseObject objectForKey:@"data"];
            [localRegistry setObject:[_responseObject objectForKey:@"timestamp"] forKey:BubbleHidePosterListLastTimeStamp];
            [Chat_hideposter MR_importFromArray:arrResult inContext:localContext];

        } completion:^(BOOL contextDidSave, NSError *error) {
            successHandler([_responseObject objectForKey:@"data"]);
        }];
    } failure:^(NSInteger _errorCode) {
        failureHandler();
    }];
}
- (void)getCouponUseListWithSuccessHandler:( void (^)( id _responseObject ) )successHandler
                                 FailureHandler:(void (^)())failureHandler
{
    
    if([[GlobalData sharedData]isSessionExpired])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
        return;
    }
    NSString *lastTimeStamp = [localRegistry objectForKey:kLastFetchTimeStampForCouponList];
    if(!lastTimeStamp) lastTimeStamp = @"";
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:lastTimeStamp forKey:@"last_time_stamp"];
    
    [[MessageService sharedData] getCouponUseListWithDictionary:params success:^(id _responseObject) {
        if(![localRegistry objectForKey:kMyUserDict])
            return ;
        NSMutableArray *arrResult=[_responseObject objectForKey:@"data"];
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            
            [localRegistry setObject:[_responseObject objectForKey:@"timestamp"] forKey:kLastFetchTimeStampForCouponList];
            [CouponUse MR_importFromArray:arrResult inContext:localContext];
        } completion:^(BOOL contextDidSave, NSError *error) {
            successHandler(arrResult);
        }];
    } failure:^(NSInteger _errorCode) {
        failureHandler();
    }];
}
- (void)getActivityLikeListWithSuccessHandler:( void (^)( id _responseObject ) )successHandler
                            FailureHandler:(void (^)())failureHandler
{
    
    if([[GlobalData sharedData]isSessionExpired])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTIFICATION_LOGOUT object:nil];
        return;
    }
    NSString *lastTimeStamp = [localRegistry objectForKey:kLastFetchTimeStampForActivityLikeList];
    if(!lastTimeStamp) lastTimeStamp = @"";
    NSMutableDictionary *params = [[NSMutableDictionary alloc]init];
    [params setObject:[[GlobalData sharedData]mUserInfo].mId forKey:@"user_id"];
    [params setObject:lastTimeStamp forKey:@"last_time_stamp"];
    
     [[BubbleService sharedData]getActivityLikeListWithDictionary:params success:^(id _responseObject) {
        if(![localRegistry objectForKey:kMyUserDict])
            return ;
        NSMutableArray *arrResult=[_responseObject objectForKey:@"data"];
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            [localRegistry setObject:[_responseObject objectForKey:@"timestamp"] forKey:kLastFetchTimeStampForActivityLikeList];
            [Activity_Like MR_importFromArray:arrResult inContext:localContext];
        } completion:^(BOOL contextDidSave, NSError *error) {
            successHandler(arrResult);
        }];
    } failure:^(NSInteger _errorCode) {
        failureHandler();
    }];
}


@end
