//
//  LoginService.m
//  Jogging
//
//  Created by Liu on 19/10/15.
//  Copyright (c) 2015 Chris Register. All rights reserved.
//

#import "UserService.h"


@implementation UserService
+(id) sharedData
{
    UserService *_SharedData = nil;
    @synchronized(self)
    {
        if (_SharedData == nil)
        {
            _SharedData = [[self alloc] init]; // assignment not done here
        }
    }
    return _SharedData;
}
- (void)updateUserRequestById:(NSString *)userid email:(NSString *)email
                      params:(NSMutableDictionary*)_params
                     success:(void (^)(id _responseObject, NSString *result_code))_success
                     failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",WEB_SERVICE_RELATIVE_URL];
    [_params setObject:@"login_signup" forKey:@"type"];
    [_params setObject:@"update" forKey:@"cmd"];
    [_params setObject:userid forKey:@"id"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:(NSData *)_responseObject options:NSJSONReadingAllowFragments error:nil];
        NSDictionary *result =[dict objectForKey:@"data"];
        NSString *result_code = [result objectForKey:@"result_code"];
        _success([dict objectForKey:@"data"], result_code);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)signupRequestByEmail:(NSString *)email
                       params:(NSMutableDictionary*)_params
                      success:(void (^)(id _responseObject, NSString *result_code))_success
                      failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",WEB_SERVICE_RELATIVE_URL];
    [_params setObject:@"login_signup" forKey:@"type"];
    [_params setObject:@"register" forKey:@"cmd"];
    
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSDictionary *dict=[NSJSONSerialization JSONObjectWithData:(NSData *)_responseObject options:NSJSONReadingAllowFragments error:nil];
        NSDictionary *result =[dict objectForKey:@"data"];
        NSString *result_code = [result objectForKey:@"result_code"];
        _success([dict objectForKey:@"data"], result_code);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)loginRequestBySocialMediaWithParams:(NSMutableDictionary*)_params
                                     success:(void (^)(id _responseObject, NSInteger result_code))_success
                                     failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/user/sociallogin/"];
    [[NetAPIClient sharedClient] sendToServiceByGET:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSDictionary *dict=[_responseObject objectForKey:@"data"];
        NSString *status = [_responseObject objectForKey:@"status"];
        _success(dict,  [status integerValue]);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)loginRequestByUserNameWithParams:(NSMutableDictionary*)_params
                                 success:(void (^)(id _responseObject))_success
                                 failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/user/login"];
    [[NetAPIClient sharedClient] sendToServiceByGET:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSDictionary *dict=[_responseObject objectForKey:@"data"];
        _success(dict);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)signUpRequestByUserNameWithParams:(NSMutableDictionary*)_params
                                     type:(NSInteger)type
                                  success:(void (^)(id _responseObject))_success
                                  failure:(void (^)(NSInteger  _errorCode))_failure
{
    
    NSString *serviceAPIUrl = @"";
    if(type == ACCOUNT_TYPE_EDIT_USER_INFO)
        serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/user/update/"];
    else
        serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/user/signup/"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSDictionary *dict=[_responseObject objectForKey:@"data"];
        _success(dict);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)passwordRecoveryWithParams:(NSMutableDictionary*)_params
                           success:(void (^)(id _responseObject))_success
                           failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/user/passwordrecovery"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSDictionary *dict=[_responseObject objectForKey:@"data"];
        _success(dict);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];

}
- (void)updateUserLocationWithParams:(NSMutableDictionary*)_params
                             success:(void (^)(id _responseObject))_success
                             failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/user/updatelocation"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSDictionary *dict=[_responseObject objectForKey:@"data"];
        _success(dict);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)checkUserNameAvailabilityWithParams:(NSMutableDictionary*)_params
                                    success:(void (^)(id _responseObject))_success
                                    failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/user/checkUsernameAvailability"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSDictionary *dict=[_responseObject objectForKey:@"data"];
        _success(dict);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
@end
