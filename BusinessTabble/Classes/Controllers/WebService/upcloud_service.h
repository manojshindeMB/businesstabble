//
//  KKMTUser.h
//  KouKouMT
//

#import <Foundation/Foundation.h>

@class upcloud_service;

typedef void (^CompletionHandler)(BOOL success, NSString *error);
typedef void (^AvatarDownloadCompletionHandler)(UIImage *result);

@interface upcloud_service : NSObject

@property (nonatomic, strong) NSString *picturePath;
@property (nonatomic, strong) UIImage *picture;

+ (upcloud_service *) sharedInstance;

- (void) uploadDocument:(UIImage *)document forKey:(NSString *)key completionHandler:(CompletionHandler)completionHandler;
- (void) uploadFile:(NSString *)strfilename imageData:(NSData*)imgData completionHandler:(CompletionHandler)completionHandler ;

- (void) uploadFile:(NSString *)strfilename videoname:(NSString*)strfileVideo imageData:(NSData*)imgData videoData:(NSData*)videoData completionHandler:(CompletionHandler)completionHandler ;

@end
