//
//  MessageService.m
//  BusinessTabble
//
//  Created by Liu Min on 29/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "MessageService.h"

@implementation MessageService
+(id) sharedData
{
    MessageService *_SharedData = nil;
    @synchronized(self)
    {
        if (_SharedData == nil)
        {
            _SharedData = [[self alloc] init]; // assignment not done here
        }
    }
    return _SharedData;
}
- (void)createMessageWithDictionary:(NSMutableDictionary*)_params
                           success:(void (^)(id _responseObject))_success
                           failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/message/create/"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSDictionary *dict=[_responseObject objectForKey:@"data"];
        _success(dict);
//
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)useCouponWithDictionary:(NSMutableDictionary*)_params
                            success:(void (^)(id _responseObject))_success
                            failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/message/usecoupon/"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSDictionary *dict=[_responseObject objectForKey:@"data"];
        _success(dict);
        //
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)deleteMessageWithDictionary:(NSMutableDictionary*)_params
                        success:(void (^)(id _responseObject))_success
                        failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/message/delete/"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
        NSDictionary *dict=[_responseObject objectForKey:@"data"];
        _success(dict);
        //
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}



- (void)getMessageListWithDictionary:(NSMutableDictionary*)_params
                            success:(void (^)(id _responseObject))_success
                            failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/message/messages/"];
    [[NetAPIClient sharedClient] sendToServiceByGET:serviceAPIUrl params:_params success:^(id _responseObject) {
//        NSMutableArray *arrResult=[_responseObject objectForKey:@"data"];
        _success(_responseObject);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)getCouponUseListWithDictionary:(NSMutableDictionary*)_params
                             success:(void (^)(id _responseObject))_success
                             failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/message/couponuse/"];
    [[NetAPIClient sharedClient] sendToServiceByGET:serviceAPIUrl params:_params success:^(id _responseObject) {
//        NSMutableArray *arrResult=[_responseObject objectForKey:@"data"];
        _success(_responseObject);
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
- (void)emailCouponUsersWithDictionary:(NSMutableDictionary*)_params
                            success:(void (^)(id _responseObject))_success
                            failure:(void (^)(NSInteger  _errorCode))_failure
{
    NSString *serviceAPIUrl = [NSString stringWithFormat:@"%@",@"api/message/emailcouponmembers/"];
    [[NetAPIClient sharedClient] sendToServiceByPOST:serviceAPIUrl params:_params success:^(id _responseObject) {
//        NSDictionary *dict=[_responseObject objectForKey:@"data"];
        _success(_responseObject);
        //
    } failure:^(NSError *_error, NSInteger _errorCode) {
        _failure(_errorCode);
    }];
}
@end
