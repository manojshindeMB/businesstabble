//
//  LoginService.h
//  Jogging
//
//  Created by Liu on 19/10/15.
//  Copyright (c) 2015 Chris Register. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BubbleService : NSObject
+ (id)sharedData;

- (void)createBubbleWithDictionary:(NSMutableDictionary*)_params
                                 success:(void (^)(id _responseObject))_success
                                 failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)updateBubbleWithDictionary:(NSMutableDictionary*)_params
                           success:(void (^)(id _responseObject))_success
                           failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)getBubbleListWithDictionary:(NSMutableDictionary*)_params
                            success:(void (^)(id _responseObject))_success
                            failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)getMyBubbleListWithDictionary:(NSMutableDictionary*)_params
                              success:(void (^)(id _responseObject))_success
                              failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)getAllBubbleListWithDictionary:(NSMutableDictionary*)_params
                               success:(void (^)(id _responseObject))_success
                               failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)joinBubbleWithDictionary:(NSMutableDictionary*)_params
                         success:(void (^)(id _responseObject))_success
                         failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)unJoinBubbleWithDictionary:(NSMutableDictionary*)_params
                         success:(void (^)(id _responseObject))_success
                         failure:(void (^)(NSInteger  _errorCode))_failure;

- (void)approveBubbleWithDictionary:(NSMutableDictionary*)_params
                            success:(void (^)(id _responseObject))_success
                            failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)removeBubbleWithDictionary:(NSMutableDictionary*)_params
                           success:(void (^)(id _responseObject))_success
                           failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)getBubbleMemberListWithDictionary:(NSMutableDictionary*)_params
                                  success:(void (^)(id _responseObject))_success
                                  failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)setMemberPostEnabledForBubbleWithDictionary:(NSMutableDictionary*)_params
                         success:(void (^)(id _responseObject))_success
                         failure:(void (^)(NSInteger  _errorCode))_failure;

- (void)getBubbleMenuListWithDictionary:(NSMutableDictionary*)_params
                                success:(void (^)(id _responseObject))_success
                                failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)updateBubbleMenuWithDictionary:(NSMutableDictionary*)_params
                                success:(void (^)(id _responseObject))_success
                               failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)getActivityLikeListWithDictionary:(NSMutableDictionary*)_params
                                  success:(void (^)(id _responseObject))_success
                                  failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)setActivityLikeListWithDictionary:(NSMutableDictionary*)_params
                                  success:(void (^)(id _responseObject))_success
                                  failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)removeBubbleMenuWithDictionary:(NSMutableDictionary*)_params
                               success:(void (^)(id _responseObject))_success
                               failure:(void (^)(NSInteger  _errorCode))_failure;
@end
