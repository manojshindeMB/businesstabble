//
//  NetAPIClient.h
//
//  Created by iDeveloper on 7/6/13.
//  Copyright (c) 2013 iDevelopers. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <AFNetworking/AFNetworking.h>
#import "AFNetworking.h"
//#import "AFNetworking.h"

#import "CTConfig.h"

typedef enum {
    MediaTypePhoto = 0,
    MediaTypeVideo = 1
} MediaType;

@interface NetAPIClient : AFHTTPSessionManager

+ (NetAPIClient *)sharedClient;

// send text data
- (void)sendToServiceByPOST:(NSString *)serviceAPIURL
                     params:(NSDictionary *)_params
                    success:(void (^)(id _responseObject))_success
                    failure:(void (^)(NSError *_error, NSInteger _errorCode))_failure;
//send photo/video data
- (void)sendToServiceByPOST:(NSString *)serviceAPIURL
                     params:(NSDictionary *)_params
                      media:(NSData* )_media
                  mediaType:(MediaType)_mediaType // 0: photo, 1: video
                    success:(void (^)(id _responseObject))_success
                    failure:(void (^)(NSError* _error))_failure;
// get text data
- ( void ) sendToServiceByGET : (NSString *) serviceAPIURL
                      params  : ( NSDictionary* ) _params
                      success : ( void (^)( id _responseObject ) ) _success
                      failure : ( void (^)( NSError* _error , NSInteger _errorCode) ) _failure;
// send delete request
- ( void ) sendToServiceByDELETE:(NSString *) serviceAPIURL
                          params:(NSDictionary *) _params
                         success:(void (^)( id _responseObject ) ) _success
                         failure:(void (^)( NSError* _error ) ) _failure;
//send text data with patch
- (void)sendToServiceByPATCH:(NSString *)serviceAPIURL
                      params:(NSDictionary *)_params
                     success:(void (^)(id _responseObject))_success
                     failure:(void (^)(NSError *_error, NSInteger _errorCode))_failure;
@end
