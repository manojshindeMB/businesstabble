//

//

#import "upcloud_service.h"

#import "ASIHTTPRequest.h"
#import "SBJson.h"

#define FBALLR_WEB_SERVICES_URL              @"http://38.100.118.21/fballr/webservice.php"


static upcloud_service *g_currentUser;

@interface upcloud_service () <ASIHTTPRequestDelegate> {
	
	ASIHTTPRequest *loginRequest;
	ASIHTTPRequest *signupRequest;
	ASIHTTPRequest *getInfoRequest;
	ASIFormDataRequest *updateRequest;
	ASIFormDataRequest *uploadDocumentRequest;
	ASIHTTPRequest *checkEmailRequest;
	ASIFormDataRequest *resetPasswordRequest;
	ASIHTTPRequest *submitLocationRequest;
}

@property (nonatomic, copy) CompletionHandler completionHandler;
@property (nonatomic, strong) NSString *currentDocumentKey;

@end

@implementation upcloud_service

+ (upcloud_service *) sharedInstance {
	if ( !g_currentUser )
		g_currentUser = [[upcloud_service alloc] init];
	
	return g_currentUser;
}

- (id) init {
	self = [super init];
	if ( self ) {
	}
	
	return self;
}

- (void) update:(CompletionHandler)completionHandler {
	self.completionHandler = completionHandler;
	
	NSDictionary * param = [self paramDictionary];
//	updateRequest = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:[KKMTAPI protocolWithUserCommand:COMMAND_UpdateUser]]];
	[updateRequest setRequestMethod:@"POST"];
	for (NSString * key in [param keyEnumerator]) {
		[updateRequest setPostValue:[param objectForKey:key] forKey:key];
	}
	
//	if ( self.picture )
//		[updateRequest addData:UIImageJPEGRepresentation(self.picture, 0.8f) withFileName:@"img.jpg" andContentType:@"image/jpeg" forKey:kUserImageKey];
	
	[updateRequest setDelegate:self];
	[updateRequest setTimeOutSeconds:20000];
	[updateRequest startAsynchronous];
}

- (void) uploadDocument:(UIImage *)document forKey:(NSString *)key completionHandler:(CompletionHandler)completionHandler {
	self.completionHandler = completionHandler;
	
	self.currentDocumentKey = key;
	
//	uploadDocumentRequest = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:[KKMTAPI protocolWithUserCommand:COMMAND_ADDDOC]]];
	[uploadDocumentRequest setRequestMethod:@"POST"];
	
	[uploadDocumentRequest addData:UIImageJPEGRepresentation(document, g_compressRatio) withFileName:@"img.jpg" andContentType:@"image/jpeg" forKey:key];
	
	[uploadDocumentRequest setDelegate:self];
	[uploadDocumentRequest setTimeOutSeconds:200000];
	[uploadDocumentRequest startAsynchronous];
}


- (void) uploadFile:(NSString *)strfilename imageData:(NSData*)imgData completionHandler:(CompletionHandler)completionHandler {
    self.completionHandler = completionHandler;
    NSURL *url = [[NSURL alloc] initWithString: FBALLR_WEB_SERVICES_URL];
    resetPasswordRequest = [[ASIFormDataRequest alloc] initWithURL:url];
    [resetPasswordRequest setRequestMethod:@"POST"];
    [resetPasswordRequest setPostValue: @"posts"                    forKey: @"type"];
    [resetPasswordRequest setPostValue: @"post_video"               forKey: @"cmd"];
    [resetPasswordRequest setPostValue: [PFUser currentUser].username      forKey: @"user_id"];
    [resetPasswordRequest setPostValue: strfilename                 forKey: @"filename"];
    [resetPasswordRequest addData:imgData withFileName:strfilename andContentType:@"image/jpeg" forKey:@"file"];
    [resetPasswordRequest setDelegate:self];
    [resetPasswordRequest startAsynchronous];
    
    
    
    
}


- (void) uploadFile:(NSString *)strfilename videoname:(NSString*)strfileVideo imageData:(NSData*)imgData videoData:(NSData*)videoData completionHandler:(CompletionHandler)completionHandler {
    self.completionHandler = completionHandler;
    NSURL *url = [[NSURL alloc] initWithString: FBALLR_WEB_SERVICES_URL];
    resetPasswordRequest = [[ASIFormDataRequest alloc] initWithURL:url];
    [resetPasswordRequest setRequestMethod:@"POST"];
    [resetPasswordRequest setPostValue: @"posts"                    forKey: @"type"];
    [resetPasswordRequest setPostValue: @"post_image"               forKey: @"cmd"];//??? must be consider previous version & users. that's why image! not video
    [resetPasswordRequest setPostValue: [PFUser currentUser].username      forKey: @"user_id"];
    [resetPasswordRequest setPostValue: strfilename                 forKey: @"imgfilename"];
    [resetPasswordRequest addData:imgData withFileName:strfilename andContentType:@"image/jpeg" forKey:@"imagefile"];
    [resetPasswordRequest setPostValue: strfileVideo                 forKey: @"videofilename"];
    [resetPasswordRequest addData:videoData withFileName:strfileVideo andContentType:@"video/quicktime" forKey:@"videofile"];
    
    [resetPasswordRequest setTimeOutSeconds:videoData.length / 10];
    [resetPasswordRequest setDelegate:self];
    [resetPasswordRequest startAsynchronous];
}




#pragma mark - ASIHTTPRequestDelegate
- (void)requestFailed:(ASIHTTPRequest *)request {
	
	dispatch_async(kMainQueue, ^{
		if ( self.completionHandler ) {
			self.completionHandler(NO, @"The connection to the server has been lost. Please try again later.");
			self.completionHandler = nil;
		}
	});
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
	SBJsonParser *parser = [[SBJsonParser alloc] init];
    NSString *strRet = [request responseString];
    NSLog(@"%@", strRet );
	NSDictionary *jsonDict = [parser objectWithString:[request responseString]];
	
	if ( resetPasswordRequest == request ) {
		dispatch_async(kMainQueue, ^{
			if ( self.completionHandler ) {
				self.completionHandler(YES, nil);
			}
		});
	}
}

@end
