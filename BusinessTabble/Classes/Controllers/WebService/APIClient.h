//
//  APIClient.h
//  TwinPoint
//
//  Created by Jing on 11/12/14.
//  Copyright (c) 2014 Bernie Brondum. All rights reserved.
//
// --- Headers ---;

#import "AFHTTPSessionManager.h"


// --- Defines ---;
// APIClient Class;
@interface APIClient : NSObject

+ (instancetype)sharedAPIClient;
//-(void)uploadPhotoWithFileName : (NSString *)strfilename imageData:(NSData*)imgData completionHandler:(CompletionHandler)completionHandler;
//-(void)uploadPhotoWithFileName : (NSString *)strfilename oldfilename:(NSString *)oldfilename imageData:(NSData*)imgData completion:(void (^)())successHandler FailureHandler:(void (^)())failureHandler;
@end

