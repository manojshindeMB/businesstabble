#import "NetAPIClient.h"


//static NSString * const kNetAPIBaseURLString = @"http://...";

@implementation NetAPIClient

static NetAPIClient* _sharedClient = nil;

+ (NetAPIClient *)sharedClient
{
    if ( _sharedClient == nil ) {
        
        _sharedClient = [[NetAPIClient alloc] init];
        _sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
    }
    
    return _sharedClient;
}

- (id)init
{
    self = [super init];
    
    if (self) {
        
    }
    
    return self ;
}

#pragma mark - Web Service

- (void)sendToServiceByPATCH:(NSString *)serviceAPIURL
                      params:(NSDictionary *)_params
                     success:(void (^)(id _responseObject))_success
                     failure:(void (^)(NSError *_error, NSInteger _errorCode))_failure
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/json", @"application/json", @"text/javascript", @"text/plain", nil];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSDictionary *parameters = _params;
    [manager PATCH:serviceAPIURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // Success ;
        if (_success) {
            _success(responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSInteger errorCode = operation.response.statusCode;
        // Failture ;
        if (_failure) {
            _failure(error,errorCode);
        }
    }];
}


// send text data
- (void)sendToServiceByPOST:(NSString *)serviceAPIURL
                     params:(NSDictionary *)_params
                    success:(void (^)(id _responseObject))_success
                    failure:(void (^)(NSError *_error, NSInteger _errorCode))_failure
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:WEB_SITE_BASE_URL]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/json", @"application/json", @"text/javascript", @"text/plain", nil];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
//    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    [manager POST:serviceAPIURL parameters:_params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // Success ;
        if (_success) {
            _success(responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSInteger errorCode = operation.response.statusCode;
        // Failture ;
        if (_failure) {
            _failure(error, errorCode);
        }
    }];
}

// get text data
- ( void ) sendToServiceByGET : (NSString *) serviceAPIURL
                      params  : ( NSDictionary* ) _params
                      success : ( void (^)( id _responseObject ) ) _success
                      failure : ( void (^)( NSError* _error , NSInteger _errorCode) ) _failure
{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:WEB_SITE_BASE_URL]];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSDictionary *parameters = _params;
    dispatch_async(kBgQueue, ^{
        [manager GET:serviceAPIURL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            // Success ;
            if (_success) {
                _success(responseObject);
            }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSInteger errorCode = operation.response.statusCode;
            // Failture ;
            if (_failure) {
                _failure(error, errorCode);
            }
        }];
    });
    
}

//send photo/video data

- (void)sendToServiceByPOST:(NSString *)serviceAPIURL
                     params:(NSDictionary *)_params
                      media:(NSData* )_media
                  mediaType:(MediaType)_mediaType // 0: photo, 1: video
                    success:(void (^)(id _responseObject))_success
                    failure:(void (^)(NSError* _error))_failure
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/json", @"application/json", nil];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSDictionary *parameters = _params;
    NSString *filename = [parameters objectForKey:@"filename"];
    [manager POST:serviceAPIURL parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        if (_media) {
            if (_mediaType == MediaTypePhoto) {
                
                [formData appendPartWithFileData:_media
                                            name:@"file"
                                        fileName:filename
                                        mimeType:@"image/jpeg" ] ;
            } else {
                [formData appendPartWithFileData:_media
                                            name:@"videofile"
                                        fileName:@"videofile"
                                        mimeType:@"video/quicktime"];
            }
        }
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        NSString* string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog( @"response : %@", responseObject ) ;
        // Success ;
        if (_success) {
            _success(responseObject);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog( @"Error : %@", error.description ) ;
        // Failture ;
        if (_failure) {
            _failure(error);
        }
        
    }];
}

// send delete request
- ( void ) sendToServiceByDELETE:(NSString *) serviceAPIURL
                          params:(NSDictionary *) _params
                         success:(void (^)( id _responseObject ) ) _success
                         failure:(void (^)( NSError* _error ) ) _failure
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"text/json", @"application/json", nil];
    
    [manager DELETE:serviceAPIURL parameters:_params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        // Success ;
        if (_success) {
            _success(responseObject);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog( @"Error : %@", error.description ) ;
        
        // Failture ;
        if (_failure) {
            _failure(error);
        }
    }];
}

@end

