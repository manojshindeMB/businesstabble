//
//  OtherService.h
//  BusinessTabble
//
//  Created by Haichen Song on 25/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainService : NSObject
{

}
+ (id)sharedData;

- (void)getBussinessBubbleListWithSuccessHandler:(void (^)())successHandler
                                  FailureHandler:(void (^)())failureHandler;
- (void)getBubbleMemberList:(void (^)())successHandler
                                  FailureHandler:(void (^)())failureHandler;
- (void)fetchBubbleMenuList:(JUserInfo *)myInfo
                    success:(void (^)(id _responseObject))_success
                    failure:(void (^)(NSInteger  _errorCode))_failure;
- (void)getBubbleMessageListWithSuccessHandler:(void (^)())successHandler
                                FailureHandler:(void (^)())failureHandler;
- (void)getBubbleChatListWithSuccessHandler:( void (^)( id _responseObject ) )successHandler
                             FailureHandler:(void (^)())failureHandler;
- (void)getBubbleChatHideListWithSuccessHandler:( void (^)( id _responseObject ) )successHandler
                             FailureHandler:(void (^)())failureHandler;
- (void)getCouponUseListWithSuccessHandler:( void (^)( id _responseObject ) )successHandler
                            FailureHandler:(void (^)())failureHandler;
- (void)getActivityLikeListWithSuccessHandler:( void (^)( id _responseObject ) )successHandler
                               FailureHandler:(void (^)())failureHandler;
@end
