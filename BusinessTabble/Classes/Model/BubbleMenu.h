//
//  BubbleMenu.h
//  BusinessTabble
//
//  Created by Haichen Song on 24/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface BubbleMenu : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "BubbleMenu+CoreDataProperties.h"
