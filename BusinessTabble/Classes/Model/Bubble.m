//
//  Bubble.m
//  BusinessTabble
//
//  Created by Haichen Song on 18/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "Bubble.h"

@implementation Bubble

// Insert code here to add functionality to your managed object subclass
+ (Bubble *)getBubbleById:(NSInteger)bubbleId
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"bubble_id = %ld", bubbleId];
    NSFetchedResultsController *fetchedResultsController = [Bubble MR_fetchAllSortedBy:@"lastmsgtime" ascending:NO withPredicate:predicate groupBy:nil delegate:nil];
    if([[fetchedResultsController fetchedObjects] count]  > 0)
    {
        Bubble *bubble = [fetchedResultsController.fetchedObjects objectAtIndex:0];
        return bubble;
    }
    return nil;
}
@end
