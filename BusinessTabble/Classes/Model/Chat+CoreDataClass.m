//
//  Chat+CoreDataClass.m
//  
//
//  Created by Liumin on 30/10/2016.
//
//

#import "Chat+CoreDataClass.h"

@implementation Chat
//- (void)reloadDataWithCategoryName:(NSString *)strCategoryName
//                      SearchString:(NSString *)strSearchString
//{
//    NSPredicate *predicate = nil;
//    predicate = [NSPredicate predicateWithFormat:@"user_id=%@ AND status=1", [[GlobalData sharedData]mUserInfo].mId];
//    NSFetchedResultsController *fetchedControllerForHidePosts = [Chat_hideposter MR_fetchAllSortedBy:@"updatedAt" ascending:NO withPredicate:predicate groupBy:nil delegate:self];
//    NSMutableArray *arrHiddenPosts = [[NSMutableArray alloc]init];
//    for(Chat_hideposter *ch in fetchedControllerForHidePosts.fetchedObjects)
//    {
//        [arrHiddenPosts addObject:ch.poster_id];
//    }
//
//    predicate = [NSPredicate predicateWithFormat:@"bubble_id = %@ AND item_deleted = 0 AND NOT(poster_id IN %@)", mCurrentBubble.bubble_id, arrHiddenPosts];
//    NSFetchRequest *fetchRequest = [Chat MR_requestAllSortedBy:@"updatedAt" ascending:YES withPredicate:predicate];
//    NSError *error;
//    NSInteger count = [[NSManagedObjectContext MR_defaultContext] countForFetchRequest:fetchRequest /*the one you have above but without limit */ error:&error];
//    NSInteger size = 20;
//    count -= size;
//    [fetchRequest setFetchOffset:count>0?count:0];
//    [fetchRequest setFetchLimit:size];
//    NSFetchedResultsController *theFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:[NSManagedObjectContext MR_defaultContext] sectionNameKeyPath:nil cacheName:nil];
//    self.fetchedResultsController = theFetchedResultsController;
//
//    BOOL success = [self.fetchedResultsController performFetch:&error];
//    if(success)
//    {
//        [self.tableView reloadData];
//        [self moveTableViewScrollToBottom];
//    }
//    //    self.fetchedResultsController = [Chat MR_fetchAllSortedBy:@"updatedAt" ascending:YES withPredicate:predicate groupBy:nil delegate:self];
//    //    [self moveTableViewScrollToBottom];
//}
+ (Chat *)getLastChatByBubbleId:(NSInteger )bubble_id
{
    NSPredicate *predicate = nil;
    predicate = [NSPredicate predicateWithFormat:@"bubble_id=%ld", bubble_id];
    NSFetchedResultsController *fetchResultsController = [Chat MR_fetchAllSortedBy:@"createdAt" ascending:NO withPredicate:predicate groupBy:nil delegate:nil];
    if([[fetchResultsController fetchedObjects] count]  > 0)
    {
        Chat *chat = [fetchResultsController.fetchedObjects objectAtIndex:0];
        return chat;
    }
    return nil;
}
@end
