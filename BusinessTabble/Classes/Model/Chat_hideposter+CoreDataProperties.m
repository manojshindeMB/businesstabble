//
//  Chat_hideposter+CoreDataProperties.m
//  
//
//  Created by Liumin on 31/10/2016.
//
//

#import "Chat_hideposter+CoreDataProperties.h"

@implementation Chat_hideposter (CoreDataProperties)

+ (NSFetchRequest<Chat_hideposter *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Chat_hideposter"];
}

@dynamic hide_id;
@dynamic poster_id;
@dynamic status;
@dynamic updatedAt;
@dynamic user_id;

@end
