//
//  Message+CoreDataProperties.h
//  
//
//  Created by macbook on 29/06/2017.
//
//

#import "Message+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Message (CoreDataProperties)

+ (NSFetchRequest<Message *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *approved;
@property (nullable, nonatomic, copy) NSNumber *approvefrom;
@property (nullable, nonatomic, copy) NSNumber *bubble_id;
@property (nullable, nonatomic, copy) NSNumber *checkout;
@property (nullable, nonatomic, copy) NSString *checkout_url;
@property (nullable, nonatomic, copy) NSNumber *coupon;
@property (nullable, nonatomic, copy) NSNumber *createdAt;
@property (nullable, nonatomic, copy) NSString *firstname;
@property (nullable, nonatomic, copy) NSNumber *item_deleted;
@property (nullable, nonatomic, copy) NSString *lastname;
@property (nullable, nonatomic, copy) NSNumber *message_id;
@property (nullable, nonatomic, copy) NSString *photo_filename;
@property (nullable, nonatomic, copy) NSNumber *poster_id;
@property (nullable, nonatomic, copy) NSNumber *price;
@property (nullable, nonatomic, copy) NSNumber *sell_item;
@property (nullable, nonatomic, copy) NSNumber *sell_quantity;

@property (nullable, nonatomic, copy) NSNumber *shipping_price;
@property (nullable, nonatomic, copy) NSString *text;
@property (nullable, nonatomic, copy) NSNumber *updatedAt;
@property (nullable, nonatomic, copy) NSString *user_photo_filename;
@property (nullable, nonatomic, copy) NSString *username;

@end

NS_ASSUME_NONNULL_END
