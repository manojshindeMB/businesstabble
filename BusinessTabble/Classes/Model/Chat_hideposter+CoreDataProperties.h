//
//  Chat_hideposter+CoreDataProperties.h
//  
//
//  Created by Liumin on 31/10/2016.
//
//

#import "Chat_hideposter+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Chat_hideposter (CoreDataProperties)

+ (NSFetchRequest<Chat_hideposter *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *hide_id;
@property (nullable, nonatomic, copy) NSNumber *poster_id;
@property (nullable, nonatomic, copy) NSNumber *status;
@property (nullable, nonatomic, copy) NSString *updatedAt;
@property (nullable, nonatomic, copy) NSNumber *user_id;

@end

NS_ASSUME_NONNULL_END
