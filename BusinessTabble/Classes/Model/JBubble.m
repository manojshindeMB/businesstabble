//
//  JBubble.m
//  BusinessTabble
//
//  Created by Haichen Song on 16/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "JBubble.h"

@implementation JBubble
@synthesize mBubbleName, mCategory, mColorBlue, mColorGreen, mColorRed, mContactNumber, mCreatedAt, mCreatorId, mDescription, mBubbleId, mLocation, mOwner, mPhotoFileName, mUpdatedAt;

- (id)initWithDictionary:(NSDictionary*)dict {
    if (self = [super init]) {
        [self setDataWithDictionary:dict];
    }
    return self;
}
- (id)setDataWithDictionary:(NSDictionary*)dict{
    mBubbleName = [dict objectForKey:@"bubblename"];
    mBubbleId = [dict objectForKey:@"id"];
    mCategory = [dict objectForKey:@"category"];
    mCreatorId = [[dict objectForKey:@"creator_id"] integerValue];
    mLocation = [dict objectForKey:@"location"];
    mContactNumber = [dict objectForKey:@"contactnumber"];
    mDescription = [dict objectForKey:@"description"];
    mOwner = [dict objectForKey:@"owner"];
    mPhotoFileName = [dict objectForKey:@"photo_filename"];
    mCreatedAt = [[dict objectForKey:@"createdAt"] integerValue];
    mUpdatedAt = [[dict objectForKey:@"createdAt"] integerValue];
    mColorRed = [[dict objectForKey:@"color_red"] floatValue];
    mColorBlue = [[dict objectForKey:@"color_blue"] floatValue];
    mColorGreen = [[dict objectForKey:@"color_green"] floatValue];
    
    return self;
}
@end
