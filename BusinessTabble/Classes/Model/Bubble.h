//
//  Bubble.h
//  BusinessTabble
//
//  Created by Haichen Song on 18/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Bubble : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
+ (Bubble *)getBubbleById:(NSInteger)bubbleId;

@end

NS_ASSUME_NONNULL_END

#import "Bubble+CoreDataProperties.h"
