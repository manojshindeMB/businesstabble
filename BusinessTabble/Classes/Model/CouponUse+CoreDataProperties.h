//
//  CouponUse+CoreDataProperties.h
//  BusinessTabble
//
//  Created by Tian Ming on 04/01/16.
//  Copyright © 2016 Liu. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CouponUse.h"

NS_ASSUME_NONNULL_BEGIN

@interface CouponUse (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *coupon_use_id;
@property (nullable, nonatomic, retain) NSString *coupon_text;
@property (nullable, nonatomic, retain) NSString *photo_filename;
@property (nullable, nonatomic, retain) NSNumber *status;
@property (nullable, nonatomic, retain) NSNumber *user_id;
@property (nullable, nonatomic, retain) NSNumber *usedAt;
@property (nullable, nonatomic, retain) NSNumber *updatedAt;
@property (nullable, nonatomic, retain) NSNumber *coupon_id;
@property (nullable, nonatomic, retain) NSString *usedDate;
@property (nullable, nonatomic, retain) NSString *usedTime;
@property (nullable, nonatomic, retain) NSString *user_photo_filename;
@property (nullable, nonatomic, retain) NSString *username;


@end

NS_ASSUME_NONNULL_END
