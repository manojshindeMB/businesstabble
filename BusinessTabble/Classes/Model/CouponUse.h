//
//  CouponUse.h
//  BusinessTabble
//
//  Created by Tian Ming on 04/01/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CouponUse : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "CouponUse+CoreDataProperties.h"
