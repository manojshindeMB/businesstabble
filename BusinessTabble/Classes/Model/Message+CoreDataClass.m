//
//  Message+CoreDataClass.m
//  
//
//  Created by macbook on 29/06/2017.
//
//

#import "Message+CoreDataClass.h"

@implementation Message
+ (Message *)getMessageByMessageId:(NSInteger)msgId{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"message_id = %ld",msgId];
    NSFetchedResultsController *fetchedResultsController = [Message MR_fetchAllSortedBy:@"message_id" ascending:NO withPredicate:predicate groupBy:nil delegate:nil];
    if(!fetchedResultsController || ![fetchedResultsController.fetchedObjects count])
        return nil;
    return [fetchedResultsController.fetchedObjects firstObject];

}
@end
