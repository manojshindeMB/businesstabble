//
//  Chat_hideposter+CoreDataClass.h
//  
//
//  Created by Liumin on 31/10/2016.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Chat_hideposter : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Chat_hideposter+CoreDataProperties.h"
