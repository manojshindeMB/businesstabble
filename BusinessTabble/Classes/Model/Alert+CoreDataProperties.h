//
//  Alert+CoreDataProperties.h
//  
//
//  Created by macbook on 03/06/2017.
//
//

#import "Alert+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Alert (CoreDataProperties)

+ (NSFetchRequest<Alert *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *arrived;
@property (nullable, nonatomic, copy) NSString *identifier;
@property (nullable, nonatomic, copy) NSNumber *is_deleted;
@property (nullable, nonatomic, copy) NSNumber *message_id;
@property (nullable, nonatomic, copy) NSString *message;
@property (nullable, nonatomic, copy) NSString *type;

@end

NS_ASSUME_NONNULL_END
