//
//  JUserInfo.m
//  BusinessTabble
//
//  Created by Tian Ming on 12/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import "JUserInfo.h"

@implementation JUserInfo
@synthesize mCity,mEmail,mFirstName,mGender,mId,mLastName,mLoginType,mPhotoFileName,mState,mUpdatedAt,mUserName;
- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.mCity forKey:@"city"];
    [encoder encodeObject:self.mEmail forKey:@"email"];
    [encoder encodeObject:self.mFirstName forKey:@"firstname"];
    [encoder encodeObject:self.mLastName forKey:@"lastname"];
    [encoder encodeObject:[NSString stringWithFormat:@"%ld", self.mGender] forKey:@"gender"];
    [encoder encodeObject:self.mId forKey:@"id"];
    [encoder encodeObject:self.mLoginType forKey:@"logintype"];
    [encoder encodeObject:self.mPhotoFileName forKey:@"photofilename"];
    [encoder encodeObject:self.mState forKey:@"state"];
    [encoder encodeObject:self.mUserName forKey:@"username"];
    [encoder encodeObject:[NSString stringWithFormat:@"%ld", self.mUpdatedAt] forKey:@"updatedat"];
}
- (id)initWithCoder:(NSCoder *)decoder
{
    self.mCity = [decoder decodeObjectForKey:@"city"];
    self.mEmail = [decoder decodeObjectForKey:@"email"];
    self.mFirstName  = [decoder decodeObjectForKey:@"firstname"];
    self.mLastName = [decoder decodeObjectForKey:@"lastname"];
    self.mGender = [[decoder decodeObjectForKey:@"gender"] integerValue];
    self.mId = [decoder decodeObjectForKey:@"id"];
    self.mLoginType = [decoder decodeObjectForKey:@"logintype"];
    
    self.mPhotoFileName =  [decoder decodeObjectForKey:@"photofilename"];
    self.mState = [decoder decodeObjectForKey:@"state"];
    
    self.mUserName = [decoder decodeObjectForKey:@"username"];
    self.mUpdatedAt = [[decoder decodeObjectForKey:@"updatedat"] integerValue];
    return self;
}

- (id)initWithDictionary:(NSDictionary*)dict {
    if (self = [super init]) {
        [self setDataWithDictionary:dict];
        
    }
    return self;
}
- (id)setDataWithDictionary:(NSDictionary*)dict{
    mId = [dict objectForKey:@"id"];
    mUserName = [dict objectForKey:@"username"];
    mGender = [[dict objectForKey:@"gender"] integerValue];
    mEmail = [dict objectForKey:@"email"];
    mCity = [dict objectForKey:@"city"];
    mState= [dict objectForKey:@"state"];
    mFirstName = [dict objectForKey:@"firstname"];
    mLastName = [dict objectForKey:@"lastname"];
    mLoginType = [dict objectForKey:@"logintype"];
    mPhotoFileName = @"";
    if([dict objectForKey:@"photo_filename"] && ![[dict objectForKey:@"photo_filename"] isKindOfClass:[NSNull class]])
        mPhotoFileName = [dict objectForKey:@"photo_filename"];
    return self;
}
@end