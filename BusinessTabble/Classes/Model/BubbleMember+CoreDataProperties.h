//
//  BubbleMember+CoreDataProperties.h
//  BusinessTabble
//
//  Created by Haichen Song on 21/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BubbleMember.h"

NS_ASSUME_NONNULL_BEGIN

@interface BubbleMember (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *joined;
@property (nullable, nonatomic, retain) NSNumber *bubble_id;
@property (nullable, nonatomic, retain) NSNumber *bubble_member_id;
@property (nullable, nonatomic, retain) NSNumber *joinedAt;
@property (nullable, nonatomic, retain) NSNumber *member_id;
@property (nullable, nonatomic, retain) NSNumber *role;
@property (nullable, nonatomic, retain) NSString *username;
@property (nullable, nonatomic, retain) NSString *photo_filename;
@property (nullable, nonatomic, retain) NSString *firstname;
@property (nullable, nonatomic, retain) NSString *lastname;
@property (nullable, nonatomic, retain) NSString *city;
@property (nullable, nonatomic, retain) NSString *state;
@property (nullable, nonatomic, retain) NSString *email;
@property (nullable, nonatomic, retain) NSNumber *postenabled;
@end

NS_ASSUME_NONNULL_END
