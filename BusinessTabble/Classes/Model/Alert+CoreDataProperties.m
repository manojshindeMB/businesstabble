//
//  Alert+CoreDataProperties.m
//  
//
//  Created by macbook on 03/06/2017.
//
//

#import "Alert+CoreDataProperties.h"

@implementation Alert (CoreDataProperties)

+ (NSFetchRequest<Alert *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Alert"];
}

@dynamic arrived;
@dynamic identifier;
@dynamic is_deleted;
@dynamic message;
@dynamic type;
@dynamic message_id;
@end
