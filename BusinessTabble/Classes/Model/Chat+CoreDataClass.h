//
//  Chat+CoreDataClass.h
//  
//
//  Created by Liumin on 30/10/2016.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Chat : NSManagedObject
+ (Chat *)getLastChatByBubbleId:(NSInteger )bubble_id;
@end

NS_ASSUME_NONNULL_END

#import "Chat+CoreDataProperties.h"
