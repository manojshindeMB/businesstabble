//
//  Bubble+CoreDataProperties.m
//  BusinessTabble
//
//  Created by Liumin on 15/07/16.
//  Copyright © 2016 Liu. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Bubble+CoreDataProperties.h"

@implementation Bubble (CoreDataProperties)

@dynamic activity_filename1;
@dynamic activity_filename2;
@dynamic banner_filename;
@dynamic bubble_id;
@dynamic bubblename;
@dynamic category;
@dynamic color_blue;
@dynamic color_green;
@dynamic color_red;
@dynamic contactnumber;
@dynamic content;
@dynamic coupon;
@dynamic createdAt;
@dynamic creator_id;
@dynamic distance;
@dynamic event_enddate;
@dynamic event_endtime;
@dynamic event_startdate;
@dynamic event_starttime;
@dynamic field_deleted;
@dynamic joined;
@dynamic last_message_photo_filename;
@dynamic lastmsg_id;
@dynamic lastmsgtime;
@dynamic latitude;
@dynamic location;
@dynamic longitude;
@dynamic membercount;
@dynamic messagecount;
@dynamic owner;
@dynamic photo_filename;
@dynamic postenabled;
@dynamic reviewstatus;
@dynamic role;
@dynamic text;
@dynamic updatedAt;
@dynamic updatedTime;
@dynamic user_latitude;
@dynamic user_longitude;
@dynamic user_photo_filename;
@dynamic username;
@dynamic website;
@dynamic paypal;

@end
