//
//  BubbleMember+CoreDataProperties.m
//  BusinessTabble
//
//  Created by Haichen Song on 21/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BubbleMember+CoreDataProperties.h"

@implementation BubbleMember (CoreDataProperties)

@dynamic joined;
@dynamic bubble_id;
@dynamic bubble_member_id;
@dynamic joinedAt;
@dynamic member_id;
@dynamic role;
@dynamic username;
@dynamic photo_filename;
@dynamic firstname;
@dynamic lastname;
@dynamic email;
@dynamic city;
@dynamic state;
@dynamic postenabled;
@end
