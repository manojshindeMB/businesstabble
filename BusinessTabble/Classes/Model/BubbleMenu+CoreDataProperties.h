//
//  BubbleMenu+CoreDataProperties.h
//  BusinessTabble
//
//  Created by Liumin on 15/07/16.
//  Copyright © 2016 Liu. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BubbleMenu.h"

NS_ASSUME_NONNULL_BEGIN

@interface BubbleMenu (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *bubble_id;
@property (nullable, nonatomic, retain) NSNumber *item_deleted;
@property (nullable, nonatomic, retain) NSString *menu_filename;
@property (nullable, nonatomic, retain) NSNumber *menu_id;
@property (nullable, nonatomic, retain) NSNumber *number;
@property (nullable, nonatomic, retain) NSNumber *updatedAt;

@end

NS_ASSUME_NONNULL_END
