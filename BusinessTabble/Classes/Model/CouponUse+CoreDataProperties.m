//
//  CouponUse+CoreDataProperties.m
//  BusinessTabble
//
//  Created by Tian Ming on 04/01/16.
//  Copyright © 2016 Liu. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "CouponUse+CoreDataProperties.h"

@implementation CouponUse (CoreDataProperties)

@dynamic coupon_use_id;
@dynamic coupon_text;
@dynamic photo_filename;
@dynamic status;
@dynamic user_id;
@dynamic usedAt;
@dynamic updatedAt;
@dynamic coupon_id;
@dynamic usedDate;
@dynamic usedTime;
@dynamic user_photo_filename;
@dynamic username;
-(void)willImport:(id)data
{

    
}
- (void)didImport:(id)data
{
    self.usedDate = [[GlobalData sharedData]getDateStringFromTimeStamp:[self.usedAt integerValue]];
    self.usedTime = [[GlobalData sharedData]getTimeStringFromTimeStamp:[self.usedAt integerValue]];
}
@end
