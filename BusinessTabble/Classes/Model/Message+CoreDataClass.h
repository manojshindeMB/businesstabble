//
//  Message+CoreDataClass.h
//  
//
//  Created by macbook on 29/06/2017.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Message : NSManagedObject
+ (Message *)getMessageByMessageId:(NSInteger)msgId;
@end

NS_ASSUME_NONNULL_END

#import "Message+CoreDataProperties.h"
