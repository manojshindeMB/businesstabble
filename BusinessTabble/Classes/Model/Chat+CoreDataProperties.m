//
//  Chat+CoreDataProperties.m
//  
//
//  Created by Liumin on 30/10/2016.
//
//

#import "Chat+CoreDataProperties.h"

@implementation Chat (CoreDataProperties)

+ (NSFetchRequest<Chat *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Chat"];
}

@dynamic approved;
@dynamic approvefrom;
@dynamic bubble_id;
@dynamic coupon;
@dynamic createdAt;
@dynamic firstname;
@dynamic item_deleted;
@dynamic lastname;
@dynamic message_id;
@dynamic photo_filename;
@dynamic poster_id;
@dynamic text;
@dynamic updatedAt;
@dynamic user_photo_filename;
@dynamic username;

@end
