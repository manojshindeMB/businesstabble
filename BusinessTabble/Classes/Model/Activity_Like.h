//
//  Activity_Like.h
//  BusinessTabble
//
//  Created by Liumin on 09/07/16.
//  Copyright © 2016 Liu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Activity_Like : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Activity_Like+CoreDataProperties.h"
