//
//  Message+CoreDataProperties.m
//  
//
//  Created by macbook on 29/06/2017.
//
//

#import "Message+CoreDataProperties.h"

@implementation Message (CoreDataProperties)

+ (NSFetchRequest<Message *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Message"];
}

@dynamic approved;
@dynamic approvefrom;
@dynamic bubble_id;
@dynamic checkout;
@dynamic checkout_url;
@dynamic coupon;
@dynamic createdAt;
@dynamic firstname;
@dynamic item_deleted;
@dynamic lastname;
@dynamic message_id;
@dynamic photo_filename;
@dynamic poster_id;
@dynamic price;
@dynamic sell_item;
@dynamic shipping_price;
@dynamic text;
@dynamic updatedAt;
@dynamic user_photo_filename;
@dynamic username;
@dynamic sell_quantity;

@end
