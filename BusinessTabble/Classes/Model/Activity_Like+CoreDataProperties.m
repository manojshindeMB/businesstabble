//
//  Activity_Like+CoreDataProperties.m
//  BusinessTabble
//
//  Created by Liumin on 09/07/16.
//  Copyright © 2016 Liu. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Activity_Like+CoreDataProperties.h"

@implementation Activity_Like (CoreDataProperties)

@dynamic activity_like_id;
@dynamic user_id;
@dynamic activity_id;
@dynamic when;
@dynamic status;

@end
