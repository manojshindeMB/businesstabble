//
//  Activity_Like+CoreDataProperties.h
//  BusinessTabble
//
//  Created by Liumin on 09/07/16.
//  Copyright © 2016 Liu. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Activity_Like.h"

NS_ASSUME_NONNULL_BEGIN

@interface Activity_Like (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *activity_like_id;
@property (nullable, nonatomic, retain) NSNumber *user_id;
@property (nullable, nonatomic, retain) NSNumber *activity_id;
@property (nullable, nonatomic, retain) NSDate *when;
@property (nullable, nonatomic, retain) NSNumber *status;

@end

NS_ASSUME_NONNULL_END
