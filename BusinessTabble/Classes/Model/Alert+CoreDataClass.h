//
//  Alert+CoreDataClass.h
//  
//
//  Created by macbook on 03/06/2017.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Alert : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Alert+CoreDataProperties.h"
