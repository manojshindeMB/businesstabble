//
//  Bubble+CoreDataProperties.h
//  BusinessTabble
//
//  Created by Liumin on 15/07/16.
//  Copyright © 2016 Liu. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Bubble.h"

NS_ASSUME_NONNULL_BEGIN

@interface Bubble (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *activity_filename1;
@property (nullable, nonatomic, retain) NSString *activity_filename2;
@property (nullable, nonatomic, retain) NSString *banner_filename;
@property (nullable, nonatomic, retain) NSNumber *bubble_id;
@property (nullable, nonatomic, retain) NSString *bubblename;
@property (nullable, nonatomic, retain) NSString *category;
@property (nullable, nonatomic, retain) NSNumber *color_index;
@property (nullable, nonatomic, retain) NSNumber *color_blue;
@property (nullable, nonatomic, retain) NSNumber *color_green;
@property (nullable, nonatomic, retain) NSNumber *color_red;
@property (nullable, nonatomic, retain) NSString *contactnumber;
@property (nullable, nonatomic, retain) NSString *content;
@property (nullable, nonatomic, retain) NSNumber *coupon;
@property (nullable, nonatomic, retain) NSNumber *createdAt;
@property (nullable, nonatomic, retain) NSNumber *creator_id;
@property (nullable, nonatomic, retain) NSNumber *distance;
@property (nullable, nonatomic, retain) NSString *event_enddate;
@property (nullable, nonatomic, retain) NSNumber *event_endtime;
@property (nullable, nonatomic, retain) NSString *event_startdate;
@property (nullable, nonatomic, retain) NSNumber *event_starttime;
@property (nullable, nonatomic, retain) NSNumber *field_deleted;
@property (nullable, nonatomic, retain) NSNumber *joined;
@property (nullable, nonatomic, retain) NSString *last_message_photo_filename;
@property (nullable, nonatomic, retain) NSNumber *lastmsg_id;
@property (nullable, nonatomic, retain) NSNumber *lastmsgtime;
@property (nullable, nonatomic, retain) NSNumber *latitude;
@property (nullable, nonatomic, retain) NSString *location;
@property (nullable, nonatomic, retain) NSNumber *longitude;
@property (nullable, nonatomic, retain) NSNumber *membercount;
@property (nullable, nonatomic, retain) NSNumber *messagecount;
@property (nullable, nonatomic, retain) NSString *owner;
@property (nullable, nonatomic, retain) NSString *photo_filename;
@property (nullable, nonatomic, retain) NSNumber *postenabled;
@property (nullable, nonatomic, retain) NSNumber *reviewstatus;
@property (nullable, nonatomic, retain) NSNumber *role;
@property (nullable, nonatomic, retain) NSString *text;
@property (nullable, nonatomic, retain) NSNumber *updatedAt;
@property (nullable, nonatomic, retain) NSDate *updatedTime;
@property (nullable, nonatomic, retain) NSNumber *user_latitude;
@property (nullable, nonatomic, retain) NSNumber *user_longitude;
@property (nullable, nonatomic, retain) NSString *user_photo_filename;
@property (nullable, nonatomic, retain) NSString *username;
@property (nullable, nonatomic, retain) NSString *website;
@property (nullable, nonatomic, retain) NSString *paypal;
@end

NS_ASSUME_NONNULL_END
