//
//  BubbleMenu+CoreDataProperties.m
//  BusinessTabble
//
//  Created by Liumin on 15/07/16.
//  Copyright © 2016 Liu. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BubbleMenu+CoreDataProperties.h"

@implementation BubbleMenu (CoreDataProperties)

@dynamic bubble_id;
@dynamic item_deleted;
@dynamic menu_filename;
@dynamic menu_id;
@dynamic number;
@dynamic updatedAt;

@end
