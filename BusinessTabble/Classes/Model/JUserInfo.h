//
//  JUserInfo.h
//  BusinessTabble
//
//  Created by Liu Min on 12/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JUserInfo : NSObject <NSCoding>
@property(nonatomic, strong) NSString *mId;
@property(nonatomic, strong) NSString *mUserName;
@property(nonatomic) NSInteger mGender;
@property(nonatomic, strong) NSString *mEmail;
@property(nonatomic, strong) NSString *mFirstName;
@property(nonatomic, strong) NSString *mLastName;
@property(nonatomic, strong) NSString *mLoginType;
@property(nonatomic, strong) NSString *mPhotoFileName;
@property(nonatomic) NSInteger mUpdatedAt;
@property(nonatomic) NSString *mCity;
@property(nonatomic) NSString *mState;
- (id)initWithDictionary:(NSDictionary*)dict;
- (id)setDataWithDictionary:(NSDictionary*)dict;
@end
