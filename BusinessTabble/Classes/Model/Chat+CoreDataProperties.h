//
//  Chat+CoreDataProperties.h
//  
//
//  Created by Liumin on 30/10/2016.
//
//

#import "Chat+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Chat (CoreDataProperties)

+ (NSFetchRequest<Chat *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *approved;
@property (nullable, nonatomic, copy) NSNumber *approvefrom;
@property (nullable, nonatomic, copy) NSNumber *bubble_id;
@property (nullable, nonatomic, copy) NSNumber *coupon;
@property (nullable, nonatomic, copy) NSNumber *createdAt;
@property (nullable, nonatomic, copy) NSString *firstname;
@property (nullable, nonatomic, copy) NSNumber *item_deleted;
@property (nullable, nonatomic, copy) NSString *lastname;
@property (nullable, nonatomic, copy) NSNumber *message_id;
@property (nullable, nonatomic, copy) NSString *photo_filename;
@property (nullable, nonatomic, copy) NSNumber *poster_id;
@property (nullable, nonatomic, copy) NSString *text;
@property (nullable, nonatomic, copy) NSNumber *updatedAt;
@property (nullable, nonatomic, copy) NSString *user_photo_filename;
@property (nullable, nonatomic, copy) NSString *username;

@end

NS_ASSUME_NONNULL_END
