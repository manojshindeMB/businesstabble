//
//  JBubble.h
//  BusinessTabble
//
//  Created by Haichen Song on 16/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JBubble : NSObject
@property (nonatomic, strong) NSString *mBubbleId;
@property (nonatomic, strong) NSString *mBubbleName;
@property (nonatomic, strong) NSString *mCategory;
@property (nonatomic) NSInteger mCreatorId;
@property (nonatomic, strong) NSString *mLocation;
@property (nonatomic, strong) NSString *mContactNumber;
@property (nonatomic, strong) NSString *mDescription;
@property (nonatomic, strong) NSString *mOwner;
@property (nonatomic, strong) NSString *mPhotoFileName;
@property (nonatomic) NSInteger mCreatedAt;
@property (nonatomic) NSInteger mUpdatedAt;
@property (nonatomic) CGFloat mColorRed;
@property (nonatomic) CGFloat mColorGreen;
@property (nonatomic) CGFloat mColorBlue;


- (id)initWithDictionary:(NSDictionary*)dict;
- (id)setDataWithDictionary:(NSDictionary*)dict;
@end
