//
//  UIImage+Alpha.h
//
//  Created by Xian on 7/18/13.
//  Copyright (c) 2013 Xian. All rights reserved.
//

@interface UIImage (Alpha)
- (BOOL)hasAlpha;
- (UIImage *)imageWithAlpha;
- (UIImage *)transparentBorderImage:(NSUInteger)borderSize;
@end
