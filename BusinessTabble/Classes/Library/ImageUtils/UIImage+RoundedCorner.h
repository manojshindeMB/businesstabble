//
//  UIImage+RoundedCorner.h
//
//  Created by Xian on 7/18/13.
//  Copyright (c) 2013 Xian. All rights reserved.
//

// Extends the UIImage class to support making rounded corners
@interface UIImage (RoundedCorner)
- (UIImage *)roundedCornerImage:(NSInteger)cornerSize borderSize:(NSInteger)borderSize;
@end
