//
//  NIDropDown.h
//  NIDropDown
//
//  Created by Bijesh N on 12/28/12.
//  Copyright (c) 2012 Nitor Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#define DROP_DOWN_TYPE_TEXT 0
#define DROP_DOWN_TYPE_COLOR 1


@class NIDropDown;
@protocol NIDropDownDelegate
- (void) niDropDownDelegateMethod: (NSInteger )index text:(NSObject *)text type:(NSInteger)type;
@end

@interface NIDropDown : UIView <UITableViewDelegate, UITableViewDataSource>
{
    NSString *animationDirection;
    UIImageView *imgView;
}
@property (nonatomic, retain) id <NIDropDownDelegate> delegate;
@property (nonatomic, retain) NSString *animationDirection;
-(void)hideDropDown:(UIButton *)b;
- (id)showDropDown:(UIButton *)btn:(CGFloat *)fHeight:(NSArray *)arrText:(NSArray *)arrImage:(NSString *)direction type:(NSInteger)type selectedIndex:(NSInteger)selectedIndex;
@property (nonatomic) NSInteger mListSelectedIndex;
@end
