//
//  NIDropDown.m
//  NIDropDown
//
//  Created by Bijesh N on 12/28/12.
//  Copyright (c) 2012 Nitor Infotech. All rights reserved.
//

#import "NIDropDown.h"
#import "QuartzCore/QuartzCore.h"

@interface NIDropDown ()
@property(nonatomic, strong) UITableView *table;
@property(nonatomic, strong) UIButton *btnSender;
@property(nonatomic, retain) NSArray *list;
@property(nonatomic, retain) NSArray *imageList;
@property(nonatomic) NSInteger dropType;
@end

@implementation NIDropDown
@synthesize table;
@synthesize btnSender;
@synthesize list;
@synthesize imageList;
@synthesize delegate;
@synthesize animationDirection;
@synthesize dropType;
@synthesize mListSelectedIndex = _mListSelectedIndex;
- (id)showDropDown:(UIButton *)button:(CGFloat *)fHeight:(NSArray *)arrText:(NSArray *)arrImage:(NSString *)direction type:(NSInteger)type selectedIndex:(NSInteger)selectedIndex {
    dropType = type;
    btnSender = button;
    animationDirection = direction;
    self.table = (UITableView *)[super init];
    if (self) {
        // Initialization code
        CGRect btn = button.frame;
        self.list = [NSArray arrayWithArray:arrText];
        self.imageList = [NSArray arrayWithArray:arrImage];
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(-5, -5);
        }else if ([direction isEqualToString:@"down"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(-5, 5);
        }
        
        self.layer.masksToBounds = NO;
        self.layer.cornerRadius = 8;
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        
        table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, btn.size.width, 0)];
        table.separatorStyle = UITableViewCellSeparatorStyleNone;
        table.delegate = self;
        table.dataSource = self;
        table.layer.cornerRadius = 5;
        table.separatorColor = [UIColor grayColor];
        [table performSelector:@selector(flashScrollIndicators) withObject:nil afterDelay:0];
        [table flashScrollIndicators];
        self.mListSelectedIndex = selectedIndex;
        if(self.mListSelectedIndex >= 0)
        {
            NSIndexPath *path = [NSIndexPath indexPathForRow:self.mListSelectedIndex inSection:0];
            [table selectRowAtIndexPath:path animated:YES scrollPosition:UITableViewScrollPositionTop];
        }
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y-*fHeight, btn.size.width, *fHeight);
        } else if([direction isEqualToString:@"down"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, *fHeight);
        }
        table.frame = CGRectMake(0, 0, btn.size.width, *fHeight);
        [UIView commitAnimations];
        [button.superview addSubview:self];
        [self addSubview:table];
        [self setUserInteractionEnabled:YES];
    }
    return self;
}
- (void)setMListSelectedIndex:(NSInteger)mListSelectedIndex
{
    _mListSelectedIndex = mListSelectedIndex;
    
}
-(void)hideDropDown:(UIButton *)b {
    CGRect btn = b.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    if ([animationDirection isEqualToString:@"up"]) {
        self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
    }else if ([animationDirection isEqualToString:@"down"]) {
        self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
    }
    table.frame = CGRectMake(0, 0, btn.size.width, 0);
    [UIView commitAnimations];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 25;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.list count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(dropType == DROP_DOWN_TYPE_TEXT)
    {
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.textLabel.font = [UIFont systemFontOfSize:12];
            cell.textLabel.textAlignment = UITextAlignmentCenter;
        }
        if ([self.imageList count] == [self.list count]) {
            cell.textLabel.text =[list objectAtIndex:indexPath.row];
            cell.imageView.image = [imageList objectAtIndex:indexPath.row];
        } else if ([self.imageList count] > [self.list count]) {
            cell.textLabel.text =[list objectAtIndex:indexPath.row];
            if (indexPath.row < [imageList count]) {
                cell.imageView.image = [imageList objectAtIndex:indexPath.row];
            }
        } else if ([self.imageList count] < [self.list count]) {
            cell.textLabel.text =[list objectAtIndex:indexPath.row];
            if (indexPath.row < [imageList count]) {
                cell.imageView.image = [imageList objectAtIndex:indexPath.row];
            }
        }
        cell.textLabel.textColor = [UIColor blackColor];
    }
    else
    {
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            [cell setBackgroundColor:[list objectAtIndex:indexPath.row]];
        }
        else{
            [cell setBackgroundColor:[list objectAtIndex:indexPath.row]];
        }
        cell.textLabel.textColor = [UIColor blackColor];
    }
    UIView * v1 = [[UIView alloc] init];  v1.backgroundColor = [UIColor whiteColor];
    UIView * v = [[UIView alloc] init];  v.backgroundColor = [UIColor colorWithRed:51/255.f green:150/255.f blue:239/255.f alpha:1.f];
    cell.selectedBackgroundView = v;
//    if(indexPath.row == self.mListSelectedIndex)
//        cell.backgroundView = v;
//    else
//        cell.backgroundView = v1;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self hideDropDown:btnSender];
//    NSIndexPath *path = [NSIndexPath indexPathForRow:self.mListSelectedIndex inSection:0];
//    [tableView deselectRowAtIndexPath:path animated:YES];
    
    UITableViewCell *c = [tableView cellForRowAtIndexPath:indexPath];
    if(dropType == DROP_DOWN_TYPE_TEXT)
    {
        [btnSender setTitle:c.textLabel.text forState:UIControlStateNormal];
        imgView.image = c.imageView.image;
        imgView = [[UIImageView alloc] initWithImage:c.imageView.image];
        imgView.frame = CGRectMake(5, 5, 25, 25);
        [btnSender addSubview:imgView];
        
    }
    else if(dropType == DROP_DOWN_TYPE_COLOR)
    {
        [btnSender setBackgroundColor:list[indexPath.row]];
        imgView.image = c.imageView.image;
        imgView = [[UIImageView alloc] initWithImage:c.imageView.image];
        imgView.frame = CGRectMake(5, 5, 25, 25);
        [btnSender addSubview:imgView];
    }
    for (UIView *subview in btnSender.subviews) {
        if ([subview isKindOfClass:[UIImageView class]]) {
            [subview removeFromSuperview];
        }
    }
    self.mListSelectedIndex = indexPath.row;
    [self.delegate niDropDownDelegateMethod:indexPath.row text:[list objectAtIndex:indexPath.row] type:dropType];
}


-(void)dealloc {
//    [super dealloc];
//    [table release];
//    [self release];
}

@end
