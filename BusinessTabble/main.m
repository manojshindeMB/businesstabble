//
//  main.m
//  BusinessTabble
//
//  Created by Tian Ming on 06/12/15.
//  Copyright © 2015 Liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
