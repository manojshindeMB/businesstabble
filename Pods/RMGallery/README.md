RMGallery
=========

A modular and lightweight gallery for iOS.

![Demo](RMGalleryDemo/demo.gif)

RMGallery is a Photos-like gallery with no dependencies that can be used at view controller, view or cell level. It supports asynchronous image loading, view controller transitions, gestures and zooming.

##Features

* Supports landscape and portrait orientations and rotations.
* Tap to toggle bars when used in navigation controller or to dismiss when presented modally.
* Manages a custom toolbar when presented modally.
* Swipe to move forward or backward.
* Double tap or pinch to zoom.
* Pan images when zoomed.
* View controller transitions.
* Asynchronous image loading.
* Keeps images centered when smaller than the view.

Sample photos:

* "[Macba](https://www.flickr.com/photos/slapbcn/1832096812)" by Ramon Llorensi
* "[Pompidou Museum](https://www.flickr.com/photos/bobjagendorf/5965149738)" by Bob Jagendorf
* "[Tate Modern](https://www.flickr.com/photos/aguichard/13009663094)" by Aurelien Guichard

##Requirements

RMGallery requires iOS 7.0 or above and ARC.

##Roadmap

RMGallery is in initial development and its public API should not be considered stable.

##License

 Copyright 2014 [Robot Media SL](http://www.robotmedia.net)
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
