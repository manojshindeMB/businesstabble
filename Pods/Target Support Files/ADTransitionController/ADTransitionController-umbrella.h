#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "ADBackFadeTransition.h"
#import "ADCarrouselTransition.h"
#import "ADCrossTransition.h"
#import "ADCubeTransition.h"
#import "ADDualTransition.h"
#import "ADFadeTransition.h"
#import "ADFlipTransition.h"
#import "ADFoldTransition.h"
#import "ADGhostTransition.h"
#import "ADGlueTransition.h"
#import "ADModernPushTransition.h"
#import "ADNavigationControllerDelegate.h"
#import "ADPushRotateTransition.h"
#import "ADScaleTransition.h"
#import "ADSlideTransition.h"
#import "ADSwapTransition.h"
#import "ADSwipeFadeTransition.h"
#import "ADSwipeTransition.h"
#import "ADTransformTransition.h"
#import "ADTransition.h"
#import "ADTransitionController.h"
#import "ADTransitioningDelegate.h"
#import "ADTransitioningViewController.h"
#import "ADZoomTransition.h"
#import "UIViewController+ADTransitionController.h"

FOUNDATION_EXPORT double ADTransitionControllerVersionNumber;
FOUNDATION_EXPORT const unsigned char ADTransitionControllerVersionString[];

